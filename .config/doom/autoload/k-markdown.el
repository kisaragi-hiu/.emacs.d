;;; ../../.dotfiles/emacs/.config/doom/autoload/k-markdown.el -*- lexical-binding: t; -*-

(require 'kisaragi-helpers)

;;;###autoload
(defun k/markdown-toggle-hiding-stuff ()
  "Toggle link display and pretty entities."
  (interactive)
  ;; Don't show any message, just like parinfer.
  (let ((inhibit-message t))
    (k/toggle-multiple
     `((markdown-hide-markup . markdown-toggle-markup-hiding)
       (markdown-inline-image-overlays . markdown-toggle-inline-images)))))

(provide 'k-markdown)
;;; k-markdown.el ends here

;;; kisaragi-timestamp-highlight.el --- Fontify full ISO 8601 timestamps -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; Fontify full ISO 8601 timestamps (like 2020-01-01T00:00:00Z).
;;
;; Everything is copied from `goto-addr'.
;;
;;; Code:

(require 'iso8601)

(defconst kisaragi-timestamp-highlight--iso8601-regexp
  (concat "....-..-..T..:..:.." iso8601--zone-match)
  "A regexp to match ISO 8601 timestamps, sacrifising correctness for speed.")

(defvar kisaragi-timestamp-highlight-maximum-size
  30000
  "Don't highlight timestamps in buffers larger than this many characters.")

(defvar kisaragi-timestamp-highlight-face 'org-date
  "Face used to highlight timestamps.")

(defun kisaragi-timestamp-highlight--unfontify (start end)
  "Remove `kisaragi-timestamp-highlight' fontification from the given region."
  (dolist (overlay (overlays-in start end))
    (when (overlay-get overlay 'kisaragi-timestamp-highlight)
      (delete-overlay overlay))))

(defun kisaragi-timestamp-highlight--fontify (&optional start end)
  "Highlight full ISO 8601 timestamps between START and END in the current buffer."
  ;; Clean up from any previous go.
  (kisaragi-timestamp-highlight--unfontify
   (or start (point-min)) (or end (point-max)))
  (save-excursion
    (goto-char (or start (point-min)))
    (when (or (eq t kisaragi-timestamp-highlight-maximum-size)
              (< (- (or end (point-max)) (point))
                 kisaragi-timestamp-highlight-maximum-size))
      (while (re-search-forward kisaragi-timestamp-highlight--iso8601-regexp end t)
        (let* ((s (match-beginning 0))
               (e (match-end 0))
               this-overlay)
          (setq this-overlay (make-overlay s e))
          (overlay-put this-overlay 'face kisaragi-timestamp-highlight-face)
          (overlay-put this-overlay 'evaporate t)
          (overlay-put this-overlay 'kisaragi-timestamp-highlight t)))
      (goto-char (or start (point-min))))))

(defun kisaragi-timestamp-highlight-region (start end)
  "Fontify full ISO 8601 timestamps in the given region."
  (save-excursion
    (let ((beg-line (progn (goto-char start) (line-beginning-position)))
          (end-line (progn (goto-char end) (line-end-position))))
      (kisaragi-timestamp-highlight--fontify beg-line end-line))))

;;;###autoload
(define-minor-mode kisaragi-timestamp-highlight-mode
  "Minor mode to highlight full ISO 8601 timestamps."
  :init-value nil
  :lighter ""
  :global nil
  (if kisaragi-timestamp-highlight-mode
      (jit-lock-register #'kisaragi-timestamp-highlight-region)
    (jit-lock-unregister #'kisaragi-timestamp-highlight-region)
    (save-restriction
      (widen)
      (kisaragi-timestamp-highlight--unfontify (point-min) (point-max)))))

(provide 'kisaragi-timestamp-highlight)

;;; kisaragi-timestamp-highlight.el ends here

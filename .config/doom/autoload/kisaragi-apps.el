;;; kisaragi-apps.el --- Register and run "apps" -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1
;; Homepage: https://kisaragi-hiu.com/.emacs.d
;; Package-Requires: ((emacs "26.1"))
;; Keywords: convenience

;;; Commentary:

;; M-x lists all commands, making it really hard to find entry points
;; to packages. This is my way of dealing with this in my config.

;;; Code:

(require 'map)

(defvar kisaragi-apps--history nil
  "History for `kisaragi-apps'.")
(defvar kisaragi-apps-table (make-hash-table :test #'equal)
  "Hash table of apps registered through `kisaragi-apps-register'.
Keys are app names, values are commands.")

;;;###autoload
(defun kisaragi-apps-register (name command)
  "Register an app.

NAME is listed in the command `kisaragi-apps'; COMMAND is run
when selected."
  (puthash name command kisaragi-apps-table))

;;;###autoload
(defun kisaragi-apps ()
  "Show a list of registered Emacs-based applications."
  (interactive)
  (when-let* ((candidates (map-keys kisaragi-apps-table))
              (answer (completing-read "App: " (sort candidates #'string<)
                                       nil t nil
                                       'kisaragi-apps--history))
              (cmd (map-elt kisaragi-apps-table answer)))
    (call-interactively cmd)))

(provide 'kisaragi-apps)

;;; kisaragi-apps.el ends here

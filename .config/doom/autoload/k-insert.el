;;; k-insert.el --- Insertion commands -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'kisaragi-paths)
(provide 'kisaragi-link)

(require 'dash)
(require 'f)

;;;###autoload
(defun k/insert-timestamp ()
  "Insert a full ISO 8601 timestamp."
  (interactive)
  (insert (format-time-string "%FT%T%z")))

;;;###autoload
(defun k/insert-date ()
  "Insert an ISO 8601 date (YYYY-MM-DD)."
  (interactive)
  (insert (format-time-string "%F")))

;;;###autoload
(defun k/insert-buffer-file-name (&optional trim)
  "Insert buffer file name at point.
When TRIM is non-nil, or with a \\[universal-argument], trim the
filename and only insert its basename."
  (interactive "P")
  (-when-let* ((f buffer-file-name))
    (when trim (setq f (f-base f)))
    (insert (format "%s" f))))

;;;###autoload
(cl-defun k/insert-graphics (&optional
                             template
                             (new-file-dir (f-join k/cloud/ "references")))
  "Like `k/new-scratchpad', but:

- put the file under \"references\" instead by default
- ask for the filename by default
- don't create a corresponding note for it.

TEMPLATE: the template file to copy from. Interactively, ask to
select a file under the \"templates\" directory under
`k/assets/'.

NEW-FILE-DIR: where the new file should be in. Asked with a
\\[universal-argument]."
  (interactive
   (list nil (f-join k/cloud/ "references")))
  (unless template
    (setq template
          (-some--> (f-join k/assets/ "templates")
            (directory-files it nil (rx bos (not ".")))
            (completing-read "Template: " it nil t)
            (f-expand it (f-join k/assets/ "templates")))))
  (let ((new-file (--> (read-string "New file name: "
                                    (format "%s.%s"
                                            (format-time-string
                                             "%Y%m%dT%H%M%S%z")
                                            (f-ext (f-filename template))))
                    (f-join new-file-dir it))))
    (insert (k/format-link new-file))
    ;; And copy the template over
    (copy-file template new-file)
    (browse-url (browse-url-file-url new-file))))

;;;###autoload
(defun k/insert-note ()
  "Insert \"<timestamp>: \", suitable for use as editing notes."
  (interactive)
  (let ((surround (cond
                   ((derived-mode-p 'outline-mode) "/")
                   ((derived-mode-p 'markdown-mode) "*")
                   (t ""))))
    (insert (format "%s%s: %s"
                    surround
                    (k/date-iso8601)
                    surround))
    (backward-char (length surround))
    (when (fboundp #'evil-insert-state)
      (evil-insert-state))))

(provide 'k-insert)

;;; k-insert.el ends here

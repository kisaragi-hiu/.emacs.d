;;; k-org.el --- extra Org commands  -*- lexical-binding: t; -*-

(require 'kisaragi-helpers)
(require 'kisaragi-paths)
(require 'dash)
(require 'org)
(require 'org-element)
(require 's)

(declare-function evil-org-open-below "evil-org")

;;;###autoload
(defun k/evil-org-insert-newline (count)
  "Insert a new line, utilizing `evil-org-open-below' if we're at end of line.

Do so COUNT times."
  (interactive "P")
  (if (and (eolp)
           (fboundp #'evil-org-open-below))
      (evil-org-open-below count)
    (newline-and-indent count)))

;;;###autoload
(defun k/a/org-alternate-path-element-wrapper (parser)
  "Make `org-element-link-parser' try some other paths.

The paths are currently hard-coded for a Hugo project.

PARSER is `org-element-link-parser', passed in by the :around advice."
  (let* ((elem (funcall parser))
         (path (org-element-property :path elem)))
    (when (and (equal "file" (org-element-property :type elem))
               (stringp path)
               (not (f-exists? path)))
      (-when-let* ((project-root (projectile-project-root))
                   ;; Allow f-join to properly join project-root/dir/filename
                   (path (if (f-absolute? path)
                             ;; (substring path 1) is also an
                             ;; option, but that fails to handle
                             ;; "//path", which is supposed to mean
                             ;; the same thing as "/path".
                             (f-relative path "/")
                           path))
                   (no-ext (f-no-ext path))
                   (newpath (-first #'f-exists?
                                    (list
                                     (f-join project-root "content" path)
                                     ;; special support for Hugo list pages
                                     (f-join project-root "content" no-ext "_index.org")
                                     (f-join project-root "static" path)))))
        (setq elem (org-element-put-property elem :path newpath))))
    elem))

;;;###autoload
(defun k/org-toggle-hiding-stuff ()
  "Toggle link display and pretty entities."
  (interactive)
  ;; Don't show any message, just like parinfer.
  (let ((inhibit-message t))
    (k/toggle-multiple
     `((org-link-descriptive . org-toggle-link-display)
       (org-pretty-entities . org-toggle-pretty-entities)
       (org-inline-image-overlays . org-toggle-inline-images)
       (valign-mode . valign-mode)
       (org-modern-mode . org-modern-mode)
       (org-hide-emphasis-markers . ,(lambda ()
                                       (interactive)
                                       (setq org-hide-emphasis-markers
                                             (not org-hide-emphasis-markers))))))))

;;;###autoload
(defun k-org:insert-subheading-or-table-copy-down (arg)
  "Run `org-insert-subheading' or `org-table-copy-down' depending on context.
ARG is the prefix argument, passed to the commands."
  (interactive "P")
  (let ((type (org-element-type (org-element-at-point))))
    (setq prefix-arg current-prefix-arg)
    (if (s-starts-with? "table" (symbol-name type))
        (call-interactively #'org-table-copy-down)
      (end-of-line)
      (call-interactively #'org-insert-subheading))))

(defun k/org-context ()
  "Experiment: Extract context around point."
  (let (elements)
    (save-excursion
      (push (org-element-at-point) elements)
      ;; This falls apart pretty quickly eg. at the first line of a
      ;; quote
      (org-backward-paragraph)
      (push (org-element-at-point) elements))
    (setq elements (nreverse elements))
    (buffer-substring-no-properties
     (-> (elt elements 1) cadr (plist-get :contents-begin))
     (-> (elt elements 0) cadr (plist-get :contents-end)))))

(defun k/org-element-extract-content (element)
  "Extract text content between ELEMENT's `:content-begin' and `:content-end' props."
  (buffer-substring-no-properties
   (plist-get (cadr element) :contents-begin)
   (plist-get (cadr element) :contents-end)))

;;;###autoload
(defun k/org-extend-today-further ()
  "Increment `org-extend-today-until'.

Good for when I stay up later than expected."
  (interactive)
  (cl-incf org-extend-today-until)
  (message "Today now extends to: %02.0f:00" (+ 24 org-extend-today-until)))

;;;###autoload
(defun k/org-wrap-subtree (type)
  "Wrap subtree under point in a block of TYPE.

For example, when TYPE is \"quote\", turn this:

  * A heading
  some |content (the bar is the cursor)

into this:

  * A heading
  #+begin_quote
  some |content (the bar is the cursor)
  #+end_quote"
  (interactive "MBlock type (such as quote, src ...): ")
  (save-mark-and-excursion
    (org-mark-subtree)
    (forward-line)
    (let ((begin (region-beginning))
          (end (region-end)))
      ;; Inserting at BEGIN first would push the text further, such
      ;; that END isn't actually the end of the subtree anymore.
      (goto-char end)
      (insert "#+end_" type "\n")
      (goto-char begin)
      (insert "#+begin_" type "\n"))))

;;;###autoload
(defun k/org-split-quote ()
  "Insert an end quote and a begin quote to split the quote block in two.

Place cursor at a blank line:

  A paragraph
  | <- here
  Another paragraph

Run this, which turns it into

  A paragraph
  #+end_quote

  #+begin_quote
  Another paragraph"
  (interactive)
  (insert "#+end_quote\n\n#+begin_quote"))

;;;###autoload
(defun k/org-split-src-block ()
  "Split the src block at point."
  (interactive)
  (let ((elem (org-element-at-point)))
    (when (eq 'src-block (org-element-type elem))
      (let ((lang (org-element-property :language elem)))
        (insert
         (if lang
             (format "#+end_src\n\n#+begin_src %s" lang)
           "#+end_src\n\n#+begin_src"))))))

;;;###autoload
(defun k/org-wrap-subtree/quote ()
  "Wrap subtree under point in a quote block."
  (interactive)
  (k/org-wrap-subtree "quote"))

;;;###autoload
(defun k/org-wrap-element-in-quote ()
  "Put everything in the element under point in a quote block."
  (interactive)
  (save-mark-and-excursion
    (org-mark-element)
    (goto-char (region-beginning))
    (insert "#+begin_quote\n")
    (goto-char (region-end))
    (insert "#+end_quote\n")))

(defun k/org-remove-markup (&optional beg end)
  "Remove markup between BEG and END."
  (interactive
   (when (region-active-p)
     (list (region-beginning)
           (region-end))))
  (unless (and beg end)
    (user-error "Please select text to remove markup from"))
  (require 'dom)
  (save-restriction
    (narrow-to-region beg end)
    (let ((parsed (org-element-parse-buffer)))
      (delete-region beg end)
      (insert (->> parsed
                   ;; Yes, this works.
                   dom-texts
                   (s-replace-regexp "[ \t]+" " "))))))

(cl-defun k/org-files-in-org-directory (&optional (dir org-directory))
  "Return all .org files in DIR.

DIR defaults to `org-directory'."
  (->> (projectile-project-files (or dir org-directory))
       (--filter (s-matches? (rx bol (not (any ".")) (0+ any) ".org" eol)
                             it))
       (--map (f-expand it dir))))

;;;###autoload
(defun k/org-clean-up-logbook ()
  "Delete all :LOGBOOK: entries.

Despite specifying nologdone in my vocabulary file, Org still
keeps adding entries to logbooks. Instead of figuring out the
currect way to fix it, I'll just delete them every so often."
  (interactive)
  (goto-char (point-min))
  (while (search-forward ":LOGBOOK:\n" nil t)
    (save-mark-and-excursion
      (org-mark-element)
      (delete-region (region-beginning) (region-end)))))

(defun k/firefox-to-org//level-from-title (title)
  "Extract nested level information from TITLE.
If TITLE looks like \"> abc...\", it's level 1, \">> ...\" means
level 2, and so on.

Return a list: first element is the title with the prefix \">\"
removed, second element is the nested level."
  (-let (((_ arrows just-title)
          (s-match (rx (group (0+ ">"))
                       (opt " ")
                       (group (0+ any)))
                   title)))
    (list just-title
          (s-count-matches ">" arrows))))

;;;###autoload
(defun k/firefox-to-org/from-list-file (file)
  "Import Firefox bookmarks into Org from FILE.
FILE: Use the \"backup\" function in Firefox to export a JSON,
then extract list of bookmarks from it. This does not yet handle
Firefox's own folders. (But Tree Style Tab levels are
supported.)"
  (interactive "fFirefox exported (and extracted) JSON: ")
  (save-excursion
    (goto-char (point-max))
    (let ((json (json-read-file file)))
      (seq-doseq (bookmark json)
        (let-alist bookmark
          (-let* (((title level) (k/firefox-to-org//level-from-title .title))
                  (stars (make-string (1+ level) ?*)))
            (insert (format "%s %s
:PROPERTIES:
:url:  %s
:created:  %s
:END:\n"
                            stars
                            title
                            .uri
                            (k/date-iso8601 (seconds-to-time (/ .dateAdded 1000000)))))))))))

;;;###autoload
(defun k/org-set-updated-keyword (timestamp)
  "Set \"#+updated\" keyword to TIMESTAMP.

Interactively, use current time by default; prompt for time with \\[universal-argument]."
  (interactive
   (list
    (if current-prefix-arg
        (read-string "Time (in full ISO 8601 format): " (k/date-iso8601))
      (k/date-iso8601))))
  (minaduki::set-global-prop "updated" timestamp))

;;;###autoload
(defun k-org:set-heading-added (timestamp)
  "Set \"added\" property of heading under point to TIMESTAMP.

Interactively, use current time by default; prompt for time with \\[universal-argument]."
  (interactive
   (list
    (if current-prefix-arg
        (read-string "Time (in full ISO 8601 format): " (k/date-iso8601))
      (k/date-iso8601))))
  (org-set-property "added" timestamp))

;;;###autoload
(defun k-org:set-heading-created (timestamp)
  "Set \"created\" property of heading under point to TIMESTAMP.

Interactively, use current time by default; prompt for time with \\[universal-argument]."
  (interactive
   (list
    (if current-prefix-arg
        (read-string "Time (in full ISO 8601 format): " (k/date-iso8601))
      (k/date-iso8601))))
  (let ((level (org-current-level)))
    (if (and level
             (not (equal 0 level)))
        (org-set-property "created" timestamp)
      ;; use current file modification time instead for now
      (setq timestamp
            (k/date-iso8601
             (file-attribute-modification-time
              (file-attributes
               (buffer-file-name)))))
      (minaduki::set-global-prop "created" timestamp))))

;;;###autoload
(defun k/org-turn-heading-into-habit ()
  "Turn heading that point is on into a habit.

This uses a custom TODO keyword \"HABIT\" if it exists; otherwise
it'll fall back to \"TODO\"."
  (interactive)
  (condition-case nil (org-todo "HABIT")
    (user-error (org-todo "TODO")))
  (org-set-property "STYLE" "habit"))

(k/defun-string-or-region k/org-turn-lines-into-links ()
  "Turn STRING into links.

STRING look like this:

Description
URL

Description
URL
..."
  (--> string
       (s-split "\n" it :omit)
       (-partition 2 it)
       (mapconcat (pcase-lambda (`(,desc ,url))
                    (format "[[%s][%s]]" url desc))
                  it "\n")))

;;;###autoload
(defun k/org-sort-entries-by-rating ()
  "Sort Org entries by their RATING property.
- Items without the RATING property come first
- Then compare RATING as numbers, higher rated items come first."
  (interactive)
  (org-sort-entries
   nil ?f
   (lambda ()
     ;; `sort-subr' behaves differently if this returns nil, but in
     ;; we'll never hit that special case here.
     (list (org-entry-get (point) "rating")
           (org-entry-get (point) "TODO")
           (org-entry-get (point) "ITEM")))
   (pcase-lambda (`(,rating-a ,todo-a ,title-a)
                  `(,rating-b ,_todo-b ,title-b))
     (or (and todo-a
              (string> title-a title-b))
         (and rating-b ; Short circuit: rating = nil -> B should be in front
              (or (not rating-a) ; empty rating first
                  (equal "" (string-trim rating-a)) ; same
                  (let
                      ;; convert to numbers first
                      ((rating-a (string-to-number rating-a))
                       (rating-b (string-to-number rating-b)))
                    ;; catch equal ratings, fall back explicitly to `string>'
                    ;; If we want to do something else we can do it here.
                    (if (= rating-a rating-b)
                        (string> title-a title-b)
                      (> rating-a rating-b)))))))
   nil nil))

;;;###autoload
(defun k/org-sort-entries-by-created-timestamp ()
  "Sort Org entries by their CREATED property.
- Items without the CREATED property come first
- Then compare CREATED as ISO 8601 timestamps, earlier items come first."
  (interactive)
  (require 'canrylog-types) ; for `canrylog-iso8601-<'
  (org-sort-entries
   nil ?f
   (lambda ()
     ;; `sort-subr' behaves differently if this returns nil, but in
     ;; we'll never hit that special case here.
     (list (org-entry-get (point) "created")
           (org-entry-get (point) "item")))
   (pcase-lambda (`(,created-a ,title-a) `(,created-b ,title-b))
     (and created-b ; Short circuit: created = nil -> B should be in front
          (or (not created-a) ; empty timestamps first
              (equal "" (string-trim created-a)) ; same
              ;; catch equal timestamps explicitly, even though it's
              ;; not really going to happen, to `string>'
              (if (= created-a created-b)
                  (string> title-a title-b)
                (canrylog-iso8601-< created-b created-a)))))
   nil nil))

(defvar kisaragi-org-send-task--hist nil
  "History for `k/org-send-task' prompt.")

;; maybe this is reimplementing part of org-refile?
;;;###autoload
(defun k/org-send-task (target-file &optional reason action-keyword todo-keyword)
  "Send task under point to TARGET-FILE with REASON.

Move the top level heading and its contents under point to
`target-file', then add the ACTION name into it, optionally with a REASON.

Set the todo keyword after moving to TODO-KEYWORD if it's
present.

Add a date attribute under ACTION-KEYWORD if it's present. For
example, if ACTION-KEYWORD is \"closed\", \"CLOSED:
<yyyy-mm-dd>\" is inserted under the heading.

This is a generalized function for eg. cancelling or finishing a task
by moving it to another file."
  (interactive
   (list (completing-read "Send to file: "
                          (k/org-files-in-org-directory)
                          nil t
                          nil 'kisaragi-org-send-task--hist)))
  (let ((original-file (buffer-file-name))
        (cursor-origin (point)))
    (org-up-heading-safe)
    (let* ((element (org-element-at-point))
           (begin (org-element-property :begin element))
           (end (org-element-property :end element))
           (content (buffer-substring-no-properties begin end)))
      (find-file target-file)
      (goto-char (point-max))
      (insert content)
      (org-up-heading-safe)
      (when todo-keyword
        (org-todo todo-keyword))
      (end-of-line)
      (electric-newline-and-maybe-indent)
      (when action-keyword
        (insert
         (concat "  " (upcase action-keyword) ": "
                 (format-time-string "<%Y-%m-%d %a>"))))
      (when (and (stringp reason)
                 (not (equal reason "")))
        (electric-newline-and-maybe-indent)
        (insert reason))
      (insert "\n")
      (save-buffer)
      (find-file original-file)
      ;; delete the task in the original file
      (delete-region begin end)
      (save-buffer)
      (goto-char cursor-origin))))

;;;###autoload
(defun k/org-cancel-task (&optional reason)
  "Cancel task under point, with REASON.

Move the top level heading and its contents under point to cancelled.org under
`k/notes/' then add current date and the reason to the task."
  (interactive)
  (k/org-send-task (f-join k/notes/ "task-archive.org")
                   reason
                   "closed"
                   "CANCELLED"))

;;;###autoload
(defun k/org-finish-task ()
  "Finish task under point.

Move the top level heading and its contents under point to done.org under
`k/notes/' then add current date into the task."
  (interactive)
  (k/org-send-task (f-join k/notes/ "task-archive.org")
                   nil
                   "closed"
                   "DONE"))

(provide 'k-org)
;;; k-org.el ends here

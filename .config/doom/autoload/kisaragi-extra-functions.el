;;; kisaragi-extra-functions.el --- where I dump extra functions  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Dump functions here when they don't yet fit in a package.
;;; Code:
(require 's)
(require 'f)
(require 'ht)
(require 'dash)
(require 'subr-x)
(require 'cl-seq)
(require 'cl-lib)
(require 'shr)

(require 'evil)

(require 'kisaragi-constants)
(require 'kisaragi-file-finders)

;;;###autoload
(require 'kisaragi-helpers)

(require 'kisaragi-paths)

(require 'transient)
(require 'ansi-color)

;;;###autoload
(defun k/capitalize-line ()
  "Capitalize current line."
  (interactive)
  ;; FIXME: I want a function that capitalizes English sentences.
  (capitalize-region (pos-bol) (pos-eol)))

(defun k/caps-mode-capitalize ()
  "Capitalize the last inserted character."
  (when-let ((char last-command-event))
    (capitalize-region (1- (point)) (point))))
(define-minor-mode k/caps-mode
  "A replacement for caps lock."
  :global t :lighter "CAPS"
  (if k/caps-mode
      (add-hook 'post-self-insert-hook #'k/caps-mode-capitalize nil t)
    (remove-hook 'post-self-insert-hook #'k/caps-mode-capitalize t)))

;; from sort.el which is not loaded by default
(defvar sort-fold-case)

;;;###autoload
(defun k/sort-comma-lists (start end reverse)
  "Sort every comma/colon list between START and END.

If REVERSE, reverse the order of the sort.

A \"comma/colon list\" here means every continuous blob of lines
that start with a non-whitespace character. Commas and colons
will be added such that each list looks like this:

abc,
def,
ghi:"
  (interactive
   `(,@(if (region-active-p)
           (list (region-beginning) (region-end))
         (list (save-excursion
                 ;; Skip the first paragraph
                 ;; Which is normally the header line
                 (goto-char (point-min))
                 (forward-paragraph)
                 (point))
               (point-max)))
     ,current-prefix-arg))
  (let ((item-bounds))
    ;; Step 1: collect where the lists to sort are
    (save-excursion
      (save-restriction
        (narrow-to-region start end)
        (goto-char start)
        (let ((item-start nil)
              (item-end nil))
          (while (not (eobp))
            (if (looking-at (rx (not space)))
                ;; When encountering a line starting with a non-blank,
                ;; Register the start of an item if we haven't seen one yet
                ;; since that last time it's flushed.
                (when (not item-start)
                  (setq item-start (point)))
              ;; When encountering a line starting with a blank,
              ;; If we've registered the start of an item, then register the end
              ;; of it, flush it to the `item-bounds' list.
              (when item-start
                ;; In Emacs we can assume line break is one char
                (setq item-end (point))
                (push (cons item-start item-end) item-bounds)
                (setq item-start nil item-end nil)))
            (forward-line)))))
    ;; Step 2: actually sort them
    (pcase-dolist (`(,item-start . ,item-end) item-bounds)
      ;; Step 2a: sort lines
      (let ((sort-fold-case t))
        (sort-lines reverse item-start item-end))
      ;; Step 2b: Add the commas or colon
      (save-excursion
        (save-restriction
          (narrow-to-region item-start item-end)
          (goto-char item-start)
          ;; End every entry with a comma
          (while (not (eobp))
            (end-of-line)
            (pcase (char-before)
              (?, nil)
              (?: (delete-char -1) (insert ","))
              (_ (insert ",")))
            (forward-line))
          ;; Then go back to end the last entry with a colon
          (while (eobp)
            (forward-line -1))
          (end-of-line)
          (delete-char -1)
          (insert ":"))))))

(declare-function magit-get-current-remote "magit")
(declare-function magit-read-remote "magit")
(declare-function magit-get "magit")
(declare-function magit-set "magit")

(defun k/git-remote-to-ssh (remote)
  "Turn REMOTE into using an SSH URL."
  (interactive (list (or (magit-get-current-remote)
                         (magit-read-remote "Convert remote URL to ssh"))))
  (cl-block nil
    (let ((urlobj (-some-> (magit-get "remote" remote "url")
                    url-generic-parse-url))
          newurl)
      (unless urlobj
        (message "Could not get the URL of remote %s" remote)
        (cl-return nil))
      (unless (equal "https" (url-type urlobj))
        (message "Remote URL is not an https URL: %s" (url-recreate-url urlobj))
        (cl-return nil))
      (setq newurl (format "git@%s:%s"
                           (url-host urlobj)
                           (-> (url-filename urlobj)
                               ;; get rid of the leading slash
                               (substring 1))))
      (kill-new newurl)
      (magit-set newurl "remote" remote "url")
      (message "Successfully set remote %s to use %s" remote newurl))))

;;;###autoload
(defun k/sort-words (start end &optional reverse)
  "Sort words in region, between START and END.
With REVERSE (interactively, with a \\[universal-argument]), sort
from z-a, otherwise from a-z."
  (interactive "r\nP")
  (sort-regexp-fields
   reverse
   "[[:alnum:]]+"
   "[[:alnum:]]+"
   start end))

(defun k/copy-defun ()
  "Copy the `defun' at point."
  (interactive)
  (let ((str (thing-at-point 'defun)))
    (set-text-properties 0 (length str) nil str)
    (kill-new str)
    (message "Copied \"%s\""
             (--> str
                  (s-replace-regexp "\n.*" "" it)
                  (truncate-string-to-width it (window-width))
                  (format "%s..." it)))))

(declare-function eshell-poor-mans-grep "em-unix")
(declare-function org-link-decode "ol")

(defun k/format-pocount-output ()
  "Format the pocount output at point.
Point should be at the word \"Type\"."
  (interactive)
  (let ((numbers-rx '((+ space)
                      (group (+ digit))
                      (+ space)
                      (opt "(" (opt space) (group (+ digit) "%") ")")))
        (start (point))
        (data '((translated) (total))))
    (re-search-forward
     (rx-to-string
      `(seq "Translated:" ,@numbers-rx ,@numbers-rx)))
    (map-put! data
              'translated
              `((strings . ((count . ,(match-string 1))
                            (% . ,(match-string 2))))
                (words . ((count . ,(match-string 3))
                          (% . ,(match-string 4))))))
    (re-search-forward
     (rx-to-string
      `(seq "Total:" ,@numbers-rx ,@numbers-rx)))
    (map-put! data
              'total
              `((strings . ,(match-string 1))
                (words . ,(match-string 3))))
    (goto-char start)
    (let-alist data
      (insert (format "Strings: %s/%s (%s)\nWords: %s/%s (%s)\n"
                      .translated.strings.count
                      .total.strings
                      .translated.strings.%
                      .translated.words.count
                      .total.words
                      .translated.words.%)))))

;;;###autoload
(defun k:xml-unescape-region (start end)
  "XML unescape text between START and END.
For instance, replace &amp; with &.
Interactively, act on the active region, or the whole accessible
part of the buffer if the region is not active."
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (save-excursion
    (save-restriction
      (goto-char start)
      (narrow-to-region start end)
      (xml-parse-string))))

;;;###autoload
(defun k:xml-escape-region (start end)
  "XML escape text between START and END.
For instance, replace & with &amp;.
Interactively, act on the active region, or the whole accessible
part of the buffer if the region is not active."
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list (point-min) (point-max))))
  (let* ((orig (buffer-substring-no-properties start end))
         (escaped (xml-escape-string orig)))
    (save-excursion
      (delete-region start end)
      (goto-char start)
      (insert escaped))))

;;;###autoload
(defun k:scaffold-utau ()
  "Scaffold an UTAU project in the current directory."
  (interactive)
  (when (and (not (file-exists-p ".git"))
             (y-or-n-p
              (format "Scaffolding UTAU project at `%s'. Continue?"
                      default-directory)))
    (k/call-process-to-string "git" "init")
    (with-temp-file ".gitignore"
      (insert (s-trim "
*.cache
*.wav
*.mp4
*.mp3")))
    (k/call-process-to-string "git" "add" ".")
    (k/call-process-to-string "git" "commit" "-m" "Initial commit"))
  (call-interactively #'magit-status))

;;;###autoload
(defun k:scaffold-reaper ()
  "Create a new Reaper project."
  (interactive)
  (let* ((timestamp (format-time-string "%Y%m%dT%H%M%S%z"))
         (dir (f-join k/cloud/ "reaper" timestamp)))
    (f-mkdir dir)
    (make-process
     :name "reaper"
     :command
     `("reaper"
       "-new"
       "-saveas" ,(f-join dir (format "%s.RPP" timestamp))))))

;;;###autoload
(let ((prev 'upper))
  (defun k:cycle-case (&optional prompt)
    "Cycle case in region or for word at point.
If PROMPT, ask instead of cycle."
    (interactive "P")
    (let* ((values '(upper lower title))
           (case (or (and prompt
                          (-some->> (completing-read "Change to case: "
                                                     '("UPPER CASE"
                                                       "lower case"
                                                       "Title Case"))
                            (s-split " ")
                            car
                            downcase
                            intern))
                     (k/next values prev))))
      (setq prev case)
      (save-mark-and-excursion
        (call-interactively
         (pcase case
           (`upper #'upcase-dwim)
           (`lower #'downcase-dwim)
           (`title #'capitalize-dwim)))
        ;; This variable is tested by the command loop after the command
        ;; returns; setting it to t in a command is how you deactivate the
        ;; region.
        ;;
        ;; The casing commands set this to t, but thankfully the test isn't
        ;; run after `call-interactively', allowing us to override that
        ;; request to the command loop.
        (setq deactivate-mark nil)))))

;;;###autoload
(defun k/add-after-save-command (command)
  "Add a shell COMMAND to be run after saving this file."
  (interactive
   (list (read-shell-command "Run command after save for this file: ")))
  (add-hook 'after-save-hook
            (lambda () (shell-command-to-string command))
            nil t))

(defvar apt-package-description-fl-keywords
  `(("^---+.*$" . font-lock-comment-face)
    ("^[[:alnum:]-]+:" . font-lock-keyword-face)
    (,(rx bow "arch=") . font-lock-keyword-face)
    (,(rx bow (or "optional" "devel" "deb") eow) . font-lock-builtin-face)))
(define-derived-mode apt-package-description-mode fundamental-mode
  "Apt Package Description"
  (setq-local font-lock-defaults '(apt-package-description-fl-keywords)))

(add-to-list 'auto-mode-alist '("\\.dsc\\'" . apt-package-description-mode))

(defun k/recompile ()
  "Recompile current directory using Make."
  (when-let ((f (buffer-file-name)))
    (let ((inhibit-message t)
          (async-shell-command-buffer 'confirm-kill-process))
      ;; kill old process without confirmation
      (cl-flet (((symbol-function 'yes-or-no-p
                                  (lambda (_prompt) t))))
        (async-shell-command "make compile")))))

(define-minor-mode k/recompile-on-save-mode
  "Minor mode to recompile after save."
  :global nil :lighter nil
  (if k/recompile-on-save-mode
      (add-hook 'after-save-hook #'k/recompile nil t)
    (remove-hook 'after-save-hook #'k/recompile t)))

(defun k/copy-current-file-name (&optional insert?)
  "Copy file name of current buffer.

With a \\[universal-argument] or with INSERT? non-nil, insert
instead."
  (interactive "P")
  (let* ((f (buffer-file-name))
         (box (if f
                  (cons 'file f)
                (cons 'dir default-directory))))
    (message "%s" box)
    (when (stringp (cdr box))
      (if insert?
          (insert (format "%S" (cdr box)))
        (kill-new (cdr box))
        (message
         (pcase (car box)
           ('file "Copied current file name.")
           ('dir "Copied current directory.")))))))

;;;###autoload
(defun k:elisp-list-errors ()
  "List all error symbols."
  (interactive)
  (let (err-syms)
    (mapatoms
     (lambda (x)
       (when (get x 'error-conditions)
         (push x err-syms))))
    (setq err-syms (sort err-syms (-on #'string< #'symbol-name)))
    (with-current-buffer (pop-to-buffer
                          (get-buffer-create
                           "*k/error symbols*"))
      (setq-local mode-name "Error Symbol List")
      (setq-local revert-buffer-function
                  (lambda (&rest _)
                    (let ((here (point)))
                      (k:elisp-list-errors)
                      (ignore-errors
                        (goto-char here))
                      (message "Refreshed"))))
      (erase-buffer)
      (insert
       (format "There are %s error symbols defined in this Emacs.\n\n"
               (length err-syms)))
      (dolist (sym err-syms)
        (insert
         (format "%s\n"
                 (make-text-button
                  (symbol-name sym)
                  nil
                  'apropos-symbol sym
                  'type 'apropos-plist
                  'skip t))))
      (goto-char (point-min)))))

(define-minor-mode k/view-scroll-mode
  "Minor mode to scroll view instead of move cursor."
  :global nil :lighter "VS "
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map [remap evil-next-line] #'scroll-up-line)
            (define-key map [remap evil-previous-line] #'scroll-down-line)
            map))

;;;###autoload
(defun k/format-facebook-backup ()
  "Normalize a Facebook backup JSON file."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let ((inhibit-read-only t)
          (size (point-max))
          bounds str)
      (while (search-forward "\"\\u" nil t)
        (message "%.f%%" (* 100 (/ (point) size 1.0)))
        (setq bounds (bounds-of-thing-at-point 'string))
        (when bounds
          (setq str (--> (json-parse-string (buffer-substring (car bounds)
                                                              (cdr bounds)))
                         (string-to-list it)
                         (apply #'unibyte-string it)
                         (decode-coding-string it 'utf-8)))
          (k/set-buffer-substring (car bounds) (cdr bounds)
            (json-serialize str))))))
  (save-buffer))

;;;###autoload
(defun k/ansi-color-apply ()
  "Apply ansi color codes in current buffer."
  (interactive)
  (ansi-color-apply-on-region
   (point-min) (point-max)
   :preserve))

;;;###autoload
(defun k/svn-to-git-svn (&optional dir)
  "Turn DIR, a SVN checkout, into a git-svn checkout."
  (interactive "DSVN dir: ")
  (with-current-buffer (pop-to-buffer
                        (get-buffer-create
                         (format "*k/svn-to-git-svn: %s*" (f-filename dir))))
    (let ((default-directory dir)
          svn-url)
      (setq svn-url (k/call-process-to-string "svn" "info" "--show-item=url"))
      (k/call-process t
        "git" "svn" "init" svn-url)
      (k/call-process t
        "git" "svn" "fetch" "-r" "HEAD")
      (k/call-process t
        "git" "reset" "HEAD" "--")
      (make-directory (f-join ".git" "info") t)
      (with-temp-file (f-join ".git" "info" "exclude")
        (insert "/.svn/")))))

;; I'm surprised that `url-copy-file' isn't interactive.
;;;###autoload
(defun k/dl (url)
  "Download URL to current directory."
  (interactive "MURL: ")
  (url-copy-file url (f-filename url)))

;;;###autoload
(defun k/grep-elisp (needle dir &optional regexp)
  "Search for NEEDLE in files in DIR matching REGEXP, without using external tools.

This allows searching through anything that `find-file-noselect'
can open, such as .el.gz files or remote files.

Utilizes `eshell-poor-mans-grep'.

Interactively, ask user for NEEDLE and DIR. With a
\\[universal-argument], also ask for REGEXP."
  (interactive
   (list
    ;; This is more or less `ag/read-from-minibuffer'.
    (let* ((suggestion (or
                        (and (use-region-p)
                             (buffer-substring-no-properties
                              (region-beginning)
                              (region-end)))
                        (-some-> (symbol-at-point)
                          symbol-name)))
           (prompt (format "Search string%s: "
                           (or (-some->> suggestion
                                 (format " (default %s)"))
                               ""))))
      (--> (read-string prompt)
           (if (> (length it) 0)
               it
             suggestion)))
    (read-directory-name "Search in directory: ")
    (when current-prefix-arg
      (read-regexp "Only search files matching this regexp: "))))
  (require 'em-unix)
  (let ((emacs-lisp-mode-hook nil)
        (lisp-mode-hook nil)
        (lisp-data-mode-hook nil))
    (cl-letf*
        ;; Patch insert to do redisplay
        ;; So it's possible to see progress
        ((orig (symbol-function 'insert))
         ((symbol-function 'insert)
          (lambda (&rest args)
            (prog1 (apply orig args)
              (redisplay)))))
      (eshell-poor-mans-grep
       (list needle
             (directory-files-recursively dir (or regexp "")))))))

(defun k/set-file-mtime-per-filename (file)
  "Set FILE's modification time and access time according to its filename.

The modification time and access time of a file with the name
\"ABC_20200104_231521\" will be set to
January 4th 2020, 23:15:21."
  (-let (((_ date time sec)
          (s-match (rx (group (= 8 digit))
                       "_"
                       (group (= 4 digit))
                       (group (= 2 digit)))
                   file))
         (exit-code nil))
    (setq exit-code (call-process "touch" nil nil nil
                                  "-t" (format "%s%s.%s" date time sec)
                                  file))
    (unless (= exit-code 0)
      (message "`touch' returned an error for file %s" file))))

;; Listing albums by album duration
;; Documented in internal note 2021-08-03T02:02:08+0900 and https://kisaragi-hiu.com/sort-albums-by-duration
(defun k/song-duration (song-file)
  "Return duration of SONG-FILE in seconds."
  (with-temp-buffer
    (call-process
     "ffprobe" nil '(t nil) nil
     "-v" "quiet"
     "-print_format" "json"
     "-show_streams"
     song-file)
    (goto-char (point-min))
    (-some--> (json-parse-buffer
               :object-type 'alist)
      (map-elt it 'streams)
      (seq-find (lambda (elem)
                  (equal (map-elt elem 'codec_type)
                         "audio"))
                it)
      (map-elt it 'duration)
      string-to-number)))
(defun k/folder-duration (folder)
  "Return duration of all songs in FOLDER."
  (--> (directory-files folder t)
       (mapcar #'k/song-duration it)
       -non-nil
       (apply #'+ it)))
(defun k/list-albums (dir)
  "List music folders in DIR, providing a duration field for sort."
  (interactive (list (xdg-user-dir "MUSIC")))
  (let (folders)
    (dolist-with-progress-reporter (folder (f-directories dir))
        "Probing folders..."
      (push (cons folder (k/folder-duration folder)) folders))
    (with-current-buffer (get-buffer-create "*k/music folders*")
      (when (= 0 (buffer-size))
        (tabulated-list-mode)
        (setq tabulated-list-format
              (vector
               '("folder" 70 t)
               (list "duration" 20
                     (lambda (a b)
                       ;; An entry is (ID ["<folder>" "<duration>"]).
                       ;;
                       ;; <duration> looks like (label :key val :key val...)
                       ;; when props are given.
                       (< (-> (cadr a) (elt 1) cdr (plist-get :seconds))
                          (-> (cadr b) (elt 1) cdr (plist-get :seconds)))))))
        (tabulated-list-init-header))
      (dolist (folder folders)
        (push (list nil (vector (f-base (car folder))
                                (list (format-seconds "%.2h:%.2m:%.2s" (cdr folder))
                                      :seconds (cdr folder))))
              tabulated-list-entries))
      (revert-buffer))))

(defun k/roc-to-gregorian (date)
  "Convert ROC year in DATE to gregorian, returning it as YYYY-MM-DD.

DATE itself should be year-month-day separated by dashes."
  (-let (((roc-y month day) (s-split "-" date)))
    (format "%s-%s-%s"
            (+ 1911 (string-to-number roc-y))
            month
            day)))

(defun k/vtt-to-txt ()
  "In a buffer containing VTT subtitles, remove everything that isn't the txet."
  (interactive)
  (call-process-region
   (point-min)
   (point-max)
   "awk" t t nil
   ;; From https://stackoverflow.com/questions/56927772/
   "FNR<=4 || ($0 ~ /^$|-->|\\[|\\]|</){next} !a[$0]++"))

;;;###autoload
(defun k/new-buffer ()
  "Create a new buffer and ask for a major mode."
  (interactive)
  (let ((buffer (generate-new-buffer "*new*")))
    (display-buffer buffer '(display-buffer-same-window))
    (with-current-buffer buffer
      (let* ((predicate
              ;; Copied from `counsel-major' Not using that directly because
              ;; its prompt is hardcoded and working around that requires an
              ;; ugly hack utilizing
              ;; `ivy-set-prompt-text-properties-function'.
              (lambda (f)
                (and (commandp f) (string-match "-mode$" (symbol-name f))
                     (or (and (autoloadp (symbol-function f))
                              (let ((doc-split (help-split-fundoc (documentation f) f)))
                                ;; major mode starters have no arguments
                                (and doc-split (null (cdr (read (car doc-split)))))))
                         (null (help-function-arglist f))))))
             (mode (completing-read
                    "Major mode for new buffer: "
                    obarray predicate))
             (mode (if (string= mode "")
                       (default-value 'major-mode)
                     (intern mode))))
        (funcall mode)))))

(defun k/files-flatten-1-subdirectory (dir)
  "Move all files in subdirectories of DIR to DIR itself.

Subdirectory names are added onto each file to prevent conflict.

Example:

  dir/2015/0.png -> dir/2015 0.png
  dir/2015/my project/readme.md -> dir/2015 my project/readme.md
    (nested structures are preserved)
  dir/2015/1.png -> dir/2015 1.png
  dir/2016/1.png -> dir/2016 1.png"
  (cl-loop for f in (f-files dir nil t)
           collect
           (let ((newname (--> (s-replace-regexp
                                (format "^%s\\([[:digit:]]\\{8\\}\\)/\\(.*\\)$" dir)
                                (format "%s\\1 \\2" dir)
                                f))))
             ;; (unless (f-exists? (f-parent newname))
             ;;   (make-directory (f-parent newname)))
             (f-move f newname))))

(defun k/org-set-creation-info-from-commit-data ()
  "Set creation properties for heading at point according to a commit header.

Copy the commit header, place the cursor on the target Org heading,
and this will set the right properties."
  (interactive)
  (let-alist (->> (current-kill 0)
                  ;; So we don't have to special case the first line
                  (concat "commit-id:")
                  ;; Convert "abc: def\nghi: jkl" to
                  ;; '((abc . "def") (ghi . "jkl"))
                  (s-split "\n")
                  (--map (s-split-up-to ":" it 1))
                  (-tree-map #'s-trim)
                  (--map `(,(intern (car it)) . ,(cadr it))))
    (org-entry-put nil "commit" .commit-id)
    (org-entry-put nil "created" (kisaragi-timestamp-commands/commit-timestamp-to-iso8601 .AuthorDate))))

;;;###autoload
(defun k/open-project (id)
  "Open one of my personal project with ID."
  (interactive (list (thing-at-point 'word)))
  (let ((found (k/org-link-abbrev-project id)))
    (unless (s-prefix? "/tmp" found)
      (find-file found))))

;;;###autoload
(defun k/zsh-wrap-timing (beg end id)
  "Wrap region between BEG and END with k-time ID.

Example (where \"|\" denotes region):

|abc
def|

becomes

k-time-start ID
abc
def
k-time-end ID

."
  (interactive
   (progn
     (unless (region-active-p)
       (user-error "This command requires an active region"))
     (list (region-beginning)
           (region-end)
           (read-string "ID: "))))
  (save-excursion
    (goto-char end)
    (insert (format "k-time-end %s\n" id))
    (goto-char beg)
    (insert (format "k-time-start %s\n" id))))

;;;###autoload
(k/defun-string-or-region k/ellipsis ()
  "Return \"[…]\".

Interactively, replace region with \"[…]\"."
  "[…]")

;;;###autoload
(k/defun-string-or-region k/wrap-square ()
  "Return \"[[STRING]]\"

Interactively, replace region with \"[[STRING]]\"."
  (format "[[%s]]" string))

;;;###autoload
(defun k/em-dash (start end)
  "Replace \"---\" in region (between START and END) with \"—\"."
  (interactive "r")
  (k/set-buffer-substring start end
    (replace-regexp-in-string "---" "—"
                              (buffer-substring start end))))

;;;###autoload
(defun k/paste-html-table ()
  "Paste HTML table from clipboard in Org or Markdown."
  (interactive)
  (let ((target-format (cond
                        ((derived-mode-p 'org-mode) "org")
                        (t "markdown"))))
    (-> (with-temp-buffer
          (insert (current-kill 0))
          ;; Clean up table
          (let ((dom (libxml-parse-html-region (point-min) (point-max))))
            ;; Extract <pre> tags
            (setq dom (--tree-map-nodes (and (consp it)
                                             (eq 'pre (dom-tag it)))
                                        (dom-text it)
                                        dom))
            ;; Attempt to remove <hr> and <br>
            (--tree-map-nodes (and (consp it)
                                   (or (memq (dom-tag it)
                                             '(hr br))))
                              (dom-remove-node dom it)
                              dom)
            ;; Replace buffer content for pandoc (which supports XHTML just fine)
            (erase-buffer)
            (insert (shr-dom-to-xml dom)))
          (call-process-region (point-min) (point-max) "pandoc"
                               t '(t nil) nil
                               "--from" "html"
                               "--wrap=preserve"
                               "--to" target-format)
          (buffer-string))
        insert)))

;;;###autoload
(defun k/paste-pandoc (source target)
  "Convert clipboard content from SOURCE to TARGET, then paste."
  (interactive
   (list (completing-read "Original format: "
                          (with-temp-buffer
                            (call-process "pandoc" nil t nil
                                          "--list-input-formats")
                            (s-lines (buffer-string))))
         (completing-read "Target format: "
                          (with-temp-buffer
                            (call-process "pandoc" nil t nil
                                          "--list-output-formats")
                            (s-lines (buffer-string))))))
  (insert
   (with-temp-buffer
     (insert (current-kill 0))
     (call-process-region (point-min) (point-max) "pandoc"
                          t '(t nil) nil
                          "--from" source
                          "--wrap=preserve"
                          "--to" target)
     (buffer-string))))

;;;###autoload
(defun k/paste-markdown ()
  "Convert clipboard from Markdown to Org or HTML, then paste."
  (interactive)
  (k/paste-pandoc
   "markdown"
   (cond
    ((derived-mode-p 'org-mode) "org")
    (t "html"))))

;;;###autoload
(defun k/pollen-code-to-org ()
  "Convert all Pollen inline code to Org syntax."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "◊code{\\(.*?\\)}" nil t)
        (replace-match "=\\1=")))))

;;;###autoload
(defun k/pollen-link-to-org ()
  "Convert all Pollen links in current buffer to Org links."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx "◊link[\"" (group (*? any)) "\"]"
                  "{" (group (*? any)) "}")
              nil t)
        (replace-match "[[\\1][\\2]]")))))

;;;###autoload
(defun k/pollen-highlight-block-to-org ()
  "Replace all ◊highlight['abc] blocks with Org source blocks."
  (interactive)
  (save-match-data
    (save-excursion
      (while (re-search-forward
              (rx "◊highlight['" (group (*? any)) "]")
              nil t)
        (let (lang beg end code-start code-end code)
          (setq lang (match-string 1)
                beg (line-beginning-position)
                code-start (1+ (point)))
          (forward-sexp)
          (setq end (point)
                code-end (1- (point))
                code (buffer-substring-no-properties code-start code-end))
          (delete-region beg end)
          (insert "#+begin_src " lang "\n"
                  (string-trim code "\n" "\n")
                  "\n#+end_src"))))))

;;;###autoload
(defun k/pollen-frontmatter-to-org ()
  "Convert Pollen ◊define-meta[foo] statements to Org #+foo keywords."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx "◊define-meta[" (group (*? any)) "]"
                  "{" (group (*? any)) "}")
              nil t)
        (replace-match "#+\\1: \\2")))))

;;;###autoload
(defun k/markdown-link-to-org ()
  "Convert all markdown links in current buffer to Org links."
  (interactive)
  (save-match-data
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward
              (rx (not (any "[" "]"))
                  "[" (group (not (any "[")) (*? any)) "]"
                  "(" (group (*? any)) ")")
              nil t)
        (replace-match " [[\\2][\\1]]")))))

;;;###autoload
(defun k/add-current-path-to-load-path ()
  "Add current buffer's directory to `load-path'."
  (interactive)
  (-some->> (or (buffer-file-name) default-directory)
    file-name-directory
    (add-to-list 'load-path)))

(defun k/eww-termux-share ()
  "Share current eww URL through Termux."
  (interactive)
  (when (bound-and-true-p eww-data)
    (shell-command-to-string
     (format "printf '%s' | termux-share -a send"
             (plist-get eww-data :url)))))

(defun k/qr-encode (str &optional buf)
  "Encode STR as a QR code.

Return a new buffer or BUF with the code in it."
  (interactive "MString to encode: ")
  (let ((buffer (get-buffer-create (or buf "*QR Code*")))
        (format (if (display-graphic-p) "PNG" "UTF8"))
        (inhibit-read-only t))
    (with-current-buffer buffer
      (delete-region (point-min) (point-max)))
    (make-process
     :name "qrencode" :buffer buffer
     :command `("qrencode" ,str "-t" ,format "-o" "-")
     :coding 'no-conversion
     ;; seems only the filter function is able to move point to top
     :filter (lambda (process string)
               (with-current-buffer (process-buffer process)
                 (insert string)
                 (goto-char (point-min))
                 (set-marker (process-mark process) (point))))
     :sentinel (lambda (process change)
                 (when (string= change "finished\n")
                   (with-current-buffer (process-buffer process)
                     (cond ((string= format "PNG")
                            (image-mode)
                            (image-transform-fit-to-height))
                           (t ;(string= format "UTF8")
                            (text-mode)
                            (decode-coding-region (point-min) (point-max) 'utf-8)))))))
    (when (called-interactively-p 'interactive)
      (display-buffer buffer))
    buffer))

(defun k/qr-encode-buffer (in &optional out)
  "Encode buffer IN contents as a QR code.

Return a new buffer or OUT with the code in it."
  (with-current-buffer in
    (k/qr-encode (buffer-substring-no-properties (point-min) (point-max)) out)))

(defun k/reset-function (func)
  "Reset FUNC, removing all its advices.

Nullify the function definition of FUNC, then reload its source
file."
  (let ((file (file-name-base (symbol-file func))))
    (setf (symbol-function func) nil)
    (load file)))

;; modified from https://emacs.stackexchange.com/questions/17417
(defun stackexchange/eww-save-image (filename)
  "Save an image opened in an *eww* buffer to FILENAME."
  (interactive "G")
  (let ((image (get-text-property (point-min) 'display)))
    (f-write-bytes
     (plist-get (if (eq (car image) 'image) (cdr image)) :data)
     filename)))

;;;###autoload
(k/defun-string-or-region k/decode-url ()
  "Decode STRING as a url."
  (org-link-decode string))

;;;###autoload
(k/defun-string-or-region k/encode-url ()
  "Encode STRING as a url."
  (url-encode-url string))

(declare-function magit-commit-at-point "magit-git")
(declare-function magit-rev-insert-format "magit-git")

;;;###autoload
(defun k/magit-copy-commit-message ()
  "Copy the commit message of the commit at point."
  (interactive)
  (require 'magit)
  (when-let ((rev (magit-commit-at-point)))
    (with-temp-buffer
      (magit-rev-insert-format "%B" rev)
      (kill-new (buffer-string)))
    (message "Copied the commit message of %s" rev)))

;;;###autoload
(defun k/git-https-to-ssh ()
  "Convert https git link at point to ssh."
  (interactive)
  (-when-let* ((bounds (bounds-of-thing-at-point 'url))
               (text (buffer-substring-no-properties (car bounds)
                                                     (cdr bounds)))
               (parsed (url-generic-parse-url text)))
    (when (member (url-type parsed)
                  (list "http" "https"))
      (k/set-buffer-substring (car bounds) (cdr bounds)
        (format "git@%s:%s"
                (url-host parsed)
                (substring (url-filename parsed) 1))))))

(define-minor-mode k/hide-cursor-mode
  "Minor mode to hide cursor."
  :global t
  (defun k/hide-cursor ()
    "Set cursor type to nil."
    (setq cursor-type nil))
  (if k/hide-cursor-mode
      (add-hook 'post-command-hook #'k/hide-cursor)
    (remove-hook 'post-command-hook #'k/hide-cursor)))

(defun k/show-string-in-buffer (str)
  "Show STR in a new buffer.

Pop to the new buffer with `helpful-switch-buffer-function'."
  (let ((buf (get-buffer-create "*string*")))
    (with-current-buffer buf
      (erase-buffer)
      (insert str)
      (goto-char (point-min)))
    (display-buffer buf)))

;;;###autoload
(defun k/magit-log-visit-changed-file ()
  "Visit a changed file of revision under point in `magit-log-mode'.

Uses `general-simulate-key', so `general-simulate-RET' will
become defined after invocation."
  (interactive)
  (general-simulate-key "RET")
  ;; visit the commit
  (general-simulate-RET)
  ;; move to first changed file
  (goto-char (point-min))
  (search-forward " | " nil t)
  ;; open the revision
  (general-simulate-RET))

(defun k/visit-init-file ()
  "Visit `user-init-file'."
  (interactive)
  (find-file user-init-file))

;;;###autoload
(defun k/pop-to-buffer (buffer-or-name)
  "Display buffer specified by BUFFER-OR-NAME and select its window.

Display BUFFER-OR-NAME with `pop-to-buffer' in file-visiting
buffers, and display it in the current window in
non-file-visiting buffers."
  (interactive (list (read-buffer "Pop to buffer: " (other-buffer))
                     (if current-prefix-arg t)))
  (funcall
   (if (buffer-file-name)
       #'pop-to-buffer
     #'pop-to-buffer-same-window)
   buffer-or-name))

(defun k/async-shell-command (command &optional buf)
  "Start COMMAND in a subprocess.

If BUF is non-nil, associate it with the subprocess."
  (interactive
   (list
    (read-shell-command "Async shell command: " nil nil
                        (let ((filename
                               (cond
                                (buffer-file-name)
                                ((eq major-mode 'dired-mode)
                                 (dired-get-filename nil t)))))
                          (and filename (file-relative-name filename))))))
  (start-process-shell-command "k/async-shell-command" buf command))

(defun k/echo-cangjie-if-han (&optional _dummy)
  "`message' out Cangjie encoding for character at point if it's a Han character."
  (interactive)
  (when-let ((cangjie (ignore-errors (cangjie-at-point))))
    (message "%s" cangjie)))
(define-minor-mode k/show-cangjie-mode
  "Minor mode to show Cangjie code in mode line."
  :global t
  (if k/show-cangjie-mode
      (add-hook 'post-command-hook #'k/echo-cangjie-if-han)
    (remove-hook 'post-command-hook #'k/echo-cangjie-if-han)))

(defun k/start-timer (seconds)
  "Ding after SECONDS.

Interactively, the prompt is evaluated so that \(* 60 30) can
mean 30 minutes."
  (interactive "XHow long? (seconds; evaluated): ")
  (start-process-shell-command
   "ding" nil
   (concat (format "sleep %s;" seconds)
           "play /usr/share/sounds/freedesktop/stereo/complete.oga")))

(defun k/delete-this-file (&optional trash)
  "Delete this file.

When called interactively, TRASH is t if no prefix argument is given.
With a prefix argument, TRASH is nil."
  (interactive)
  (when (and (called-interactively-p 'interactive)
             (not current-prefix-arg))
    (setq trash t))
  (if-let ((file (buffer-file-name)))
      (when (y-or-n-p "Delete this file? ")
        (delete-file file trash)
        (kill-buffer (current-buffer)))
    (user-error "Current buffer is not visiting a file")))

;;;###autoload
(defun k/git-commit (&optional message callback)
  "Commit whatever's staged with MESSAGE.

If MESSAGE is nil or empty (only whitespace), a generic message
is used.

Run CALLBACK with the process object when done."
  (interactive)
  (unless (and (stringp message)
               (not (string= "" (s-trim message))))
    (if-let ((name (buffer-file-name)))
        (setq message (format "update %s" (file-name-base name)))
      (setq message "update")))
  (async-start-process
   "git commit" "git" callback
   "commit" "-m" message))

(defun k/git-reset (&optional callback)
  "Run \"git reset HEAD\" asynchronously.

Run CALLBACK with the process object when done."
  (async-start-process
   "git reset" "git" callback
   "reset" "HEAD" "--"))

(defun k/git-add (files &optional callback)
  "Run \"git add -- FILES\" asynchronously.

Run CALLBACK with the process object when done."
  (let ((files (if (listp files) files (list files))))
    (apply #'async-start-process
           "git add" "git" callback
           "add" "--" files)))

(defun k/git-stage-everything ()
  "Stage everything."
  (interactive)
  (let ((default-directory (projectile-project-root)))
    (call-process "git" nil nil nil "add" ".")))

;;;###autoload
(defun k/git-commit-everything ()
  "Stage everything then run `magit-commit'."
  (interactive)
  (require 'magit)
  (k/git-stage-everything)
  (magit-commit-create))

;;;###autoload
(defun k/git-commit-this-file-with-message (message &optional callback)
  "Stage and commit this file with a one-line MESSAGE.

A generic message is used if MESSAGE is empty (only whitespace);
see `k/git-commit'.

Run CALLBACK with the process object when done."
  (interactive "MMessage: ")
  (require 'magit)
  (when (magit-merge-in-progress-p)
    (error "Merge in progress"))
  (when-let ((file (file-relative-name (buffer-file-name))))
    (k/git-reset
     (lambda (_)
       (k/git-add
        file
        (lambda (_)
          (k/git-commit message callback)))))))

;;;###autoload
(defun k/git-commit-this-file ()
  "Stage and commit this file, using Magit's editor to edit the commit message."
  (interactive)
  (shell-command-to-string "git reset HEAD --")
  (shell-command-to-string
   (concat "git add -- " (file-relative-name (buffer-file-name))))
  ;; use Magit's editor
  (require 'magit)
  (magit-commit-create))

(defun k/git-push (&optional callback)
  "Run git push asynchronously.

Run CALLBACK with the process object when done."
  (async-start-process
   "git push" "git" callback "push"))

;;;###autoload
(defun k/git-commit-this-and-push (&optional message)
  "Commit this file to Git and push asynchronously.

MESSAGE is used as the commit message; if nil or empty, a generic
message is used."
  (k/git-commit-this-file-with-message
   message
   (lambda (_)
     (k/git-push))))

(cl-defun k/new-scratchpad (&optional
                            template
                            (new-file-dir k/cloud/scratchpad/))
  "Create a new file in a scratchpad from TEMPLATE.

Put the new file under NEW-FILE-DIR (default `k/cloud/scratchpad/').

The new file is named with current time and inherits the
extention of TEMPLATE, for example with a Krita template it looks
like this: \"20201201T000000+0900.kra\".

Interactively, ask to select a file under the \"templates\"
directory under `k/assets/' for TEMPLATE."
  (interactive
   (list nil k/cloud/scratchpad/))
  ;; We have to do this here as org-capture doesn't go through the
  ;; `interactive' form.
  (unless template
    (setq template
          (-some--> (f-join k/assets/ "templates")
            (directory-files it nil (rx bos (not ".")))
            (completing-read "Template: " it nil t)
            (f-expand it (f-join k/assets/ "templates")))))
  (let* ((now (current-time))
         (new-file (f-join new-file-dir
                           (format (format-time-string "%Y%m%dT%H%M%S%z.%%s" now)
                                   (f-ext template)))))
    ;; Create a note entry for the new file
    (minaduki/new-fleeting-note now)
    (insert (format "\n[[%s]]" (minaduki::apply-link-abbrev new-file)))
    ;; And copy the template over
    (copy-file template new-file)
    (browse-url (browse-url-file-url new-file))))

(defun k/open-this-file-with-nvim ()
  "Open current file with nvim."
  (interactive)
  (let ((file-name (buffer-file-name))
        (terminal (or (getenv "TERMINAL")
                      "konsole")))
    (unless file-name
      (error "Current buffer not visiting a file"))
    (start-process "nvim" nil
                   terminal "-e" "nvim" file-name)))

;;;###autoload
(defun k/open-this-path-with-terminal ()
  "Open current path in a new terminal."
  (interactive)
  (let ((path (f-expand default-directory)))
    ;; TODO: use system's default terminal instead
    (start-process "alacritty" nil "alacritty")))

;;;###autoload
(defun k/open-this-file-with-system ()
  "Open current file with system default."
  (interactive)
  (let ((path
         (or (buffer-file-name)
             (f-expand default-directory))))
    (browse-url path)))

(defalias 'k/open-externally #'k/open-this-file-with-system)

(defun k/open-current-file-as-sudo ()
  "Re-open current file with sudo.

From URL `https://www.reddit.com/r/emacs/comments/9sp7hh/show_me_your_functions/e8qibkq/'."
  (interactive)
  (when-let ((file-name (or (buffer-file-name)
                            (and (derived-mode-p 'dired-mode)
                                 default-directory))))
    (find-alternate-file (concat "/sudo::" file-name))))

;;;###autoload
(defun k/pollen-add-meta (meta)
  "Add a Pollen define-meta entry named META."
  (interactive "MMeta: ")
  (forward-line)
  (insert "◊define-meta[" meta "]{}\n")
  (backward-char 2))

(defun k/wikipedia-first-paragraph (url)
  "Get the first paragraph from the Wikipedia entry located at URL."
  (require 'eww)
  (require 'dom)
  (require 's)
  ;; document
  ;;  .querySelector(".mw-parser-output > p:not(.mw-empty-elt)")
  ;;  .innerText
  ;;  .replace(/\[\d\]/, "")
  (with-current-buffer (url-retrieve-synchronously url t)
    (eww-parse-headers) ; skip the headers
    (decode-coding-region (point) (point-max) 'utf-8)
    (let* (dom text p)
      (setq dom (libxml-parse-html-region (point) (point-max)))
      (setq p
            (->> (dom-search
                  dom
                  (lambda (elem)
                    (and (equal (dom-attr elem 'class) "mw-parser-output")
                         (not (dom-attr elem 'id))
                         (dom-by-tag elem 'p))))
                 dom-children
                 (-filter #'consp)
                 (--filter (eq 'p (dom-tag it)))
                 (--remove (dom-attr it 'class))
                 car))
      (setq text (dom-texts p ""))
      (->> text
           (s-replace-regexp (rx "[" (+? any) "]") "")
           s-trim))))

(provide 'kisaragi-extra-functions)
;;; kisaragi-extra-functions.el ends here

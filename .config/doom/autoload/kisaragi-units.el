;;; kisaragi-units.el --- integration with the units command  -*- lexical-binding: t; -*-
;;; Commentary:
;; Functions and commands that integrate with the `units' command.
;;; Code:

(require 'dash)
(require 's)

(defvar k/units--from-history nil "Minibuffer history for first argument of `k/units'.")
(defvar k/units--to-history nil "Minibuffer history for second argument of `k/units'.")

;;;###autoload
(defun k/units (from to)
  "Interface for the Unix `units' command.
FROM and TO are passed directly to the command as the first and
second arguments."
  (interactive
   (list
    (read-string "(units) Convert from: " nil 'k/units--from-history)
    (read-string "(units) to: " (car k/units--to-history) 'k/units--to-history)))
  (let ((from (format "%s" from))
        (to (format "%s" to)))
    (with-temp-buffer
      (call-process "units" nil '(t nil) nil
                    from to)
      (--> (buffer-string)
        (s-replace "\t" "" it)
        s-trim
        (message "%s" it)))))

;;;###autoload
(defun k/add-with-units (values target-unit)
  "Add VALUES together with the `units' command.

TARGET-UNIT is the unit the result should be in.

This can probably also be used for other purposes, not just
currency addition.

$1 in VALUES is assumed to be 1 TWD."
  (let ((inhibit-message t)
        ;; $1 = 1 TWD
        ;; Doesn't handle "-$1"
        (values (--map (if (s-matches? "^\\$" it)
                           (replace-regexp-in-string "^\\$\\(.*\\)" "\\1 TWD" it)
                         it)
                       values)))
    (--> (k/units
          (s-join " + " values)
          target-unit)
      (s-match "\\* \\(.*\\)$" it)
      cadr
      (format "%s %s" it target-unit))))

(provide 'kisaragi-units)

;;; kisaragi-units.el ends here

;;; k-tbx.el --- Functions for dealing with TBX glossaries -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(require 'xml)
(require 'outline)
(require 'dom)
(require 'shr)
(require 'sort)
(require 'kisaragi-helpers)

(defun k-tbx--next-entry (&optional stay)
  "Jump forward to the start of the next termEntry element.
If STAY is non-nil, stay put if we're already at the start of a
termEntry element.
Return nil if no element is found."
  (let ((tag-open-re (format "<termEntry\\( +%s=%s\\)*>"
                             xml-att-type-re
                             xml-att-value-re)))
    ;; Make sure we still move to the next one even if we're looking at an entry
    (when (and (not (eobp))
               (not stay)
               (looking-at tag-open-re))
      (forward-char 1))
    (when (re-search-forward tag-open-re nil t)
      (goto-char (match-beginning 0)))))

(defun k-tbx--map-entries (fun &optional beg end)
  "Call FUN for every entry between BEG and END.

FUN is called with point at the start of the entry, with two
arguments: the start of the entry and the end of the entry.

If BEG or END are nil, default to `point-min' or `point-max', respectively.

This is based off of the code for `org-map-region'."
  (unless beg
    (setq beg (point-min)))
  (unless end
    (setq end (point-max)))
  (save-excursion
    (setq end (copy-marker end))
    (goto-char beg)
    ;; Make sure we're at the first entry
    ;; Then call FUN for the first entry
    (when (and (k-tbx--next-entry t)
               (< (point) end))
      (funcall fun
               (point)
               (save-excursion
                 (re-search-forward "</termEntry>"))))
    (while (and
            ;; We also need this to return non-nil (not the last entry).
            (k-tbx--next-entry)
            (< (point) end)
            (not (eobp)))
      (funcall fun
               (point)
               (save-excursion
                 (re-search-forward "</termEntry>"))))))

(defun k-tbx--do-on-all-entries (transformer &optional sort)
  "Run TRANSFORMER for all entries.
TRANSFORMER receives one argument, the list of entries within <body>.
TRANSFORMER should return a list of entries. This list is written back
to the buffer as XML.
If SORT is non-nil, sort the entries by their first <term> before writing.
If SORT is `reverse', sort in the reversed direction."
  (save-excursion
    (goto-char (point-min))
    (let* ((body-start (save-excursion
                         (search-forward "<body>")
                         (match-beginning 0)))
           (body-end (save-excursion
                       (search-forward "</body>")
                       (match-end 0)))
           (entries
            ;; We can't use libxml-parse-xml-region because it will mangle
            ;; "xml:lang" tags to just "lang". Lokalize requires the former.
            (save-excursion
              (goto-char body-start)
              (dom-by-tag (xml-parse-tag) 'termEntry)))
           (entries (funcall transformer entries)))
      (delete-region body-start body-end)
      (goto-char body-start)
      (shr-dom-print
       `(body
         ()
         ,@(if sort
               (sort
                entries
                (lambda (a b)
                  (funcall
                   (if (eq sort 'reverse)
                       #'string>
                     #'string<)
                   (dom-text
                    (dom-by-tag a 'term))
                   (dom-text
                    (dom-by-tag b 'term)))))
             entries))))))

(defun k/tbx-delete-duplicate-entries ()
  "Delete duplicate entries in the current TBX buffer."
  (interactive)
  (let (before-count after-count)
    (k-tbx--do-on-all-entries
     (lambda (entries)
       (let ((table (make-hash-table :test #'equal)))
         (setq before-count (length entries))
         (dolist (entry entries)
           (puthash (dom-children entry)
                    entry
                    table))
         (setq after-count (length (hash-table-values table)))
         (hash-table-values table)))
     :sort)
    (unless (= before-count after-count)
      (message "Removed %s duplicate entries"
               (- before-count
                  after-count)))))

(defun k/tbx-reassign-ids ()
  "Reassign ids of entries in the current TBX buffer.

You might want to run a reformat afterwards. With Apheleia and
html-tidy, just run `apheleia-format-buffer'; with xmllint and
Evil, use the Ex command \":%!xmllint --format -\"."
  (interactive)
  (let ((i 0))
    (k-tbx--map-entries
     (lambda (start end)
       (let ((node (xml-parse-region start end))
             ;; Imitate Lokalize's id style
             (new-id (format "%s-%s"
                             (thread-last (downcase user-full-name)
                                          (replace-regexp-in-string " " "_"))
                             i)))
         (cl-incf i)
         (dom-set-attribute node 'id new-id)
         (delete-region start end)
         (goto-char start)
         (shr-dom-print node))))))

(defun k/tbx-sort-entries (&optional reverse)
  "Sort termEntry elements in the current TBX buffer.
If REVERSE, sort in the other direction instead."
  (interactive)
  (k-tbx--do-on-all-entries
   #'identity
   (if reverse
       'reverse
     t)))

(provide 'k-tbx)

;;; k-tbx.el ends here

;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; TODO: notes!

;; Difference between defining my own modules and writing them here: each module
;; can be scoped to a single topic containing its config, autoloads, and
;; packages, while without defining a standalone module the thing's config would
;; have to go into three files and mixed with everything else.

;; Load not preferring newer file makes elisp development extremely painful.
;; Just turn it back on after init is done.
(setq-hook! 'after-init-hook
  load-prefer-newer t)

;;;; Load theme early

;; Load theme early and restore redisplay and message
;;
;; My config loads in the order of seconds, so the subsecond startup time
;; benefit really isn't worth not being able to see what's happening.
;;
;; The theme has to be loaded before re-enabling redisplay to avoid showing the
;; default theme. The story is the same for font settings, but they're harder to
;; move to an early stage, so they can stay in after-init-hook.
;;
;; We also need to run load theme hooks (`doom-load-theme-hook') after init,
;; which contains stuff like making Evil cursors reflect the theme. Those hooks
;; are set up after this point, but since we've already loaded the theme early
;; they wouldn't get a chance to run if we don't do this.
(setq doom-theme
      ;; Dark theme on even months; doom-solarized-light as light theme on Windows.
      (cond ((cl-evenp (decoded-time-month (decode-time)))
             'doom-vibrant)
            ((eq system-type 'windows-nt)
             'doom-solarized-light)
            (t
             'doom-one-light))
      doom-modeline-unicode-fallback t)

(if (daemonp)
    ;; In daemon mode we just want redisplay to actually happen, giving up the
    ;; subsecond startup speed gains.
    (setq-default inhibit-redisplay nil
                  inhibit-message nil)
  (doom-init-theme-h) ; load `doom-theme' now
  ;; Now restore redisp/msg, normally suppressed by Doom until `after-init-hook'
  (setq-default inhibit-redisplay nil
                inhibit-message nil)
  ;; Don't load the theme or reset inhibit-{redisplay,message} a second time
  (remove-hook 'after-init-hook #'doom-init-theme-h)
  (with-no-warnings
    (remove-hook 'after-init-hook #'doom--reset-inhibited-vars-h))
  ;; But do run hooks after loading the theme after init, since that's when they
  ;; expect to be run
  (add-hook 'after-init-hook (fn! (run-hooks 'doom-load-theme-hook))))

;;;; Requires

(require 'kisaragi-paths)
(require 'kisaragi-helpers)
(require 'kisaragi-constants)

(require 'kisaragi-link)

(require 'f)

(require 'doom-lib)

(general-auto-unbind-keys)
(minibuffer-depth-indicate-mode)


;;;;; UI

;; Could be useful for the native Android build (not through Termux)
(prog1 'tool-bar
  (when (and (fboundp 'tool-bar-mode)
             ;; disable this for now
             nil)
    (tool-bar-mode)
    (when (fboundp 'modifier-bar-mode)
      (modifier-bar-mode))
    (setq! tool-bar-style 'both
           tool-bar-position 'top
           tool-bar-always-show-default t)))

(prog1 'dashboard
  (setq +doom-dashboard-pwd-policy k/notes/)
  (setq +doom-dashboard-functions
        '(doom-dashboard-widget-shortmenu
          doom-dashboard-widget-loaded))
  (setq +doom-dashboard-menu-sections
        '(("Recently used projects"
           :icon (nerd-icons-octicon "nf-oct-project" :face 'doom-dashboard-menu-title)
           :action projectile-switch-project)
          ("Recently opened files"
           :icon (nerd-icons-octicon "nf-oct-file" :face 'doom-dashboard-menu-title)
           :action recentf-open-files)
          ("Reload last session"
           :icon (nerd-icons-octicon "nf-oct-history" :face 'doom-dashboard-menu-title)
           :when (cond ((modulep! :ui workspaces)
                        (file-exists-p (expand-file-name persp-auto-save-fname persp-save-dir)))
                       ((require 'desktop nil t)
                        (file-exists-p (desktop-full-file-name))))
           :action doom/quickload-session)
          ("Open private configuration"
           :icon (nerd-icons-octicon "nf-oct-tools" :face 'doom-dashboard-menu-title)
           :when (file-directory-p doom-user-dir)
           :action doom/open-private-config)
          ("Open documentation"
           :icon (nerd-icons-octicon "nf-oct-book" :face 'doom-dashboard-menu-title)
           :action doom/help))))

;; I do want GUI tooltips, not for them to show in the echo area
(when (fboundp #'tooltip-mode)
  (tooltip-mode))

(setq!
 +snippets-dir (f-join k-old-emacs-dir "snippets")
 read-minibuffer-restore-windows nil
 suggest-key-bindings nil
 x-stretch-cursor t
 message-log-max 10000)

(setq user-full-name "Kisaragi Hiu"
      user-mail-address "mail@kisaragi-hiu.com"
      js-doc-author user-full-name)
(setf (map-elt fringe-indicator-alist 'continuation) nil
      (map-elt default-frame-alist 'auto-raise) t)

(setq transient-display-buffer-action
      '(display-buffer-in-side-window (side . bottom)))

(setq display-line-numbers-type t
      display-line-numbers-width 4)

;;;;; Startup
(k/once-after-make-frame
  ;; HACK: Close the loud warning window for pgdk.
  ;; https://kisaragi-hiu.com/emacs-29-pgtk-woes/
  ;;
  ;; That I have to rely on X11 automation is frustrating.
  (when (and (display-graphic-p)
             (executable-find "xdotool"))
    (when-let (window-id (ignore-errors
                           (k/call-process-to-string
                             "xdotool" "search"
                             "--all"
                             "--pid" (format "%s" (emacs-pid))
                             "--name" "^Warning$")))
      (doom-call-process "xdotool" "windowclose" window-id))))

;;;;; Fonts
(prog1 'fonts
  ;; This is necessary as our doom-font setting block needs to be run after
  ;; making a frame, due to `k/first-available-font' not working before a frame
  ;; is created
  (defun k--set-font-a (&rest _)
    "Set fonts for `doom-init-fonts-h'."
    (setq k/font-mono (k/first-available-font
                       '("Iosevka" "Noto Mono"))
          k/font-serif (k/first-available-font
                        '("Equity OT" "Noto Serif"))
          k/font-sans (k/first-available-font
                       '("Inter" "Fira Sans" "Source Sans Pro" "Noto Sans")))
    (setq! doom-font (font-spec :family k/font-mono
                                :size (if (k/system-is-p "MF-manjaro")
                                          28
                                        20))
           doom-variable-pitch-font (font-spec :family k/font-sans)
           doom-serif-font (font-spec :family k/font-serif)))
  (advice-add 'doom-init-fonts-h :before #'k--set-font-a)
  (add-hook! 'after-setting-font-hook
    (fn!
     (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
     (set-face-attribute 'line-number-current-line nil :inherit '(hl-line fixed-pitch))))
  (defun k--set-cjk-font-fallback-h ()
    "Set CJK font fallback to use Noto Sans CJK."
    (dolist (charset '(han hangul kana bopomofo cjk-misc))
      (ignore-errors
        (set-fontset-font (face-attribute 'default :fontset)
                          charset
                          (k/first-available-font
                           '("Noto Sans Mono CJK JP"
                             "Noto Sans CJK JP"))))))
  (add-hook! '(server-after-make-frame-hook
               doom-after-init-hook
               after-setting-font-hook)
    #'k--set-cjk-font-fallback-h))

;;;;; Faces
(--each `(markup-anchor-face
          markup-attribute-face
          markup-command-face
          markup-comment-face
          markup-complex-replacement-face
          markup-internal-reference-face
          (markup-list-face :foreground unspecified
                            :background unspecified)
          (markup-meta-face :height 1.0)
          (markup-meta-hide-face :height 1.0)
          markup-preprocessor-face
          (markup-table-cell-face :background unspecified
                                  :foreground unspecified
                                  :inherit org-table)
          (markup-table-face :background unspecified
                             :foreground unspecified
                             :inherit org-table)
          (markup-table-row-face :background unspecified
                                 :foreground unspecified
                                 :inherit org-table)
          markup-value-face

          (markup-gen-face :foreground unspecified
                           :inherit outline-1)
          markup-big-face
          markup-bold-face
          markup-code-face
          markup-emphasis-face
          markup-gen-face
          markup-italic-face
          markup-reference-face
          (markup-secondary-text-face :height 1.0)
          markup-small-face
          markup-strong-face
          markup-subscript-face
          markup-superscript-face
          (markup-title-0-face :height 1.5
                               :foreground ,(face-attribute 'org-document-title :foreground))
          (markup-title-1-face :height 1.5)
          (markup-title-2-face :height 1.0)
          (markup-title-3-face :height 1.0)
          (markup-title-4-face :height 1.0)
          (markup-title-5-face :height 1.0)
          markup-typewriter-face
          markup-underline-face
          markup-verbatim-face)
  (when (consp it)
    (custom-set-faces! it)))
(custom-set-faces!
  '(org-headline-done
    :background unspecified
    :foreground unspecified
    :strike-through t
    :underline unspecified)
  ;; verse and quote blocks should be shown like text, not code.
  '(org-verse
    :inherit unspecified
    :slant unspecified
    :extend t
    :slant unspecified)
  '(org-quote
    :inherit unspecified
    :slant unspecified
    :extend t
    :slant unspecified))
(custom-set-faces!
  `(flyspell-incorrect :underline (:style line :color ,(doom-color 'error)))
  `(flyspell-duplicate :underline (:style line :color ,(doom-color 'warning))))

;; TODO
;; (use-package! k-kde-window-colors)

(use-package! page-break-lines
  :hook (doom-first-input . global-page-break-lines-mode)
  :init (global-page-break-lines-mode)
  :config (k/add! page-break-lines-modes
            'emacs-news-view-mode
            'emacs-news-mode))

(prog1 'display-buffer
  (setq display-buffer-alist
        `(("\\*Async Shell Command\\*.*" . (display-buffer-no-window))
          ("\\*cider-error\\*" . (display-buffer-no-window))
          ("\\*format-all-errors\\*" . (display-buffer-no-window))
          ("\\*apheleia-.*-log\\*" . (display-buffer-no-window))
          ("*info*" . (display-buffer-in-new-tab))
          ;; Switch to the window automatically after running rg or ag
          ;; The action alist entry `body-function' was added in Emacs 28
          ("\\*[ra]g.*\\*" . (nil . ((body-function . select-window))))
          ("\\*eldoc\\*" . (nil . ((body-function . select-window)))))))

;;;;; TODO Prose editing, Org and Markdown

(use-package! markdown-mode
  :mode ("\\.mdx\\'")
  :config
  (setq! markdown-hide-markup t
         markdown-enable-wiki-links t
         markdown-max-image-size '(600 . 400)
         markdown-list-item-bullets '("-" "◦"))
  (add-hook 'markdown-mode-hook #'variable-pitch-mode)
  (general-def
    :states 'normal
    :keymaps 'markdown-mode-map
    "<tab>" 'markdown-cycle
    "TAB" 'markdown-cycle
    "RET" #'markdown-follow-thing-at-point
    "M-p" #'k/markdown-toggle-hiding-stuff)
  (setq-hook! 'markdown-mode-hook
    display-line-numbers nil
    fill-column 120))

(use-package org-import-simplenote
  :after org
  :config
  (setq org-import-simplenote-title-format 'both))

(use-package! org
  :defer t
  :init
  (setq evil-org-key-theme
        '(navigation insert textobjects additional calendar todo return))
  (after! evil-org
    (evil-org-set-key-theme))
  (add-hook 'org-mode-hook #'kisaragi-timestamp-highlight-mode)
  (setq-hook! 'org-mode-hook
    show-trailing-whitespace t
    display-line-numbers nil
    fill-column 90
    ;; I want to keep the effects of `visual-line-mode' which Org starts by
    ;; default, but I don't want the variable to be t.
    ;;
    ;; That forces `org-beginning-of-line' to not respect `visual-line-mode'.
    ;; This is more consistent with Evil when `evil-respect-visual-line-mode' is
    ;; nil.
    ;;
    ;; (When `evil-respect-visual-line-mode' is nil and "0" ends up running
    ;; `org-beginning-of-line' because of evil-org, we'll end up unable to move
    ;; to the start of line because the latter refuses to move to the real start
    ;; of line but j/k moves to the next/previous real line.
    visual-line-mode nil)
  (setq org-directory k/notes/
        org-log-into-drawer t
        org-fold-core-style 'overlays
        ;; Don't deny my explicit command
        ;; What should actually happen is show-all skipping archived
        ;; trees. But this is just the way it is.
        org-cycle-open-archived-trees t
        org-startup-indented t
        org-startup-truncated nil
        org-agenda-use-tag-inheritance nil
        org-fold-catch-invisible-edits 'show
        org-cycle-separator-lines 0
        org-cycle-emulate-tab nil
        org-refile-targets '((nil . (:maxlevel . 2))
                             (org-agenda-files . (:maxlevel . 3)))
        org-refile-use-outline-path nil
        org-ellipsis "..."
        org-pretty-entities t
        org-priority-highest ?A
        org-priority-lowest ?F
        ;; show me all the tags
        org-complete-tags-always-offer-all-agenda-tags t
        org-hide-emphasis-markers t
        org-return-follows-link t
        org-babel-default-header-args:emacs-lisp '((:lexical . "yes"))
        org-src-fontify-natively t
        ;; stop throwing extra indentation into source code
        org-edit-src-content-indentation 0
        ;; Use a sensible display size; otherwise the image will push
        ;; everything else away from the screen. We can always just
        ;; visit the link itself to view the full size image.
        ;;
        ;; Putting it in a list allows for #+attr_org: :width overrides.
        org-image-actual-width '(400)
        org-imenu-depth 6
        ;; consider time before 01:00 to be yesterday
        org-extend-today-until 1)

  :config
  (setq evil-org-special-o/O '(table-row item))
  ;; allow fuzzy search with file:filename.org::text
  (setq org-link-search-must-match-exact-headline nil)
  (org-link-set-parameters "eww" :follow #'eww-browse-url)
  (org-link-set-parameters "chromium" :follow #'browse-url-chromium)
  (advice-add 'org-element-link-parser :around #'k/a/org-alternate-path-element-wrapper)
  ;; TODO: I want to replace this with Minaduki's template support
  (setq org-capture-templates
        `(("i" "Inbox" entry (file "Inbox.org")
           "* %?\n:PROPERTIES:\n:created:  %(k/date-iso8601)\n:END:\n"
           :prepend t)
          ("p" "Public")
          ("pd" "Blog entry (public diary)" plain
           (function
            ,(lambda ()
               (let* ((base (read-string "Base name for new public diary entry: "))
                      (today (k/today))
                      (now (k/date-iso8601))
                      (file (f-join k/notes/ "public" "content" "blog"
                                    (concat today "-" base ".org"))))
                 (find-file file)
                 (when (= 0 (buffer-size))
                   (insert "#+title: " base "\n"
                           "#+created: " now "\n"
                           "#+language: en\n"
                           "#+tags[]:")))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("pc" "Public concept note" plain
           (function
            ,(lambda ()
               (minaduki/new-concept-note
                :title (read-string "New public concept note: ")
                :dir (f-join k/notes/ "public" "content"))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("t" "Diary entry" plain
           #'minaduki/new-fleeting-note
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("d" "Diary entry (day)" plain
           ;; Abusing the #' reader syntax
           #'minaduki/new-daily-note
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("j" "Code" plain
           (function ,(lambda ()
                        (minaduki/new-fleeting-note
                         nil
                         (f-join org-directory "code"))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("f" "Fiction entry" plain
           (function ,(lambda ()
                        (minaduki/new-fleeting-note
                         nil
                         (f-join org-directory "fiction"))))
           ""
           :immediate-finish t
           :jump-to-captured t)
          ("c" "Stray concept note" plain
           #'minaduki/new-concept-note
           ""
           :immediate-finish t
           :jump-to-captured t)
          ,@(unless k/android?
              '(("s" "Scratchpad entry" plain
                 #'k/new-scratchpad ""
                 :immediate-finish t
                 :jump-to-captured t)))
          ("P" "Practice log"
           entry
           (file "trumpet-practice-log.org")

           "* %(k/date-iso8601)\n\n%?\n"
           :prepend t
           :empty-lines-before 1
           :immediate-finish t
           :jump-to-captured t)
          ("v" "Vocabulary")
          ("vj" "Japanese" entry (file+olp "vocab/vocab.org" "Japanese")
           "* %?\n"
           :prepend t)
          ("vd" "German" entry (file+olp "vocab/german.org" "German")
           "* %?\n"
           :prepend t)
          ("ve" "English" entry (file+olp "vocab/english.org" "English")
           "* %?\n"
           :prepend t)))
  (k/append-unique! org-src-lang-modes
    '(("go-html-template" . web)
      ("go-text-template" . text)
      ("json" . js-json)
      ("robots.txt" . conf-colon)
      ("tree" . text)))
  ;; TODO: consider moving to Doom Emacs's keywords:
  ;; - There's the [ ] -> [-] -> [?] -> [X] set, for todo, wip, wait, and done
  ;; - There's also an OKAY/YES/NO set, which sounds useful
  ;; - remove HABIT as we don't use org-habit anymore
  ;; - WAITING -> WAIT/HOLD
  ;; - WISH/MAYBE -> IDEA
  ;; - CANCELLED -> KILL
  (k/add! org-todo-keywords
    ;; "TODO": generic todo item
    ;; "WAITING": blocked
    ;; "NEXT": asap
    ;; "MAYBE": I want to do it eventually, but not now
    ;; "WISH": optional
    ;; "WIP": in-progress projects
    ;; "HABIT": highlight this heading is a habit entry
    '(sequence
      "TODO" "WAITING" "NEXT" "MAYBE" "WISH" "WIP" "HABIT" "|"
      "CANCELLED(c@)" "DONE(d)"))
  (k/append-unique! org-todo-keyword-faces
    '(("TODO" . org-todo)
      ("NEXT" . org-todo)
      ("WAITING" . org-todo)
      ("DONE" . org-done)
      ("CANCELLED" . org-done)))
  (k/add! org-file-apps
    (cons (rx "." (or "mkv" "mp4" "ogv") eos)
          "mpv %s")
    (cons (image-file-name-regexp)
          'default)
    (cons (rx "." (or "kra"))
          "xdg-open %s"))
  (when (executable-find "evince")
    (k/add! org-file-apps
      (cons (rx "." (or "pdf") "::" (group (+ (any "0-9"))) eos)
            "evince -p %1 %s")))
  (autoload #'k/insert-link/double-key "kisaragi-link")
  (map! (:after org-colview :map 'org-columns-map
                "i" #'org-columns-edit-value)
        (:map evil-org-mode-map
         :i "[" #'k/insert-link/double-key
         :i "RET" #'k/evil-org-insert-newline
         :n "<f5>" #'org-ctrl-c-ctrl-c
         :n "zr" nil)
        (:map org-mode-map
         :localleader
         "A" #'k-org:set-heading-added
         "C" #'k-org:set-heading-created
         "T" #'minaduki-org-set-heading-tags)
        (:map org-mode-map
              "S-<return>" #'k-org:insert-subheading-or-table-copy-down
              "S-RET" #'k-org:insert-subheading-or-table-copy-down
              "M-p" #'k/org-toggle-hiding-stuff
              "M-o" #'org-mark-ring-goto)))

(after! thingatpt
  (k/add! thing-at-point-uri-schemes
    "org-protocol://"))

(use-package! org-modern
  :hook (org-mode . org-modern-mode)
  :config
  (setq! org-modern-keyword nil
         org-modern-table nil
         org-modern-timestamp nil
         org-modern-todo nil
         ;; We can't just make it infinite as org-modern loops through
         ;; this list.
         org-modern-star '("◉" "○" "◈" "◇" "◉" "○" "◈" "◇"))
  (when k/android?
    (setq! org-modern-checkbox nil)))

(use-package! org-edit-indirect
  :hook (org-mode . org-edit-indirect-mode))

(use-package! org-variable-pitch
  :hook ((org-mode . org-variable-pitch-minor-mode)
         (markdown-mode . org-variable-pitch-minor-mode))
  :config
  (k/append-unique! org-variable-pitch-fixed-faces
    '(org-column-title
      markdown-list-face
      markdown-pre-face
      markdown-code-face))
  (defun k/refresh-ovp-faces ()
    "Set face settings for org-variable-pitch."
    (set-face-attribute 'org-variable-pitch-fixed-face nil
                        ;; Make sure we follow the mono CJK fallback
                        :fontset (face-attribute 'default :fontset)
                        :family k/font-mono))
  (add-hook 'doom-load-theme-hook #'k/refresh-ovp-faces))

(use-package! minaduki
  :after org
  :init
  (setq minaduki-mode:command-prefix "C-c"
        minaduki:index-file "readme.md")
  (map! (:map (org-mode-map markdown-mode-map)
         :v "RET" #'minaduki-insert
         :v "<return>" #'minaduki-insert
         :localleader
         "f" #'minaduki-open
         "if" #'minaduki-insert)
        :map org-mode-map
        :localleader
        :desc "more minaduki commands" "r" nil
        :desc "Tags"                   "rt" nil
        :desc "Add tag..."             "rta" #'minaduki-add-tag
        :desc "Delete tag..."          "rtd" #'minaduki-delete-tag
        :desc "Aliases"                "ra" nil
        :desc "Add alias..."           "raa" #'minaduki-add-alias
        :desc "Delete alias..."        "rad" #'minaduki-delete-alias)
  (map!
   "C-c N" #'minaduki:global-commands
   "C-c n" #'minaduki:local-commands
   "C-c l" #'minaduki:toggle-sidebar)
  :config
  (minaduki-mode)
  (add-hook 'minaduki-buffer/after-insert-hook (fn! (org-content 2)))
  (setq minaduki/literature-notes-directory k/literature-notes-directory
        minaduki:link-insertion-format 'absolute-in-vault
        orb-templates
        '(("r" "ref" plain
           #'minaduki-capture//get-point ""
           :file-name "reflection/${slug}"
           :head "#+TITLE: ${author} - ${title}\n#+key: ${ref}\n#+key: ${url}"
           :unnarrowed t))
        minaduki-buffer/hidden-tags '("reflection" "diary")
        minaduki-db/gc-threshold (* 100 1000 1000)
        minaduki/diary-directory (f-join k/notes/ "diary")
        minaduki/templates-directory (f-join k/notes/ "templates")
        minaduki-vaults-file
        (expand-file-name "minaduki-vaults.json" doom-user-dir)
        minaduki-vaults-extra `((:name "cloud" :path ,k/cloud/ :skipped t)
                                (:name "notes"
                                 :path ,k/notes/
                                 :skipped nil
                                 :main t)
                                (:name "public"
                                 :path ,(-some-> k/notes/
                                          (f-join "public" "content"))
                                 :skipped t))
        minaduki-lit/key-prop "bibtex_id"
        minaduki-lit/bibliography
        (--map
         (f-join k/notes/ it)
         '("anime.org"
           "apps.org"
           "articles.org"
           "books.org"
           "bookmarks.json"
           "contacts.org"
           "games.org"
           "music.org"
           "nkust.org"
           "people.org"
           "tweets.org"
           "videos.org"))
        minaduki-buffer/position (if k/android? 'bottom 'right)
        minaduki-tag-sources '(org-prop first-directory nested-vault)
        minaduki/slug-replacements '(("[^[:alnum:][:digit:]]" . "-")
                                     ("--*" . "-")
                                     ("^-" . "")
                                     ("-$" . ""))
        minaduki-db/update-method 'immediate
        ;; I don't want a date in most notes; I'll use
        ;; `k/diary-new-entry' if I want it.
        minaduki-capture/templates
        '(("d" "default" plain #'minaduki-capture//get-point "%?"
           :file-name "${slug}"
           :head "#+title: ${title}\n"
           :unnarrowed t))
        minaduki-file-exclude-regexp
        (rx (or "task-archive.org"
                "node_modules"
                (regexp
                 (f-join "obsidian-docs"
                         (rx (or "da"
                                 "es"
                                 "fr"
                                 "it"
                                 "ru"
                                 "vi"
                                 "zh"))))
                (regexp
                 (f-join "templates"
                         ".*\\.org"))))))

;; TODO: this should be integrated into Minaduki
(use-package! kisaragi-file-finders
  :after org
  :config
  (setq org-link-abbrev-alist
        (-filter
         #'cdr
         `(("notes" . ,k/notes/)
           ("people" . ,(f-slash
                         (f-join k/notes/ "people")))
           ("school" . ,k/school/)
           ("git" . ,(f-slash (f-join "~/" "git")))
           ("documents" . ,(-some-> k/cloud/
                             (f-join "documents")
                             f-slash))
           ;; do we need this to be absolute?
           ("projects" . ,(->> (f-join k/notes/ "projects.org")
                               (format "file:%s::*")))
           ("project" . k/org-link-abbrev-project)
           ("scratchpad" . ,k/cloud/scratchpad/)
           ("uni-conversation-class-journal" .
            "~/git/uni-conversation-class-journal/")
           ("ledger" . ,k/ledger/)
           ("fiction" . ,(f-join k/notes/ "fiction/"))
           ("mandarin" . ,(->> (f-join k/notes/ "vocab/vocab.org")
                               (format "file:%s::*")))
           ("japanese" . ,(->> (f-join k/notes/ "vocab/vocab.org")
                               (format "file:%s::*")))
           ("english" . ,(->> (f-join k/notes/ "vocab/english.org")
                              (format "file:%s::*")))
           ("poems" . ,(f-join k/notes/ "poems/"))
           ("books" . ,k/books/)
           ("photo" . k/org-link-abbrev-photo)
           ("recording" . k/org-link-abbrev-recording)
           ("reference" . k/org-link-abbrev-reference)
           ("wikipedia-en" . "eww:https://en.wikipedia.org/wiki/")
           ("wikipedia-zh" . "eww:https://zh.wikipedia.org/wiki/")
           ("wikipedia-ja" . "eww:https://ja.wikipedia.org/wiki/")))))

(use-package! typo
  :hook ((markdown-mode . typo-mode)
         (org-mode . typo-mode))
  :after elisp-mode
  :init
  ;; This will require typo right after elisp-mode loads, to get the indentation
  ;; definition for `define-typo-cycle'.
  (require 'typo)
  :config
  (define-typo-cycle k/typo-cycle-$
    "My cycle for \"$\"."
    ("$" "§"))
  (define-typo-cycle k/typo-cycle->
    "My cycle for \">\"."
    (">" "→"))
  (define-typo-cycle k/typo-cycle-<
    "My cycle for \"<\"."
    ("<" "←"))
  (define-typo-cycle k/typo-cycle-right-single
    "My cycle for \"'\"."
    ("'" "’"
     ;; prime and double prime, used for foot and inch marks
     ;; you can also emulate them with italicized straight quotes
     "′" "″"))
  (define-typo-cycle k/typo-cycle-left-single
    "My cycle for \"`\"."
    ("`" "‘"))
  (general-def
    :keymaps 'typo-mode-map
    "'" #'k/typo-cycle-right-single
    "`" #'k/typo-cycle-left-single
    ">" #'k/typo-cycle->
    "<" #'k/typo-cycle-<
    "$" #'k/typo-cycle-$))

(use-package! valign
  :hook ((org-mode . valign-mode)
         (markdown-mode . valign-mode)))

;; Currently this package does its job when loaded, not in a minor mode. It
;; should do the modification in a minor mode instead.
(use-package! org-inline-video-thumbnails
  :after org)

;; Fixes

(define-advice org-open-file (:around (func &rest args))
  "Use pipes `process-connection-type' so that xdg-open actually works.

See https://askubuntu.com/questions/646631/emacs-doesnot-work-with-xdg-open"
  (let ((process-connection-type nil))
    (apply func args)))

;;;;; Input

;; This would ensure all keypresses get intercepted by the IME,
;; but shift doesn't go to the IME for some reason.
;; It's not usable as a result, unfortunately.
;; (setq x-gtk-use-native-input t)
(after! quail
  ;; Also: type "qq" to toggle 英数
  (setq quail-japanese-use-double-n t))

;;;;; Minor modes

(after! time
  (setq display-time-24hr-format t
        display-time-default-load-average nil))

(use-package! link-hint
  :defer t
  :init
  (map! :leader
        :desc "Open a link" "oo" #'link-hint-open-link
        :desc "Copy a link" "oc" #'link-hint-copy-link)
  (map! :n "f" #'link-hint-open-link)
  (setq avy-all-windows t)
  :config
  (defun k/link-hint-process-text-url (url &rest _)
    "Process text URL before opening it."
    (let ((url (link-hint--process-url url nil)))
      (save-match-data
        (when (string-match "svn://anonsvn.kde.org.*@\\(.*\\)" url)
          (k--update url
            (format "https://websvn.kde.org/?view=revision&revision=%s"
                    (match-string 1 it))))
        url)))
  (put 'link-hint-text-url :parse #'k/link-hint-process-text-url))

(use-package! dumb-jump
  :defer t
  ;; This might not be necessary as Doom Emacs's lookup tries both dumb-jump and xref
  :config (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package! macrostep
  :defer t
  :config
  (general-def
    :keymaps 'macrostep-mode-map
    :states 'normal
    "c" #'macrostep-collapse))

(use-package! goto-addr
  :defer t
  :init
  (k/add-hook-not-derived '(text-mode eshell-mode)
    #'goto-address-mode)
  (k/add-hook 'prog-mode-hook #'goto-address-prog-mode)
  :config
  (map! :map goto-address-highlight-keymap
        "RET" #'goto-address-at-point))

(global-subword-mode)
(didyoumean-mode)
(global-auto-revert-mode)
(tab-bar-mode)

;;;;; Checkers

(use-package! flycheck
  :defer t
  :init
  (general-add-hook 'flycheck-mode-hook
                    (list #'sideline-mode
                          #'sideline-flycheck-setup))
  (setq sideline-backends-right '(sideline-flycheck))
  :config
  (setq flycheck-textlint-config "~/.config/textlint/textlintrc.json")
  (add-to-list 'flycheck-textlint-plugin-alist '(org-mode . "org")))

(use-package! flyspell
  :defer t
  :init
  ;; ispell.el relies on Hunspell to load a default and report it,
  ;; but Hunspell just errors out if it can't find a dictionary for
  ;; the system locale. And because ispell.el is trying to get a
  ;; default dictionary, it doesn't pass `ispell-dictionary' onto
  ;; Hunspell.
  ;;
  ;; The only way around this is to set the DICTIONARY environment
  ;; variable. It is supposed to do the same thing as the `-d'
  ;; option for Hunspell.
  ;;
  ;; It's ispell.el that needs to be fixed here: it should know that
  ;; `ispell-dictionary' is already the default and pass `-d' onto
  ;; Hunspell.
  (setenv "DICTIONARY" "en_US")
  (setq ispell-dictionary "en_US")
  :config
  (setq! flyspell-prog-text-faces '(font-lock-comment-face font-lock-doc-face)))

;;;;; Tools

(use-package! switch-to
  :commands (switch-to-vterm switch-to-eww switch-to-eshell)
  :init
  (map! :map doom-leader-open-map
        "t" #'switch-to-vterm
        "w" #'switch-to-eww
        "e" #'switch-to-eshell))

(use-package! persistent-scratch
  :defer t
  :init
  (defun k:switch-to-scratch ()
    "Switch to the *scratch* buffer."
    (interactive)
    (unless persistent-scratch-autosave-mode
      ;; The default setup function just runs --auto-restore and then enables
      ;; the mode.
      ;; This mode itself also runs --auto-restore.
      ;; So we can just use the mode itself.
      (persistent-scratch-autosave-mode))
    (switch-to-buffer "*scratch*")
    (message "Switched to *scratch*. Changes are persisted."))
  :config
  (add-hook! 'after-change-functions
    (defun k/persistent-scratch-save-eagerly (&rest _)
      "Save after each change in a scratch buffer."
      (when (funcall persistent-scratch-scratch-buffer-p-function)
        (persistent-scratch-save)))))

;;;;; Editor

(prog1 'word-wrap
  (require 'kinsoku nil t)
  (setq-default word-wrap nil)
  (setq word-wrap-by-category t)
  (setq-hook! '(org-mode-hook markdown-mode-hook)
    word-wrap t))

(use-package! apheleia
  :defer t
  :init
  (setq! +format-on-save-disabled-modes
         '(emacs-lisp-mode sql-mode tex-mode latex-mode org-msg-edit-mode))
  (add-hook! 'apheleia-inhibit-functions
    (defun k/turn-off-apheleia-suspicious ()
      "Return t if the current context is not suitable for automatically turning on `apheleia-mode'."
      ;; Exclude just ~/kde for now
      (f-ancestor-of? "~/kde" default-directory)))
  (setq!
   ;; Formatters should usually be configured project-wide, not in the editor
   apheleia-formatters-respect-indent-level nil))

(use-package! sideline
  :defer t
  :init
  (k/add-hook 'flycheck-mode-hook
    #'sideline-mode
    #'sideline-flycheck-setup)
  (setq sideline-display-backend-name t
        sideline-flycheck-show-checker-name t
        sideline-backends-right '(sideline-flycheck)))

(use-package! lsp-mode
  :defer t
  :init
  ;; Use sideline instead
  (setq lsp-ui-sideline-enable nil)
  (setq! lsp-auto-guess-root t
         lsp-disabled-clients '(json-ls html-ls)
         lsp-eldoc-render-all t
         lsp-enable-suggest-server-download nil
         lsp-log-max nil))

;; Disable dtrt-indent.
;; We can't just (package! dtrt-indent :disable t) because some modules assume
;; it exists.
(use-package! dtrt-indent
  :init
  (setq-default doom-inhibit-indent-detection t))

(use-package! evil
  :defer t
  :init
  (setq! evil-want-C-u-delete nil
         evil-want-C-u-scroll nil
         evil-want-C-w-delete nil
         evil-want-C-w-in-emacs-state t
         evil-move-beyond-eol t
         evil-maybe-remove-spaces t
         evil-ex-search-persistent-highlight nil
         evil-collection-setup-minibuffer t
         +evil-want-o/O-to-continue-comments nil
         ;; should Evil collect one insertion into one undo?
         ;; I want to try without.
         evil-want-fine-undo t
         evil-cross-lines t
         undo-tree-visualizer-diff t
         ;; evil-collection's dependency
         annalist-record nil
         anzu-search-threshold 100000)
  :config
  (remove-hook 'evil-goto-definition-functions #'evil-goto-definition-imenu)
  (define-advice evil-insert-newline-above (:override () no-field)
    "For performance, don't narrow to field."
    (evil-move-beginning-of-line)
    (insert (if use-hard-newlines hard-newline "\n"))
    (forward-line -1)
    (back-to-indentation))
  (define-advice evil-insert-newline-below (:override () no-field)
    "For performance, don't narrow to field."
    (evil-move-end-of-line)
    (insert (if use-hard-newlines hard-newline "\n"))
    (back-to-indentation)))

(use-package! evil-terminal-cursor-changer
  :after evil
  :init (evil-terminal-cursor-changer-activate))

(after! evil-snipe
  (setq evil-snipe-scope 'whole-visible
        evil-snipe-repeat-scope 'whole-visible
        evil-snipe-aliases `((?\[ ,(rx (syntax open-parenthesis)))
                             (?\] ,(rx (syntax close-parenthesis)))))
  (map! :map evil-snipe-parent-transient-map
        ";" (evilem-create #'evil-snipe-repeat
                           :bind ((evil-snipe-enable-highlight nil)
                                  (evil-snipe-enable-incremental-highlight nil)))))

(prog1 'auto-fill
  (setq comment-auto-fill-only-comments t)
  (add-hook 'prog-mode-hook #'auto-fill-mode))

(use-package! projectile
  :defer t
  :init
  ;; Restore caching and indexing to Projectile defaults.
  ;; - Doom uses the hybrid indexing method. Alien works just fine.
  ;; - Alien indexing is fast enough that the cache almost always just gets in
  ;;   the way.
  (setq projectile-indexing-method
        (if (eq system-type 'windows-nt) 'native 'alien)
        projectile-enable-caching
        (eq projectile-indexing-method 'native))
  :config
  (k/add! projectile-project-root-files
    "project.edn" "config.yaml" "hugo.yaml" ".svn")
  (setq counsel-ag-base-command '("ag" "--vimgrep" "%s" "--hidden"))
  (setq projectile-switch-project-action
        (fn! (or (ignore-errors (magit-status))
                 (ignore-errors (vc-dir default-directory))
                 (find-file default-directory))))
  (cl-loop
   for x in (list (-some-> k/cloud/
                    (f-join "Projects" "1.done"))
                  (-some-> k/cloud/
                    (f-join "Projects"))
                  (f-canonical "/sdcard/0.git")
                  (f-canonical "~/git")
                  (f-canonical "~/kde")
                  (f-canonical "~/kde-translation"))
   when (and x (f-dir? x))
   do (cl-pushnew x projectile-project-search-path
                  :test #'equal)))

;; FIXME: why is the paging text covering the window?
(use-package! which-key
  :defer t
  :init
  (setq! which-key-idle-delay 0.4
         which-key-idle-secondary-delay 0.1)
  :config
  ;; HACK to get rid of Doom's SPC p description text.
  ;; Doom has custom bindings for SPC p, whereas I want SPC p to just use
  ;; `projectile-command-map', and this is necessary to clear the prior
  ;; description text after rebinding.
  (setq which-key-replacement-alist
        (--remove
         (and (consp (car it))
              (stringp (caar it))
              (string-match-p "SPC p" (caar it)))
         which-key-replacement-alist))
  (require 'which-key-replacements))

;;;;; Completion

(use-package! consult
  :defer t
  :config
  (setq! consult-preview-key nil))

;;;;; Major modes

(after! comint
  (after! evil
    (evil-set-initial-state 'comint-mode 'normal)))

(use-package! prog-mode
  :config
  (setq-hook! 'prog-mode-hook
    show-trailing-whitespace t))

(use-package! text-mode
  :init
  ;; Don't highlight nobreak-space with underline.
  ;; This allows me to use a full-width space normally in text-mode.
  (k/add-hook 'text-mode-hook
    (fn! (face-remap-add-relative 'nobreak-space 'default)))
  (k/add-hook-not-derived 'text-mode
    (fn! (setq display-line-numbers t)))
  (when (modulep! :kisaragi whitespace)
    (add-hook! 'text-mode-hook #'k/auto-delete-trailing-whitespace-mode)))

(after! image-mode
  (define-advice image-mode (:around (func))
    "Open image externally if the display don't support images."
    (if (display-images-p)
        (call-interactively func)
      (org-open-file (buffer-file-name))))
  (general-def
    :keymaps 'image-mode-map
    :states 'motion
    "+" #'image-increase-size
    "-" #'image-decrease-size))

(use-package! gitignore-mode
  :mode "\\.\\(?:eslint\\|prettier\\)ignore\\'")

(use-package! lisp-data-mode
  :mode "[CE]ask\\'")

(use-package! conf-mode
  :mode (("dunstrc" . conf-mode)
         (".*\\.pa\\'" . conf-mode)
         (".*\\.hook\\'" . conf-mode)
         ("\\.directory\\'" . conf-desktop-mode)
         ("_redirects\\'" . conf-space-mode)
         ("Pipfile\\'" . conf-toml-mode))
  :init
  (setq-hook! conf-mode
    display-line-numbers 'relative)
  (after! org
    (add-to-list 'org-src-lang-modes '("desktop" . conf-desktop))))

(declare-function evil-collection-set-readonly-bindings "evil-collection" (map-sym))
(use-package! k-po-mode
  ;; Because it's just a file in k-old-emacs-dir/kisaragi/k-po-mode.el, its
  ;; autoload doesn't actually take effect. So we have to add it explicitly here.
  :mode "\\.po[tx]?\\'\\|\\.po\\."
  :config
  (evil-set-initial-state 'k-po-mode 'normal)
  (evil-collection-set-readonly-bindings 'k-po-mode-map)
  (setq k-po-auto-update-file-header nil)
  (general-def
    :keymaps 'k-po-mode-map
    :states 'normal
    "RET" #'k-po-edit-msgstr
    "C-c '" #'k-po-edit-msgstr
    "C-c C-c" #'k-po-edit-msgstr
    "i" #'k-po-edit-msgstr
    "TAB" #'k-po-toggle-fuzzy))

(after! make-mode
  (setq-hook! 'makefile-mode-hook
    fill-column 80
    tab-width 4
    outline-regexp "# [*]+"))

(after! sh-mode
  (setq-hook! 'sh-mode-hook
    outline-regexp "# [*]+"))

;;;;; Apps

(use-package! atomic-chrome
  :init (atomic-chrome-start-server))

(use-package! ready-player
  :config
  ;; Leave that to typescript
  (setq ready-player-supported-video
        (remove "mts" ready-player-supported-video))
  (ready-player-add-to-auto-mode-alist))

(use-package! wakatime-mode
  :init (global-wakatime-mode))

(after! yabridgectl
  (kisaragi-apps-register "yabridgectl" #'yabridgectl))
(kisaragi-apps-register "View system trash" #'trashed)
(kisaragi-apps-register "Vterm" #'vterm)
(after! pacfiles-mode
  (kisaragi-apps-register "Pacfiles" #'pacfiles))
(use-package! speed-type
  :defer t
  :init
  (cl-loop for (str func)
           being the elements of
           '(("Typing practice (buffer)" speed-type-buffer)
             ("Typing practice (builtin text)" speed-type-text)
             ("Typing practice (top words)" speed-type-top-1000))
           do
           (progn
             (kisaragi-apps-register str func))))

;; TODO: add help-fns-*-functions again
(use-package! helpful
  :defer t
  :config
  (setq! helpful-switch-buffer-function #'pop-to-buffer-same-window
         helpful-max-highlight 50000
         helpful-max-buffers nil)
  (let ((dir "~/git/emacs/"))
    (when (f-exists? dir)
      (setq find-function-C-source-directory
            (f-join dir "src")))))

(use-package! canrylog
  :defer t
  :init
  (setq canrylog-repo k/timetracking/
        calendar-week-start-day 1))

(after! git-commit
  ;; Undo Doom's limit.
  (setq git-commit-summary-max-length 68))

(after! git-link
  (k/add! git-link-remote-alist
    '("invent.kde.org" git-link-gitlab)))

(use-package! magit
  :defer t
  :init
  (setq! evil-collection-magit-want-horizontal-movement t)
  :config
  ;; TODO: magit-delta, magit-svn, git-link
  (setq! magit-commit-ask-to-stage 'stage
         magit-no-confirm '(stage-all-changes unstage-all-changes)
         magit-diff-refine-hunk 'all
         magit-module-sections-nested nil
         ;; To Doom: Do save buffers. Otherwise how would I know which buffers
         ;; are unsaved? It's already gated with a y/n prompt.
         magit-save-repository-buffers t)
  (k/add-hook [magit-status-sections-hook 90]
    #'magit-insert-modules-overview)
  (--each '("LANG=en_US.UTF-8"
            "LANGUAGE=en_US")
    (add-to-list 'magit-git-environment it))
  (map! :map magit-log-mode-map
        :n "C-<return>" #'k/magit-log-visit-changed-file)
  ;; Unbind `evil-magit-toggle-text-mode' on C-t. It's still provided
  ;; on \, and IMO is more consistent with Evil that way.
  (general-unbind
    :keymaps '(evil-collection-magit-toggle-text-minor-mode-map
               magit-mode-map)
    :states 'normal
    "C-t"))

(use-package! vterm
  :defer t
  :init
  ;; don't ask, just compile
  (setq vterm-always-compile-module t
        vterm-buffer-name-string "*vterm: %s*")
  :config
  (setq vterm-shell (executable-find "zsh"))
  (general-def
    :keymaps 'vterm-mode-map
    :states 'normal
    "gk" #'vterm-previous-prompt
    "gj" #'vterm-next-prompt))

(after! timer-list
  (general-def
    :keymaps 'timer-list-mode-map
    :states 'normal
    "c" #'timer-list-cancel
    "x" #'timer-list-cancel
    "d" #'timer-list-cancel))

(k/add! +lookup-provider-url-alist
  '((dictgoo "https://dictionary.goo.ne.jp/srch/all/%s/m0u/")
    (dictwebliocjjc "https://cjjc.weblio.jp/content_find?searchType=exact&query=%s")
    (wikipedia-en "https://en.wikipedia.org/w/index.php?search=%s")
    (wikipedia-zh "https://zh.wikipedia.org/w/index.php?search=%s")
    (wikipedia-ja "https://ja.wikipedia.org/w/index.php?search=%s")
    (kemdict "https://kemdict.com/search?q=%s")
    (kotobank "https://kotobank.jp/word/%s")
    (dictweblio "https://www.weblio.jp/content_find?query=%s&searchType=exact")
    (google "https://google.com/search?q=%s")
    (duckduckgo "https://duckduckgo.com/?q=%s")
    (wiktionary-zh "https://zh.wiktionary.org/w/index.php?search=%s")
    (wiktionary-en "https://en.wiktionary.org/w/index.php?search=%s")
    (wiktionary-ja "https://ja.wiktionary.org/w/index.php?search=%s")
    (jsl-nhk "https://www2.nhk.or.jp/signlanguage/index.cgi?md=search&tp=page&qw=%s")))

(setq-default +lookup-definition-functions
              (remove '+lookup-project-search-backend-fn +lookup-definition-functions))

;;;;; Keybinds

(prog1 "C-g behavior"
  ;; Return C-g to vanilla behavior
  ;; See `doom/escape'
  (map! [remap keyboard-quit] nil
        :irov "C-g" nil)
  (defadvice! k/keyboard-quit--clear-evil-highlights-a (&rest _)
    "Clear evil highlights before `keyboard-quit'."
    :before #'keyboard-quit
    (ignore-errors
      (evil-ex-nohighlight))))

(general-unbind
  :keymaps '(custom-mode-map special-mode-map)
  "SPC")

;; TAB binding
(setq tab-always-indent 'complete)
(map! :map 'help-mode-map
      :n "<tab>" (general-simulate-key "za"))

(map!
 :v "TAB" #'indent-for-tab-command
 :n "TAB" (general-simulate-key "za")
 :n "<tab>" (general-simulate-key "za")
 :map (ledger-mode-map emacs-news-view-mode-map)
 :n "TAB" (general-simulate-key "za")
 :n "<tab>" (general-simulate-key "za")
 ;; The TAB binding above overrides too much. This is messy, but... sure.
 :map (button-buffer-map shortdoc-mode-map)
 :n "TAB" #'forward-button)
;; Use <end> to save
(general-def :states 'motion
  ;; I never use <end> to move to end of line; use it to save a
  ;; file. For me, this is mainly useful in Termux as key combos are
  ;; annoying to input with a virtual keyboard.
  "<end>" (general-simulate-key "C-x C-s"))

(general-def :keymaps 'evil-window-map
  ;; I keep losing window configurations because I expected
  ;; "window-quit" to never mean "quit the application".
  "q" #'delete-window
  :desc "Maximize window" "o" #'doom/window-maximize-buffer
  :desc "Enlarge window" "O" #'doom/window-enlargen
  :desc "Rotate windows" "SPC" #'evil-window-rotate-upwards)

;; Global map
(map!
 :v "A" #'k/ellipsis
 ;; Doom's binding for this, `evil-multiedit-match-all', seems to actually
 ;; be useful. Might want to use some other key instead.
 ;; :v "R" #'k/wrap-square
 :v "M" #'k/em-dash
 :nvm "<kp-add>" #'evil-numbers/inc-at-pt)

;; Overriding map
(map! :map 'general-override-mode-map
      "M-c" #'k/capitalize-line
      "M-q" #'unfill-toggle
      "M-w" #'apheleia-format-buffer
      "M-e" #'eldoc-doc-buffer
      "C-x f" #'minaduki-open
      "C-x b" #'minaduki/literature-entries
      ;; Simulated caps lock
      "C-<menu>" #'k/caps-mode
      "M-!" #'k/async-shell-command
      "M-;" #'k:cycle-case
      "C-<tab>" #'tab-next
      "C-S-<tab>" #'tab-previous)

(map!
 :nvi "C-<tab>" #'tab-next
 :nvi "C-S-<tab>" #'tab-previous
 :nvi "C-<iso-lefttab>" #'tab-previous
 ;; Same shortcut as Blender so we can use the buttons on our mouse
 :nvi "C-<next>" #'tab-next
 :nvi "C-<prior>" #'tab-previous)

;; Let's try these bindings out
(map! "<f6>" #'doom/quickload-session
      "<f9>" #'doom/quicksave-session)

;; Normal "z" keys for avy style stuff
(map!
 :n "zj" #'evilem-motion-next-line
 :n "zk" #'evilem-motion-previous-line
 :n "zf" #'link-hint-open-link)

;; Normal state global map stuff
(general-def :states 'normal
  "Q" (general-simulate-key "@@")
  "gb" #'+lookup:online
  "gr" #'revert-buffer
  "gi" #'imenu
  "go" #'counsel-outline)
(general-def :keymaps (--map (intern (format "%s-map" it)) k/lisp-modes)
  "C-c ^" #'unpackaged/sort-sexps)

(map! :map help-map
      "c" #'describe-char
      :desc "Describe widget or button" "w" (cmd!
                                             (or (widget-describe)
                                                 (button-describe)))
      :desc "Show major mode keybinds" "W" #'which-key-show-major-mode
      ;; this just switches to *Messages* by default
      "e" #'describe-face
      ;; The metahelp is useless to me. Leave this binding to
      ;; which-key's paging.
      "C-h" nil

      ;; This is by default bound to `view-echo-area-messages', which opens the
      ;; *Messages* buffer. I only ever trigger this binding when I fat-finger
      ;; Termux's Esc key and end up hitting the inactive minibuffer instead.
      ;; Just unbind it.
      :map minibuffer-inactive-mode-map
      "<mouse-1>" nil)

(map! :after ivy
      :map ivy-minibuffer-map
      :n "j" #'ivy-next-line
      :n "k" #'ivy-previous-line
      :ni "C-n" #'ivy-next-line
      :ni "C-p" #'ivy-previous-line
      ;; Like `dired-mark', `dired-unmark', `dired-toggle-marks'
      :n "m" #'ivy-mark
      :n "u" #'ivy-unmark
      :n "t" #'ivy-toggle-marks)

(map! :map key-translation-map
      "<menu>" [escape]
      "C-@" [escape]
      "C-SPC" [escape]
      "M-<f2>" [escape]
      [hiragana-katakana] [escape])

(map! :map doom-leader-git-map
      :desc "Status" "s" #'magit-status
      :desc "Magit file dispatch" "g" #'magit-file-dispatch
      :desc "Commit every change" "C" #'k/git-commit-everything
      :desc "Commit with generic message" "c" #'k/git-commit
      :desc "Commit this (prompt)" "a" #'k/git-commit-this-file-with-message
      :desc "Commit every change" "A" #'k/git-stage-everything
      :desc "Pull" "j" (cmd!
                        (require 'magit)
                        (with-no-warnings
                          (magit-run-git-with-editor "pull")))
      :desc "Push" "k" (cmd!
                        (require 'magit)
                        (with-no-warnings
                          (magit-run-git-async "push"))))

(map! :map doom-leader-file-map
      :desc "Find library" "l" #'find-library
      :desc "Open externally" "x" #'k/open-this-file-with-system
      ;; Some modes provide custom save functions and bind them to C-x C-s;
      ;; binding <leader>fs to `save-buffer' would miss those bindings.
      :desc "Open a recent file" "o" #'recentf-open-files
      :desc "Rename this file" "r" #'doom/move-this-file
      :desc "Rename a file" "R" #'rename-file
      :desc "Delete this file" "d" #'doom/delete-this-file
      :desc "Delete a file" "D" #'delete-file
      :desc "Save" "s" (general-simulate-key "C-x C-s")
      :desc ":wq<CR>" "q" (general-simulate-key ('evil-ex "wq RET")))

(map! :map doom-leader-toggle-map
      "a" #'auto-save-visited-mode
      "m" #'menu-bar-mode
      "i" #'toggle-input-method
      "V" #'visible-mode
      ;; `visual-line-mode' redefines some editing commands. I don't want that.
      ;; So use this instead.
      "v" #'toggle-truncate-lines
      "de" #'toggle-debug-on-error
      "dq" #'toggle-debug-on-quit
      "n" #'doom/toggle-line-numbers
      ;; I don't want to use the global toggle
      "c" #'display-fill-column-indicator-mode
      ;; I sometimes might want to turn it off, but it's not often
      "C" #'company-mode)

(map! :map doom-leader-buffer-map
      ;; alternative to delete -> the key next to "d"
      ;; not f because bf and bd would be a set, making fd seem like a
      ;; similar action
      "c" #'bury-buffer
      "i" #'clone-indirect-buffer
      "n" #'k/new-buffer
      "s" #'k:switch-to-scratch)

(map! :map doom-leader-insert-map
      "c" #'insert-char
      "u" #'uuidgen
      :desc "ISO 8601 timestamp" "t" #'k/insert-timestamp
      :desc "Today" "d" #'k/insert-date
      :desc "An inline note" "n" #'k/insert-note
      :desc "A new image" "g" #'k/insert-graphics)

(map! :map doom-leader-open-map
      "j" #'k/hugo-jump-language)

(map! :leader
      "ww" #'ace-window
      "gb" #'k/git-link
      "z" #'text-scale-adjust
      "x" (general-simulate-key "M-x")
      "j" #'evil-mc-make-cursor-move-next-line
      "k" #'evil-mc-make-cursor-move-prev-line
      (:prefix-map ("e" . "eval as elisp")
       :desc "Entire buffer" "b" #'eval-buffer
       :desc "Top level form" "d" #'eval-defun
       :desc "Closest paren" "e" #'k/emacs-lisp-eval-closest-paren
       :desc "Region" "r" #'eval-region
       :desc "Last sexp" "l" #'eval-last-sexp
       :desc "Last sexp (pp)" "L" #'pp-eval-last-sexp
       :desc "Macro" "m" #'macrostep-expand)
      (:after projectile
       :desc "projectile" "p" 'projectile-command-map
       "pss" #'counsel-projectile-ag
       "psa" #'projectile-ag)
      "u" #'undo-tree-visualize
      (:when (fboundp #'other-tab-prefix)
        (:prefix-map ("1" . "Display next command buffer in...")
         :desc "This window" "q" #'same-window-prefix
         :desc "Another window" "w" #'other-window-prefix
         :desc "Another frame" "f" #'other-frame-prefix
         :desc "Another tab" "t" #'other-tab-prefix))
      ;; Quitting doesn't need a shortcut
      "qq" nil
      "qQ" nil
      "qK" nil
      (:prefix-map ("d" . "Extra editing commands")
       :desc "Get cangjie code" "c" #'cangjie-at-point
       :desc "Count words (buffer or region)" "w" #'count-words
       :desc "Convert Han characters" "h" #'opencc-replace-at-point
       :desc "Encode URL" "u" #'k/encode-url
       :desc "Decode URL" "U" #'k/decode-url
       :desc "Delete blank lines" "o" #'delete-blank-lines
       :desc "https git link to ssh" "s" #'k/git-https-to-ssh
       :desc "Convert to current timezone" "tz"
       #'kisaragi-timestamp-commands/convert-iso8601-timezone
       :desc "20200101... -> 2020-01-01..." "ts"
       #'kisaragi-timestamp-commands/iso8601-no-symbol-to-full
       :desc "2020-01-01... -> 20200101... " "tS"
       #'kisaragi-timestamp-commands/iso8601-full-to-no-symbol
       :desc "Commit timestamp to ISO 8601" "tc"
       #'kisaragi-timestamp-commands/commit-timestamp-to-iso8601)
      (:prefix-map ("a" . "Apps")
       :desc "Ag" "g" #'ag
       :desc "Rg" "G" #'rg
       :desc "App list" "a" #'kisaragi-apps
       (:after flycheck
        :desc "Flycheck" "f" (:keymap flycheck-command-map)))
      (:prefix-map ("fe" . "Fast file access")
       :desc "Open old init.el"    "o" (cmd! (find-file (f-join k-old-emacs-dir "init.el")))
       :desc "Open init.el"        "i" (cmd! (find-file (f-join doom-user-dir "init.el")))
       :desc "Open config.el"      "c" (cmd! (find-file (f-join doom-user-dir "config.el")))
       :desc "Open packages.el"    "p" (cmd! (find-file (f-join doom-user-dir "packages.el")))
       :desc "Browse Doom source"  "e" (cmd! (find-file doom-emacs-dir))
       :desc "Latest ledger file"  "l" #'k/ledger-visit-latest
       :desc "notes: main index"   "n" #'minaduki/open-index
       :desc "notes: books.org"    "b" (cmd! (find-file (f-join k/notes/ "books.org")))
       :desc "notes: Inbox.org"    "I" (cmd! (find-file (f-join k/notes/ "Inbox.org")))
       :desc "notes: todos.org"    "t" (cmd! (find-file (f-join k/notes/ "todos.org")))
       :desc "diary: today's entry" "d" #'minaduki/open-diary-entry))

;; Local Variables:
;; byte-compile-warnings: (not docstrings)
;; End:

;;; init.el -*- lexical-binding: t; -*-

(defconst k-old-emacs-dir
  (file-truename (format "%s/../../.emacs.d/" (file-truename doom-user-dir)))
  "My old Emacs config directory.")

;; Set SHELL to bash
(when-let ((bash (executable-find "bash")))
  (setenv "SHELL" bash)
  (setq-default explicit-shell-file-name bash
                shell-file-name bash))

;; HACK: get constants in here for now
(push (expand-file-name "kisaragi" k-old-emacs-dir) load-path)
(require 'kisaragi-constants)
;; I don't use this anymore as I don't deal with Org timestamps much nowadays.
;; Keeping this around in the repository for now in case something stupid pops
;; up again.
;; (require 'format-time-string-patch)

(define-advice require (:after (feature &optional _filename _noerror) detect-cl)
  "Detect who's requiring `cl' or `iswitchb'."
  ;; (when (string-match-p
  ;;        (rx bos (or "cl" "iswitchb" (seq "evil-collection" (* any))) eos)
  ;;        (symbol-name feature))
  ;;   (message "Warning: %s required `%s'"
  ;;            (or load-file-name buffer-file-name)
  ;;            feature))
  (when (memq feature '(cl iswitchb))
    (message "Warning: %s required `%s'" load-file-name feature)))

;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

(doom! :input
       ;;bidi
       :completion
       company
       ;;helm              ; the *other* search engine for love and life
       ;;ido               ; the other *other* search engine...
       ;; ivy
       (vertico +icons)           ; the search engine of the future

       :ui
       tab-bar
       ;; workspaces
       (:when (fboundp 'scroll-bar-mode) sane-scroll-bar)
       (:unless k/android? kde-window-colors)

       ;;deft              ; notational velocity for Emacs
       doom              ; what makes DOOM look the way it does
       doom-dashboard    ; a nifty splash screen for Emacs
       ;; (emoji +unicode)  ; 🙂
       hl-todo           ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW
       ;;hydra
       ;;indent-guides     ; highlighted indent columns
       ;;ligatures
       modeline
       ;;nav-flash         ; blink cursor line after big motions
       ;;neotree           ; a project drawer, like NERDTree for vim
       ophints           ; highlight the region an operation acts on
       ;;(popup +defaults)   ; tame sudden yet inevitable temporary windows

       ;; This is buffer tabs, whereas I prefer window configuration tabs
       ;; provided by tab-bar. I implemented that in my own :ui/tab-bar module.
       ;;tabs

       ;;treemacs          ; a project drawer, like neotree but cooler
       unicode           ; extended unicode support for various languages
       (vc-gutter +pretty) ; vcs diff in the fringe
       ;;vi-tilde-fringe   ; fringe tildes to mark beyond EOB
       window-select     ; visually switch windows
       zen               ; distraction-free coding or writing

       :editor
       (evil +everywhere); come to the dark side, we have cookies
       file-templates    ; auto-snippets for empty files

       ;; This folding implementation has successfully eaten (deleted) my code
       ;; multiple times, somehow. I don't trust it.
       ;;fold              ; (nigh) universal code folding

       (format +onsave)  ; automated prettiness
       multiple-cursors  ; editing in many places at once
       snippets
       ;; word-wrap

       :emacs
       electric          ; smarter, keyword-based electric-indent
       ;;ibuffer         ; interactive buffer management
       (undo +tree)      ; persistent, smarter undo for your inevitable mistakes
       vc                ; version-control and Emacs, sitting in a tree

       :term
       eshell
       ;;shell             ; simple shell REPL for Emacs
       ;;term              ; basic terminal emulator for Emacs
       vterm

       :checkers
       syntax
       ;; (spell +flyspell +hunspell)

       :tools
       ;;ansible
       ;;biblio            ; Writes a PhD for you (citation needed)
       ;;collab            ; buffers with friends
       ;;debugger          ; FIXME stepping through code, to help you add bugs
       ;;direnv
       ;;docker
       editorconfig
       (:unless k/android? ein)
       (eval +overlay)     ; run code, run (also, repls)
       lookup              ; navigate your code and its documentation
       lsp               ; M-x vscode
       magit             ; a git porcelain for Emacs
       (:unless (featurep :system 'windows) pdf)
       (:unless k/android? tree-sitter)
       :os
       (:if (featurep :system 'macos) macos)
       (:if k/android? android)
       ;;tty               ; improve the terminal Emacs experience

       :lang
       adoc
       picolisp
       pkgbuild
       pollen
       vimrc

       (cc +lsp)         ; C > C++ == 1
       (:unless k/android? clojure)
       common-lisp
       ;;coq               ; proofs-as-programs
       ;;crystal           ; ruby at the speed of c
       (csharp +lsp)
       data
       ;;(dart +flutter)   ; paint ui and not much else
       ;;dhall
       ;;elixir            ; erlang done right
       ;;elm               ; care for a cup of TEA?
       emacs-lisp        ; drown in parentheses
       ;;erlang            ; an elegant language for a more civilized age
       ;;ess               ; emacs speaks statistics
       ;;factor
       ;;faust             ; dsp, but you get to keep your soul
       ;;fortran           ; in FORTRAN, GOD is REAL (unless declared INTEGER)
       ;;fsharp            ; ML stands for Microsoft's Language
       ;;fstar             ; (dependent) types and (monadic) effects and Z3
       ;;gdscript          ; the language you waited for
       (go +lsp)
       ;;(graphql +lsp)    ; Give queries a REST
       ;;(haskell +lsp)    ; a language that's lazier than I am
       ;;hy                ; readability of scheme w/ speed of python
       ;;idris             ; a language you can depend on
       ;;json ; we configure js-json-mode in our :extras/web instead
       ;;(java +lsp)       ; the poster child for carpal tunnel syndrome
       (javascript +lsp +tree-sitter)
       ;;julia
       ;;kotlin
       ;;latex             ; writing papers in Emacs has never been so fun
       ;;lean
       ledger
       lua
       markdown
       ;;nim               ; python + lisp at the speed of c
       ;;nix
       ;;ocaml             ; an objective camel
       (org +dragndrop)
       ;;php
       ;;plantuml          ; diagrams
       ;;purescript
       (python +lsp +pyright)
       qt
       racket
       ;;raku              ; the artist formerly known as perl6
       ;;rest              ; Emacs as a REST client
       rst
       ;;(ruby +rails)
       (rust +lsp)
       ;;scala             ; java, but good
       ;;(scheme +guile)   ; a fully conniving family of lisps
       sh
       ;;sml
       swift
       (web +lsp +tree-sitter)
       yaml
       (zig +lsp)

       :email
       ;;(mu4e +org +gmail)
       ;;notmuch
       ;;(wanderlust +gmail)

       :app
       ;;calendar
       ;;emms
       (:unless k/android? everywhere)
       ;;irc
       ;;(rss +org)        ; emacs as an RSS reader

       :config
       ;;literate
       (default +bindings +smartparens)

       :kisaragi
       (dired +icons)
       auto-save
       folding
       ;;org-exporters
       git-extras
       sanity
       canrylog
       (:unless k/android? fcitx)
       hl-indent-scope
       parinfer
       rgb
       search
       taskrunner

       :extras
       vertico
       company
       ledger
       elisp-mode
       (web +lsp)
       spell)

;; -*- no-byte-compile: t; lexical-binding: t; -*-
;;; $DOOMDIR/packages.el

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;; (package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;; (package! builtin-package :recipe (:nonrecursive t))
;; (package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Use `:pin' to specify a particular commit to install.
;; (package! builtin-package :pin "1a2b3c4d5e")

(cl-defmacro k/package! (name &rest plist &key when unless &allow-other-keys)
  "A wrapper around Doom\\='s `package!' to add conditional installation.

A plain (when FORM (package! ...)) statement would not disable `use-package!'
and `after!' blocks, meaning the condition would need to be doubled there.

There are two extra keywords:

 :when FORM
   If FORM is non-nil, behave like `package!'. Otherwise, do not install or
   update it, and also disable its `use-package!' and `after!' blocks.

 :unless FORM
   Like :when (not FORM).

The conditions are evaluated at macro expansion time. This means
if the conditions change a \"doom sync\" is required for it to
take effect."
  (declare (indent 1))
  (setq plist (map-delete plist :when))
  (setq plist (map-delete plist :unless))
  (let ((when-form when)
        (unless-form unless))
    (macroexp-progn
     `((package! ,name ,@plist)
       ;; Be careful to test this out: there are many edge cases regarding
       ;; if :when or :unless are not passed in (or are nil in source code)
       ,(cond
         ((and when-form unless-form)
          (when (or (not (eval when-form t))
                    (eval unless-form t))
            `(package! ,name :disable t)))
         (when-form
          (when (not (eval when-form t))
            `(package! ,name :disable t)))
         (unless-form
          (when (eval unless-form t)
            `(package! ,name :disable t))))))))

(package! evil-escape :disable t)
(package! winner :disable t)

(package! sideline)
(package! sideline-flycheck)
(package! sideline-lsp)
;; Doom sets this up by default but sideline is better
(package! flycheck-popup-tip :disable t)

(package! auto-yasnippet :disable t)

(package! emmet-mode
  :pin "cb6a15ac151d902041cd278b10662d02046c71bd"
  :recipe '(:host github :repo "kisaragi-hiu/emmet-mode"))
(package! evil-terminal-cursor-changer)
(package! unfill)

(package! set
  :recipe '(:host github :repo "kisaragi-hiu/set.el"))

;; Prose editing

(package! org-yt
  :disable t)
(package! org-modern)
(package! org-edit-indirect)
(k/package! org-variable-pitch
  :unless k/android?)
(package! typo)
(k/package! valign
  :unless k/android?)
(package! minaduki
  :recipe (:repo "git@github.com:kisaragi-hiu/minaduki"
           :branch "main"))
(package! pplist
  :recipe (:repo "git@github.com:kisaragi-hiu/pplist"
           :branch "main"))
(package! org-import-simplenote
  :recipe (:repo "git@github.com:kisaragi-hiu/org-import-simplenote"))
(package! org-inline-video-thumbnails
  :recipe (:repo "kisaragi-hiu/org-inline-video-thumbnails"
           :host github))

;; Apps

(k/package! wakatime-mode
  :when (or (executable-find "wakatime")
            (file-executable-p "~/.wakatime/wakatime-cli")))
(k/package! atomic-chrome
  :unless k/android?)
(k/package! ready-player
  :recipe (:host github :repo "xenodium/ready-player")
  :when (and (not k/android?)
             (executable-find "mpv")))

(package! trashed)
(package! suggest)

;; (package! yasearch
;;   :recipe (:type git :repo "git@git.sr.ht:~kisaragi_hiu/yasearch"))
(package! lorem-ipsum)
(package! pangu-spacing
  :recipe (:host nil :repo "git@github.com:kisaragi-hiu/pangu-spacing"))
(package! yabridgectl
  :recipe (:host nil :repo "git@github.com:kisaragi-hiu/yabridgectl-emacs"))
(package! kisaragi-log
  :recipe (:repo "kisaragi-hiu/kisaragi-log" :host gitlab))
(package! kisaragi-translate
  :recipe (:repo "git@github.com:kisaragi-hiu/kisaragi-translate.el"))

(package! k-po-mode
  :recipe (:repo "git@github.com:kisaragi-hiu/k-po-mode"))

(k/package! pacfiles-mode
  :when (executable-find "pacman"))

(package! uuidgen)
(package! cangjie)
(package! opencc)

;; (cl-loop for value in '()
;;          collect (let ((pkg (if (symbolp value) value (car value)))
;;                        (recipe (cdr-safe value)))
;;                    (if recipe
;;                        (list 'package! pkg :recipe recipe)
;;                      (list 'package! pkg))))

(unpin! doom-themes)

(package! didyoumean
  :recipe (:host gitlab
           :repo "kisaragi-hiu/didyoumean.el"))

(package! page-break-lines)
(package! speed-type)

;; Major modes

(package! nushell-mode)
(package! meson-mode)
(package! ninja-mode)
(package! systemd)
(package! gitlab-ci-mode)
(package! ust-mode
  :recipe (:repo "git@github.com:kisaragi-hiu/ust-mode"))

(package! persistent-scratch)

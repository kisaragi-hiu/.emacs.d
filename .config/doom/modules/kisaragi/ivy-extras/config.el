;;; kisaragi/ivy-extras/config.el -*- lexical-binding: t; -*-

;; FIXME: right now the functions are in doom/autoload/k-ivy.el. They should
;; live in ./autoload.el. This can only be done after we no longer need to
;; switch back to the old config.

(after! ivy
  (advice-add 'ivy-rotate-sort :after #'k-ivy:rotate-sort-msg)
  (define-advice counsel--font-with-sample (:override
                                            (font-name)
                                            customizable-sample)
    "Make `counsel-fonts' sample customizable."
    (k-ivy:font-with-sample font-name))
  (define-advice ivy-previous-line (:around
                                    (func &rest args)
                                    show-history)
    "Show history when trying to move past the start."
    (apply #'k-ivy:previous-line func args))
  (define-advice completing-read-multiple (:override (&rest args) k:c-r-m)
    "Replace `completing-read-multiple' with an interface based on Ivy."
    (apply #'k-ivy:completing-read-multiple args)))

;; Using marginalia instead of ivy-rich
;; Marginalia works with more builtin facilities and has better support

;; Copied from :completion/vertico/config.el
(use-package! marginalia
  :hook (doom-first-input . marginalia-mode)
  :init
  (map! :map minibuffer-local-map
        :desc "Cycle marginalia views" "M-A" #'marginalia-cycle)
  :config
  (when (modulep! +icons)
    (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))
  (advice-add #'marginalia--project-root :override #'doom-project-root)
  (pushnew! marginalia-command-categories
            '(+default/find-file-under-here . file)
            '(doom/find-file-in-emacsd . project-file)
            '(doom/find-file-in-other-project . project-file)
            '(doom/find-file-in-private-config . file)
            '(doom/describe-active-minor-mode . minor-mode)
            '(flycheck-error-list-set-filter . builtin)
            '(persp-switch-to-buffer . buffer)
            '(projectile-find-file . project-file)
            '(projectile-recentf . project-file)
            '(projectile-switch-to-buffer . buffer)
            '(projectile-switch-project . project-file)))

(general-def :keymaps 'minibuffer-local-map
  "<backtab>" #'marginalia-cycle)
(general-def :keymaps '(minibuffer-local-map
                        ivy-minibuffer-map)
  "TAB" #'ivy-dispatching-call)

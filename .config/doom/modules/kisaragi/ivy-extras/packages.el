;; -*- no-byte-compile: t; -*-
;;; kisaragi/ivy-extras/packages.el

(package! ivy-rich :disable t)
(package! marginalia
  :pin "e4ff0838da33bf5102ee009ff28d541f0b51c9a3")

;;; kisaragi/auto-save/config.el -*- lexical-binding: t; -*-

(defvar k:auto-save-visited-turned-off-for-undo-tree nil
  "Marks if `auto-save-visited-mode' is turned off for `undo-tree-visualizer'.")

(defun k:manage-auto-save-visited-mode-for-undo-tree ()
  "Manage `auto-save-visited-mode' state for undo tree visualizer."
  (cond
   ((eq major-mode 'undo-tree-visualizer-mode)
    (auto-save-visited-mode -1)
    (setq k:auto-save-visited-turned-off-for-undo-tree t)
    (message "Turned off `auto-save-visited-mode' in undo-tree-visualizer session. Leave the buffer to turn it back on"))
   (t
    (when (and k:auto-save-visited-turned-off-for-undo-tree
               (not auto-save-visited-mode))
      (setq k:auto-save-visited-turned-off-for-undo-tree nil)
      (auto-save-visited-mode)
      (message "Turned `auto-save-visited-mode' back on")))))

(defun k:auto-save-visited-predicate ()
  "Don't auto save in some cases."
  (let ((case-fold-search t))
    ;; buffer-file-name is already guaranteed to be non-nil in the timer
    (and
     ;; Don't auto save even if I'm editing a buffer whose file is read only.
     ;; This situation happens if I, say, open a builtin Lisp file and want to
     ;; modify a function for the current session. I can copy the function
     ;; elsewhere, or I could just turn off `read-only-mode' and directly edit
     ;; it in place.
     (file-writable-p buffer-file-name)
     (memq evil-state '(normal emacs))
     (not (string-match-p
           "undo"
           (format "%s%s" this-command last-command))))))

(defun k:auto-save-immediately ()
  "Save the buffer immediately, but as if it's saved by `auto-save-visited-mode'.
This is meant to be added to `evil-insert-state-exit-hook'."
  (when
      ;; This is copied from what `auto-save-visited-mode' passes to
      ;; `save-some-buffers'.
      ;; Except instead of using `auto-save-visited-predicate' which this file
      ;; sets to `k:auto-save-visited-predicate' anyways, we call it directly.
      (and buffer-file-name
           auto-save-visited-mode
           (not (and buffer-auto-save-file-name
                     auto-save-visited-file-name))
           (or (not (file-remote-p buffer-file-name))
               (not remote-file-name-inhibit-auto-save-visited))
           ;; The predicate also prevents auto saving in insert state, but since
           ;; the exit hook is run with `evil-state' still in the state being
           ;; exited, the predicate would also prevent this saving altogether.
           ;; Pretending we're in normal state works around that.
           (let ((evil-state 'normal))
             (funcall #'k:auto-save-visited-predicate)))
    (save-buffer)))

(define-minor-mode k:auto-save-on-insert-state-exit-mode
  "Minor mode to save buffer when exiting insert state."
  :global t :group 'auto-save
  (if k:auto-save-on-insert-state-exit-mode
      (add-hook 'evil-insert-state-exit-hook #'k:auto-save-immediately)
    (remove-hook 'evil-insert-state-exit-hook #'k:auto-save-immediately)))

(setq! auto-save-visited-interval 0.5)
(setq! auto-save-visited-predicate #'k:auto-save-visited-predicate)
(add-hook 'doom-first-file-hook #'auto-save-visited-mode)
(add-hook 'doom-first-file-hook #'k:auto-save-on-insert-state-exit-mode)
(add-hook 'post-command-hook #'k:manage-auto-save-visited-mode-for-undo-tree)

;; -*- no-byte-compile: t; -*-
;;; kisaragi/parinfer/packages.el

(package! parinfer
  :recipe (:host github :repo "kisaragi-hiu/parinfer-mode"))

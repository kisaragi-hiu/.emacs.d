;;; kisaragi/parinfer/config.el -*- lexical-binding: t; -*-

(require 'kisaragi-utils)

(use-package! parinfer
  :defer t
  :init (k/add-hook (mapcar #'derived-mode-hook-name k/lisp-modes) #'parinfer-mode)
  :config
  (map! :map parinfer-mode-map
        "M-p" #'parinfer-toggle-state)
  (map! :map parinfer-mode-map
        :localleader
        "p" #'parinfer-toggle-state)
  (setq parinfer-extensions '(defaults pretty-parens smart-yank smart-tab)
        parinfer-change-to-indent-state-automatically nil)
  (after! evil
    (evil-declare-not-repeat #'parinfer-toggle-state))
  (after! smartparens
    ;; Let parinfer handle it
    (sp-local-pair sp-lisp-modes "(" ")" :actions nil)))

;; (add-hook 'parinfer-mode-hook (k/turn-off-minor-mode electric-pair-local-mode))

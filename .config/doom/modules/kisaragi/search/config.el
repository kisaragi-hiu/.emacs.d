;;; kisaragi/search/config.el -*- lexical-binding: t; -*-

(use-package! ag
  :defer t
  :config
  (map! :map ag-mode-map
        :n "i" #'wgrep-change-to-wgrep-mode))

(use-package! rg
  :defer t
  ;; wgrep binding is provided out-of-the-box
  :config
  (setq rg-align-position-numbers nil
        rg-hide-command nil
        rg-show-columns t
        rg-show-header t
        rg-use-transient-menu t
        rg-command-line-flags '("--sort" "path"))
  (map! :map rg-mode-map
        :n "?" #'rg-menu)
  ;; Use rga if possible
  (when (executable-find "rga")
    (setq rg-executable (executable-find "rga"))
    (after! (counsel projectile)
      (setf (car counsel-rg-base-command) "rga")))
  (defadvice! k/rg-search-everything (_oldfunc)
    "Search everything without asking."
    :around #'rg-read-files
    "everything"))

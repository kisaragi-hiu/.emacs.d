;;; kisaragi/sanity/config.el -*- lexical-binding: t; -*-

;; `files--message' isn't actually silent --- it just clears the echo area
;; immediately after. The message still shows for a split second.
(define-advice files--message (:around (orig format &rest args) actually-be-silent)
  "Actually don't show in echo area if `save-silently' is non-nil."
  (let ((inhibit-redisplay t))
    (apply orig format args)))
(define-advice write-region (:around (orig &rest args) actually-be-silent)
  "Actually don't show in echo area if `save-silently' is non-nil."
  (let ((inhibit-message save-silently))
    (apply orig args)))

(setq-default
 ;; How much faster to warrant maybe breaking bidi text display?
 bidi-inhibit-bpa nil
 save-interprogram-paste-before-kill t

 ;; If I decide to load a theme, it serves no purpose to remind me that I need
 ;; to trust it.
 custom-safe-themes t
 ;; make sure Custom never looks at an existing file
 custom-file (make-temp-file "emacs-custom")
 delete-by-moving-to-trash t
 ;; If I do know what a command does, why would I need a confirmation?
 ;; If I don't know what a command does, how would I know if I want
 ;; to use it or not?
 disabled-command-function nil
 inhibit-x-resources t
 large-file-warning-threshold (* 100 1024 1024)
 ;; This being t would force dates to be between 1970-01-01 and
 ;; 2038-01-01. I don't know how useful this would be right now
 ;; (2020-12-05) with 2038 being 17 years away, but I don't want to
 ;; be bitten by this.
 ;;
 ;; The docstring says "Currently this setting is not recommended
 ;; because the likelihood that you will open your Org files in an
 ;; Emacs that has limited date range is not negligible." As this
 ;; option was added in Emacs 24.1 (and my config requires Emacs 27
 ;; anyways) I'm pretty sure it is negligible for me.
 org-read-date-force-compatible-dates nil
 warning-minimum-level :emergency
 ;; widget images are ugly
 widget-image-enable nil)

(setq
 ;; These should never be non-nil by default...
 print-level nil
 print-length nil
 ;; Never abbreviate.
 ;; If it's too slow I'll cancel it myself.
 ;; Abbreviation doesn't speed up pretty printing anyways.
 eval-expression-print-level nil
 eval-expression-print-length nil)

(setq hscroll-margin 5
      hscroll-step 0
      scroll-margin 15
      scroll-conservatively 101)

;; <rant>
;; I don't need a keybinding just to see NEWS...
;; Or just to visit the GNU website...
;; That should be a default bookmark, not a default keybind...
;; </rant>
(general-unbind :keymaps '(ehelp-map help-map)
  "C-n" "n" "g"
  "h")
;; I use 0 to move back to BOL
(general-unbind
  :keymaps 'view-mode-map
  :states 'normal
  "0")
;; This literally just runs the same command as TAB. Give
;; `tab-bar-mode' this binding.
(after! magit-section
  (general-unbind
    :keymaps 'magit-section-mode-map
    "C-<tab>" "<normal-state> C-<tab>"))
;; Heading demoting and promoting are already provided by `evil-org'
;; through << and >> commands.
(general-unbind
  :keymaps 'org-mode-map
  "M-h" "M-l" "M-<left>" "M-<right>")
(general-unbind
  :keymaps 'org-mode-map
  :states 'normal
  "M-h" "M-l" "M-<left>" "M-<right>")
(general-unbind
  :keymaps 'outline-mode-map
  :states 'normal
  "M-j" "M-k")
;; outline-mode defines ^ to jump to the closest header. overwrite that.
(general-def
  :keymaps 'outline-mode-map
  :states 'normal
  "^" 'evil-first-non-blank)
(general-unbind
  ;; suspend-frame
  "C-x C-z"
  ;; disable facemenu
  "M-o")

(after! so-long
  (fset 'so-long-mode (lambda (&rest _)))
  (fset 'global-so-long-mode (lambda (&rest _)))
  (fset 'so-long-minor-mode (lambda (&rest _)))
  (setq doom-inhibit-large-file-detection t))

(after! flycheck
  (setq flycheck-emacs-lisp-checkdoc-form
        (if (s-contains? "'editmarker"
                         flycheck-emacs-lisp-checkdoc-form)
            flycheck-emacs-lisp-checkdoc-form
          (format
           "(progn 'editmarker %S %s)"
           '(setq
             checkdoc-create-error-function
             (lambda (text start end &optional unfixable)
               (unless (or
                        ;; It's already "Probably". This gives more false positives
                        ;; than it is helpful.
                        (string-prefix-p "Probably " text))
                        ;; Don't force Commentary and Code blocks in the init file.
                        ;; (and (string-match-p (regexp-opt '(";;; Commentary:"
                        ;;                                    ";;; Code:"))
                        ;;                      text)
                        ;;      (string-match-p (rx (or ".emacs.d/init.el"
                        ;;                              ".emacs.d/kisaragi/package.el"))
                        ;;                      (buffer-file-name))))
                 (checkdoc--create-error-for-checkdoc text start end unfixable))))
           flycheck-emacs-lisp-checkdoc-form))))

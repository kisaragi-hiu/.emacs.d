;;; kisaragi/git-extras/config.el -*- lexical-binding: t; -*-

(use-package! git-link
  :defer t
  :config
  (setq git-link-open-in-browser t))

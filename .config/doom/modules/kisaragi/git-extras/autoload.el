;;; kisaragi/git-extras/autoload.el -*- lexical-binding: t; -*-

;;;###autoload
(defun k/git-link ()
  "Copy or open URL representing buffer location in forge.

With a \\[universal-argument], reverse the setting of
`git-link-open-in-browser' --- that is, if that setting is
non-nil, this will open the link by default and copy with a
\\[universal-argument], and vice versa."
  (interactive)
  (require 'git-link)
  (let ((git-link-open-in-browser
         (funcall
          (if current-prefix-arg
              #'not
            #'identity)
          git-link-open-in-browser)))
    (call-interactively #'git-link)))

;;; kisaragi/folding/config.el -*- lexical-binding: t; -*-

(require 'kisaragi-utils)

(k/add-hook k/lisp-modes
  #'hs-minor-mode)
(add-hook! (python-mode
            web-mode
            js-mode
            js-json-mode
            typescript-mode
            c-mode-common
            css-mode)
           #'hs-minor-mode)
(add-hook! (diff-mode
            makefile-mode
            sh-mode
            ledger-mode
            adoc-mode)
           #'outline-minor-mode)

(general-def
  :states '(normal motion)
  "zx" #'k/toggle-hs-outshine)

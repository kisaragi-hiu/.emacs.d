;;; kisaragi/folding/autoload.el -*- lexical-binding: t; -*-

;;;###autoload
(defun k/toggle-hs-outshine ()
  "Toggle between HideShow and Outshine."
  (interactive)
  (cond
   ((bound-and-true-p hs-minor-mode)
    (hs-minor-mode -1)
    (outshine-mode)
    (message "%s" "Using Outshine"))
   ((bound-and-true-p outline-minor-mode)
    (outline-minor-mode -1)
    (hs-minor-mode)
    (message "%s" "Using HideShow"))
   ((bound-and-true-p outshine-mode)
    (outshine-mode -1)
    (hs-minor-mode)
    (message "%s" "Using HideShow"))
   (t
    (message "%s" "Not using Outshine or HideShow"))))

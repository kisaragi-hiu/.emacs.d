;; -*- no-byte-compile: t; -*-
;;; kisaragi/canrylog/packages.el

(package! canrylog
  :recipe (:host gitlab :repo "canrylog/canrylog.el"))

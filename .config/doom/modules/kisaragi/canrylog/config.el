;;; kisaragi/canrylog/config.el -*- lexical-binding: t; -*-

(use-package! canrylog
  :defer t
  :init
  (map! :leader "r" #'canrylog-dispatch)
  :config
  (add-hook 'canrylog-after-save-hook #'k/git-commit-this-and-push))

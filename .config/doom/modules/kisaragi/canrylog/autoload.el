;;; kisaragi/canrylog/autoload.el -*- lexical-binding: t; -*-

(defvar k/git-commit--queue nil
  "Alist of file -> timer for `k/git-commit-and-push-in-5min'.")

;;;###autoload
(defun k/git-commit-and-push-in-5min ()
  "Commit and push this file after 5 minutes."
  (let ((f (buffer-file-name)))
    (when f
      (unless (assoc f k/git-commit--queue)
        (push
         (cons (buffer-file-name)
               (run-at-time 300 nil
                            (lambda (buffer file)
                              (with-current-buffer buffer
                                (k/git-commit-this-and-push)
                                (setq k/git-commit--queue (map-delete k/git-commit--queue file))))
                            (current-buffer) f))
         k/git-commit--queue)))))

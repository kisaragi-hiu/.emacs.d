;;; taskrunner/autoload.el -*- lexical-binding: t; -*-

(defvar k/taskrunner-switch-buffer-hist)

;;;###autoload
(defun k/taskrunner-switch-buffer ()
  "Switch to a taskrunner buffer."
  (interactive)
  (let ((bufs (->> (buffer-list)
                   (--filter
                    (s-matches? "\\*taskrunner" (buffer-name it)))
                   (-map #'buffer-name))))
    (pcase (length bufs)
      (0 (message "No taskrunner buffers to switch to"))
      (1 (switch-to-buffer (car bufs)))
      (_ (switch-to-buffer
          (completing-read "Switch to taskrunner buffer: "
                           bufs
                           nil t nil
                           'k/taskrunner-switch-buffer-hist))))))

;;;###autoload
(defun k-ivy:taskrunner (reset-cache)
  "Like `ivy-taskrunner' except reset cache when RESET-CACHE is non-nil."
  (interactive "P")
  (when reset-cache
    (taskrunner-invalidate-tasks-cache))
  (ivy-taskrunner))

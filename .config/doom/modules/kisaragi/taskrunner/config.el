;;; taskrunner/config.el -*- lexical-binding: t; -*-

(after! taskrunner
  (set-popup-rule! taskrunner--buffer-name-regexp :quit t))

(use-package! helm-taskrunner
  :when (modulep! :completion helm)
  :defer t
  :config (helm-taskrunner-minor-mode))

(use-package! ivy-taskrunner
  :when (modulep! :completion ivy)
  :defer t
  :config (ivy-taskrunner-minor-mode))

(map! :leader
      :desc "Taskrunner" "l" nil
      :desc "Taskrunner" "ll" #'k-ivy:taskrunner
      :desc "Switch to a taskrunner buffer" "lb" #'k/taskrunner-switch-buffer)

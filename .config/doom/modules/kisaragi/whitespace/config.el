;;; kisaragi/whitespace/config.el -*- lexical-binding: t; -*-

(global-whitespace-mode)

(define-advice delete-trailing-whitespace
    (:around (func &rest args) ignore-in-insert)
  "Refuse to delete trailing whitespace if I'm still in insert state."
  (unless (eq evil-state 'insert)
    (apply func args)))

(after! whitespace
  (setq-default whitespace-style '(face tabs newline space-mark tab-mark newline-mark)
                whitespace-display-mappings '(((newline-mark 10 [?¬ 10])
                                               (tab-mark 9 [?▷ 9] [?\\ 9])))))

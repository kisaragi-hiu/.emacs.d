;;; kisaragi/whitespace/autoload.el -*- lexical-binding: t; -*-


;;;###autoload
(define-minor-mode k/auto-delete-trailing-whitespace-mode
  "Minor mode to delete trailing whitespace before save."
  :global nil :lighter ""
  (if k/auto-delete-trailing-whitespace-mode
      (add-hook 'before-save-hook #'delete-trailing-whitespace nil t)
    (remove-hook 'before-save-hook #'delete-trailing-whitespace)))

;;;###autoload
(define-minor-mode k/show-trailing-whitespace-mode
  "`show-trailing-whitespace' as a minor mode."
  :global nil :variable show-trailing-whitespace)

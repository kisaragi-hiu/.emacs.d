#+title: :kisaragi/whitespace

Minor mode: k/auto-delete-trailing-whitespace-mode
Minor mode: k/show-trailing-whitespace-mode

Also sets default whitespace-style and whitespace-display-mappings.

Advises delete-trailing-whitespace to do nothing in insert state.

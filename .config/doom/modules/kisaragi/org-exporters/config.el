;;; kisaragi/org-exporters/config.el -*- lexical-binding: t; -*-

(use-package! ox-reveal
  :after org
  :config
  (setq org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js"))

(use-package! ox-texinfo+
  :after org)

(use-package! ox-pollen
  :after org)

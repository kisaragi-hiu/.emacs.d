;; -*- no-byte-compile: t; -*-
;;; kisaragi/org-exporters/packages.el

(package! ox-reveal)
(package! ox-texinfo+
  :recipe (:host github :repo "tarsius/ox-texinfo-plus"))
(package! ox-pollen
  :recipe (:host github :repo "kisaragi-hiu/ox-pollen"))

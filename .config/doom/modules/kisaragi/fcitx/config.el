;;; kisaragi/fcitx/config.el -*- lexical-binding: t; -*-

(use-package! fcitx
  :hook (doom-first-input . fcitx-default-setup)
  :init
  (if (executable-find "fcitx5")
      (setq fcitx-use-dbus nil
            fcitx-remote-command "fcitx5-remote")
    (setq fcitx-use-dbus t)))

(map!
 "<insert>" #'k/fcitx-cycle-current-im
 "S-<insert>" (cmd!! #'k/fcitx-cycle-current-im -1)
 :i "<insert>" #'k/fcitx-cycle-current-im
 :i "S-<insert>" (cmd!! #'k/fcitx-cycle-current-im -1))

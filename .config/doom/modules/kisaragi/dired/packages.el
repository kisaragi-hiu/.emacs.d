;; -*- no-byte-compile: t; -*-
;;; emacs/dired/packages.el

(package! diredfl :pin "f6d599c30875ab4894c1deab9713ff2faea54e06")

(when (modulep! +icons)
  (package! nerd-icons-dired :pin "c1c73488630cc1d19ce1677359f614122ae4c1b9"))

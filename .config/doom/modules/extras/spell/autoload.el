;;; extras/spell/autoload.el -*- lexical-binding: t; -*-

(defun k/hunspell-personal-dictionary ()
  "Return the path to the current Hunspell personal dictionary, if it exists."
  (let ((dict (f-join "~/" (format ".hunspell_%s"
                                   ispell-current-dictionary))))
    (when (and (f-readable? dict)
               (f-exists? dict))
      dict)))

;;;###autoload
(defun k/add-word-to-hunspell-personal-dictionary (word)
  "Add WORD to the current Hunspell personal dictionary."
  (interactive (let ((forward (equal current-prefix-arg '(4))))
                 (list (car (flyspell-get-word forward)))))
  (let* ((dict (k/hunspell-personal-dictionary))
         (undo? (eq last-command 'k/add-word-to-hunspell-personal-dictionary)))
    (unless (not dict)
      (cond (undo?
             (k/with-file dict t
               (goto-char (point-max))
               (skip-chars-backward "\n\r")
               (delete-region (line-beginning-position) (point-max))))
            (t
             ;; skip if word is already in there
             (k/with-file dict nil
               (re-search-forward (format "^%s$" word) nil t))
             (k/with-file dict t
               (goto-char (point-max))
               (skip-chars-backward "\n\r")
               (insert "\n" word))
             (message
              (substitute-command-keys
               "Added \"%s\" to personal dictionary. Undo (\\[k/add-word-to-hunspell-personal-dictionary])?")
              word)))
      (when (process-live-p ispell-process)
        (kill-process ispell-process)
        (ispell-init-process)))
    (let ((inhibit-message t))
      (ignore-errors
        ;; Restart flyspell
        (flyspell-mode -1)
        (flyspell-mode)))))

;; -*- no-byte-compile: t; -*-
;;; extras/spell/packages.el

;; This causes a large number of errors in non English buffers
(package! flyspell-lazy
  :disable t)

;;; extras/spell/config.el -*- lexical-binding: t; -*-

(map! :map flyspell-mode-map
      "<f12>" #'flyspell-correct-wrapper
      "<f10>" #'k/add-word-to-hunspell-personal-dictionary)

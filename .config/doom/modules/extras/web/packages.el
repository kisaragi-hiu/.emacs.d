;; -*- no-byte-compile: t; -*-
;;; extras/web/packages.el

(package! js-doc
  :recipe (:repo "kisaragi-hiu/js-doc" :host github)
  :pin "590744019e073a59efa996ed75fb62b3f9a11eca")

(package! eldoc-box)

(package! cakecrumbs :pin "cf8c1df885eee004602f73c4f841301e200e5850")

(when (and (modulep! +lsp)
           (not (modulep! :tools lsp +eglot)))
  (package! lsp-biome
    :recipe (:repo "cxa/lsp-biome" :host github)
    :pin "5c7711eb5934939e61ce44061a29b0dee1d10150")
  (package! lsp-tailwindcss
    :recipe (:host github :repo "merrickluo/lsp-tailwindcss")
    :pin "b36304210421160477a4ab453fa272fc411ce297"))

;; This is based on before/after-change-functions and works incredibly well
(package! auto-rename-tag
  :pin "c6c5bcca6ca1897ecf87c63223f01a45f517a71d")

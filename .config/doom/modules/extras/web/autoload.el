;;; extras/web/autoload.el -*- lexical-binding: t; -*-

(require 'kisaragi-helpers)
(require 'js-doc)

;;;###autoload
(defun k:js-doc-insert-before ()
  "Add a JSDoc comment before this line."
  (interactive)
  (when (string-match-p (rx bos (zero-or-more (any "\n" space)) eos)
                        (buffer-substring (point-min) (point)))
    (cl-return (js-doc-insert-file-doc)))
  ;; Go to the right place
  (forward-line -1)
  (end-of-line)
  ;; Create the comment
  (let ((begin (point))
        (end-marker (point-marker)))
    ;; Reindent would insert new text, so use a marker to keep track
    ;; of it.
    (set-marker-insertion-type end-marker t)
    (insert "
/**
 *
 */")
    (goto-char (1+ begin))
    (while (<= (point) end-marker)
      (indent-according-to-mode)
      (forward-line)))
  ;; Position point
  (forward-line -2)
  (end-of-line)
  (insert " ")
  (when (bound-and-true-p evil-local-mode)
    (evil-insert-state)))

;;; extras/web/config.el -*- lexical-binding: t; -*-

(use-package! web-mode
  :mode ("\\.phtml\\'" "\\.tpl\\.php\\'"
         "\\.[agj]sp\\'" "\\.as[cp]x\\'"
         "\\.erb\\'" "\\.mustache\\'"
         "\\.svelte\\'" "\\.njk\\'"
         "\\.astro\\'" "\\.hbs\\'"
         "\\.djhtml\\'" "\\.html?\\'")
  :config
  (setq
   ;; given <abc|
   ;; type ">" -> <abc>|</abc>
   ;; (3 would be ideal if it didn't conflict with typing HTML comments)
   web-mode-auto-close-style 2
   web-mode-enable-comment-annotation t
   web-mode-enable-auto-closing t
   ;; todo keywords
   web-mode-enable-comment-interpolation t
   web-mode-enable-element-tag-fontification t
   web-mode-part-padding 2
   web-mode-script-padding 2
   web-mode-style-padding 2
   web-mode-code-indent-offset 2
   web-mode-markup-indent-offset 2)
  (after! org
    (--each '("svelte" "html")
      (push `(,it . web) org-src-lang-modes))))

(use-package! js-doc
  :defer t
  :init
  (map! :map (js2-mode-map typescript-mode-map web-mode-map)
        "C-c i" #'k:js-doc-insert-before
        ;; "C-c i" #'js-doc-insert-function-doc
        :i "@" #'js-doc-insert-tag)
  :config
  (defadvice! k/js-doc-file-doc-position-point-a ()
    "Place point in the right place after inserting the file doc."
    :after #'js-doc-insert-file-doc
    (when (search-backward "@file")
      (end-of-line)
      (insert " ")
      (evil-insert-state)))
  (setq js-doc-file-doc-lines
        '(js-doc-top-line
          " * @file\n"
          js-doc-bottom-line)))

(use-package! lsp-biome
  :when (modulep! +lsp)
  :after lsp-mode
  :config
  (setq lsp-biome-organize-imports-on-save t))

(use-package! lsp-tailwindcss
  :when (modulep! +lsp)
  :after lsp-mode
  :init (setq lsp-tailwindcss-add-on-mode t))

(use-package! lsp-astro
  :defer t
  :when (modulep! +lsp)
  :config
  (define-advice lsp-astro--get-initialization-options (:override () try-harder)
    "Try harder to find the TypeScript server path for astro-ls."
    (let* ((location "node_modules/typescript/lib")
           (base (f-traverse-upwards
                  (lambda (path)
                    (f-exists? (f-join path location)))
                  default-directory)))
      (unless base
        (setq base (--first (f-exists? (f-join it location))
                            '("/usr/lib"
                              "~/.bun/install/global"))))
      (if base
          `(:typescript (:tsdk ,(f-join base location)))
        (lsp-warn "Unable to find typescript server path for astro-ls. Completion will not work.")))))

(use-package! lsp-volar
  :defer t
  :when (modulep! +lsp)
  :config
  (define-advice lsp-volar-get-typescript-tsdk-path (:override () try-harder)
    "Try harder to find the TypeScript server path for lsp-volar."
    (let* ((location "node_modules/typescript/lib")
           (base (f-traverse-upwards
                  (lambda (path)
                    (f-exists? (f-join path location)))
                  default-directory)))
      (unless base
        (setq base (--first (f-exists? (f-join it location))
                            '("/usr/lib"
                              "~/.bun/install/global"))))
      (if base
          (f-join base location)
        (lsp-warn "[lsp-volar] Typescript is not detected correctly. Please ensure the npm package typescript is installed in your project or system (npm install -g typescript), otherwise open an issue")))))

(use-package! cakecrumbs
  :init
  ;; Equivalent to adding `cakecrumbs-enable-if-disabled' to hooks
  (cakecrumbs-auto-setup))

(map! :map 'typescript-tsx-mode-map
      ;; web-mode-fold-or-unfold does not work for typescript
      :n "z a" #'evil-toggle-fold)

;; TODO: Emacs 30 supports env -S and so can recognize deno as the interpreter
;; ie. you might be able to use `interpreter-mode-alist' instead
(use-package! typescript-mode
  :magic
  "#!/usr/bin/env -S deno run --ext ts"
  "#!/usr/bin/env -S deno run$"
  "#!/usr/bin/env -S bun$")
;; RJSX mode isn't necessarily suitable for non JSX files. Some syntax ends up
;; being treated as JSX tags and stuff get highlighted as errors as a result.
;; Use `js2-mode' instead.
(use-package! js2-mode
  :mode "\\.js\\'")
(use-package! js
  :mode ("\\.js\\(?:on\\|[hl]int\\(?:rc\\)?\\)\\'" . js-json-mode))
(add-hook! 'js2-mode-hook #'+javascript-init-lsp-or-tide-maybe-h)
(remove-hook! 'rjsx-mode-local-vars-hook #'+javascript-init-lsp-or-tide-maybe-h)

(add-hook! (typescript-mode web-mode) #'eldoc-box-hover-mode)

(defun k-buffer-match-magic-mode-alist ()
  "Return the `magic-mode-alist' match for the current buffer.
This is the same logic inside `set-auto-mode', except we return
the whole element (not just the major mode) so we know which
expression matched."
  (save-excursion
    (goto-char (point-min))
    (save-restriction
      (narrow-to-region
       (point-min)
       (min (point-max)
            (+ (point-min) magic-mode-regexp-match-limit)))
      (--first
       (let ((re (if (consp it) (car it) it)))
         (cond
          ((functionp re)
           (funcall re))
          ((stringp re)
           (let ((case-fold-search nil))
             (looking-at re)))
          (t
           (error "Problem in magic-mode-alist with element %s" re))))
       magic-mode-alist))))

(define-advice +javascript-init-lsp-or-tide-maybe-h (:before () deno)
  "Set to use deno-ls when applicable."
  (when (modulep! +lsp)
    (let ((matched (k-buffer-match-magic-mode-alist)))
      (when (and matched
                 (stringp (car matched))
                 (string-match-p "deno" (car matched)))
        (setq-local lsp-disabled-clients '(ts-ls))))))

(add-hook! (web-mode html-mode nxml-mode sgml-mode)
           #'auto-rename-tag-mode)

;;; extras/inline-completion/config.el -*- lexical-binding: t; -*-

;; Disable in specifically Emacs 29.4 due to a segfault that shows up when using
;; complete-as-you-type
;;
;; https://github.com/emacs-lsp/lsp-mode/issues/4510
;; https://github.com/doomemacs/doomemacs/issues/7915
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=71744
(defconst k/is-emacs-29.4 (and (equal emacs-major-version 29)
                               (equal emacs-minor-version 4)))
(when k/is-emacs-29.4
  (setq company-idle-delay nil))
(setq! company-show-quick-access t
       company-transformers '(company-sort-prefer-same-case-prefix
                              company-sort-by-backend-importance))
(after! company
  (define-advice completion-at-point (:around (func &optional arg) company)
    "Run `company-complete' instead if company-mode is on."
    (if (bound-and-true-p company-mode)
        (company-complete)
      (if (eql 0 (cdr (func-arity func)))
          (funcall func)
        (funcall func arg))))
  (map! :map company-active-map
        "M-/" #'company-other-backend
        "SPC" #'k/company-complete-or-counsel)
  ;; Emacs 30 inline completion preview
  ;; Seems to not interfere too badly with Company (or at all) and is pretty nice
  (when (fboundp #'global-completion-preview-mode)
    (global-completion-preview-mode)))

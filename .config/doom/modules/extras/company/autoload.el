;;; extras/inline-completion/autoload.el -*- lexical-binding: t; -*-

;;;###autoload
(defun k/company-complete-or-counsel ()
  "Complete the current match or run `counsel-company'."
  (interactive)
  ;; When the completion is explicit, SPC:
  ;; - selects the only candidate
  ;; - or selects the exact match
  ;; - or runs `counsel-company'
  ;; Otherwise SPC aborts the completion.
  (if (company-explicit-action-p)
      (cond ((equal 1 (length company-candidates))
             (company-finish (car company-candidates))
             (insert last-command-event))
            ((member company-prefix company-candidates)
             (company-finish company-prefix)
             (insert last-command-event))
            (t
             (counsel-company)))
    (company-abort)
    (insert last-command-event)))

;; -*- no-byte-compile: t; -*-
;;; kisaragi/elisp-mode-extras/packages.el

(package! highlight-defined)
(package! lisp-extra-font-lock)
(package! xr)

(package! elisp-unused
  :recipe (:host github :repo "kisaragi-hiu/elisp-unused"))

(package! flycheck-cask :disable t)

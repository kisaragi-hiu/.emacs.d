;;; kisaragi/elisp-mode-extras/autoload.el -*- lexical-binding: t; -*-

(require 'evil)
(require 'dash)
(require 'eros)

;;;###autoload
(defun k/emacs-lisp-insert-declare-function (func)
  "Insert a `declare-function' declaration for FUNC.
Tries to automatically figure out which library FUNC comes from."
  (interactive
   (list
    (completing-read "Declare function: " obarray #'fboundp t)))
  (when (stringp func)
    (setq func (intern-soft func)))
  (unless func
    (error "FUNC must be a string or a symbol"))
  (let ((library (-> (cdr (find-function-library func))
                     file-name-nondirectory
                     file-name-sans-extension)))
    (prin1
     (if library
         `(declare-function ,func ,library)
       `(declare-function ,func))
     (current-buffer))))

;;;###autoload
(defun k/emacs-lisp-toggle-autoload-cookie ()
  "Add or remove the autoload cookie to the current defun."
  (interactive)
  (save-excursion
    (beginning-of-thing 'defun)
    (forward-line -1)
    (save-match-data
      (if (looking-at lisp-mode-autoload-regexp)
          (delete-line)
        (insert "\n;;;###autoload")))))

;;;###autoload
(defun k/emacs-lisp-eval-closest-paren (printflag)
  "Evaluate the closest paren.

If PRINTFLAG is non-nil, print it."
  (interactive "P")
  (-let (((start end) (evil-a-paren 1)))
    (eros--eval-overlay
     (eval-region start end printflag)
     (save-excursion
       (goto-char end)
       (point)))))

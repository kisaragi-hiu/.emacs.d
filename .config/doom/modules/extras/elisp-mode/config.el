;;; kisaragi/elisp-mode-extras/config.el -*- lexical-binding: t; -*-

(after! elisp-mode
  (add-hook! 'emacs-lisp-mode-hook
    (progn
      (dash-fontify-mode)
      ;; HACK: lisp-extra-font-lock-mode must be enabled before
      ;; highlight-defined-mode to retain the former's highlighting for dynamic
      ;; variables. I don't know why it goes in that order.
      (lisp-extra-font-lock-mode)
      (highlight-defined-mode)))
  ;; :lang/emacs-lisp turns on outline-minor-mode.
  ;; Use hs-minor-mode by default instead.
  (remove-hook! 'emacs-lisp-mode-hook #'outline-minor-mode)
  (add-hook! 'emacs-lisp-mode-hook #'hs-minor-mode))

(map! :map emacs-lisp-mode-map
      "M-a" #'k/emacs-lisp-toggle-autoload-cookie)

(map! :map emacs-lisp-mode-map
      :localleader
      "u" #'elisp-unused-list-unused-callables)

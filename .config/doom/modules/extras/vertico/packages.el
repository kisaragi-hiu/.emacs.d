;; -*- no-byte-compile: t; -*-
;;; extras/vertico/packages.el

(package! swiper :pin "8dc02d5b725f78d1f80904807b46f5406f129674")
(package! counsel)
(package! counsel-projectile :pin "40d1e1d4bb70acb00fddd6f4df9778bf2c52734b")

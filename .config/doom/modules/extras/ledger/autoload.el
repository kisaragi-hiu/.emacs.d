;;; kisaragi/ledger-extras/autoload.el -*- lexical-binding: t; -*-

(require 'kisaragi-helpers)
(require 'ledger-mode)

(defun k/ledger-exec-ledger (input-buffer &optional output-buffer &rest args)
  "Like `ledger-exec-ledger' but not broken.

We actually make an effort to pass the file name to Ledger so
that Ledger's `include' can resolve relative paths."
  (unless (and ledger-binary-path
               (or (and (file-exists-p ledger-binary-path)
                        (file-executable-p ledger-binary-path))
                   (executable-find ledger-binary-path)))
    (error "`ledger-binary-path' (value: %s) is not executable" ledger-binary-path))
  (let* ((buf (or input-buffer (find-file-noselect (ledger-master-file))))
         (path (buffer-file-name buf))
         (outbuf (or output-buffer
                     (generate-new-buffer " *ledger-tmp*")))
         (errfile (make-temp-file "ledger-errors")))
    (unwind-protect
        (with-current-buffer buf
          (let ((exit-code
                 (let ((coding-system-for-write 'utf-8)
                       (coding-system-for-read 'utf-8))
                   (if path
                       (apply #'call-process
                              ledger-binary-path nil `(,outbuf ,errfile) nil
                              "-f" path
                              "--date-format" ledger-default-date-format
                              args)
                     (apply #'call-process-region
                            (point-min) (point-max)
                            ledger-binary-path nil `(,outbuf ,errfile) nil
                            "-f" "-"
                            "--date-format" ledger-default-date-format
                            args)))))
            (if (ledger-exec-success-p exit-code outbuf)
                outbuf
              (display-buffer (ledger-exec-handle-error errfile))
              (error "Ledger execution failed"))))
      (delete-file errfile))))

;;;###autoload
(defun k/ledger-rename-account (account new &rest files)
  "Rename ACCOUNT to NEW in FILES."
  (interactive
   (list (completing-read "Rename account: " (ledger-accounts-list))
         (read-string "New name: ")
         (directory-files "." nil "ledger\\'")))
  (dolist (f files)
    (k/with-file f t
      (goto-char (point-min))
      (while (re-search-forward
              ;; c.f. `ledger-account-regexp'
              (format "\\(%s\\)" (regexp-quote account))
              nil t)
        (replace-match new t t nil 1)))))

;;;###autoload
(defun k/ledger-insert-account ()
  "Insert a ledger account."
  (interactive)
  (insert (completing-read "Account: " (ledger-accounts-list))))

;;;###autoload
(defun k/ledger-add-simple-transaction (payee from to amount date)
  "Insert a simple ledger transaction at end of buffer.

Interactively, if a prefix argument is given, prompt for DATE
instead of using today.

DATE PAYEE
    TO  AMOUNT
    FROM"
  (interactive
   (progn
     (require 'org) ; for org-parse-time-string and org-read-date
     (list
      (s-trim (completing-read "Payee: " (ledger-payees-in-buffer)))
      (completing-read "From account: " (ledger-accounts-list))
      (completing-read "To account: " (ledger-accounts-list))
      (s-trim (read-string "Amount: " "$"))
      (or (and current-prefix-arg
               (ledger-format-date
                (apply #'encode-time
                       (org-parse-time-string (org-read-date)))))
          (ledger-format-date)))))
  (save-excursion
    (goto-char (point-max))
    (insert (format
             "
%s %s
    %s  %s
    %s"
             date payee to amount from))
    (ledger-post-align-dwim)))

(defun k/ledger-read-transaction ()
  "Read the text of a transaction.

This provides completion for existing payees, and fills in the
entire date."
  (let ((date (ledger-format-date
               ;; (or ledger-add-transaction-last-date
               (current-time))))
    (read-string "Transaction: " (s-lex-format "${date} ")
                 'ledger-minibuffer-history)))

(defun k/ledger-xact-ensure-date-format ()
  "Make date of xact under point use - or / per `ledger-default-date-format'.

Only understands YYYY-MM-DD or YYYY/MM/DD as the original date."
  (save-excursion
    (ledger-navigate-beginning-of-xact)
    (pcase-let* ((`(,start . ,end) (bounds-of-thing-at-point 'sexp))
                 (timestamp (buffer-substring-no-properties start end))
                 (`(,year ,month ,day)
                  (mapcar #'string-to-number
                          (list (substring timestamp 0 4)
                                (substring timestamp 5 7)
                                (substring timestamp 8 10)))))
      (save-excursion
        (delete-region start end)
        (goto-char start)
        (insert (ledger-format-date
                 (encode-time 0 0 0 day month year 0)))))))

;;;###autoload
(defun k/ledger-add-transaction (transaction-text &optional insert-at-point file)
  "Like `ledger-add-transaction', but in the context of FILE.

Use ledger xact TRANSACTION-TEXT to add a transaction to the buffer, in the
context of FILE.

If INSERT-AT-POINT is non-nil insert the transaction there,
otherwise call ledger-xact-find-slot to insert it at the
correct chronological place in the buffer."
  ;; changes from vanilla `ledger-add-transaction':
  ;; - explicitly require ledger-xact
  ;; - run ledger xact in the context of FILE (default `k/ledger-file')
  ;;   instead of the current buffer
  ;; - remove a useless progn
  ;; - use k/ledger-read-transaction, which does not for some
  ;;   reason remove the day
  ;; - go to the first account and enter insert state, doing the most common
  ;;   next step automatically
  (interactive (list (k/ledger-read-transaction) nil
                     k/ledger-file))
  (require 'ledger-xact)
  (let* ((args (with-temp-buffer
                 (insert transaction-text)
                 (eshell-parse-arguments (point-min) (point-max))))
         (preexisting-buffer nil)
         (ledger-buf (or (setq preexisting-buffer
                               (find-buffer-visiting file))
                         (find-file-noselect file))))
    (prog1
        (progn
          (unless insert-at-point
            (let* ((date (car args))
                   ;; This is an encoded time object
                   (parsed-date (ledger-parse-iso-date date)))
              (setq ledger-add-transaction-last-date parsed-date)
              (push-mark)
              (ledger-xact-find-slot (or parsed-date date))))
          (if (> (length args) 1)
              ;; The normal case
              (progn
                (save-excursion
                  (insert
                   (with-temp-buffer
                     (apply #'ledger-exec-ledger ledger-buf (current-buffer) "xact"
                            (mapcar 'eval args))
                     (goto-char (point-min))
                     (ledger-post-align-postings (point-min) (point-max))
                     (k/ledger-xact-ensure-date-format)
                     (buffer-string))
                   "\n"))
                ;; Go to the first account for quick editing
                (forward-line 1)
                (end-of-line)
                (when (fboundp #'evil-insert-state)
                  (evil-insert-state)))
            (insert (car args) " \n\n")
            (end-of-line -1)))
      (unless preexisting-buffer
        (kill-buffer ledger-buf)))))

;;;;; Reporting

;;;###autoload
(defun k/ledger-report-this-file ()
  "`ledger-report' with `ledger-master-file' set to nil."
  (interactive)
  (let ((ledger-master-file nil))
    (call-interactively #'ledger-report)))

;;;###autoload
(defun k/ledger-monthly-subtotal (query)
  "Report the monthly subtotals for QUERY."
  (interactive (list (completing-read "Query: " (ledger-accounts-list))))
  (let ((subtotals (cl-loop for period in (k/all-months-last-year-to-now)
                            collect
                            (with-temp-buffer
                              (apply #'call-process
                                     "ledger" nil '(t nil) nil
                                     "--date-format" "%F"
                                     "--period" period
                                     "-f" ledger-master-file
                                     "emacs"
                                     (split-string-and-unquote query))
                              (if (= 0 (buffer-size))
                                  (cons period "0 TWD")
                                (--> (cl-loop
                                      for (_file _ _datetime _ _payee
                                                 (_ _account amount _))
                                      in (read (buffer-string))
                                      collect amount)
                                     (k/add-with-units it "TWD")
                                     (cons period it)))))))
    (with-current-buffer (get-buffer-create
                          (format "*k/ledger monthly subtotal for %s*" query))
      (erase-buffer)
      (cl-loop for (period . subtotal) in subtotals
               do (insert period ": " subtotal "\n"))
      (display-buffer (current-buffer)))))

;;;;; Visiting

(defun k/ledger-dashboard (&rest _)
  "Show a dashboard including a financial summary."
  (let ((buffer "*k/ledger dashboard*"))
    (with-current-buffer (get-buffer-create buffer)
      (setq-local revert-buffer-function #'k/ledger-dashboard)
      (let ((inhibit-read-only t))
        (erase-buffer)
        (k/call-process nil
          ledger-binary-path "-f" (ledger-master-file)
          "bal" "Assets:Cash" "--real")
        (progn
          (goto-char (point-min))
          (search-forward "---")
          (delete-line)
          (delete-line)))
      (read-only-mode))
    (unless (get-buffer-window buffer)
      (switch-to-buffer buffer))))

;;;###autoload
(defun k/ledger-visit-latest ()
  "Visit the latest ledger file.

My ledger files are split into each month's; this visits the latest one.

More accurately speaking, this visits the file written exactly -2 chars from
`point-max'."
  (interactive)
  (k/ledger-dashboard)
  (split-window-below)
  (find-file
   (with-temp-buffer
     (insert-file-contents k/ledger-file)
     (goto-char (- (point-max) 2))
     (concat (file-name-directory k/ledger-file) (thing-at-point 'filename)))))

;;;;; Formatting

;;;###autoload
(defun k/ledger-format-all-files ()
  "Format all ledger files (file name ending in .ledger) in current project."
  (interactive)
  (->> (projectile-project-files (projectile-project-root))
       (--filter (equal (file-name-extension it) "ledger"))
       (--map
        (k/with-file it t
          (k/ledger-format-buffer)))))

;;;###autoload
(defun k/ledger-format-buffer ()
  "Format the current ledger buffer."
  (interactive)
  (ledger-post-align-postings (point-min) (point-max)))

;;;###autoload
(define-minor-mode k/ledger-auto-format-mode
  "Minor mode to automatically format the current ledger buffer before save."
  :global nil
  (if k/ledger-auto-format-mode
      (add-hook 'before-save-hook #'k/ledger-format-buffer nil t)
    (remove-hook 'before-save-hook #'k/ledger-format-buffer t)))

;;;;; Extracting info

(defun k/ledger--write (type &optional in-file out-file)
  "Extract existing accounts or commodities from IN-FILE and write to OUT-FILE.
TYPE: `accounts' or `commodities'.
IN-FILE and OUT-FILE are automatically determined from TYPE if not specified."
  (let* ((singular (cl-case type
                     (accounts "account")
                     (commodities "commodity")
                     (t (error "TYPE can only be `accounts' or `commodities'"))))
         (plural (symbol-name type))
         (in-file (or in-file ledger-master-file))
         (out-file (or out-file (f-join k/ledger (format "%s.ledger" plural)))))
    (with-temp-file out-file
      (shell-command (format (format "ledger %s -f %%s" plural) in-file)
                     (current-buffer))
      (goto-char (point-min))
      (while (search-forward "\n" nil t)
        (save-excursion
          (forward-line -1)
          (insert (format "%s " singular)))))))

;;;###autoload
(defun k/ledger-write-accounts (&optional in-file out-file)
  "Write accounts from IN-FILE to OUT-FILE.

If not specified, IN-FILE is `ledger-master-file' and OUT-FILE is
accounts.ledger.

Interactively, if given a \\[universal-argument], ask for IN-FILE and OUT-FILE."
  (interactive
   (when current-prefix-arg
     (list (read-file-name "Extract accounts from: " nil nil t)
           (read-file-name "Write extracted accounts to: " nil nil t))))
  (k/ledger--write 'accounts in-file out-file))

;;;###autoload
(defun k/ledger-write-commodities (&optional in-file out-file)
  "Write commodities from IN-FILE to OUT-FILE.

If not specified, IN-FILE is `ledger-master-file' and OUT-FILE is
commodities.ledger.

Interactively, if given a \\[universal-argument], ask for IN-FILE and OUT-FILE."
  (interactive
   (when current-prefix-arg
     (list (read-file-name "Extract commodities from: " nil nil t)
           (read-file-name "Write extracted commodities to: " nil nil t))))
  (k/ledger--write 'commodities in-file out-file))

;;;;; Integration with Org

(defvar k/ledger--org-allowed-value-func
  (lambda (prop)
    (when (member (downcase prop)
                  '("from" "to"))
      (cons ":ETC" (ledger-accounts-list)))))

;;;###autoload
(define-minor-mode k/ledger-org-account-complete-mode
  "Minor mode adding completion for ledger accounts to some Org properties."
  :global nil
  (if kisaragi-org-ledger-account-complete-mode
      (add-hook 'org-property-allowed-value-functions
                k/ledger--org-allowed-value-func
                nil t)
    (remove-hook 'org-property-allowed-value-functions
                 k/ledger--org-allowed-value-func
                 t)))

(defun k/ledger-account-overlay-clear (&optional start end)
  "Remove account overlays between START and END."
  (dolist (overlay (overlays-in (or start (point-min))
                                (or end (point-max))))
    (when (overlay-get overlay 'k/ledger--account)
      (delete-overlay overlay))))

(defun k/ledger-account-overlay-setup (&rest _)
  "Add overlays to accounts in current buffer."
  (let ((start (point-min))
        (end (point-max)))
    (k/ledger-account-overlay-clear start end)
    (save-excursion
      (goto-char start)
      ;; ...don't bother? I'm not sure if I still want this
      ;; (when (or (eq t kisaragi-timestamp-highlight-maximum-size)
      ;;           (< (- (or end (point-max)) (point))
      ;;              kisaragi-timestamp-highlight-maximum-size))
      (while (re-search-forward
              (rx bol (one-or-more " ")
                  (regexp ledger-account-name-regex))
              end t)
        (let* ((s (match-beginning 1))
               (e (match-end 1)))
          (make-button
           s e
           'k/ledger--account t
           'extend t
           'face 'underline
           'help-echo "Change this account"
           'action (lambda (button)
                     (let* ((ov-start (overlay-start button))
                            (ov-end (overlay-end button))
                            (orig (buffer-substring ov-start ov-end))
                            (orig-parent (replace-regexp-in-string
                                          (rx ":" (* (not ":")) eol)
                                          ""
                                          orig))
                            (new (completing-read
                                  "New account: "
                                  (ledger-accounts-list)
                                  nil nil
                                  ;; keep smart case search
                                  (downcase orig-parent))))
                       (when new
                         (delete-region ov-start ov-end)
                         (insert new)
                         (k/ledger-account-overlay-setup)))))))
      (goto-char start))))

;; TODO: rename this
;;;###autoload
(define-minor-mode k/ledger-account-mode
  "Minor mode for accounts actions."
  :global nil
  (cond
   (k/ledger-account-mode
    (k/ledger-account-overlay-setup)
    (add-hook 'after-change-functions #'k/ledger-account-overlay-setup nil t))
   (t
    (k/ledger-account-overlay-clear)
    (remove-hook 'after-change-functions #'k/ledger-account-overlay-setup t))))

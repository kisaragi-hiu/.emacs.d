;;; kisaragi/ledger/config.el -*- lexical-binding: t; -*-

(require 'kisaragi-paths)

(use-package! ledger-mode
  :defer t
  :init
  (add-hook! 'ledger-mode-hook
             #'auto-fill-mode
             #'k/ledger-auto-format-mode
             #'k/ledger-account-mode)
  (when (modulep! :kisaragi whitespace)
    (add-hook 'ledger-mode-hook #'k/auto-delete-trailing-whitespace-mode))
  :config
  (defun k/ledger-format-specifier-period ()
    "Substitutes %(period) in `ledger-reports'."
    (read-string "Reporting period: "))
  (cl-pushnew (cons "period" #'k/ledger-format-specifier-period)
              ledger-report-format-specifiers)
  (setq-hook! ledger-mode
    show-trailing-whitespace t)
  (setq-hook! ledger-report-mode
    display-line-numbers nil)
  (setq-default ledger-master-file k/ledger-file)
  (advice-add #'ledger-exec-ledger :override #'k/ledger-exec-ledger)
  (define-advice ledger-report-redo (:around (func &rest _) keep-point)
    "Try to keep point in ledger report after refresh."
    (when-let ((current-buffer (current-buffer))
               (report-buffer (get-buffer ledger-report-buffer-name)))
      (with-current-buffer report-buffer
        (let ((point-in-report (point)))
          (funcall func)
          (ignore-errors (goto-char point-in-report))))
      (pop-to-buffer current-buffer)))

  (setq ledger-highlight-xact-under-point nil
        ledger-clear-whole-transactions t
        ;; Unfortunately ledger does not (yet?) support just using a
        ;; timestamp. Use 2000-01-01 style instead of 2000/01/01 so
        ;; that we don't have to convert dates if support is added.
        ;; We can't use `ledger-iso-date-format' because it's not
        ;; loaded yet.
        ledger-default-date-format "%Y-%m-%d"
        ledger-accounts-file (f-join k/ledger/ "accounts.ledger"))
  (cl-flet ((report (command) (s-join " " (list "%(binary)"
                                                "--date-format %F"
                                                "-f %(ledger-file)"
                                                command))))
    (setq ledger-reports
          `(("budget"                     ,(report "bal Budget"))
            ("balance"                    ,(report "bal --real -S '-(T)'"))
            ("assets and liabilities"     ,(report "bal --real Assets Liabilities"))
            ("assets and liabilities ($)" ,(report "bal --real Assets Liabilities -X $"))
            ("balance, including budget"  ,(report "bal"))
            ("cash flow (outflow: positive means losing money)"
             ,(report "balance Income Expenses"))
            ("expenses, sorted by date"   ,(report "reg Expenses -S amount --real"))
            ("expenses, sorted by amount" ,(report "reg Expenses -S amount --real"))
            ("prices"                     ,(report "prices"))
            ("reg"                        ,(report "reg"))
            ("Assets, by month"           ,(report "reg Assets --monthly --collapse --period-sort '(amount)'"))
            ("payee"                      ,(report "reg @%(payee)"))
            ("account"                    ,(report "reg %(account)"))
            ("account (with period)"      ,(report "reg %(account) --period %(period)"))))))

(map! :after evil-ledger
      :map ledger-mode-map
      :localleader
      "A" #'k/ledger-add-transaction
      :desc "Add a simple transaction" "a" #'k/ledger-add-simple-transaction
      "c" #'ledger-mode-clean-buffer
      "C" #'ledger-copy-transaction-at-point
      ;; "d" #'ledger-delete-current-transaction
      :desc "Report (All imported files)" "rr" #'ledger-report
      :desc "Report (this file)" "rc" #'k/ledger-report-this-file
      :desc "write" "w" nil
      :desc "Write existing accounts to file" "wa" #'k/ledger-write-accounts
      :desc "Write existing commodities to file" "wc" #'k/ledger-write-commodities
      :desc "Format buffer" "f" #'k/ledger-format-buffer)

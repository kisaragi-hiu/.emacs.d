;;; ui/tab-bar/autoload.el -*- lexical-binding: t; -*-

(defcustom k//tab-bar-padding 4
  "Padding for tab-bar, in pixels."
  :group 'tab-bar
  :type 'integer)

(defun k//tab-bar-padding-truncate-name (name width)
  "Add padding and do truncation for NAME.
Aim to make each tab WIDTH chars wide."
  (let* ((final (- width 3)) ; close button
         (without-padding (- final 1))
         (truncated (truncate-string-to-width name without-padding nil nil t))
         (truncated-width (string-width truncated))
         (one-side (/ (max 0 (- without-padding truncated-width)) 2)))
    (format
     " %s%s%s"
     (make-string one-side ?\s)
     truncated
     (concat
      (make-string one-side ?\s)
      (if (cl-oddp truncated-width)
          " "
        "")))))

;;;###autoload
(defun k//tab-bar-tab-name-format (tab i)
  "Format name for TAB at index I."
  ;; This needs to copy `tab-bar-tab-name-format-default' because we're doing
  ;; the modification to just the name, not the entire formatted output (which
  ;; includes the close button etc.). To do this without copying would require
  ;; instrumenting `alist-get' with `cl-letf', which is even uglier.
  (let ((current-p (eq (car tab) 'current-tab))
        (name (alist-get 'name tab)))
    (propertize
     (concat
      (if tab-bar-tab-hints (format "%d " i) "")
      (k//tab-bar-padding-truncate-name
       name
       (-> (/ (- (frame-width) 3) ; new tab button
              ;; `tab-bar-tabs' will modify the current tab's name with the
              ;; output of this function, so pass it the original name and make
              ;; it have no effect.
              ;; We need to set it to something here to not end up with recursion.
              (let ((tab-bar-tab-name-format-function (-const name)))
                (length (funcall tab-bar-tabs-function))))
           (min 30)
           (max 5)))
      (or (and tab-bar-close-button-show
               (not (eq tab-bar-close-button-show
                        (if current-p 'non-selected 'selected)))
               tab-bar-close-button)
          ""))
     'face (funcall tab-bar-tab-face-function tab))))

(defvar-local k//tab-bar-tab-name-project//cache nil
  "Used to cache the result of `k//tab-bar-tab-name-project'.")

;;;###autoload
(defun k//tab-bar-tab-name-project ()
  "Use project root as tab name."
  (or k//tab-bar-tab-name-project//cache
      ;; Note that we're not mainly just caching the result of
      ;; `projectile-project-root'. `projectile-project-root' already has a
      ;; cache. We're caching `abbreviate-file-name' which calls `file-truename'
      ;; a few times and can be slow enough to show up on the profiler.
      (setq k//tab-bar-tab-name-project//cache
            (abbreviate-file-name
             (or (and (fboundp 'projectile-project-root) (projectile-project-root))
                 (buffer-name))))))

;;;###autoload
(defun k//set-tab-bar-style ()
  "Set up the tab bar style."
  (let ((padding k//tab-bar-padding))
    (setq-default
     tab-bar-close-button (propertize
                           " × "
                           'close-tab t
                           'help-echo "Click to close tab")
     tab-bar-new-button (propertize
                         " ＋ "
                         'help-echo "New tab"))
    (custom-set-faces!
      `(tab-bar
        :background ,(or (doom-color 'bg-alt) 'unspecified)
        :foreground ,(or (doom-color 'fg) 'unspecified)
        :height 0.8
        :inherit nil)
      `(tab-bar-tab
        :background ,(or (doom-color 'bg) 'unspecified)
        :foreground ,(or (doom-color 'fg) 'unspecified)
        :weight bold
        :inherit nil
        :box (:line-width ,padding :color ,(doom-color 'bg)))
      `(tab-bar-tab-inactive
        :background ,(or (doom-color 'bg-alt) 'unspecified)
        :foreground ,(or (doom-blend 'fg 'bg-alt 0.7) 'unspecified)
        :box (:line-width ,padding :color ,(doom-color 'bg-alt))))))

;;;###autoload
(defun k//tab-bar-close-last-tab-make-new-tab (_tab)
  "Make a new empty tab, like browsers, when the last tab is closed.

To be set as the value of `tab-bar-close-last-tab-choice'."
  (tab-bar-new-tab)
  (switch-to-buffer "*doom*")
  ;; This function is only called when there is only one tab. We just created
  ;; another. The "other tabs" thus only includes the original tab that we were
  ;; closing.
  (tab-bar-close-other-tabs))

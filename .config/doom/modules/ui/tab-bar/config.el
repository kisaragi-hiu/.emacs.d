;;; ui/tab-bar/config.el -*- lexical-binding: t; -*-

(use-package! tab-bar
  :hook (doom-first-file . tab-bar-mode)
  :config
  (setq tab-bar-tab-name-function #'k//tab-bar-tab-name-project)
  (setq tab-bar-tab-name-format-function #'k//tab-bar-tab-name-format)
  (setq tab-bar-close-last-tab-choice #'k//tab-bar-close-last-tab-make-new-tab)
  (setq tab-bar-auto-width nil)
  (setq tab-bar-separator "")
  (add-hook! '(doom-load-theme-hook tab-bar-mode-on-hook)
             #'k//set-tab-bar-style)
  (k//set-tab-bar-style))

(map!
 (:when (modulep! :editor evil)
   (:n "gt" #'tab-next
    :n "gT" #'tab-previous
    :n "]w" #'tab-next
    :n "[w" #'tab-previous))
 :leader
 (:when (modulep! :ui tab-bar)
   (:prefix-map ("TAB" . "tabs")
    :desc "Display tab bar"       "TAB" #'tab-bar-mode
    :desc "Switch tab"            "."   #'tab-switch
    :desc "Switch to last tab"    "`"   #'tab-recent
    :desc "New tab"               "n"   #'tab-new
    ;; :desc "New named tab"         "N"   #'k/tab/new-named
    ;; :desc "Load workspace from file"  "l"   #'+workspace/load
    ;; :desc "Save workspace to file"    "s"   #'+workspace/save
    ;; :desc "Delete session"            "x"   #'+workspace/kill-session
    :desc "Delete this tab"       "d"   #'tab-close
    :desc "Rename tab"            "r"   #'tab-rename
    ;; :desc "Restore last session"      "R"   #'+workspace/restore-last-session
    :desc "Next tab"              "]"   #'tab-next
    :desc "Previous tab"          "["   #'tab-previous
    :desc "Switch to 1st tab"     "1"   #'tab-select
    :desc "Switch to 2nd tab"     "2"   #'tab-select
    :desc "Switch to 3rd tab"     "3"   #'tab-select
    :desc "Switch to 4th tab"     "4"   #'tab-select
    :desc "Switch to 5th tab"     "5"   #'tab-select
    :desc "Switch to 6th tab"     "6"   #'tab-select
    :desc "Switch to 7th tab"     "7"   #'tab-select
    :desc "Switch to 8th tab"     "8"   #'tab-select
    :desc "Switch to 9th tab"     "9"   #'tab-select
    :desc "Switch to final tab"   "0"   #'tab-last)))

;;; ui/sane-scroll-bar/config.el -*- lexical-binding: t; -*-

(add-hook! '(server-after-make-frame-hook after-make-frame-functions)
  (defun k//no-minibuffer-scroll-bar-h (&optional frame)
    "Turn off scroll bar in minibuffer in FRAME."
    (let ((h 0)
          (w 0))
      (set-window-scroll-bars
       (minibuffer-window frame)
       w nil h nil :persistent))))

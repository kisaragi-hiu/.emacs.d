;;; kisaragi/kde-window-colors/autoload.el -*- lexical-binding: t; -*-

;;; Commentary:

;; macOS has the `ns-transparent-titlebar' frame property. Emulate the
;; same thing in KDE Plasma.

;; Setup:
;; 1. Run `k/kde-window-colors-refresh' once to create the theme file
;; 2. Right click the window decoration of Emacs, then
;;    Other Actions → Configure Special Application Settings
;; 3. Press Add Property..., select Titlebar color scheme
;; 4. Set the first field to Force, the theme field to "Emacs window colors"
;; 5. Press OK
;;
;; Afterwards, run `k/kde-window-colors-refresh' everytime after
;; `load-theme', with an advice or using Doom's hook.

;;; Code:

(require 'kisaragi-helpers)

(require 'dash)
(require 's)
(require 'f)
(require 'seq)
(require 'xdg)

(defun k/color-hex-to-rgb (hex-string)
  "Convert HEX-STRING in #RRGGBB notation to a list of RGB values.
k/color-hex-to-rgb \"#556b72\" -> (85 107 114)"
  (let* ((without-hash (substring hex-string 1))
         (parts (seq-partition without-hash (/ (length without-hash) 3))))
    (--map (read (concat "#x" it)) parts)))

(defun k/color-hex-to-rgb-comma-string (hex-string)
  "Convert HEX-STRING in #RRGGBB notation to decimal \"RRR,GGG,BBB\"."
  (s-join "," (mapcar #'prin1-to-string (k/color-hex-to-rgb hex-string))))

(cl-defun k/kde-window-colors-generate (&optional (face 'default))
  "Use the colors of FACE to generate a KDE window color scheme."
  (let ((bg (k/color-hex-to-rgb-comma-string (face-attribute face :background)))
        (fg (k/color-hex-to-rgb-comma-string (face-attribute face :foreground))))
    (s-lex-format "[General]
ColorScheme=Emacs window colors
Name=Emacs window colors

[Colors:Button]
BackgroundAlternate=${bg}
BackgroundNormal=${bg}
DecorationFocus=${fg}
DecorationHover=${fg}
ForegroundActive=${fg}
ForegroundInactive=${fg}
ForegroundLink=${fg}
ForegroundNegative=${fg}
ForegroundNeutral=${fg}
ForegroundNormal=${fg}
ForegroundPositive=${fg}
ForegroundVisited=${fg}

[Colors:Window]
BackgroundAlternate=${bg}
BackgroundNormal=${bg}
DecorationFocus=${fg}
DecorationHover=${fg}
ForegroundActive=${fg}
ForegroundInactive=${fg}
ForegroundLink=${fg}
ForegroundNegative=${fg}
ForegroundNeutral=${fg}
ForegroundNormal=${fg}
ForegroundPositive=${fg}
ForegroundVisited=${fg}

[WM]
activeBackground=${bg}
activeBlend=${bg}
activeForeground=${fg}
inactiveBackground=${bg}
inactiveBlend=${bg}
inactiveForeground=${fg}")))

(defun k/kde-window-colors-write-theme ()
  "Create a KDE window color scheme and write it to the right location."
  (let ((color-scheme-dir (f-join (xdg-data-home) "color-schemes")))
    (make-directory color-scheme-dir t)
    (with-temp-file (f-join color-scheme-dir "emacs-window-colors.colors")
      (insert
       (k/kde-window-colors-generate 'default)))))

;;;###autoload
(defun k/kde-window-colors-refresh ()
  "Write Emacs colors to a KDE color scheme and reload KWin."
  (interactive)
  (condition-case _
      (progn
        (k/kde-window-colors-write-theme)
        (when (k/pgrep-boolean "kwin_x11")
          ;; The "org.kde.KWin /KWin reconfigure" dbus method is not
          ;; enough to reload the color scheme, so we have to restart
          ;; kwin.
          ;;
          ;; Restart the kwin service if we're using plasma's systemd
          ;; startup (General.systemdBoot = true in startkderc)
          (if (ignore-errors
                (with-temp-buffer
                  (call-process "systemctl" nil '(t nil) nil
                                "show" "--user" "plasma-kwin_x11"
                                "--property=ActiveState")
                  (equal (buffer-substring-no-properties
                          (point-min)
                          ;; without the newline
                          (1- (point-max)))
                         "ActiveState=active")))
              ;; We could also use the dbus interface, but
              ;; `dbus-call-method' will require quite a mouthful of arguments
              (make-process
               :name "restart kwin"
               :command '("systemctl" "restart" "--user" "plasma-kwin_x11"))
            ;; If not, we can only restart kwin by starting a new
            ;; process directly. Hopefully this doesn't cause issues.
            ;;
            ;; setsid tip from:
            ;; https://emacs.stackexchange.com/questions/22363/
            (start-process "restart kwin" nil "setsid" "kwin_x11" "--replace"))))
    ;; If something errored out when converting the color... just
    ;; don't do anything.
    (invalid-read-syntax nil)))

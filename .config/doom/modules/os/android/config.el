;;; os/android/config.el -*- lexical-binding: t; -*-

;; Set DISPLAY so that browse-url works
(setenv "DISPLAY" "dummy")

;; This is from Termux's site-lisp
(xterm-mouse-mode 1)
(global-set-key [mouse-4] 'scroll-down-line)
(global-set-key [mouse-5] 'scroll-up-line)

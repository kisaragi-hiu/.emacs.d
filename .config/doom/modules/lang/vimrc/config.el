;;; lang/vimrc/config.el -*- lexical-binding: t; -*-

(use-package! vimrc-mode
  :mode ("\\.vim\\(rc\\)?\\'" . vimrc-mode)
  :init
  (after! org-src
    (dolist (name '("vimscript" "viml" "vimrc"))
      (add-to-list 'org-src-lang-modes (cons name 'vimrc)))))

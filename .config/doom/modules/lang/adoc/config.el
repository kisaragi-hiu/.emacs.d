;;; lang/adoc/config.el -*- lexical-binding: t; -*-

(use-package! adoc-mode
  :defer t
  :config
  (setq-hook! 'adoc-mode-hook
    display-line-numbers nil)
  (add-hook! 'adoc-mode-hook
             #'variable-pitch-mode
             #'outline-minor-mode))

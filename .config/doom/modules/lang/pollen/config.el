;;; lang/pollen/config.el -*- lexical-binding: t; -*-

(use-package! pollen-mode
  :mode ("\\.ptree\\'" "\\.html.pm\\'")
  :config
  (setq-mode-local pollen-mode comment-start "◊;")
  (after! rainbow-delimiters
    (add-hook 'pollen-mode-hook #'rainbow-delimiters-mode))
  (map! :map pollen-mode-map
        :localleader
        :desc "Start Server" "s" #'pollen-server-start
        :desc "Stop Server" "p" #'pollen-server-stop
        :desc "Edit Block" "e" #'pollen-edit-block-other-window))

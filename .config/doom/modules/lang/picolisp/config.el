;;; lang/picolisp/config.el -*- lexical-binding: t; -*-

(use-package! plisp-mode
  :mode ("\\.l\\'" . plisp-mode)
  :config
  (setq plisp-documentation-method 'plisp--shr-documentation
        plisp-documentation-directory
        (if (getenv "PREFIX")
            (f-join (getenv "PREFIX") "lib/picolisp/doc/")
          "/usr/lib/picolisp/doc/")))

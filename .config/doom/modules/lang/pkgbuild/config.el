;;; lang/pkgbuild/config.el -*- lexical-binding: t; -*-

(after! pkgbuild-mode
  (add-hook! 'pkgbuild-mode-hook
    (fn!
     (remove-hook 'flymake-diagnostic-functions #'sh-shellcheck-flymake t)))
  (setq! pkgbuild-update-sums-on-save nil))

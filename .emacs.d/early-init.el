;;; early-init.el --- Init before package.el and GUI -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(require 'cl-lib)
(require 'subr-x)

;;;; Load

(defconst k-doom-user-dir
  (file-truename (format "%s/../.config/doom" (file-truename user-emacs-directory)))
  "My new Doom Emacs config directory.")

(setq load-prefer-newer t)
(cl-loop for file in '("~/.local/share/npm-global/bin"
                       "~/.local/share/pnpm"
                       "~/.local/bin")
         when (file-exists-p file)
         do (cl-pushnew (let ((file-name-handler-alist nil))
                          (expand-file-name file))
                        exec-path
                        :test #'equal))
(setenv "PATH" (string-join exec-path ":"))

;; From Doom Emacs:
;;
;; > `file-name-handler-alist' is consulted on each `require', `load'
;; > and various path/io functions. You get a minor speed up by
;; > unsetting this.
;;
;; This can cause problems if the .elc of builtin libraries are not
;; present, as without `file-name-handler-alist' we cannot load .el.gz
;; files. In my case I don't have environments like that.
(unless (daemonp)
  (let ((original file-name-handler-alist))
    (setq-default file-name-handler-alist nil)
    (add-hook 'emacs-startup-hook
              (lambda ()
                ;; Startup might have changed it, so preserve those additions
                (setq file-name-handler-alist
                      (delete-dups (append file-name-handler-alist
                                           original)))))))

;; Modified from Doom Emacs. `load-file' sends the "Loading X..."
;; message, which triggers a redisplay, which has an appreciable
;; effect on startup times. This supresses the message.
(define-advice load-file (:override (file) silence)
  (load (expand-file-name file) nil 'nomessage 'nosuffix))
(add-hook 'emacs-startup-hook
          (lambda ()
            (advice-remove 'load-file #'load-file@silence)))

(cl-pushnew (expand-file-name "kisaragi" user-emacs-directory) load-path)
(cl-pushnew (expand-file-name "autoload" k-doom-user-dir) load-path)

;; absolutely make sure builtin org is never loaded
(setq load-path
      (cl-remove (thread-last data-directory
                              directory-file-name
                              file-name-directory
                              (expand-file-name "lisp")
                              (expand-file-name "org"))
                 load-path
                 :test #'equal))

;;;; GC

(setq gc-cons-threshold (* 100 1000 1000))

;;;; TLS

(require 'gnutls)

(setq
 ;; don't defeat the whole point of TLS
 gnutls-verify-error t
 ;; "acceptably modern value" according to Radian.el
 gnutls-min-prime-bits 3072)

;;;; Package management

(setq package-enable-at-startup nil)

(defvar elpaca-installer-version 0.6)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop (call-process "git" nil buffer t "clone"
                                       (plist-get order :repo) repo)))
                 ((zerop (call-process "git" nil buffer t "checkout"
                                       (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

;; (define-advice make-directory-autoloads
;;     (:around (func &rest args) reset-find-file-visit-truename)
;;   (let ((find-file-visit-truename nil))
;;     (apply func args)))

;;; early-init.el ends here

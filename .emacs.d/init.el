;;; init.el --- Kisaragi Hiu's Emacs config  -*- lexical-binding: t; -*-

;;; Commentary:
;; TODO: command to go to the next / previous thing
;;       select the thing in a prompt
;;       use the selected if the command is run again

;;; Code:

;;;; Refuse to load on old Emacsen
(let ((minver "29"))
  (when (version< emacs-version minver)
    (warn "This configuration depends on Emacs %s or higher" minver)))

;; Make absolutely sure nothing ever touches how 0 and $ work in evil.
;; (You'd think `evil-respect-visual-line-mode' = nil would do that
;; already, but it didn't when I enabled Olivetti mode.)
(fset 'visual-line-mode (symbol-function #'ignore))

;;;; Requires
(require 'seq)
(require 'map)
(require 'xdg)
(require 'subr-x)
(require 'mode-local)
(require 'cl-lib)

(load "packages.el")

(require 'async)
(require 'dash)
(require 'f)
(require 'ht)
(require 'parseedn)
(require 's)
(require 'ts)
(require 'tst)
(require 'xr)

(require 'kisaragi-constants)
(require 'kisaragi-paths)
(require 'kisaragi-helpers)

(require 'leaf)
(require 'leaf-keywords)
(leaf-keywords-init)

;; This will only take effect in the flycheck checker if it's in a top
;; level require like this. It also mutates `leaf-keywords' on load.
;;
;; This adds a :when-available keyword, proceeding inly if
;; `locate-library' can find the arguments.
(require 'k-leaf-keywords)

;; (defvar k-now--map (make-hash-table :test #'equal))
;; (defun k-now (msg)
;;   "Message MSG and the current time."
;;   (if-let (start (gethash msg k-now--map))
;;       (progn
;;         (message "%s: %.2fs" msg (- (float-time) start))
;;         (remhash msg k-now--map))
;;     (puthash msg (float-time) k-now--map)))
;; (load "k-leaf-timing")
;; (k-leaf-timing-mode)

;;;;; packages

(defvar k/load-theme-hook nil "Hook run after `k/load-theme'.")
(defun k/load-theme (theme)
  "Load THEME, unless it's already the only enabled theme."
  (interactive
   (list
    (completing-read "Load custom theme: "
                     (custom-available-themes))))
  (when (symbolp theme) (setq theme (symbol-name theme)))
  (unless (and (= 1 (length custom-enabled-themes))
               (eq (car custom-enabled-themes)
                   (intern theme)))
    (mapc #'disable-theme custom-enabled-themes)
    (load-theme (intern theme) t)
    (run-hooks 'k/load-theme-hook)))

;; NOTE: migrated
(defun k/setup-fonts ()
  "Set up fonts."
  (setq k/font-mono (k/first-available-font
                     '("Iosevka" "Noto Mono"))
        k/font-serif (k/first-available-font
                      '("Equity OT" "Noto Serif"))
        k/font-sans (k/first-available-font
                     '("Inter" "Fira Sans" "Source Sans Pro" "Noto Sans")))
  (when
      (and
       ;; This function doesn't exist in Emacs compiled without
       ;; graphical support, like in Termux.
       (fboundp 'create-fontset-from-fontset-spec)
       ;; If (set-fontset-font (face-attribute 'default :fontset) ...)
       ;; is run in a daemon before any frame is created, Emacs
       ;; segfaults.
       ;;
       ;; So how do we figure out we are in a daemon before any frame
       ;; is created? After some testing in emacs -Q --fg-daemon:
       ;;
       ;; - `frame-list' isn't empty for some reason
       ;; - nor are `selected-frame' and `terminal-list'
       ;; - and (frame-live-p (car (frame-list))) -> t
       ;;
       ;; This seems to be the best option. Confusing, but not as
       ;; hacky as, say, checking if current-buffer is " *server*".
       ;;
       ;; See https://kisaragi-hiu.com/emacs-detect-daemon-before-frame
       (not (equal "initial_terminal" (terminal-name))))
    (defvar k/fontset-proportional
      (create-fontset-from-fontset-spec "-*-*-*-*-*-*-*-*-*-*-*-*-fontset-proportional"))
    ;; Emacs for some reason sees Iosevka and resolves that to Noto
    ;; Serif for all characters, even when I specify it just for `latin'.
    (prog1 'default
      (k/face-set 'default
        :family k/font-mono
        :height (if (k/system-is-p "MF-manjaro")
                    140
                  160))
      (--each '(han hangul kana bopomofo cjk-misc)
        (set-fontset-font (face-attribute 'default :fontset)
                          it "Noto Sans Mono CJK JP")))
    (prog1 'emoji
      (set-fontset-font t 'symbol "Noto Color Emoji"))
    (prog1 'variable-pitch
      (k/face-set 'variable-pitch
        :font k/font-sans
        :height 'unspecified
        :fontset k/fontset-proportional)
      (add-hook 'buffer-face-mode-hook
                (k/set-x-to-val line-spacing 0.1)
                t)
      ;; And if I don't do this it doesn't use Equity for eg. ö :)
      (set-fontset-font (face-attribute 'variable-pitch :fontset)
                        'latin
                        k/font-sans)
      (--each '(han hangul kana bopomofo cjk-misc)
        (set-fontset-font (face-attribute 'variable-pitch :fontset)
                          it
                          (k/first-available-font
                           (list
                            ;; "Noto Serif CJK JP"
                            "Noto Sans CJK JP")))))
    (k/face-set 'fixed-pitch-serif
      :font (k/first-available-font
             '("mononoki" "Fira Mono" "Source Code Pro" "Iosevka" "Noto Mono")))))

;; NOTE: migrated
(defun k/setup-cursors ()
  "Set up cursors."
  (cl-flet ((cursor (face &optional (style 'box))
              (list (face-attribute face :foreground nil t) style)))
    (with-eval-after-load 'doom-modeline
      (setq evil-emacs-state-cursor     (cursor 'doom-modeline-evil-emacs-state 'bar)
            evil-normal-state-cursor    (cursor 'doom-modeline-evil-normal-state)
            evil-operator-state-cursor  (cursor 'doom-modeline-evil-normal-state 'hollow)
            evil-insert-state-cursor    (cursor 'doom-modeline-evil-insert-state 'bar)
            evil-visual-state-cursor    (cursor 'doom-modeline-evil-visual-state)
            evil-motion-state-cursor    (cursor 'doom-modeline-evil-motion-state)
            evil-replace-state-cursor   (cursor 'doom-modeline-evil-replace-state)))))

(defun k/setup-frame (&optional frame)
  "Set up my font and cursor preferences in FRAME.
Doing this mainly allows face customizations to take effect even
when using the server."
  (interactive)
  ;; otherwise the theme hasn't been applied
  (run-at-time 1 nil #'k/setup-cursors)
  (when frame (select-frame frame))
  (k/setup-fonts)
  (with-eval-after-load 'markdown-mode
    (k/face-set 'markdown-header-delimiter-face
      :inherit 'markdown-markup-face
      :weight 'bold)
    (k/face-set '(markdown-header-face
                  markdown-bold-face
                  markdown-italic-face
                  markdown-list-face)
      :foreground :default)
    (k/face-set 'markdown-header-face-1
      :height 2.1)
    (k/face-set 'markdown-header-face-2
      :height 1.6)
    (k/face-set 'markdown-header-face-3
      :height 1.3)
    (k/face-set '(markdown-header-face-4
                  markdown-header-face-5)
      :height 1.125)
    (k/face-set 'markdown-header-face-6
      :height 1.0))
  (with-eval-after-load 'avy
    (--each '(avy-lead-face
              avy-background-face
              avy-lead-face-2
              avy-lead-face-1
              avy-lead-face-0
              avy-goto-char-timer-face)
      (set-face-attribute it nil :weight 'unspecified)))
  (with-eval-after-load 'speed-type
    (k/face-set 'speed-type-correct
      :foreground 'unspecified
      :inherit 'success)
    (k/face-set 'speed-type-mistake
      :foreground 'unspecified
      :underline 'unspecified
      :inherit 'error))
  (with-eval-after-load 'ledger
    (k/face-set 'ledger-font-payee-uncleared-face
      :foreground 'unspecified
      :weight 'bold
      :slant 'italic))
  (with-eval-after-load 'anzu
    (k/face-set 'anzu-mode-line
      :inherit 'font-lock-constant-face
      :foreground 'unspecified)))

;; NOTE: migrated
(leaf adoc-mode
  :config
  (setq-mode-local adoc-mode
    display-line-numbers nil)
  (k/add-hook 'adoc-mode-hook
    #'variable-pitch-mode
    #'outline-minor-mode))

(defun k/setup-appearance ()
  "Set up appearance."
  (leaf doom-themes
    :config
    (setq doom-org-special-tags nil)
    (doom-themes-org-config))
  (leaf solaire-mode
    :require t
    :config
    ;; Solaire incorrectly thinks it's not supported when loaded before
    ;; a graphical frame has been created.
    (setq solaire-mode--supported-p t)
    (solaire-mode-swap-faces-maybe)
    (solaire-global-mode))
  (leaf k-kde-window-colors
    :unless k/android?
    :disabled t
    :require t
    :config (add-hook 'k/load-theme-hook #'k/kde-window-colors-refresh))
  (if (daemonp)
      (add-hook 'after-make-frame-functions #'k/setup-frame)
    (k/setup-frame))
  (add-hook 'k/load-theme-hook #'k/setup-frame)
  (blink-cursor-mode -1)
  (ignore-errors
    (tool-bar-mode -1)
    (menu-bar-mode -1)
    (scroll-bar-mode -1))
  (let ((theme
         ;; Dark theme on even months; doom-one-light as light theme
         ;; on Windows.
         (cond ((cl-evenp (ts-month (ts-now)))
                "doom-vibrant")
               ((eq system-type 'windows-nt)
                "doom-solarized-light")
               (t
                "doom-material"))))
    (k/load-theme theme)))

;; NOTE: migrated
(defun k/setup-transient ()
  "Setup transient.el."
  (require 'transient)
  (setq transient-display-buffer-action
        '(display-buffer-in-side-window (side . bottom))))

(require 'general)
(general-auto-unbind-keys)
(require 'k-general-definers)

;; NOTE: migrated
(defun k/setup-keys ()
  "Set up general keybinds."
  (when (fboundp #'widget-describe)
    (general-def
      "C-h w" (list (lambda ()
                      (interactive)
                      (or (widget-describe)
                          (button-describe)))
                    :wk "Describe widget or button")))
  ;; Some modes provide custom save functions and bind them to
  ;; C-x C-s; binding <leader>fs to `save-buffer' would miss
  ;; those bindings.
  (general-simulate-key "C-x C-s"
    :which-key "Save")
  ;; This is bound to `view-echo-area-messages', which opens the
  ;; *Messages* buffer. I only ever trigger this ninding when I
  ;; fat-finger Termux's Esc key and end up hitting the inactive
  ;; minibuffer instead.
  (general-unbind
    :keymaps 'minibuffer-inactive-mode-map
    "<mouse-1>")
  (general-def
    ;; this just switches to *Messages* by default
    "C-h e" #'describe-face
    ;; The metahelp is useless to me. Leave this binding to
    ;; which-key's paging.
    "C-h C-h" nil)
  (general-def
    :keymaps 'key-translation-map
    [hiragana-katakana] [escape]
    "M-<f2>" [escape]
    "C-SPC" [escape]
    "<menu>" [escape]
    "C-@" [escape])
  (k/general-leader/frame
    "d" '(delete-frame :wk "Close frame")
    "n" '(make-frame-command :wk "New frame")
    "o" '(other-frame :wk "Jump to another frame"))
  (when (fboundp 'other-tab-prefix)
    (k/general-leader/display-buffer-prefixes
      "q" '(same-window-prefix :wk "This window")
      "w" '(other-window-prefix :wk "Another window")
      "f" '(other-frame-prefix :wk "Another frame")
      "t" '(other-tab-prefix :wk "Another tab")))
  (k/general-leader/file
    "#" '(k/open-current-file-as-sudo :wk "Edit this file with sudo")
    "c" '(copy-file :wk "Copy a file")
    "d" '(k/delete-this-file :wk "Delete this file")
    "D" '(delete-file :wk "Delete a file")
    "f" #'find-file
    "l" #'find-library
    "r" '(rename-visited-file :wk "Rename this file")
    "o" '(consult-recent-file :wk "Recent files")
    "p" #'find-file-at-point ; also available as <normal>gf
    "q" (general-simulate-key
          ('evil-ex "wq RET")
          :which-key ":wq<CR>")
    "s" #'general-simulate-C-x_C-s
    "S" (general-simulate-key
          (#'save-some-buffers "!") ; means "save the rest" in prompt
          :which-key "Save all")
    "x" '(consult-file-externally :wk "Open file externally")
    "X" '(k/open-this-file-with-system :wk "Open this externally")
    "y" '(k/copy-current-file-name :wk "Copy current file name")
    "v" '(k/open-this-file-with-nvim
          :wk "Edit this file with Neovim"))
  (k/general-leader/file
    "e" '(:ignore t :wk "fast file access")
    "eb" `(,(k/find-file-command (f-join k/notes/ "books.org")) :wk "notes: books.org")
    "ec" '(k/visit-core-repo :wk "Core repositories")
    "ei" '(k/visit-init-file :wk "init.el")
    "eI" `(,(k/find-file-command (f-join k/notes/ "Inbox.org")) :wk "notes: inbox.org")
    "eh" `(,(k/find-file-command (f-join k/notes/ "habits/habits.org")) :wk "notes: habits.org")
    "el" '(k/ledger-visit-latest :wk "latest ledger file")
    "en" `(minaduki/open-index :wk "notes: main index")
    "et" `(,(k/find-file-command (f-join k/notes/ "todos.org")) :wk "notes: todos")
    "ep" `(,(k/find-file-command (f-join k/notes/ "projects.org")) :wk "notes: projects.org"))
  (k/general-leader/toggle
    ;; `visual-line-mode' redefines some editing commands. I don't
    ;; want that. So use this instead.
    "v" '(toggle-truncate-lines :wk "Toggle soft line wrap")
    "de" '(toggle-debug-on-error :wk "Debug on error")
    "dq" '(toggle-debug-on-quit :wk "Debug on quit")
    "r" #'read-only-mode)
  (k/general-leader/buffer
    "s" '(k:switch-to-scratch :wk "*scratch*")
    ;; `counsel-switch-buffer' previews selected buffer. I don't know
    ;; if I want that.
    ;; Just use `switch-to-buffer'. `consult-buffer' does not appear to mark the
    ;; category, so Embark doesn't seem to be able to know that they are buffers
    ;; to be acted on.
    "b" '(switch-to-buffer :wk "Switch buffer")
    "r" '(rename-buffer :wk "Rename buffer")
    "d" '(evil-delete-buffer :wk "Delete buffer and window")
    "D" '(kill-current-buffer :wk "Delete buffer only")
    ;; alternative to delete -> the key next to "d"
    ;; not f because bf and bd would be a set, making fd seem like a
    ;; similar action
    "c" '(bury-buffer :wk "Bury buffer")
    "i" '(clone-indirect-buffer :wk "Clone indirect buffer")
    "n" '(k/new-buffer :wk "New buffer")
    "h" '(previous-buffer :wk "Switch to previous buffer")
    "l" '(next-buffer :wk "Switch to next buffer"))
  (general-def
    :states 'motion
    ;; I never use <end> to move to end of line; use it to save a
    ;; file. For me, this is mainly useful in Termux as key combos are
    ;; annoying to input with a virtual keyboard.
    "<end>" #'general-simulate-C-x_C-s)
  (k/general-leader
   "c" #'evil-ex-nohighlight
   "x" #'execute-extended-command
   "z" #'text-scale-adjust
   "h" '(:keymap help-map :wk "Help"))
  (k/general-leader/extra-insert-commands
    "c" '(counsel-unicode-char :wk "Insert character")
    "u" '(uuidgen :wk "Insert random UUID (v4)"))
  (k/general-leader/extra-editing-commands
    "c" '(cangjie-at-point :wk "Get cangjie code")
    "w" '(count-words :wk "Count words (buffer or region)")
    "h" '(opencc-replace-at-point :wk "Convert Han characters")
    "u" '(k/encode-url :wk "Encode URL")
    "U" '(k/decode-url :wk "Decode URL")
    "o" '(delete-blank-lines :wk "Delete blank lines")
    "s" '(k/git-https-to-ssh :wk "https git link to ssh")))

;;;;;; Frameworks

;; NOTE: migrated
(leaf prescient
  :require t
  :config
  (prescient-persist-mode)
  (setq prescient-history-length 500
        prescient-filter-method '(literal regexp)
        prescient-sort-full-matches-first t
        prescient-sort-length-enable nil)
  (setq ivy-prescient-enable-filtering t
        ivy-prescient-retain-classic-highlighting t))

(k/setup-appearance)

(k/setup-transient)
(k/setup-keys)

(k/setup-projectile)

;; NOTE: migrated
(leaf ag
  :when-available t
  :config
  ;; These are already autoloaded in wgrep-ag.el.
  ;; (autoload 'wgrep-ag-setup "wgrep-ag")
  ;; (add-hook 'ag-mode-hook 'wgrep-ag-setup)
  (general-def
    :keymaps 'ag-mode-map
    :states 'normal
    "i" #'wgrep-change-to-wgrep-mode)
  (k/general-leader/apps
    "g" #'ag))

;; NOTE: migrated
(leaf rg
  :when-available t
  ;; wgrep binding is provided out-of-the-box
  :init
  (k/general-leader/apps
    "G" #'rg)
  :config
  (setq rg-align-position-numbers nil
        rg-hide-command nil
        rg-show-columns t
        rg-show-header t
        rg-use-transient-menu t
        rg-command-line-flags '("--sort" "path"))
  (general-def
    :keymaps 'rg-mode-map
    :states 'normal
    "?" #'rg-menu)
  ;; Use rga if possible
  (when (executable-find "rga")
    (setq rg-executable (executable-find "rga"))
    (with-eval-after-load 'counsel
      (with-eval-after-load 'projectile
        (setf (car counsel-rg-base-command) "rga"))))
  (define-advice rg-read-files (:around (_func) search-everything)
    "Search everything without asking."
    "everything"))

(load "org-and-markdown")

;;;;;; Features / Minor modes

;; NOTE: migrated
(leaf hl-indent-scope
  :hook (emacs-lisp-mode-hook
         c-mode-common-hook
         js-mode-hook)
  :config (add-hook 'k/load-theme-hook #'hl-indent-scope--auto-color-calc))

;; NOTE: not migrating
(leaf aggressive-indent
  :when-available t
  :require t
  :config
  ;; (global-aggressive-indent-mode -1)
  :defer-config
  (k/add-hook 'aggressive-indent-mode-hook
    (defun k/aggressive-indent-mode-avoid-conflict ()
      "Turn off `aggressive-indent-mode' in conflicting minor modes.
This is necessary because `aggressive-indent-excluded-modes' only
works for major modes."
      (when aggressive-indent-mode ; only run when turning on!
        (when (or (bound-and-true-p parinfer-mode)
                  (and (bound-and-true-p apheleia-mode)
                       (boundp 'apheleia-mode-alist)
                       (apply #'derived-mode-p (map-keys apheleia-mode-alist))))
          (aggressive-indent-mode -1)))))
  (k/add! aggressive-indent-protected-commands
    'evil-undo))

(leaf flyspell
      :when-available flyspell-correct
      :init
      (general-add-hook '(org-mode-hook
                          markdown-mode-hook
                          text-mode-hook
                          message-mode-hook)
                        #'flyspell-mode)
      ;; these modes inherit hooks from text-mode, but we want flyspell
      ;; off there.
      (general-add-hook '(ledger-mode-hook
                          html-mode-hook)
                        (k/turn-off-minor-mode flyspell-mode))
      (autoload #'flyspell-correct-helm "flyspell-correct-helm")
      (setq flyspell-correct-interface #'flyspell-correct-helm
            flyspell-duplicate-distance 0)
      (general-def
        :keymaps 'flyspell-mode-map
        "<f12>" #'flyspell-correct-wrapper
        ;; "M-RET" #'flyspell-correct-wrapper
        "<f10>" #'k/add-word-to-hunspell-personal-dictionary))

;; center text
(leaf olivetti
  :config
  (setq-default olivetti-body-width 80
                olivetti-margin-width 10
                olivetti-style 'fancy)
  (add-hook 'olivetti-mode-on-hook #'toggle-word-wrap)
  (add-hook 'olivetti-mode-off-hook #'toggle-word-wrap)
  (k/general-leader/toggle
    "w" '(olivetti-mode :wk "Distraction-free writing")))

(leaf fzf
  ;; note that I'm not using fzf.el
  :config
  (when (executable-find "fd")
    ;; use `fd' because it understands .gitignore
    ;; use -H to still show hidden directories
    (setenv "FZF_DEFAULT_COMMAND" "fd -H"))
  (k/general-leader/file
    "F" #'counsel-fzf))

;; NOTE: migrated
(leaf unpackaged
  :require unpackaged-subset
  ;; Functions copied from https://github.com/alphapapa/unpackaged.el
  ;; because I don't want to load the entire file.
  :config
  (general-def
    :keymaps (--map (intern (format "%s-map" it)) k/lisp-modes)
    "C-c ^" #'unpackaged/sort-sexps))

;; NOTE: no need to migrate
(leaf hl-todo
  :config (global-hl-todo-mode))

;; NOTE: no need to migrate
(leaf hl-line
  :config
  (general-add-hook '(canrylog-file-mode-hook
                      org-agenda-mode-hook
                      profiler-report-mode-hook)
    #'hl-line-mode))

;; NOTE: migrated
(leaf rainbow-delimiters
  :hook (prog-mode-hook pollen-mode-hook))

(leaf rainbow-mode
  :hook (js-mode-hook web-mode-hook css-mode-hook))

;; NOTE: migrated
(leaf tab-bar
  :config
  (with-eval-after-load 'doom-modeline
    (setq tab-bar-tab-name-function
          (lambda ()
            (abbreviate-file-name (or (doom-modeline--project-root)
                                      (buffer-name))))))
  ;; only show tab bar when there's more than one tab
  ;; show the tab bar and enable C-TAB / C-S-TAB tab switching
  (tab-bar-mode)
  ;; (The mode is enabled in the custom setter, so use
  ;; `setopt' here)
  (setopt tab-bar-show 1)
  ;; The binding... doesn't come back?
  (map!
    "C-<tab>" #'tab-next
    :nv "C-<tab>" #'tab-next
    "C-S-<tab>" #'tab-previous
    "C-<iso-lefttab>" #'tab-previous
    ;; Same shortcut as Blender so we can use the buttons on our mouse
    "C-<next>" #'tab-next
    "C-<prior>" #'tab-previous)
  (k/general-leader/tabs
    "c" (and (fboundp 'tab-duplicate)
             '(tab-duplicate :wk "Clone tab"))
    "d" '(tab-close :wk "Close tab")
    "t" '(tab-bar-switch-to-tab :wk "Go to tab…")
    "o" '(tab-close-other :wk "Delete other tabs")
    "n" '(tab-new :wk "New tab")
    "r" '(tab-rename :wk "Rename this tab")))

(leaf multiple-cursors
  :config
  (general-def
    :prefix "gp"
    :states 'motion
    "h" 'mc/edit-lines
    "j" 'mc/mark-next-like-this
    "k" 'mc/mark-previous-like-this
    "m" 'mc/mark-all-like-this
    "n" 'mc/mark-next-like-this)
  (k/general-leader
   "j" 'mc/mark-next-like-this
   "k" 'mc/mark-previous-like-this)
  (general-def
    "C-S-<mouse-1>" 'mc/add-cursor-on-click))

(leaf highlight-numbers
  :hook prog-mode-hook)

(leaf folding
  :config
  (k/add-hook `(,@(mapcar #'k/symbol-hook k/lisp-modes)
                python-mode-hook
                web-mode-hook
                js-mode-hook
                typescript-mode-hook
                c-mode-common-hook
                css-mode-hook)
    #'hs-minor-mode)
  (k/add-hook 'diff-mode-hook
    #'outline-minor-mode)
  (general-def
    :states 'motion
    "zx" #'k/toggle-hs-outshine))

(leaf apheleia
  :init (apheleia-global-mode)
  :defer-config
  (defun k/format-parinfer (&rest _)
    (parinfer-auto-fix))
  (add-hook 'apheleia-mode-hook
            (k/turn-off-minor-mode aggressive-indent-mode))
  (k/add-hook 'emacs-lisp-mode-hook
    (k/turn-off-minor-mode apheleia-mode))
  ;; This doesn't work with zsh, which is why it's not enabled by
  ;; default.
  (setf (map-elt apheleia-mode-alist 'sh-mode) 'shfmt))

;; NOTE: migrated
(leaf which-key
  :defer 10
  :setq ((which-key-idle-delay . 0.4)
         (which-key-idle-secondary-delay . 0.1)
         (which-key-sort-order . #'which-key-key-order-alpha))
  :defer-config
  (require 'which-key-replacements)
  (general-def
    "C-h W" '(which-key-show-full-major-mode
              :wk "Show major mode keybinds"))
  (which-key-mode))

(k/define-initial-template editorconfig-conf-mode
  "root = true

# Unix-style newlines with a newline ending every file
[*]
end_of_line = lf
insert_final_newline = true

# Matches multiple files with brace expansion notation
# Set default charset
[*.{js,py}]
charset = utf-8
indent_style = space
indent_size = 2
tab_width = 8
trim_trailing_whitespace = true
max_line_length = 80")
(k/define-initial-template lisp-interaction-mode
  ";; -*- mode: lisp-interaction; lexical-binding: t; -*-")

;; NOTE: migrated
(leaf editorconfig
  :config
  (editorconfig-mode))

;; NOTE: migrated
(leaf didyoumean
  :config (didyoumean-mode))

(leaf flymake
  :config
  (general-def
    :keymaps 'flymake-mode-map
    "C-c ! l" #'flymake-show-buffer-diagnostics)
  (leaf flymake-diagnostic-at-point
    :disabled t
    :hook flymake-mode-hook))

(leaf jsonrpc
  :config
  ;; This eliminated constant freezes for me. Turns out those freezes
  ;; are pp-to-string doing its work. Why...?
  ;;
  ;; FIXME: this breaks the truncation code in jsonrpc--log-event.
  (define-advice jsonrpc--log-event (:around (func &rest args)
                                     dont-pretty-print)
    "Could you please not waste time pretty printing an internal value?"
    (cl-letf (((symbol-function 'pp-to-string)
               (lambda (obj)
                 (prin1-to-string obj))))
      (apply func args))))

(leaf abbrev
  :config
  (setq-default abbrev-mode t))

(leaf eldoc-box
  :when-available t
  :config
  (general-def
    "<f1>" #'eldoc-box-help-at-point)
  (add-hook 'lsp-mode-hook #'eldoc-box-hover-mode))

(leaf lsp-mode
  :when-available t
  :config
  (setq lsp-enable-suggest-server-download nil
        lsp-auto-guess-root t
        lsp-completion-provider :capf
        lsp-restart 'ignore
        lsp-log-max nil
        lsp-ui-doc-enable nil
        lsp-signature-render-documentation nil
        ;; "[...] Again the emacs default 4k [sic] is too low
        ;; considering that the some of the language server responses
        ;; are in 800k - 3M range."
        read-process-output-max (* 1024 1024)
        lsp-eldoc-render-all t
        lsp-keymap-prefix "C-c l"
        lsp-idle-delay 0.5
        lsp-disabled-clients '(json-ls html-ls)
        lsp-headerline-arrow
        (propertize "›" 'face 'lsp-headerline-breadcrumb-separator-face))
  (general-add-hook 'lsp-mode-hook
    (list
     #'lsp-enable-which-key-integration
     (defun k//lsp-add-completion-back-to-capf ()
       (add-hook 'completion-at-point-functions
                 #'lsp-completion-at-point nil t))))
  (general-add-hook 'lsp-completion-mode-hook
    (defun k//lsp-set-up-completion-style ()
      (setf (alist-get 'lsp-capf completion-category-defaults)
            '(styles flex))))
  (k/add-hook-not-derived 'js-mode #'lsp-deferred)
  (general-add-hook (-non-nil `(clojure-mode-hook
                                web-mode-hook
                                dart-mode-hook
                                css-mode-hook
                                js2-mode-hook
                                typescript-mode-hook

                                ,@(when (executable-find "gopls")
                                    '(go-mode-hook))))
    ;; Using `lsp-deferred' not only waits until the buffer is visible
    ;; before starting the server, but also ensures that LSP only
    ;; starts *after local variables have been set up*.
    ;;
    ;; The order of `run-mode-hooks' is:
    ;; - `change-major-mode-after-body-hook'
    ;; - `delayed-mode-hooks' (previously delayed hooks)
    ;; - The mode's own hooks (before local variables are set up!)
    ;; - `hack-local-variables'
    ;; ...
    #'lsp-deferred)
  (setq-mode-local clojure-mode
    lsp-enable-indentation nil)
  (defun k/lsp-diagnostic-filter (params workspace)
    (cond ((eq (lsp--client-server-id
                (lsp--workspace-client workspace))
               'ts-ls)
           (ht-update-with!
            params "diagnostics"
            (lambda (xs)
              (->> xs
                   (seq-remove
                    (lambda (x)
                      (eql (lsp:diagnostic-code? x) 2307)))
                   vconcat)))))
    params)
  (setq lsp-diagnostic-filter #'k/lsp-diagnostic-filter)
  (leaf lsp-pyright
    :when (executable-find "pyright")
    :config
    (k/add-hook 'python-mode-hook
      (k:deferred-require 'lsp-pyright
        (lsp-deferred))))
  (leaf lsp-javascript
    :defer-config
    (setopt lsp-clients-typescript-javascript-server-args
            (list "--tsserver-log-file"
                  (expand-file-name "ts-log" user-emacs-directory)
                  "--tsserver-log-verbosity" "off")
            lsp-javascript-implicit-project-config-check-js t))
  (leaf lsp-tailwindcss
    :init (setq lsp-tailwindcss-add-on-mode t)
    :after lsp-mode
    :require t))

(leaf treesit
  :disabled t
  :config
  (setq major-mode-remap-alist
        '((yaml-mode . yaml-ts-mode)
          (typescript-mode . typescript-ts-mode)
          (json-mode . json-ts-mode)
          (css-mode . css-ts-mode)
          (python-mode . python-ts-mode))))

;; NOTE: migrated
(leaf goto-addr
  :hook ((text-mode-hook . goto-address-mode)
         (eshell-mode-hook . goto-address-mode))
  :config
  (general-def :keymaps 'goto-address-highlight-keymap
    "RET" #'goto-address-at-point)
  (general-add-hook '(markdown-mode-hook
                      org-mode-hook)
    (k/turn-off-minor-mode goto-address-mode)))

;; C-l as horizontal bar
;; NOTE: migrated
(leaf page-break-lines
  :config
  (global-page-break-lines-mode)
  (k/add! page-break-lines-modes
    'emacs-news-view-mode
    'emacs-news-mode))

;; NOTE: not migrating
(leaf context-menu
  :when (commandp #'context-menu-mode)
  :config
  ;; I want context menus, but I still want <menu> to be equal to M-x
  (general-unbind
    :keymaps 'context-menu-mode-map
    "<menu>"))

;; NOTE: migrated
(leaf word-wrap
  :config
  (require 'kinsoku nil t)
  (setq word-wrap-by-category t)
  (k/set-mode-local '(org-mode markdown-mode)
    word-wrap t))

;; NOTE: migrated
(leaf input-methods
  :config
  ;; Type "qq" to toggle 英数
  (setq quail-japanese-use-double-n t)
  ;; This would ensure all keypresses get intercepted by the IME,
  ;; but shift doesn't go to the IME for some reason.
  ;; It's not usable as a result, unfortunately.
  ;; (setq x-gtk-use-native-input t)
  (k/general-leader/toggle
    "i" #'toggle-input-method))

;; NOTE: not migrating
(leaf rime
  :init
  (setq rime-show-candidate 'minibuffer
        rime-disable-predicates '(rime-predicate-evil-mode-p)
        rime-translate-keybindings '("C-f" "C-b" "C-n" "C-p" "C-g" "<left>" "<right>" "<up>" "<down>" "<prior>" "<next>" "<delete>")
        rime-commit1-forall t))

;; NOTE: migrated
(leaf display-time
  :config
  (setq display-time-24hr-format t
        display-time-default-load-average nil))

;; Miscellaneous Emacs settings that have nothing to do with sanity
(leaf emacs
  :config
  (general-unbind
    :keymaps '(custom-mode-map special-mode-map)
    "SPC")
  (general-def
    :keymaps 'timer-list-mode-map
    :states 'normal
    "c" #'timer-list-cancel
    "x" #'timer-list-cancel
    "d" #'timer-list-cancel)
  (setq locate-dominating-stop-dir-regexp
        (rx bos
            (or (seq (= 2 (any "/\\"))
                     (+ (not (any "/\\")))
                     (any "/\\"))
                (seq "/storage/emulated/"
                     (+ (not "/"))
                     "/")
                "/net/" "/afs/" "/.../")
            eos))
  ;; Detect CamelCase words. Neat!
  (global-subword-mode)
  (add-to-list 'magic-mode-alist (cons (rx "<?xml version=") #'nxml-mode))
  (setq-default indicate-buffer-boundaries t
                tab-width 4)
  (setq-mode-local emacs-lisp-mode
    ;; Tab-based bundled Emacs Lisp often assume a tab-width of 8
    tab-width 8
    fill-column 80
    display-line-numbers nil)
  (define-advice completing-read
      (:around (func prompt collection &rest args) auto-category)
    "Automatically mark category for COLLECTION based on the predicate."
    (let ((predicate (car args)))
      (apply func prompt
             (if (functionp collection)
                 collection
               (cl-case predicate
                 ((fboundp functionp macrop commandp)
                  (k/mark-category collection 'function))
                 (helpful--variable-p
                  (k/mark-category collection 'variable))
                 (t collection)))
             predicate
             (cdr args))))
  (setf (map-elt fringe-indicator-alist 'continuation) nil
        (map-elt default-frame-alist 'auto-raise) t)
  (setq find-file-visit-truename t
        read-minibuffer-restore-windows nil
        suggest-key-bindings nil
        mouse-yank-at-point t
        x-stretch-cursor t))

(leaf auto-save
  :config
  (setq auto-save-visited-interval 0.5)
  (auto-save-visited-mode)
  (setq
   auto-save-visited-predicate
   (defun k:auto-save-visited-predicate ()
     "Don't auto save after undo."
     (let ((case-fold-search t))
       (and (memq evil-state '(normal emacs))
            (not (string-match-p
                  "undo"
                  (format "%s%s" this-command last-command)))))))
  (defvar k:auto-save-visited-turned-off-for-undo-tree nil
    "Global marker for if `auto-save-visited-mode' is turned off for `undo-tree-visualizer'.")
  (k/add-hook 'post-command-hook
    (defun k:manage-auto-save-visited-mode-for-undo-tree ()
      "Manage `auto-save-visited-mode' state for undo tree visualizer."
      (cond
       ((eq major-mode 'undo-tree-visualizer-mode)
        (auto-save-visited-mode -1)
        (setq k:auto-save-visited-turned-off-for-undo-tree t)
        (message "Turned off `auto-save-visited-mode' in undo-tree-visualizer session. Leave the buffer to turn it back on"))
       (t
        (when (and k:auto-save-visited-turned-off-for-undo-tree
                   (not auto-save-visited-mode))
          (setq k:auto-save-visited-turned-off-for-undo-tree nil)
          (auto-save-visited-mode)
          (message "Turned `auto-save-visited-mode' back on")))))))

(leaf woman
  :commands woman
  :config
  (when-let ((usr (getenv "PREFIX")))
    (setq woman-manpath
          (--map (s-replace "/usr" usr it)
                 woman-manpath))))

;; NOTE: migrated
(leaf menu-bar
  :when (fboundp #'menu-bar-mode)
  :config
  (k/general-leader/toggle
    "m" '(menu-bar-mode :wk "Menu bar")))

;; NOTE: migrated
(leaf bookmark
  :init
  (setq bookmark-save-flag 1)
  (k/general-leader/file
    "b" '(consult-bookmark :wk "Bookmarks")))

;; NOTE: migrated
(leaf sanity
  ;; name inspired by https://github.com/rougier/elegant-emacs
  :config
  ;; In Emacs 28, `find-library' lists "./", a bunch of "README.md"s,
  ;; .elc, .texi, etc. for some reason. This patches that.
  ;;
  ;; This should perhaps wrap around `locate-file-completion-table' to
  ;; take effect for all functions that use this completion table, but
  ;; as `find-library' is the only one I use it's probably more
  ;; efficient to only run this wrapper once.
  (define-advice read-library-name (:around (func) exclude-irrelevant)
    "Only show files matching value returned from `find-library-suffixes'."
    (cl-letf* ((suffix (concat (regexp-opt (find-library-suffixes) t)
                               "\\'"))
               (orig (symbol-function 'file-name-all-completions))
               ((symbol-function 'file-name-all-completions)
                (lambda (file directory)
                  (--filter
                   (string-match suffix it)
                   (funcall orig file directory)))))
      (funcall func)))
  (leaf fix-ffap-on-iso8601
    :after ffap tramp ; this code should run after ffap and tramp
    :require t)
  (when (eq system-type 'windows-nt)
    ;; `set-locale-environment' will mess things up because Windows
    ;; doesn't support UTF-8 properly. This *should* be good though.
    (set-language-environment "UTF-8"))
  ;; This also applies to "A and B are the same file" when
  ;; `find-file-visit-truename' is non-nil.
  (setq-default save-silently t)
  ;; `files--message' isn't actually silent --- it just clears the echo area
  ;; immediately after. The message still shows for a split second.
  (define-advice files--message (:around (orig format &rest args) actually-be-silent)
    "Actually don't show in echo area if `save-silently' is non-nil."
    (let ((inhibit-redisplay t))
      (apply orig format args)))
  (define-advice write-region (:around (orig &rest args) actually-be-silent)
    "Actually don't show in echo area if `save-silently' is non-nil."
    (let ((inhibit-message save-silently))
      (apply orig args)))
  (setq-default
   create-lockfiles nil
   ;; Copy elsewhere -> copy in Emacs -> the text copied from
   ;; elsewhere is overwritten without being saved into `kill-ring'.
   ;;
   ;; Setting this to t makes Emacs first save the text copied from
   ;; elsewhere (the "interprogram paste") into `kill-ring' so that
   ;; it's not lost.
   ;;
   ;; WHY IS THIS NOT DEFAULT?
   save-interprogram-paste-before-kill t
   ;; save backups and autosaves to system temp dir
   auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
   backup-directory-alist `((".*" . ,temporary-file-directory))
   ;; make sure Custom never looks at an existing file
   custom-file (make-temp-file "emacs-custom")
   ;; If I decide to load a theme, it serves no purpose to remind me
   ;; that I need to trust it.
   custom-safe-themes t
   delete-by-moving-to-trash t
   ;; If I do know what a command does, why would I need a confirmation?
   ;; If I don't know what a command does, how would I know if I want
   ;; to use it or not?
   disabled-command-function nil
   ;; Never abbreviate.
   ;; If it's too slow I'll cancel it myself.
   ;; Abbreviation doesn't speed up pretty printing anyways.
   eval-expression-print-level nil
   eval-expression-print-length nil
   print-level nil
   print-length nil
   frame-title-format "%b - GNU Emacs" ; buffer name
   frame-resize-pixelwise t
   inhibit-compacting-font-caches t
   inhibit-x-resources t
   large-file-warning-threshold (* 100 1024 1024)
   ;; Don't do automatic line breaking to code.
   comment-auto-fill-only-comments t
   ;; This being t would force dates to be between 1970-01-01 and
   ;; 2038-01-01. I don't know how useful this would be right now
   ;; (2020-12-05) with 2038 being 17 years away, but I don't want to
   ;; be bitten by this.
   ;;
   ;; The docstring says "Currently this setting is not recommended
   ;; because the likelihood that you will open your Org files in an
   ;; Emacs that has limited date range is not negligible." As this
   ;; option was added in Emacs 24.1 (and my config requires Emacs 27
   ;; anyways) I'm pretty sure it is negligible for me.
   org-read-date-force-compatible-dates nil
   mouse-drag-and-drop-region t
   require-final-newline t
   ;; why ask me when you know I haven't modified the buffer?
   revert-without-query (list ".*")
   ;; This being nil means to use the "default", which on Termux means
   ;; vibrate. Set to `ignore' to ensure it never does that. (The
   ;; meaning of its value can be seen in its `custom-type' property.)
   ring-bell-function #'ignore
   ;; don't force double space on me.
   sentence-end-double-space nil
   tab-always-indent 'complete
   ;; Never use the "graphical" dialog box.
   use-dialog-box nil
   vc-follow-symlinks t
   warning-minimum-level :emergency
   widget-image-enable nil) ; widget images are ugly
  :config
  (delete-selection-mode)
  (column-number-mode)
  ;; FIXME: IMO savehist should attempt to ignore entries that cannot be loaded
  ;; properly. I had evil-jump-history being saved with circular values that
  ;; ... for some reason failed to read even though read-circle is t. I'm not
  ;; sure what happened there.
  (savehist-mode)
  ;; <rant>
  ;; I don't need a keybinding just to see NEWS...
  ;; Or just to visit the GNU website...
  ;; That should be a default bookmark, not a default keybind...
  ;; </rant>
  (general-unbind :keymaps '(ehelp-map help-map)
    "C-n" "n" "g"
    "h")
  ;; I use 0 to move back to BOL
  (general-unbind
    :keymaps 'view-mode-map
    :states 'normal
    "0")
  ;; This literally just runs the same command as TAB. Give
  ;; `tab-bar-mode' this binding.
  (with-eval-after-load 'magit-section
    (general-unbind
      :keymaps 'magit-section-mode-map
      "C-<tab>" "<normal-state> C-<tab>"))
  ;; Heading demoting and promoting are already provided by `evil-org'
  ;; through << and >> commands.
  (general-unbind
    :keymaps 'org-mode-map
    "M-h" "M-l" "M-<left>" "M-<right>")
  (general-unbind
    :keymaps 'org-mode-map
    :states 'normal
    "M-h" "M-l" "M-<left>" "M-<right>")
  (general-unbind
    :keymaps 'outline-mode-map
    :states 'normal
    "M-j" "M-k")
  ;; outline-mode defines ^ to jump to the closest header. overwrite that.
  (general-def
    :keymaps 'outline-mode-map
    :states 'normal
    "^" 'evil-first-non-blank)
  (general-unbind
    ;; suspend-frame
    "C-x C-z"
    ;; disable facemenu
    "M-o")
  (setq use-short-answers t)
  (with-eval-after-load 'org-agenda
    (defun k/org-agenda-todo-not-broken ()
      "Running `org-agenda-todo' sometimes don't actually advance an item with a repeater.

I DON'T KNOW WHY."
      (interactive)
      (org-agenda-show)
      (other-window 1)
      (org-todo)
      (other-window 1))
    (define-key org-agenda-mode-map
                [remap org-agenda-todo] #'k/org-agenda-todo-not-broken)))

;; NOTE: migrated
(leaf image-mode
  :config
  (define-advice image-mode (:around (func))
    "Open image externally if the display don't support images."
    (if (display-images-p)
        (call-interactively func)
      (org-open-file (buffer-file-name))))
  (general-def
    :keymaps 'image-mode-map
    :states 'motion
    "+" #'image-increase-size
    "-" #'image-decrease-size))

;; NOTE: migrated
(leaf recentf
  :config
  ;; or `nil' for unlimited
  (setq recentf-max-saved-items 10000)
  (recentf-mode))

;; NOTE: migrated
(leaf paren
  :config
  (setopt show-paren-delay 0
          show-paren-when-point-inside-paren t
          show-paren-context-when-offscreen 'overlay
          blink-matching-paren t)
  (show-paren-mode))

;; NOTE: migrated
(leaf whitespace
  :config
  (global-whitespace-mode)
  (setq-default whitespace-style '(face tabs newline space-mark tab-mark newline-mark)
                whitespace-display-mappings '(((newline-mark 10 [?¬ 10])
                                               (tab-mark 9 [?▷ 9] [?\\ 9])))
                show-trailing-whitespace nil
                indent-tabs-mode nil)
  ;; don't touch whitespace if I'm still in insert state
  (advice-add 'delete-trailing-whitespace :around
              (lambda (func &rest args)
                (unless (eq evil-state 'insert)
                  (apply func args))))
  (general-add-hook '(text-mode-hook
                      ledger-mode-hook)
    (lambda ()
      (add-hook 'before-save-hook
                #'delete-trailing-whitespace nil t)))
  (define-minor-mode k/show-trailing-whitespace-mode
    "`show-trailing-whitespace' as a minor mode."
    :global nil
    (k-update show-trailing-whitespace #'not))
  (general-add-hook '(org-mode-hook
                      ledger-mode-hook
                      prog-mode-hook)
    #'k/show-trailing-whitespace-mode))

;;;;;; Commands

;; NOTE: migrated
(leaf link-hint
  :config
  (k/general-leader
   "o" '(link-hint-open-link :wk "Open a link")
   "O" '(link-hint-copy-link :wk "Copy a link"))
  (defun k/link-hint-process-text-url (url &rest _)
    "Process text URL before opening it."
    (let ((url (link-hint--process-url url nil)))
      (save-match-data
        (when (string-match "svn://anonsvn.kde.org.*@\\(.*\\)" url)
          (k--update url
            (format "https://websvn.kde.org/?view=revision&revision=%s"
                    (match-string 1 it))))
        url)))
  (put 'link-hint-text-url :parse #'k/link-hint-process-text-url))

;; NOTE: migrated
(leaf dumb-jump
  :setq ((dumb-jump-selector . 'completing-read))
  :config
  (add-hook 'xref-backend-functions
            #'dumb-jump-xref-activate))

;; NOTE: migrated
(leaf unfill
  :init
  (general-def
    ;; unfill-paragraph, unfill-region
    ;; Maybe bind M-S-q instead of toggling
    "M-q" #'unfill-toggle))

;;;;;; Misc.

(leaf yasnippet
  :commands
  yas-new-snippet yas-insert-snippet yas-visit-snippet-file yas-expand
  ivy-yasnippet
  :defer 10
  :init
  (setq yas-wrap-around-region t)
  (k/general-leader/yasnippet
    "n" #'yas-new-snippet
    "i" #'ivy-yasnippet
    "v" #'yas-visit-snippet-file
    "e" #'yas-expand)
  :defer-config
  (advice-add 'ivy-yasnippet--preview :override #'ignore)
  (--each (list [(tab)]
                (kbd "TAB")
                (kbd "<tab>"))
    (define-key yas-minor-mode-map
                it nil))
  (require 'common-lisp-snippets)
  (require 'project-local-yasnippets)
  (setq project-local-yasnippets/dbg? nil)
  ;; Only start the minor mode when we need it; I only use yasnippet
  ;; through the above commands
  (yas-global-mode)
  ;; (general-unbind :keymaps 'yas-minor-mode-map
  ;;   "TAB" "<tab>")
  ;; "3 is too chatty about initializing yasnippet. 2 is just right
  ;; (only shows errors)." --- doom-emacs
  (k/remove! yas-prompt-functions
    'yas-dropdown-prompt)
  (setq yas-verbosity 2))

(leaf Info
  :config
  (add-hook 'Info-mode-hook #'olivetti-mode)
  (leaf info-variable-pitch
    :hook 'Info-mode-hook)
  ;; I replace evil-collection's binding for Info because "/" and "n"
  ;; and "?" etc. don't work as I'd expect.
  (with-eval-after-load 'evil
    ;; This adapts normal state for view (read only) modes
    (evil-collection-set-readonly-bindings 'Info-mode-map)
    (evil-set-initial-state 'Info-mode 'normal)
    (general-define-key
     :keymaps 'Info-mode-map
     :states 'normal
     "gg" #'evil-goto-first-line
     "[[" #'Info-prev
     "]]" #'Info-next
     ;; Like {forward,backward}-button
     "<tab>" #'Info-next-reference
     "S-<tab>" #'Info-prev-reference
     "RET" #'Info-follow-nearest-node
     "H" #'Info-history-back
     "L" #'Info-history-forward
     "gh" #'Info-backward-node
     "gl" #'Info-forward-node
     "u" #'Info-up
     "t" #'Info-top-node
     "T" #'Info-toc
     "gi" #'Info-menu ; The menu is like imenu
     "g?" (list (concat (string ?\C-h ?f)
                        "Info-mode"
                        (string ?\C-m))
                :wk "About Info")
     [mouse-2] #'Info-mouse-follow-nearest-node
     [follow-link] 'mouse-face
     [double-wheel-left]  'Info-history-back
     [double-wheel-right] 'Info-history-forward))
  (setq-mode-local Info-mode display-line-numbers nil))

;; NOTE: migrated
(leaf shell
  :config
  ;; Set SHELL to bash
  (when-let ((bash (executable-find "bash")))
    (setenv "SHELL" bash)
    (setq-default explicit-shell-file-name bash
                  shell-file-name bash)))

;; NOTE: migrated
(leaf yasearch
  :after evil
  :init
  (k/general-g*
    "b" #'yasearch-operator)
  (k/general-leader
   "s" #'yasearch)
  :config
  (setq
   yasearch-engine-alist
   '((dictgoo . "https://dictionary.goo.ne.jp/srch/all/%s/m0u/")
     (dictwebliocjjc
      . "https://cjjc.weblio.jp/content_find?searchType=exact&query=%s")
     (wikipedia-en . "https://en.wikipedia.org/w/index.php?search=%s")
     (wikipedia-zh . "https://zh.wikipedia.org/w/index.php?search=%s")
     (wikipedia-ja . "https://ja.wikipedia.org/w/index.php?search=%s")
     (kemdict . "https://kemdict.com/search?q=%s")
     (kotobank
      . "https://kotobank.jp/word/%s")
     (dictweblio
      . "https://www.weblio.jp/content_find?query=%s&searchType=exact")
     (google . "https://google.com/search?q=%s")
     (duckduckgo . "https://duckduckgo.com/?q=%s")
     (wiktionary-zh . "https://zh.wiktionary.org/w/index.php?search=%s")
     (wiktionary-en . "https://en.wiktionary.org/w/index.php?search=%s")
     (wiktionary-ja . "https://ja.wiktionary.org/w/index.php?search=%s")
     (jsl-nhk . "https://www2.nhk.or.jp/signlanguage/index.cgi?md=search&tp=page&qw=%s"))
   yasearch-browser-function #'eww-browse-url))

;; NOTE: migrated
(leaf helpful
  :setq (helpful-switch-buffer-function . #'k/pop-to-buffer)
  :config
  (let ((dir "~/git/emacs/"))
    (when (f-exists? dir)
      (setq find-function-C-source-directory
            (f-join dir "src"))))
  (setq helpful-max-highlight 50000
        helpful-max-buffers nil
        counsel-describe-function-function
        #'helpful-callable
        counsel-describe-variable-function
        #'helpful-variable)
  (general-def
    "C-h k" #'helpful-key
    "C-h f" #'helpful-callable
    "C-h v" #'helpful-variable
    "C-h o" #'helpful-symbol
    "C-h c" #'describe-char)
  (general-def :keymaps 'embark-symbol-map
    "h" #'helpful-symbol))

(leaf elec-pair
  :config
  (electric-pair-mode)
  :defer-config
  (k/add-hook 'parinfer-mode-hook
    (k/turn-off-minor-mode electric-pair-local-mode))
  (--each (list "「」"
                "『』"
                "《》")
    (cl-pushnew (cons (elt it 0)
                      (elt it 1))
                electric-pair-text-pairs
                :test #'equal)
    (cl-pushnew (cons (elt it 0)
                      (elt it 1))
                electric-pair-pairs
                :test #'equal)))

;; Still have to learn how to use this
(leaf puni
  :config
  (general-def
    :keymaps 'puni-mode-map
    "M-h" #'puni-slurp-backward
    "M-j" #'puni-barf-forward
    "M-k" #'puni-barf-backward
    "M-l" #'puni-slurp-forward)
  (puni-global-mode)
  (general-add-hook '(term-mode-hook vterm-mode-hook)
    #'puni-disable-puni-mode))

(leaf smartparens
  :require t smartparens-config
  :disabled t
  :config
  (sp-pair "「" "」")
  (with-eval-after-load 'parinferlib
    (--each (map-keys parinferlib--PARENS)
      (ignore-errors
        (sp-local-pair k/lisp-modes it nil :actions nil))))
  (smartparens-global-mode)
  (general-def :states 'visual :keymaps 'smartparens-mode-map
    "(" #'sp-wrap-round
    ")" #'sp-wrap-round
    "[" #'sp-wrap-square
    "]" #'sp-wrap-square
    "{" #'sp-wrap-curly
    "}" #'sp-wrap-curly
    "\"" (k/expose (-partial #'sp-wrap-with-pair "\""))))

;; Maybe switch to parinfer-rust-mode, this isn't even on MELPA anymore
;; NOTE: migrated
(leaf parinfer
  :defer-config
  (general-def
    :keymaps 'parinfer-mode-map
    "M-p" #'parinfer-toggle-state)
  (k/general-mode-leader
   :keymaps 'parinfer-mode-map
   "p" #'parinfer-toggle-state)
  (setq parinfer-extensions '(defaults pretty-parens smart-yank smart-tab)
        parinfer-change-to-indent-state-automatically nil))

(leaf atomic-chrome
  :when-available t
  :require t
  :config (atomic-chrome-start-server))

;;;;;; Apps

(leaf lorum-ipsum
  :config
  (setq-default lorem-ipsum-sentence-separator " "))

(leaf pangu-spacing
  :disabled t
  :init (general-add-hook '(org-mode-hook markdown-mode-hook) #'pangu-mode))

(leaf smerge
  :init
  (k/define-command-palette k/smerge
    "Prompt for an smerge command."
    :prompt "SMerge command: "
    :commands smerge-mode-map)
  (general-def
    :keymaps 'smerge-mode-map
    "C-c ^" #'k/smerge))

(defun k/devdocs--get-icon (slug)
  "Return the path to the icon for SLUG; download if needed.

Icons are saved in \"`devdocs-data-dir'/.icons/\"."
  (let ((icon-dir
         ;; devdocs.el uses the list of non-hidden paths in
         ;; `devdocs-data-dir' as the list of installed documents in
         ;; multiple places, so we must either put it elsewhere or in
         ;; a hidden folder.
         (f-join devdocs-data-dir ".icons"))
        (icon-slug
         ;; The "~" denotes the version. Slug without version is how
         ;; the icons are stored in devdocs's repository:
         ;; https://github.com/freeCodeCamp/devdocs/tree/main/public/icons/docs
         (car (s-split "~" slug))))
    (make-directory icon-dir t)
    (let ((path (f-join icon-dir (format "%s.png" icon-slug))))
      (unless (f-exists? path)
        (url-retrieve
         (format (concat "https://raw.githubusercontent.com"
                         "/freeCodeCamp/devdocs"
                         "/main/public/icons/docs/%s/16.png")
                 icon-slug)
         (lambda (status)
           (unless (plist-get status :error)
             (goto-char (point-min))
             ;; this also skips past the headers, which is what we're after here
             (eww-parse-headers)
             (let ((coding-system-for-write 'no-conversion))
               (write-region (point) (point-max) path))
             (message "Written %s" path)))))
      path)))

(defun k/devdocs--read-document (prompt &optional multiple available)
  "A prettier version of `devdocs--read-document'.

PROMPT: passed to `completing-read'
MULTIPLE: allow selecting multiple documents
AVAILABLE: offer all documents that can be downloaded

Return the selected document metadata alist."
  (let* ((docs (if available
                   (devdocs--available-docs)
                 (or (devdocs--installed-docs)
                     (user-error "No documents in `%s'" devdocs-data-dir))))
         (cands (seq-map
                 (lambda (it)
                   (let-alist it
                     (let (icon-path
                           (icon ""))
                       (when (display-graphic-p)
                         (setq icon-path (k/devdocs--get-icon .slug))
                         (unless (f-exists? icon-path)
                           ;; Yes, this is the placeholder image, for
                           ;; some reason. Notice how JSDoc, Enzyme,
                           ;; Koa etc. that don't have an icon all
                           ;; actually use the same
                           ;; background-position as Mongoose on
                           ;; devdocs.io. (Sidebar icons on devdocs.io
                           ;; are set to the same image containing all
                           ;; sprites, then selected with
                           ;; background-position.)
                           (setq icon-path (k/devdocs--get-icon "mongoose")))
                         (setq icon (concat
                                     (if (f-exists? icon-path)
                                         (propertize
                                          " "
                                          'display (create-image icon-path nil nil
                                                                 :width '(1 . em)
                                                                 :margin 2))
                                       "")
                                     (propertize
                                      " "
                                      'display '(space :align-to (36))))))
                       (cons (if (string-equal
                                  (downcase .name)
                                  (downcase .slug))
                                 (format "%s%s" icon .name)
                               (format "%s%s (%s)" icon .name .slug))
                             it))))
                 docs)))
    (if multiple
        (delq nil (mapcar (lambda (s) (cdr (assoc s cands)))
                          (completing-read-multiple prompt cands)))
      (cdr (assoc (completing-read prompt cands nil t) cands)))))

(leaf devdocs
  :init
  (k/define-command-palette k/devdocs
    "devdocs"
    :prompt "Devdocs command: "
    :commands '(devdocs-lookup
                devdocs-delete
                devdocs-install
                devdocs-update-all
                devdocs-peruse))
  (general-def
    "C-h d" #'devdocs-lookup
    "C-h D" #'k/devdocs)
  :config
  (general-add-hook 'devdocs-mode-hook
    (list (k/set-x-to-val word-wrap nil))
    (lambda () (toggle-truncate-lines -1)))
  (advice-add 'devdocs--read-document :override #'k/devdocs--read-document))

(leaf eww
  :init
  (k/define-command-palette k/eww-commands
    "EWW commands."
    :prompt "EWW command: "
    :commands (let ((cmds))
                (mapatoms
                 (lambda (atom)
                   (when (and (commandp atom)
                              (s-prefix? "eww" (symbol-name atom)))
                     (unless (memq atom '(eww eww-mode eww-buffers-mode))
                       (push atom cmds)))))
                cmds))
  (general-def
    :keymaps 'eww-mode-map
    "<M-menu>" #'k/eww-commands)
  ;; Leave it to evil-visual-char
  (general-unbind
    :keymaps '(eww-link-keymap eww-image-link-keymap shr-image-map shr-map)
    "v" "w"))

(leaf altcs
  :require t
  :config
  (altcs-mode)
  (general-def :keymaps 'altcs-mode-map
    "C-j" #'altcs-choose))

(leaf kisaragi-apps
  :require t
  :config
  (kisaragi-apps-register "DiG (DNS lookup utility)" #'dig)
  (kisaragi-apps-register "Eshell" #'eshell)
  (k/general-leader/apps
    "a" #'kisaragi-apps
    "A" '(awk-ward :wk "WIP Awk editing environment")
    ;; just find-file a directory
    ;; "d" #'dired
    "n" #'deft
    "o" #'org-agenda
    "p" #'pacfiles
    "P" #'proced
    "s" #'suggest))

(leaf yabridgectl
  :when-available t
  :init
  (kisaragi-apps-register "yabridgectl" #'yabridgectl))

(leaf profiler
  :init
  (k/define-command-palette k/profiler
    "Profiler commands."
    :prompt "Profiler command: "
    :commands '(profiler-start
                profiler-stop
                profiler-report
                profiler-reset))
  (kisaragi-apps-register "Profiler" #'k/profiler))

(leaf speed-type
  :unless k/android?
  :init
  (k/for-in (str func)
      '(("Typing practice (buffer)" speed-type-buffer)
        ("Typing practice (builtin text)" speed-type-text)
        ("Typing practice (top words)" speed-type-top-1000))
    (kisaragi-apps-register str func)))

(leaf docker
  :when-available t
  :init (kisaragi-apps-register "Docker" #'docker))

(leaf kisaragi-log
  :config
  (setq kisaragi-log-data-file
        (f-join k/notes/ "kisaragi-log.json")
        kisaragi-log-types
        '(("water" ((:amounts . (350 600))
                    (:target . 1500)))
          ("typing speed" ((:amounts . (65 70 80))
                           (:target . 80)
                           (:skip-zero . t)))))
  (k/general-leader/apps
    "h" '(kisaragi-log :wk "Tracker for stuff")))

(leaf proced
  :init (kisaragi-apps-register "Process Editor" #'proced))

(leaf magit
  ;; If there's a magit blob buffer open (like file.x<master~1>), update
  ;; it when moving in the log buffer
  ;; https://www.reddit.com/r/emacs/comments/bcpexy/magit_how_to_quickly_view_the_history_of_a_file/ekt41mg/
  :hook ((magit-section-movement-hook . magit-log-maybe-update-blob-buffer))
  :commands magit-inside-worktree-p
  :init
  (setq magit-commit-ask-to-stage 'stage
        magit-no-confirm '(stage-all-changes unstage-all-changes)
        magit-diff-refine-hunk 'all)
  (k/general-leader/git
    "s" '(magit-status :wk "Status")
    "i" '(magit-init :wk "Init")
    "g" '(magit-file-dispatch :wk "+Magit file commands")
    "G" '(magit-dispatch :wk "+Magit commands")
    "C" '(k/git-commit-everything
          :wk "Commit every change")
    "c" '(k/git-commit
          :wk "Commit with generic message")
    "a" '(k/git-commit-this-file-with-message
          :wk "Commit this (prompt)")
    "A" '(k/git-stage-everything
          :wk "Commit every change")
    "j" `(,(lambda ()
             (interactive)
             (require 'magit)
             (magit-run-git-with-editor "pull"))
          :wk "Pull")
    "k" `(,(lambda ()
             (interactive)
             (require 'magit)
             (magit-run-git-async "push"))
          :wk "Push"))
  :config
  (general-def
    :keymaps 'magit-section-mode-map
    :states 'normal
    "gh" #'magit-section-up)
  (general-define-key
   :keymaps '(magit-log-mode-map)
   :states 'normal
   "C-<return>" #'k/magit-log-visit-changed-file)
  ;; I'm used to Git in English, especially in Magit.
  (--each '("LANG=en_US.UTF-8"
            "LANGUAGE=en_US")
    (cl-pushnew it magit-git-environment :test #'equal))
  ;; Unbind `evil-magit-toggle-text-mode' on C-t. It's still provided
  ;; on \, and IMO is more consistent with Evil that way.
  (general-unbind
    :keymaps '(evil-collection-magit-toggle-text-minor-mode-map
               magit-mode-map)
    :states 'normal
    "C-t")
  ;; NOTE: migrated till here
  (leaf magit-delta
    :when-available t
    :hook magit-mode-hook)
  (leaf magit-svn
    :hook magit-mode-hook
    :config
    (evil-collection-define-key
      (if evil-collection-magit-use-y-for-yank
          `(,evil-collection-magit-state visual)
        `(,evil-collection-magit-state))
      'magit-mode-map
      "N" #'magit-svn)
    (transient-insert-suffix 'magit-dispatch "M"
      '("N" "git-svn" magit-svn)))
  (leaf git-gutter
    :require t
    :config
    (cl-pushnew 'focus-in-hook git-gutter:update-hooks)
    (cl-pushnew #'ivy-switch-buffer git-gutter:update-commands)
    ;; `git-gutter-mode' isn't turned on if a buffer isn't visiting a
    ;; file or if its major mode is in this list.
    (setq git-gutter:disabled-modes '(image-mode))
    (global-git-gutter-mode)
    (k/general-leader/git
      "h" '(:ignore t :wk "Hunks")
      "hS" '(git-gutter:statistic :wk "Unstaged stats")
      "hx" '(git-gutter:revert-hunk :wk "Revert this")
      "hj" '(git-gutter:next-hunk :wk "Next")
      "hk" '(git-gutter:previous-hunk :wk "Previous")
      "hs" '(git-gutter:stage-hunk :wk "Stage")))
  (leaf magit-todos
    :after org)
  :defer-config
  (setq magit-module-sections-nested nil)
  (k/add-hook [magit-status-sections-hook 90]
    #'magit-insert-modules-overview))

(leaf eshell
  :defer-config
  (require 'em-smart)
  (require 'em-tramp)
  (setq eshell-prompt-function
        (lambda ()
          (concat (abbreviate-file-name (eshell/pwd))
                  (if (= (user-uid) 0) "\n# " "\n$ ")))
        ;; only needs to match last line of multi-line prompt
        eshell-prompt-regexp "[#$] "
        eshell-ls-use-colors t
        eshell-history-size 1024
        eshell-hist-ignoredups t
        eshell-destroy-buffer-when-process-dies t
        eshell-where-to-jump 'begin
        eshell-review-quick-commands nil
        eshell-smart-space-goes-to-end t))

;; NOTE: migrated
(leaf taskrunner
  :config
  (defvar k/taskrunner-switch-buffer-hist)
  (defun k/taskrunner-switch-buffer ()
    "Switch to a taskrunner buffer."
    (interactive)
    (let ((bufs (->> (buffer-list)
                     (--filter
                      (s-matches? "\\*taskrunner" (buffer-name it)))
                     (-map #'buffer-name))))
      (pcase (length bufs)
        (0 (message "No taskrunner buffers to switch to"))
        (1 (switch-to-buffer (car bufs)))
        (_ (switch-to-buffer
            (completing-read "Switch to taskrunner buffer: "
                             bufs
                             nil t nil
                             'k/taskrunner-switch-buffer-hist))))))
  (defun k-ivy:taskrunner (reset-cache)
    "Like `ivy-taskrunner' except reset cache when RESET-CACHE is non-nil."
    (interactive "P")
    (when reset-cache
      (taskrunner-invalidate-tasks-cache))
    (ivy-taskrunner))
  (k/general-leader
   "l" '(:ignore t :wk "Taskrunner")
   "ll" '(k-ivy:taskrunner :wk "Taskrunner")
   "lb" '(k/taskrunner-switch-buffer :wk "Switch to a taskrunner buffer")))

;; NOTE: migrated
(leaf vterm
  :when-available t
  :init
  (kisaragi-apps-register "Vterm" #'vterm)
  ;; don't ask, just compile
  (setq vterm-always-compile-module t
        vterm-buffer-name-string "*vterm: %s*")
  :config
  ;; banish ansi-term :)
  (defalias 'ansi-term (lambda (&rest _) (call-interactively #'vterm)))
  (setq vterm-shell (executable-find "zsh"))
  (general-def
    :keymaps 'vterm-mode-map
    :states 'normal
    "gk" #'vterm-previous-prompt
    "gj" #'vterm-next-prompt))

;; NOTE: migrated
(leaf trashed
  :init
  (kisaragi-apps-register "View system trash" #'trashed))

;; NOTE: migrated
(leaf canrylog
  :init
  (kisaragi-apps-register "Canrylog" #'canrylog)
  (setq canrylog-file (f-join k/timetracking/ "tracking.canrylog")
        canrylog-db-file (f-join k/timetracking/ "canrylog.db")
        calendar-week-start-day 1)
  :require t
  :config
  (let (queue) ; alist of file -> timer
    (defun k/git-commit-and-push-in-5min ()
      "Commit and push this file after 5 minutes."
      (let ((f (buffer-file-name)))
        (when f
          (unless (assoc f queue)
            (push
             (cons (buffer-file-name)
                   (run-at-time 300 nil
                                (lambda (buffer file)
                                  (with-current-buffer buffer
                                    (k/git-commit-this-and-push)
                                    (setq queue (map-delete queue file))))
                                (current-buffer) f))
             queue))))))
  (with-eval-after-load 'kisaragi-extra-functions
    (add-hook 'canrylog-after-save-hook #'k/git-commit-and-push-in-5min))
  (add-hook
   'canrylog-before-switch-task-hook
   (byte-compile
    (lambda ()
      (let ((default-directory (f-dirname canrylog-file)))
        (shell-command "git pull")))))
  (k/general-leader
   "r" #'canrylog-dispatch))

(leaf suggest
  :init (kisaragi-apps-register "Suggest" #'suggest)
  :config
  (setq-mode-local suggest-mode show-trailing-whitespace nil))

(leaf kisaragi-link
  :commands k/insert-link/double-key
  :after org)

(leaf dired
  :hook ((dired-mode-hook . dired-hide-details-mode))
  :setq ((dired-auto-revert-buffer . t)
         (dired-listing-switches . "-avhl")
         (dired-hide-details-hide-symlink-targets . nil))
  :config
  (require 'dired-aux)
  (add-to-list 'dired-compress-files-alist
               '("\\.7z\\'" . "7z a %o %i"))
  (leaf dired-async
    ;; from `async.el'.
    :require async dired-async
    :bind (dired-mode-map
           ;; There is never a reason to block Emacs when we can avoid it.
           ;; This is actually built into Dired.
           ("!" . dired-do-async-shell-command))
    :config (dired-async-mode))
  (leaf dired-open
    :require t
    :config
    (setq dired-open-extensions
          (leaf-normalize-list-in-list
           '((("wav" "ogg" "mkv" "mp4" "pdf") . "xdg-open")
             (("png" "jpg" "exr") . "eog"))
           :dotlist)))
  (k/add-hook 'dired-mode-hook
    #'diredfl-mode
    ;; #'dired-show-readme-mode
    #'dired-auto-readme-mode
    #'hl-line-mode
    #'variable-pitch-mode)
  (leaf dired-narrow
    :config
    (k/general-mode-leader
     :keymaps 'dired-mode-map
     "n" '(:ignore t :wk "dired-narrow")
     "nn" '(dired-narrow-fuzzy :wk "Filter (fuzzy)")
     "nr" '(dired-narrow-regexp :wk "Filter by regexp")
     "ns" '(dired-narrow :wk "Filter by string")))
  (k/general-mode-leader
   :states 'normal
   :keymaps 'dired-mode-map
   "r" 'dired-toggle-read-only)
  (general-def
    :keymaps 'dired-mode-map
    :states 'normal
    "a" #'dired-toggle-read-only
    "C-<return>" #'dired-open-xdg
    ")" #'dired-git-info-mode
    ", o" #'dired-omit-mode))

(leaf awk-ward
  :init (kisaragi-apps-register "Awk-ward.el" #'awk-ward))

;; NOTE: migrated
(leaf pacfiles-mode
  :when-available t
  :init (kisaragi-apps-register "Pacfiles" #'pacfiles))

;;;;;; Languages / Major modes

(require 'generic-x)
(require 'eel2)

;; NOTE: migrated
(leaf pkgbuild-mode
  :config
  (setopt pkgbuild-update-sums-on-save nil)
  (k/add-hook 'pkgbuild-mode-hook
    (lambda ()
      (remove-hook 'flymake-diagnostic-functions #'sh-shellcheck-flymake t))))

;; NOTE: migrated
(leaf gitignore-mode
  :mode (("\\.\\(?:eslint\\|prettier\\)ignore\\'")))

(leaf ini-mode
  :mode (("\\.npmrc\\'" . ini-mode)))

(leaf dart-mode
  :when-available t)

(leaf rustic
  :when-available t)

(leaf k-po-mode
  :require k-po-mode
  :defer-config
  (evil-set-initial-state 'k-po-mode 'normal)
  (evil-collection-set-readonly-bindings 'k-po-mode-map)
  (setq k-po-auto-update-file-header nil)
  (general-def
    :keymaps 'k-po-mode-map
    :states 'normal
    "RET" #'k-po-edit-msgstr
    "C-c '" #'k-po-edit-msgstr
    "C-c C-c" #'k-po-edit-msgstr
    "i" #'k-po-edit-msgstr
    "TAB" #'k-po-toggle-fuzzy))

(leaf cc-mode
  :config
  (require 'init-cc))

;; NOTE: migrated
(leaf make
  :config
  (general-add-hook 'makefile-mode-hook
    #'outline-minor-mode)
  (setq-mode-local makefile-mode
    tab-width 4
    outline-regexp "# [*]+"))

(leaf doc-view
  :config
  (setopt doc-view-resolution 300
          doc-view-mupdf-use-svg t))

;; NOTE: migrated
(leaf sh-mode
  :config
  (setq-mode-local sh-mode
    outline-regexp "# [*]+")
  (general-add-hook 'sh-mode-hook
    #'outline-minor-mode))

;; NOTE: migrated
(leaf conf-mode
  :mode (("dunstrc" . conf-mode)
         (".*\\.pa\\'" . conf-mode)
         (".*\\.hook\\'" . conf-mode)
         ("\\.directory\\'" . conf-desktop-mode)
         ("_redirects\\'" . conf-space-mode)
         ("Pipfile\\'" . conf-toml-mode))
  :init
  (with-eval-after-load 'org
    (--each '("desktop")
      (push `(,it . conf-desktop) org-src-lang-modes))))

;; NOTE: migrated
(leaf vimrc-mode
  :mode ("\\.vim\\(rc\\)?\\'" . vimrc-mode)
  :init
  (with-eval-after-load 'org
    (--each '("vimscript" "viml" "vimrc")
      (push `(,it . vimrc) org-src-lang-modes))))

;; NOTE: migrated
(leaf yaml-mode
  :mode ("\\.yml\\'" . yaml-mode))

;; NOTE: migrated
(leaf go-mode
  :when-available t
  :config
  (add-hook 'go-mode-hook #'origami-mode)
  (define-advice godef-jump (:around (func &rest args) fallback)
    "Jump to definition using `godef'.

If it failed, fall back to `evil-goto-definition'."
    (let ((orig-location (list (current-buffer) (point)))
          (inhibit-message t))
      (prog1 (apply func args)
        ;; We treat "haven't moved" as "have failed" because
        ;; `godef-jump' seems to catch errors and emit them with
        ;; `message' instead.
        (when (equal (list (current-buffer) (point))
                     orig-location)
          (call-interactively #'evil-goto-definition))))))

;; NOTE: migrated
(leaf lisp-modes
  :config
  (k/set-mode-local k/lisp-modes
    evil-shift-width 1)
  (cl-loop for mode in k/lisp-modes
           do (k/add-mode-hook mode #'parinfer-mode))
  (general-add-hook (mapcar #'k/symbol-hook k/lisp-modes)
    (list #'electric-indent-local-mode)))

;; NOTE: not migrating
(leaf clojure-mode
  :unless k/android?
  :init
  (add-hook 'clojure-mode-hook #'cider-mode)
  :defer-config
  ;; Copied and adapted from `doom-emacs'.
  (leaf cider
    :config
    (setq nrepl-hide-special-buffers t
          nrepl-log-messages nil
          cider-font-lock-dynamically '(macro core function var deprecated)
          cider-overlays-use-font-lock t
          cider-prompt-for-symbol nil
          cider-repl-history-display-duplicates nil
          cider-repl-history-display-style 'one-line
          cider-repl-history-highlight-current-entry t
          cider-repl-history-quit-action 'delete-and-restore
          cider-repl-history-highlight-inserted-item t
          cider-repl-history-size 1000
          cider-repl-result-prefix ";; => "
          cider-repl-print-length 100
          cider-repl-use-clojure-font-lock t
          cider-repl-use-pretty-printing t
          cider-repl-wrap-history nil
          cider-stacktrace-default-filters '(tooling dup)
          ;; https://github.com/clojure-emacs/cider/issues/1872
          cider-repl-pop-to-buffer-on-connect 'display-only)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "'" #'cider-jack-in-clj
     "\"" #'cider-jack-in-cljs
     "c" #'cider-connect-clj
     "C" #'cider-connect-cljs
     "m" #'cider-macroexpand-1
     "M" #'cider-macroexpand-all)
    (k/general-mode-leader
     ;; "e" '(:ignore t :wk "eval")
     ;; "eb" #'cider-eval-buffer
     ;; "ed" #'cider-eval-defun-at-point
     ;; "eD" #'cider-insert-defun-in-repl
     ;; "ee" #'cider-eval-last-sexp
     ;; "eE" #'cider-insert-last-sexp-in-repl
     ;; "er" #'cider-eval-region
     ;; "eR" #'cider-insert-region-in-repl
     ;; "eu" #'cider-undef
     :keymaps '(clojure-mode-map clojurescript-mode-map))
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "g" '(:ignore t :wk "goto")
     "gb" #'cider-pop-back
     "gg" #'cider-find-var
     "gn" #'cider-find-ns
     "h" '(:ignore t :wk "help")
     "hn" #'cider-find-ns
     "ha" #'cider-apropos
     "hc" #'cider-clojuredocs
     "hd" #'cider-doc
     "hj" #'cider-javadoc
     "hw" #'cider-clojuredocs-web)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "i" '(:ignore t :wk "inspect")
     "ie" #'cider-enlighten-mode
     "ii" #'cider-inspect
     "ir" #'cider-inspect-last-result
     "n" '(:ignore t :wk "namespace")
     "nn" #'cider-browse-ns
     "nN" #'cider-browse-ns-all
     "nr" #'cider-ns-refresh)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "r" '(:ignore t :wk "repl")
     "rn" #'cider-repl-set-ns
     "rq" #'cider-quit
     "rr" #'cider-ns-refresh
     "rR" #'cider-restart
     "rb" #'cider-switch-to-repl-buffer
     ;; "rB" #'+clojure/cider-switch-to-repl-buffer-and-switch-ns
     "rc" #'cider-find-and-clear-repl-output
     "rl" #'cider-load-buffer
     "rL" #'cider-load-buffer-and-switch-to-repl-buffer)
    (k/general-mode-leader
     :keymaps '(clojure-mode-map clojurescript-mode-map)
     "t" '(:ignore t :wk "test")
     "ta" #'cider-test-rerun-test
     "tl" #'cider-test-run-loaded-tests
     "tn" #'cider-test-run-ns-tests
     "tp" #'cider-test-run-project-tests
     "tr" #'cider-test-rerun-failed-tests
     "ts" #'cider-test-run-ns-tests-with-filters
     "tt" #'cider-test-run-test)
    (general-def
      :keymaps 'cider-repl-mode-map
      :states 'insert
      "S-RET" #'cider-repl-newline-and-indent
      "M-RET" #'cider-repl-return)
    (k/general-mode-leader
     :keymaps 'cider-repl-mode-map
     :states 'insert
     "n" #'cider-repl-set-ns
     "q" #'cider-quit
     "r" #'cider-ns-refresh
     "R" #'cider-restart
     "c" #'cider-repl-clear-buffer)
    (general-def
      :keymaps 'cider-repl-history-mode-map
      :states 'insert
      "RET" #'cider-repl-history-insert-and-quit
      "q" #'cider-repl-history-quit
      "l" #'cider-repl-history-occur
      "s" #'cider-repl-history-search-forward
      "r" #'cider-repl-history-search-backward
      "U" #'cider-repl-history-undo-other-window))
  (k/general-mode-leader
   :keymaps '(clojure-mode-map clojurescript-mode-map)
   "eR" #'cider
   "eb" #'cider-eval-buffer
   "ed" #'cider-eval-defun-at-point
   "er" #'cider-eval-region
   "el" #'cider-eval-last-sexp))

;; NOTE: migrated
(leaf lisp-data-mode
  :mode ("[CE]ask\\'"))

(leaf rmail-mode
  :mode "\\.eml\\'"
  :config
  (general-def
    :states 'normal
    :keymaps '(rmail-mode-map)
    "j" nil
    "k" nil)
  (defun k/rmail-faces ()
    "Set face settings for rmail."
    (k/face-set 'rmail-highlight
      :inherit 'bold)
    (k/face-set 'rmail-header-name
      :family k/font-mono))
  (add-hook 'k/load-theme-hook #'k/rmail-faces)
  (add-hook 'rmail-mode-hook
            (lambda ()
              (buffer-face-mode-invoke
               `(list :family ,k/font-mono)
               1))))

(leaf lisp-mode
  :mode ("\\.sbclrc\\'")
  :config
  (setq inferior-lisp-program "sbcl")
  ;; Don't use SLIME: it still doesn't support using completion
  ;; frontends other than the default one.
  (when (fboundp 'cl-font-lock-built-in-mode)
    (add-hook 'lisp-mode-hook #'cl-font-lock-built-in-mode))
  (leaf sly
    :when-available t
    :config
    (add-hook 'sly-mode-hook (k/turn-off-minor-mode company-mode))
    (setq sly-mrepl-history-file-name (f-join user-emacs-directory
                                              ".sly-mrepl-history")
          ;; The minor mode has no body, so it's just a boolean with
          ;; an interactive toggle function. We can therefore set the
          ;; variable directly.
          sly-symbol-completion-mode nil)
    ;; Leave this to `parinfer-toggle-state'
    (general-unbind :keymaps 'sly-editing-mode-map
      "M-p")
    (setq org-babel-lisp-eval-fn #'sly-eval)
    (k/general-mode-leader
     :keymaps 'lisp-mode-map
     "eb" #'sly-eval-buffer
     "ed" #'sly-eval-defun
     "er" #'sly-eval-region
     "el" #'sly-eval-last-expression
     "eR" #'sly)))

;; NOTE: not migrating
(leaf racket-mode
  :when-available t
  :setq ((racket-indent-sequence-depth . 3))
  :config
  ;; Make absolutely sure Racket files are started in racket-mode
  (defun k/dont-load-.rkt-with-scheme ()
    (k/remove! auto-mode-alist
      '("\\.rkt\\'" . scheme-mode))
    (when (-some->> buffer-file-name
            (s-suffix? ".rkt"))
      (racket-mode)))
  (add-hook 'scheme-mode-hook #'k/dont-load-.rkt-with-scheme)
  (add-hook 'racket-mode-hook #'racket-xp-mode)
  ;; (general-add-hook 'racket-repl-mode-hook
  ;;   (k/set-x-to-val
  ;;    eldoc-documentation-function #'racket-repl-eldoc-function))
  (general-add-hook 'racket-xp-mode-hook
    (k/set-x-to-val
     eldoc-documentation-function #'racket-xp-eldoc-function))
  (k/general-mode-leader
   :keymaps '(racket-mode-map racket-repl-mode-map)
   "d" 'racket-xp-documentation
   "e" '(:ignore t :which-key "eval")
   "er" #'racket-send-region
   "ed" #'racket-send-definition
   "el" #'racket-eval-last-sexp
   "eR" #'racket-run
   "E" '(:ignore t :which-key "expand")
   "Ea" 'racket-expand-again
   "o" 'racket-open-require-path
   "t" 'racket-test))

(leaf kotlin-mode
  :when-available t)

(leaf typescript-mode
  :when-available typescript-mode
  :magic "#!/usr/bin/env -S deno run --ext ts"
  :config
  (setq typescript-indent-level 2)
  (k/add-hook 'typescript-mode-hook
    #'tide-setup
    #'eldoc-mode
    #'tide-hl-identifier-mode)
  (when (k/available? 'lsp-mode)
    (k/add-hook 'typescript-mode-hook
      #'lsp-deferred)))

(leaf python
  :when (executable-find "python")
  :commands python-mode
  :config
  (general-def
    :keymaps '(python-mode-map inferior-python-mode-map)
    "<f5>" #'run-python)
  (k/general-mode-leader
   :keymaps '(python-mode-map inferior-python-mode-map)
   "eR" #'run-python
   "ed" #'python-shell-send-defun
   "eb" #'python-shell-send-buffer
   "er" #'python-shell-send-region
   "es" #'python-shell-send-statement)
  (setq-mode-local python-mode
    tab-width 4
    fill-column 79)
  (leaf pipenv
    :when-available t
    :after python
    :config
    (add-hook 'python-mode-hook #'pipenv-mode)
    (setq pipenv-projectile-after-switch-function
          #'pipenv-projectile-after-switch-extended))
  (setq python-shell-interpreter "poetry"
        python-shell-interpreter-args "run python"))

(leaf ein
  :unless k/android?
  :init (kisaragi-apps-register "EIN (Jupyter Notebook in Emacs)" #'ein:run))

(leaf prog-mode
  :config
  (setq-mode-local prog-mode
    display-line-numbers 'relative
    show-trailing-whitespace t)
  (k/add-hook 'prog-mode-hook
    #'goto-address-prog-mode
    #'auto-fill-mode
    #'hl-line-mode
    #'highlight-numbers-mode
    #'display-fill-column-indicator-mode))

(leaf safe-local-variables
  :config
  ;; see docstring of `safe-local-variable-p'.
  (--each '(k/org-insert-note-default-prefix
            bibtex-completion-bibliography
            bibtex-completion-notes-path
            nameless-separator)
    (put it 'safe-local-variable #'stringp))
  (--each '(evil-org-special-o/O
            org-src-preserve-indentation
            org-reverse-note-order)
    (put it 'safe-local-variable #'booleanp))
  (--each '(org-image-actual-width)
    (put it 'safe-local-variable
         (-orfn (k/listof #'numberp)
                #'numberp
                #'booleanp)))
  (--each '(orb-templates)
    (put it 'safe-local-variable
         (k/listof
          (-orfn #'stringp
                 #'symbolp
                 #'booleanp
                 #'consp))))
  (--each '(bibtex-completion-bibliography)
    (put it 'safe-local-variable
         (-orfn #'stringp
                (k/listof
                 (-orfn #'stringp
                        (k/consof #'stringp))))))
  (add-to-list 'safe-local-eval-forms '(org-content 2)))

(leaf flycheck-elsa
  :when-available t
  ;; It doesn't work right now: "An error happened trying to analyse nil"
  :disabled t
  :init (add-hook 'emacs-lisp-mode-hook #'flycheck-elsa-setup))

;; NOTE: migrated
(leaf emacs-lisp-mode
  :config
  (setq auto-compile-display-buffer nil)
  (k/add-hook 'emacs-lisp-mode-hook
    (lambda ()
      ;; As I'm not using the global modes, these should probably come first.
      ;; (Global modes are enabled through `after-change-major-mode-hook'.)
      (auto-compile-on-save-mode)
      (dash-fontify-mode)
      ;; HACK: lisp-extra-font-lock-mode must be enabled before
      ;; highlight-defined-mode to retain the former's highlighting for dynamic
      ;; variables. I don't know why it goes in that order.
      (lisp-extra-font-lock-mode)
      (highlight-defined-mode)))
  (with-eval-after-load 'flymake
    (setq elisp-flymake-byte-compile-load-path
          (-uniq
           (append elisp-flymake-byte-compile-load-path
                   load-path))))
  (with-eval-after-load 'flycheck
    (setq flycheck-emacs-lisp-load-path 'inherit
          ;; HACK: without this, the keywords added by leaf-keywords don't
          ;; take effect in the checker process, so they all trigger an
          ;; "unrecognized keyword" error.
          flycheck-emacs-lisp-check-form
          (if (s-contains? "leaf-keywords"
                           flycheck-emacs-lisp-check-form)
              ;; Don't do anything on subsequent evals
              flycheck-emacs-lisp-check-form
            (format
             "(progn %S %s)"
             '(progn
                (require 'leaf-keywords nil t)
                (leaf-keywords-init))
             flycheck-emacs-lisp-check-form))
          flycheck-emacs-lisp-checkdoc-form
          (if (s-contains? "'editmarker"
                           flycheck-emacs-lisp-checkdoc-form)
              flycheck-emacs-lisp-checkdoc-form
            (format
             "(progn 'editmarker %S %s)"
             '(setq
               checkdoc-create-error-function
               (lambda (text start end &optional unfixable)
                 (unless (or
                          ;; It's already "Probably". This gives more false positives
                          ;; than it is helpful.
                          (string-prefix-p "Probably " text)
                          ;; Don't force Commentary and Code blocks in the init file.
                          (and (string-match-p (regexp-opt '(";;; Commentary:"
                                                             ";;; Code:"))
                                               text)
                               (string-match-p (rx (or ".emacs.d/init.el"
                                                       ".emacs.d/kisaragi/package.el"))
                                               (buffer-file-name))))
                   (checkdoc--create-error-for-checkdoc text start end unfixable))))
             flycheck-emacs-lisp-checkdoc-form))))
  (cl-loop
   for (symbol indent)
   in '((general-add-hook 1)
        (setq-mode-local 1)
        (general-remove-hook 1)
        (elpaca 0)
        (flycheck-ert-def-checker-test 3))
   do (put symbol 'lisp-indent-function indent))
  (prog1 'plist-indent
    ;; Based on `emacsql-fix-vector-indentation'.
    (defun k/inside-plist-or-vector? ()
      "Is point situated inside a plist?

We determine a plist to be a list that starts with a keyword."
      (let ((start (point)))
        (save-excursion
          (beginning-of-defun)
          (let ((sexp (nth 1 (parse-partial-sexp (point) start))))
            (when sexp
              (goto-char sexp)
              (looking-at
               (rx (or (seq "(" (* (syntax whitespace)) ":")
                       "["))))))))
    (define-advice calculate-lisp-indent (:around (func &rest args)
                                          plist)
      "Indent plists in a sane way."
      (if (save-excursion
            (beginning-of-line)
            (k/inside-plist-or-vector?))
          (let ((lisp-indent-offset 1))
            (apply func args))
        (apply func args))))
  (define-advice eval-last-sexp (:around (func arg) keep-point)
    "Keep point position after inserting result."
    (save-excursion
      (funcall func arg)))
  (with-eval-after-load 'org
    (--each '(("emacs-lisp" . lisp-interaction)
              ("elisp" . lisp-interaction))
      (add-to-list 'org-src-lang-modes it)))
  (leaf eros
    :config
    (eros-mode)
    (advice-add 'eros-eval-last-sexp :after #'font-lock-flush)
    (advice-add 'eros-eval-defun :after #'font-lock-flush))
  (leaf flycheck-relint
    :config (flycheck-relint-setup))
  (leaf flycheck-package
    :hook (emacs-lisp-mode-hook . flycheck-package-setup)
    :config
    (define-advice package-lint--check-symbol-separators
        (:around (_func &rest _) allow-nonstandard-prefix)
      "Ignore the package-lint error about non-standard prefixes."
      nil)))

;; NOTE: migrated
(leaf ledger-mode
  :init
  (leaf kisaragi-ledger
    :commands k/ledger-visit-latest)
  (add-hook 'ledger-mode-hook #'evil-ledger-mode)
  (add-hook 'ledger-report-mode-hook #'evil-motion-state)
  :defer-config
  (leaf flycheck-ledger
    :require t)
  (with-eval-after-load 'doom-themes
    (add-hook 'ledger-report-after-report-hook #'k/ledger-report--apply-doom-colors))
  (defun k/ledger-format-specifier-period ()
    "Substitutes %(period) in `ledger-reports'."
    (read-string "Reporting period: "))
  (cl-pushnew (cons "period" #'k/ledger-format-specifier-period)
              ledger-report-format-specifiers)
  (setq ledger-highlight-xact-under-point nil
        ledger-clear-whole-transactions t
        ;; Unfortunately ledger does not (yet?) support just using a
        ;; timestamp. Use 2000-01-01 style instead of 2000/01/01 so
        ;; that we don't have to convert dates if support is added.
        ;; We can't use `ledger-iso-date-format' because it's not
        ;; loaded yet.
        ledger-default-date-format "%Y-%m-%d"
        ledger-accounts-file (f-join k/ledger/ "accounts.ledger"))
  (put 'ledger-reports 'safe-local-variable
       (lambda (v)
         (and (listp v)
              (--all? (and (listp it)
                           (= 2 (length it))
                           (stringp (elt it 0))
                           (stringp (elt it 1)))
                      v))))
  (cl-flet ((report (command) (s-join " " (list "%(binary)"
                                                "--date-format %F"
                                                "-f %(ledger-file)"
                                                command))))
    (setq ledger-reports
          `(("budget"                     ,(report "bal Budget"))
            ("budget, in terms of $"      ,(report "bal Budget -X $"))
            ("balance, in terms of $"     ,(report "bal --real -X $"))
            ("balance"                    ,(report "bal --real"))
            ("assets and liabilities"     ,(report "bal --real Assets Liabilities"))
            ("assets and liabilities ($)" ,(report "bal --real Assets Liabilities -X $"))
            ("balance, including budget"  ,(report "bal"))
            ("cash flow (outflow: positive means losing money)"
             ,(report "balance Income Expenses"))
            ("expenses, sorted by date"   ,(report "reg Expenses -S amount --real"))
            ("expenses, sorted by amount" ,(report "reg Expenses -S amount --real"))
            ("prices"                     ,(report "prices"))
            ("reg"                        ,(report "reg"))
            ("Assets, by month"           ,(report "reg Assets --monthly --collapse --period-sort '(amount)'"))
            ("payee"                      ,(report "reg @%(payee)"))
            ("account"                    ,(report "reg %(account)"))
            ("account (with period)"      ,(report "reg %(account) --period %(period)")))))
  (setq-mode-local ledger-report-mode
    display-line-numbers nil)
  (setq-default ledger-master-file k/ledger-file)
  (general-add-hook 'ledger-mode-hook
    `(outline-minor-mode
      auto-fill-mode
      ,(lambda () (add-hook 'before-save-hook #'k/ledger-format-buffer nil t))))
  (advice-add #'ledger-exec-ledger :override #'k/ledger-exec-ledger)
  (define-advice ledger-report-redo (:around (func &rest _) keep-point)
    "Try to keep point in ledger report after refresh."
    (when-let ((current-buffer (current-buffer))
               (report-buffer (get-buffer ledger-report-buffer-name)))
      (with-current-buffer report-buffer
        (let ((point-in-report (point)))
          (funcall func)
          (ignore-errors (goto-char point-in-report))))
      (pop-to-buffer current-buffer)))
  (k/general-mode-leader
   :keymaps 'ledger-mode-map
   ;; bindings copied from here
   ;; https://github.com/rememberYou/.emacs.d/blob/master/config.org#hydra--ledger
   "A" #'k/ledger-add-transaction
   "a" '(k/ledger-add-simple-transaction :wk "Add a simple transaction")
   "c" #'ledger-mode-clean-buffer
   "C" #'ledger-copy-transaction-at-point
   "d" #'ledger-delete-current-transaction
   "rr" '(ledger-report :wk "Report (All imported files)")
   "rc" '(k/ledger-report-this-file :wk "Report (this file)")
   "w" '(:ignore t :wk "write")
   "wa" '(k/ledger-write-accounts :wk "Write existing accounts to file")
   "wc" '(k/ledger-write-commodities :wk "Write existing commodities to file")
   "f" '(k/ledger-format-buffer :wk "Format buffer")))

;; NOTE: migrated
(leaf plisp-mode
  :leaf-defer nil
  :when-available t
  :mode ("\\.l\\'" . plisp-mode)
  :config
  (setq plisp-documentation-method 'plisp--shr-documentation
        plisp-documentation-directory
        (if k/android?
            (f-join (getenv "PREFIX")
                    "lib/picolisp/doc/")
          "/usr/lib/picolisp/doc/")))

;; NOTE: migrated
(leaf pollen-mode
  :mode ("\\.ptree\\'"
         "\\.html.pm\\'")
  :config
  (setq-mode-local pollen-mode comment-start "◊;")
  (k/general-mode-leader
   :keymaps 'pollen-mode-map
   "s" '(pollen-server-start :wk "Start Server")
   "p" '(pollen-server-stop :wk "Stop Server")
   "e" '(pollen-edit-block-other-window :wk "Edit Block")))

(leaf kisaragi-desktop
  :require t
  :config
  (when (file-exists-p "~/.config/discord/settings.json")
    (with-temp-file "~/.config/discord/settings.json"
      (insert-file-contents "~/.config/discord/settings.json")
      (let ((json-object-type 'hash-table)
            (json-encoding-pretty-print t))
        (let ((hash (json-read)))
          (puthash "SKIP_HOST_UPDATE" t hash)
          (erase-buffer)
          (insert (json-encode hash))))))
  (defun k/send-time-notification ()
    (send-notification (format-time-string "Current time: %H:%M")
      :body (format "Emacs has been running for %s." (emacs-uptime)))
    (let ((sound "/usr/share/sounds/Oxygen-Im-Low-Priority-Message.ogg"))
      (when (file-exists-p sound)
        (start-process "mpv" nil "mpv" sound))))
  (defvar k/alarm-timer
    (run-at-time
     ;; The next hour
     (format "%s:00" (1+ (cl-third (decode-time (current-time)))))
     ;; Repeat every hour
     (* 60 60)
     #'k/send-time-notification)))

;; NOTE: migrated
(leaf fcitx
  :when-available t
  :config
  (if (executable-find "fcitx5")
      (setq fcitx-use-dbus nil
            fcitx-remote-command "fcitx5-remote")
    (setq fcitx-use-dbus t))
  (fcitx-default-setup))

;; NOTE: migrated
;; This only works for fcitx5
(leaf kisaragi-fcitx
  :require t
  :when-available fcitx
  :config
  (general-def
    "<insert>" #'k/fcitx-cycle-current-im
    "S-<insert>" (k/expose (lambda () (k/fcitx-cycle-current-im -1))))
  (general-def
    :states 'insert
    "<insert>" #'k/fcitx-cycle-current-im
    "S-<insert>" (k/expose (lambda () (k/fcitx-cycle-current-im -1)))))

;;;; Load other config files
;; NOTE: migrated
(leaf kisaragi-extra-functions
  :require t
  :config
  (general-def
    "M-!" #'k/async-shell-command
    "M-;" #'k:cycle-case)
  (general-def :states 'visual
    "A" #'k/ellipsis
    "R" #'k/wrap-square
    "M" #'k/em-dash))

;; NOTE: migrated
(leaf kisaragi-hugo
  :require t
  :config
  (k/general-leader
   "mj" #'k/hugo-jump-language))

;; NOTE: migrated
(leaf kisaragi-units
  :require t)

(leaf synchronize-git
  :init
  (setq synchronize-git-default-repos
        `(,k/ledger/
          ,k/notes/
          ,(file-name-directory canrylog-file)
          ,user-emacs-directory
          "~/git/kemdict"
          ,@(when (k/system-is-p "MF-manjaro")
              '((svn     "~/kde-translations/ja/summit")
                (svn     "~/kde-translations/templates/kf5")
                (svn     "~/kde-translations/templates/kf6")
                (git-svn "~/kde-translations/zh_TW/kf5")
                (git-svn "~/kde-translations/zh_TW/kf6")
                (git-svn "~/kde-translations/zh_TW-stable/kf5")))
          ,@(when (k/system-is-p "MF-PC")
              '((svn     "~/kde-translations/ja/kf5")
                (svn     "~/kde-translations/ja/kf6")
                (svn     "~/kde-translations/templates/kf5")
                (svn     "~/kde-translations/templates/kf6")
                (git-svn "~/kde-translations/zh_TW/kf5")
                (git-svn "~/kde-translations/zh_TW/kf6")
                (git-svn "~/kde-translations/zh_TW-stable/kf5")
                "~/git/voice-practice/")))))

;; NOTE: migrated
(leaf kisaragi-timestamp-commands
  :commands
  kisaragi-timestamp-commands/iso8601-full-to-no-symbol
  kisaragi-timestamp-commands/iso8601-no-symbol-to-full
  kisaragi-timestamp-commands/commit-timestamp-to-iso8601
  kisaragi-timestamp-commands/convert-iso8601-timezone
  :config
  (k/general-leader/extra-editing-commands
    "tz" '(kisaragi-timestamp-commands/convert-iso8601-timezone
           :wk "Convert to current timezone")
    "ts" '(kisaragi-timestamp-commands/iso8601-no-symbol-to-full
           :wk "20200101... -> 2020-01-01...")
    "tS" '(kisaragi-timestamp-commands/iso8601-full-to-no-symbol
           :wk "2020-01-01... -> 20200101... ")
    "tc" '(kisaragi-timestamp-commands/commit-timestamp-to-iso8601
           :wk "Commit timestamp to ISO 8601")))

(require 'format-time-string-patch)

;; NOTE: migrated
(when k/android?
  ;; (ignore-errors
  ;;   (server-start))
  ;; set DISPLAY in Termux so browse-url works
  (setenv "DISPLAY" "dummy")
  ;; This is from Termux's site-lisp
  (xterm-mouse-mode 1)
  (global-set-key [mouse-4] 'scroll-down-line)
  (global-set-key [mouse-5] 'scroll-up-line))

(leaf server
  :when (daemonp)
  :config
  ;; Set XDG_CURRENT_DESKTOP from within Emacs.
  ;;
  ;; If we start Emacs as a systemd unit, it does not inherit
  ;; XDG_CURRENT_DESKTOP from the desktop environment. This causes
  ;; `xdg-open' to act differently: for example, the fallback mode
  ;; `xdg-open' uses does not recognize UST files, and so opens them
  ;; as text files. This works around that.
  ;;
  ;; Let's not think about what happens in a TTY. I'll worry about
  ;; that later.
  (unless (or (getenv "XDG_CURRENT_DESKTOP")
              k/android?)
    (cond ((k/pgrep-boolean "plasmashell")
           (setenv "XDG_CURRENT_DESKTOP" "KDE")
           ;; xdg-open runs code for KDE 3 if this isn't set
           (setenv "KDE_SESSION_VERSION" "5")))))
;; (defun k/set-display-if-empty (&optional _)
;;   "Set the environment variable DISPLAY to \":0\" if it's not set.

;; This allows running X related stuff even when there's no frame active."
;;   (unless (or (getenv "DISPLAY")
;;               (getenv "WAYLAND_DISPLAY"))
;;     (if (getenv "WAYLAND_DISPLAY")
;;         (setenv "DISPLAY" nil)
;;       (setenv "DISPLAY" ":0"))))
;; (general-add-hook '(delete-frame-functions
;;                     after-init-hook)
;;   #'k/set-display-if-empty))

(leaf send-notification
  :when-available t
  :config
  (when (daemonp)
    (send-notification-on-startup-mode))
  (with-eval-after-load 'magit
    (send-notification-on-magit-error-mode)))

;; NOTE: migrated
(leaf startup
  :config
  (setq initial-buffer-choice (f-join k/notes/ "projects.org")
        initial-scratch-message nil
        initial-major-mode 'text-mode
        inhibit-startup-screen t)
  (k/once-after-make-frame
    ;; HACK: Close the loud warning window.
    ;; https://kisaragi-hiu.com/emacs-29-pgtk-woes/
    ;; I can understand it's "unsupported" and I'd be on my own for any
    ;; stability issues, but pgtk on X11 is literally flawless on my system,
    ;; so... no.
    ;;
    ;; That I have to rely on X11 automation is frustrating.
    (when (and (display-graphic-p)
               (executable-find "xdotool"))
      (when-let (window-id (ignore-errors
                             (k/call-process-to-string
                               "xdotool" "search"
                               "--all"
                               "--pid" (format "%s" (emacs-pid))
                               "--name" "^Warning$")))
        (k/call-process-to-string
          "xdotool" "windowclose" window-id))))
  (k/once-after-make-frame
    ;; When starting Emacs as a daemon, Org mode is started without
    ;; graphics and so images are not shown. Then when a graphical frame
    ;; is opened, the buffer remains without images until it's reverted.
    ;; This forces a revert after creating a frame if the frame is
    ;; graphical.
    (when (and (display-graphic-p)
               (buffer-file-name))
      (revert-buffer))))

;;; init.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:

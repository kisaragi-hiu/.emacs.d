;;; format-time-string-patch.el --- Force English format-time-string day names -*- lexical-binding: t; -*-
;;; Commentary:
;;; Make it so that (format-time-string "%a") is always in English,
;;; regardless of $LANG or other locale settings.
;;; I'm fed up with org-todo having to be run twice because of this.
;;; Code:

(require 'calendar)

(defun k/english-dow (&optional time zone abbreviated)
  "Return ABBREVIATED name of the day of week at TIME and ZONE.

If TIME or ZONE is nil, use `current-time' or `current-time-zone'."
  (unless time (setq time (current-time)))
  (unless zone (setq zone (current-time-zone)))
  (calendar-day-name
   (pcase-let ((`(,_ ,_ ,_ ,d ,m ,y . ,_)
                (decode-time time zone)))
     (list m d y))
   abbreviated))

(defun k/english-month (&optional time zone abbreviated)
  "Return ABBREVIATED name of month in English at TIME and ZONE."
  (unless time (setq time (current-time)))
  (unless zone (setq zone (current-time-zone)))
  (calendar-month-name
   (elt (decode-time time zone) 4)
   abbreviated))

(defun k/advice-format-time-string (func format &optional time zone)
  "Pass FORMAT, TIME, and ZONE to FUNC.

Replace \"%A\" in FORMAT with English day of week of today,
\"%a\" with the abbreviated version."
  ;; HACK: this of course breaks when given "%%A". It should be "%A" in the
  ;; final string, whereas with this patch it first gets turned into something
  ;; like "%Monday" then get interpreted a second time by format-time-string
  ;; itself, producing eg. "29onday" when the time is 22:29.
  (let* ((case-fold-search nil)
         (format (replace-regexp-in-string
                  "%A" (k/english-dow time zone nil)
                  format
                  t))
         (format (replace-regexp-in-string
                  "%a" (k/english-dow time zone t)
                  format
                  t))
         (format (replace-regexp-in-string
                  "%B" (k/english-month time zone nil)
                  format
                  t))
         (format (replace-regexp-in-string
                  "%b" (k/english-month time zone t)
                  format
                  t)))
    (funcall func format time zone)))

(advice-add 'format-time-string :around #'k/advice-format-time-string)

(provide 'format-time-string-patch)
;;; format-time-string-patch.el ends here

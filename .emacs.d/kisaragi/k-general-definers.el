;;; k-general-definers.el --- My general.el key definers  -*- lexical-binding: t -*-

;;; Commentary:

;; Set up general.el key definers and a scaffolding for leader maps.

;;; Code:

(require 'kisaragi-constants)
(require 'general)

;; I don't know why `general-create-definer' defines a macro, not
;; a function. Define these manually as I want them to be
;; functions so I can use `apply'.
(defun k/general-mode-leader (&rest args)
  "Wrapper around `general-define-key'."
  (apply #'general-define-key
         (append args
                 ;; ARGS first as first keyword takes precedence
                 (list
                  :states '(normal visual insert)
                  :prefix k/mode-leader
                  :global-prefix k/global-mode-leader))))
(defun k/general-leader (&rest args)
  "Wrapper around `general-define-key'."
  (apply #'general-define-key
         (append args
                 ;; ARGS first as first keyword takes precedence
                 (list
                  :states '(normal visual motion insert emacs)
                  :global-prefix k/global-leader
                  :prefix k/primary-leader
                  :keymaps 'override))))
(defmacro k/general-leader-derive (name keymap suffix which-key)
  "Define a general definer, NAME, derived off `k/general-leader'.)

The keys defined with the new definer will be stored in a new
ymap named KEYMAP.

Both NAME and KEYMAP should be unquoted symbols.

SUFFIX is what to add after the leader, for instance with SUFFIX
being \"a\" and the leader being SPC, commands are bound under
\"SPC a\".

WHICH-KEY is the which-key name for the map, as viewed in the
ader map."
  (declare (indent 1))
  `(progn
     (k/general-leader
      ,suffix '(:ignore t :wk ,which-key))
     (defvar ,keymap (make-sparse-keymap))
     (defmacro ,name (&rest args)
       "A wrapper around `k/general-leader'."
       (declare (indent defun))
       `(progn
          (k/general-leader
           :global-prefix ,',(format "%s %s" k/global-leader suffix)
           :prefix ,',(format "%s %s" k/primary-leader suffix)
           ,@args)
          ;; Add the keys to KEYMAP so that it can be reused
          ;; (which-key prompts won't carry over, though.)
          (general-def
            ;; ehh, it works...
            :keymaps ',',keymap
            ,@args)))))
(k/general-leader-derive k/general-leader/new
  k/leader-map-new-stuff
  "n" "Add new...")
(k/general-leader-derive k/general-leader/yasnippet
  k/leader-map-yasnippet
  "y" "Yasnippet")
(k/general-leader-derive k/general-leader/toggle
  k/leader-map-toggle
  "T" "Toggle")
(k/general-leader-derive k/general-leader/tabs
  k/leader-map-tabs
  "t" "Tabs")
(general-def
  :states 'normal
  "C-t" '(:keymap k/leader-map-tabs :wk "Tabs"))
(k/general-leader-derive k/general-leader/display-buffer-prefixes
  k/leader-map-display-buffer-prefixes
  "q" "Display next command buffer in...")
(k/general-leader-derive k/general-leader/file
  k/leader-map-file
  "f" "File")
(k/general-leader-derive k/general-leader/git
  k/leader-map-git
  "g" "Git")
(k/general-leader-derive k/general-leader/buffer
  k/leader-map-buffer
  "b" "Buffer")
(k/general-leader-derive k/general-leader/extra-editing-commands
  k/leader-map-editing
  "d" "Editing")
(k/general-leader-derive k/general-leader/extra-insert-commands
  k/leader-map-insert
  "i" "Insert")
(k/general-leader-derive k/general-leader/apps
  k/leader-map-apps
  "a" "Apps")
(k/general-leader-derive k/general-leader/frame
  k/leader-map-frame
  "F" "Frame")
(general-create-definer k/general-g*
  :states 'motion
  ;; :global-prefix k/global-leader
  :prefix "g")

(provide 'k-general-definers)

;;; k-general-definers.el ends here

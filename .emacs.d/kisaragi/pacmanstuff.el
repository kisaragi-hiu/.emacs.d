;;; pacmanstuff.el --- some commands for pacman -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(require 'ansi-color)
(require 'button)
(require 'kisaragi-helpers)
(require 'set)
(require 'ht)
(require 'dash)

(defvar pacmanstuff--queue (ht))

(defun pacmanstuff--pacman (&rest args)
  "Run pacman with ARGS and return the exit code."
  (apply #'call-process "pacman" nil nil nil args))

(defun pacmanstuff--display--pacman (&rest args)
  "Wrapper function to call pacman with ARGS and insert the result."
  (let ((process-environment '("LANG=en_US.UTF-8")))
    (save-excursion
      (apply #'call-process "pacman" nil t nil "--color" "always" args))))

(defun pacmanstuff--display--buttonize (&optional start end func)
  "Turn package names in current buffer into buttons.

A package name is a string without whitespace that doesn't start
with a number.

If START is non-nil, start from there instead of `point'.
If END is non-nil, only do the transformation forward until
END (as a buffer location). This is passed on to
`re-search-forward' as its BOUND argument.

If FUNC is provided, it is called after replacement, with point
just after each item, and with one argument (the package name)."
  (let ((case-fold-search nil))
    (save-excursion
      (goto-char (or start (point)))
      (while (re-search-forward
              (rx (or bol space)
                  (group (not (any digit space "A-Z"))
                         (+ (not (any space "="))))
                  word-boundary)
              end t)
        (buttonize-region
         (match-beginning 1)
         (match-end 1)
         (lambda (pkg)
           (pacmanstuff--show-package-info pkg))
         (match-string 1))
        (when func
          (funcall func (match-string 1)))))))

(defmacro pacmanstuff--display (name &rest body)
  "Set up a buffer with NAME for BODY to insert into, then show it."
  (declare (indent 1))
  (cl-with-gensyms (loc)
    `(with-current-buffer (get-buffer-create ,name)
       (view-mode)
       (setq buffer-read-only nil)
       (let ((,loc (point)))
         (erase-buffer)
         ,@body
         (message "%S" ,loc)
         (goto-char ,loc))
       (pop-to-buffer-same-window (current-buffer)))))

(defsubst pacmanstuff--display--section ()
  "Insert a section separator."
  (insert "\n\n"))

(defsubst pacmanstuff--display--heading (str)
  "Insert STR as a heading."
  (insert
   (propertize str 'face 'bold) "\n"
   (make-string (string-width str) ?=) "\n"))

(defun pacmanstuff--display-buttons (&rest buttons)
  "Insert BUTTONS."
  (insert (string-join buttons " ")))

(defun pacmanstuff--show-orphaned ()
  "Show all orphaned packages (pacman -Qdt)."
  (pacmanstuff--display "*pacmanstuff list*"
    (setq-local revert-buffer-function
                (lambda (&rest _)
                  (pacmanstuff--show-orphaned)))
    (pacmanstuff--display--heading "Orphaned packages")
    (let ((start (point)))
      (pacmanstuff--display--pacman "-Qdt")
      (ansi-color-apply-on-region (point-min) (point-max))
      (pacmanstuff--display--buttonize
       start nil
       (lambda (pkg)
         (goto-char (pos-eol))
         (insert " ")
         (apply
          #'pacmanstuff--display-buttons
          (--map
           (buttonize
            (format "[%s]" (car it))
            (lambda (_)
              (puthash pkg (cadr it) pacmanstuff--queue)
              (message "Marked %s as %s." pkg (cadr it))))
           '(("x" "remove")
             ;; ("deps" "asdeps")
             ("explicit" "asexplicit"))))))
      (let ((count (count-lines start (point-max))))
        (goto-char (point-max))
        (insert "\n")
        (insert (format "%s orphaned packages" count))))
    (insert "\n\n")
    (pacmanstuff--display--heading "Actions")
    (insert
     (buttonize
      "Show Queue"
      (lambda (_)
        (pacmanstuff--show-queue))))))

(defun pacmanstuff--show-queue ()
  "Show the content of the queue under KEY."
  (pacmanstuff--display "*pacmanstuff queue*"
    (setq-local revert-buffer-function
                (lambda (&rest _)
                  (pacmanstuff--show-queue)))
    (pacmanstuff--display--heading
     (format "Queued items"))
    (let ((lines nil))
      (map-do
       (lambda (pkg action)
         (push (concat
                (buttonize pkg
                           (lambda (pkg)
                             (pacmanstuff--show-package-info pkg))
                           pkg)
                "\t"
                action)
               lines))
       pacmanstuff--queue)
      (insert
       (-> (sort lines #'string<)
           (string-join "\n"))))
    (insert "\n\n")
    (pacmanstuff--display--heading "Actions")
    (insert
     (buttonize
      "Reset(!)"
      (lambda (_)
        (setq pacmanstuff--queue (ht))
        (revert-buffer))))
    (insert "\n")
    (pacmanstuff--display-buttons
     (buttonize
      "Refresh"
      (lambda (_)
        (let ((pkgs (hash-table-keys pacmanstuff--queue)))
          (--each pkgs
            (unless (= 0 (pacmanstuff--pacman "-Q" it))
              (remhash it pacmanstuff--queue))))
        (revert-buffer)
        (message "Cleared packages that are no longer installed")))
     (buttonize
      "Copy..."
      (lambda (_)
        (let ((reversed (ht))
              action packages)
          (maphash (lambda (pkg action)
                     (ht-update-with!
                      reversed
                      action
                      (lambda (acc)
                        ;; Work around ht-update-with! not being able to start
                        ;; with a nil initializer, as when its DEFAULT argument
                        ;; is nil it does nothing instead.
                        ;;
                        ;; I can justify that choice as the person who submitted
                        ;; the PR to add ht-update-with!. There are three
                        ;; options:
                        ;; 1. initialize the value to nil
                        ;; 2. do nothing
                        ;; 3. differentiate between DEFAULT being passed in
                        ;; explicitly or not
                        ;;
                        ;; (3) would be ideal except that it would require
                        ;; writing an alternative to `defun' etc. that uses eg.
                        ;; argument length to distinguish between a value that
                        ;; isn't passed in and passing in an explicit nil.
                        ;; `cl-defun' does that internally but does not expose
                        ;; it to the function body. (1) makes it impossible to
                        ;; do nothing when the value does not exist. (2) can at
                        ;; least be worked around, as seen here.
                        (if (listp acc)
                            (cons pkg acc)
                          (list pkg)))
                      :dummy))
                   pacmanstuff--queue)
          (message "%S" reversed)
          (setq action
                (completing-read "Copy package names with which planned action: "
                                 (hash-table-keys reversed)))
          (setq packages (gethash action reversed))
          (kill-new
           (-> packages
               (seq-into 'list)
               (string-join " ")))
          (message "Copied %s package names." (seq-length packages))))))
    (insert "\n")
    (insert
     (buttonize
      "Back to orphaned package listing"
      (lambda (_)
        (pacmanstuff--show-orphaned))))))

(defun pacmanstuff--show-package-info (package)
  "Show information for PACKAGE.
This is roughly the output of `pacman -Qi PACKAGE`."
  (pacmanstuff--display (format "*pacmanstuff info: %s*" package)
    (setq-local revert-buffer-function
                (lambda (&rest _)
                  (pacmanstuff--show-package-info package)))
    (pacmanstuff--display--pacman "-Qi" package)
    (ansi-color-apply-on-region (point-min) (point-max))
    (goto-char (point-min))
    (--each '("Depends On" "Required By")
      (when (search-forward it nil t)
        (pacmanstuff--display--buttonize (point) (pos-eol))))
    (goto-char (point-max))
    (insert "\n")
    (pacmanstuff--display--heading "Queued actions")
    (pacmanstuff--display-buttons
     (buttonize
      "Mark as deps"
      (lambda (_)
        (puthash package "asdeps" pacmanstuff--queue)
        (message "Marked %s as deps." package)))
     (buttonize
      "Mark as explicit"
      (lambda (_)
        (puthash package "asexplicit" pacmanstuff--queue)
        (message "Marked %s as explicit." package)))
     (buttonize
      "Mark for removal"
      (lambda (_)
        (puthash package "remove" pacmanstuff--queue)
        (message "Marked %s for removal." package))))
    (insert "\n")
    (pacmanstuff--display-buttons
     (buttonize
      "Reset queue"
      (lambda (_)
        (setq pacmanstuff--queue (ht))
        (message "All queues have been reset.")))
     (buttonize
      "Show Queue"
      (lambda (_)
        (pacmanstuff--show-queue))))
    (insert "\n\n")
    (pacmanstuff--display--heading "Other actions")
    (insert
     (buttonize
      "Back to orphaned package listing"
      (lambda (_)
        (pacmanstuff--show-orphaned))))))

(defun pacmanstuff-orphaned ()
  "Show orphaned packages in a buffer."
  (interactive)
  (pacmanstuff--show-orphaned))

(provide 'pacmanstuff)

;;; pacmanstuff.el ends here

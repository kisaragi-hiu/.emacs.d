;;; kisaragi-helpers.el --- various helper functions and macros  -*- lexical-binding: t; -*-
;;; Commentary:
;;; Helper functions used throughout my init file.
;;; Code:
(require 'cl-lib)
(require 's)
(require 'subr-x)
(require 'map)
(require 'dash)
(require 'parse-time)
(require 'derived)

(declare-function calendar-current-date "calendar")

;; Name inspired by Doom Emacs.
(defun k:set-popup-rule (condition action)
  "Add an entry to `display-buffer-alist'.
CONDITION is how a buffer should be matched; ACTION is how
`display-buffer' should display it. See `display-buffer-alist'
for details."
  (setf (map-elt display-buffer-alist condition) action))

;; Inspired by Doom Emacs
(defmacro k:set-company-backend (mode &rest backends)
  "Set BACKENDS as the first backends for MODE."
  (let ((hook (intern (format "%s-hook" mode)))
        (func (intern (format "k:set-company-backend-for-%s" mode))))
    `(progn
       (defun ,func ()
         ,(format "Set company backend for `%s'." mode)
         (setq-local company-backends
                     (append (list ,@backends) company-backends)))
       (add-hook ',hook #',func))))

(defmacro k:deferred-require (library &rest body)
  "Return a function which requires LIBRARY then runs BODY.
Useful for loading libraries after a mode starts."
  (declare (indent 1))
  `(lambda ()
     (require ,library)
     ,@body))

;;;; Action / interaction with system

(let ((font-list (font-family-list)))
  (defun k/first-available-font (fonts)
    "Return the first font in FONTS that is available on this system."
    ;; If font-list turns out to be nil (perhaps because this
    ;; definition was run when graphics haven't been initialized),
    ;; try to set it again.
    (unless font-list (setq font-list (font-family-list)))
    (when font-list
      ;; cl-intersection seems to reverse the order. Reverse it back
      ;; so the first availble font in FONTS is still the first
      ;; element in the return value.
      (car (reverse (cl-intersection fonts font-list :test #'equal))))))

(defun k/toggle-multiple (toggle-alist)
  "Toggle many things, making sure they\\='re all on or off at once.

TOGGLE-ALIST looks like \\='((VARIABLE . TOGGLE-FUNCTION) ...),
where:
- VARIABLE determines if the thing is on (non-nil) or off (nil);
- TOGGLE-FUNCTION is the function that actually toggles it.

Both VARIABLE and TOGGLE-FUNCTION can be void and would be ignored."
  ;; Grab the actual current values, replacing the keys
  (let (truthy falsey)
    (pcase-dolist (`(,sym . ,toggle-function) toggle-alist)
      (if (ignore-error void-variable
            (symbol-value sym))
          (push toggle-function truthy)
        (push toggle-function falsey)))
    (--each (if (and truthy falsey)
                ;; If both are populated, then the state is mixed. Call the
                ;; functions for the non-nil ones to bring everything to nil.
                truthy
              ;; Otherwise everything is synchronized, so call every function.
              (append truthy falsey))
      (when (commandp it t)
        (call-interactively it)))))

(defun k/pgrep-boolean (name)
  "Does a process matching NAME exist?"
  (= 0 (call-process "pgrep" nil nil nil name)))

(defun k/system-name ()
  "Return name and type of this system."
  (pcase (system-name)
    ("localhost"
     (if (executable-find "getprop")
         (string-trim
          (shell-command-to-string
           "getprop ro.product.model"))
       "localhost"))
    (name name)))

(defun k/system-is-p (name)
  "Is NAME equal to the hostname of this machine?"
  (string= (k/system-name) name))

;;;; Data
;; FIXME: replace with `derived-mode-hook-name'
(defun k/symbol-hook (symbol)
  "Given SYMBOL `foo', return `foo-hook'."
  (intern (concat (symbol-name symbol) "-hook")))

;; FIXME: replace with `derived-mode-map-name'
(defun k/symbol-map (symbol)
  "Given SYMBOL `foo', return `foo-map'."
  (intern (concat (symbol-name symbol) "-map")))

(defun k/mode-ancestors (mmode)
  "Return the ancestors of MMODE, a major mode."
  ;; This is... abusing the notion of "traversing a list".
  ;;
  ;; "for x in lst by func do body" means
  ;;
  ;; 0. continue only when lst is a cons cell
  ;;    (nil is not a cons cell)
  ;; 1. bind x to (car lst)
  ;; 2. run body
  ;; 3. set lst to (funcall func lst)
  (cl-loop for mode in (list mmode)
           by (lambda (f)
                (when-let (parent (get (car f) 'derived-mode-parent))
                  (list parent)))
           unless (eq mode mmode)
           collect mode))

(defun k/mode-descendants (mmode)
  "Return the descendants of MMODE, a major mode."
  (let (ret)
    (mapatoms
     (lambda (s)
       (when (and (provided-mode-derived-p s mmode)
                  (not (eq s mmode)))
         (push s ret))))
    ret))

(defun k/add-mode-hook (mode func &rest args)
  "Add FUNC to MODE's hook with `add-hook'.

FUNC will not be added if an ancestor of MODE already runs
it. This prevents FUNCTION from being run multiple times.

Other ARGS are passed to `add-hook'. See its docstring for what
they are."
  (let ((hook (k/symbol-hook mode)))
    (unless (--any (memq func (symbol-value (k/symbol-hook it)))
                   (k/mode-ancestors mode))
      ;; This increases time complexity by a *lot*, plus `mapatoms'
      ;; isn't fast (for good reason) in the first place. It jumps
      ;; from 0.0001 seconds all the way up to 2 seconds on my
      ;; machine.
      ;;
      ;; (cl-loop for descendant in (k/mode-descendants mode)
      ;;          do (remove-hook (derived-mode-hook-name descendant) func))
      (apply #'add-hook hook func args))))

(defun k/average (&rest numbers)
  "Return average of NUMBERS.

NUMBERS is `-flatten'ed before use."
  (setq numbers (-flatten numbers))
  (/ (apply #'+ numbers) (length numbers)))

(defun k/length< (a b)
  "Return non-nil if (length A) < (length B).
Named like `string<' but unlike `length<'."
  (< (length a)
     (length b)))

(defun k/length> (a b)
  "Return non-nil if (length A) > (length B).
Named like `string>' but unlike `length>'."
  (> (length a)
     (length b)))


(defmacro k/update-from-standard (sym &rest body)
  "Update SYM to the value of BODY with `it' bound to its standard value.

SYM should be a quoted symbol or a list of symbols.

If one symbol is provided alone, its new value will be returned;
if a list of symbols are provided, their new values are returned
in a list. The latter case also applies if the list only has one
item."
  (declare (indent 1))
  (let* (;; 'a in '(quote a) or '(a b) in '(quote (a b))
         (raw-sym (cadr sym))
         (symbols (if (consp raw-sym)
                      raw-sym
                    (list raw-sym))))
    `(,(if (consp raw-sym)
           'list
         'progn)
      ,@(cl-loop
         for sym in symbols
         collect
         `(let* ((it (-some--> (car (get ',sym 'standard-value))
                       (with-no-warnings
                         (eval it t)))))
            (set ',sym ,@body))))))

;; (cl-typep '(a b c) '(k/listof symbol)) => t
;; (cl-typep '(a b c) '(k/listof string)) => nil
(cl-deftype k/listof (type)
  `(satisfies (lambda (thing) (and (cl-typep thing 'list)
                                   ;; list of t?
                                   (car-safe
                                    (cl-remove-duplicates
                                     (--map (cl-typep it ',type) thing)))))))

(defun k/next (sequence val &optional n testfn)
  "Return element in SEQUENCE next to ELT.

N defaults to 1, which means the element at VAL's index plus 1.

TESTFN is passed directly to `seq-position'."
  (unless (numberp n) (setq n 1))
  (seq-elt sequence (% (+ n (or (seq-position sequence val testfn)
                                0))
                       (seq-length sequence))))

(defun k/mark-category (seq category)
  "Mark SEQ as being in CATEGORY.

Return a collection (as defined by `completing-read') that is the
same as passing SEQ to `completing-read' except with category
information attached.

See Info node `(elisp) Programmed Completion' for the official
documentation on this system.

Category information is conveyed by having collection functions
respond metadata when Emacs asks for it. This has begun being
utilized by packages such as Marginalia to display different
information for different types of completion entries, or by
Embark to create what are in effect context menus."
  (lambda (str pred action)
    (pcase action
      ('metadata
       `(metadata (category . ,category)))
      (_
       (all-completions str seq pred)))))

;;;; Date / time

(defun k/iso8601-p (thing)
  "Is THING a ISO 8601 timestamp?"
  (ignore-errors
    (parse-iso8601-time-string thing)))

(defun k/contains-iso8601-p (string)
  "Does STRING contain a ISO 8601 timestamp?

This will return a lot of false positives, but speed is more
important than correctness here because the main use of this
function is telling Tramp to not treat a timestamp as a protocal
and attempt to connect to it."
  (not (not (string-match-p
             "....-..-..T..:..:.."
             string))))

(defun k/iso8601->rfc822 (iso8601-date)
  "Convert ISO8601-DATE to RFC 822 format.

More accurately, convert an ISO 8601 date string to one that
`parse-time-string' understands.

Example:
\"2019-08-09T01:40:17+0900\" -> \"Fri,  9 Aug 2019 01:40:17 +0900\""
  (format-time-string
   "%a, %e %b %Y %H:%M:%S %z"
   (parse-iso8601-time-string iso8601-date)))

(defun k/date-iso8601 (&optional date copy?)
  "Get current time in ISO 8601 format.

If DATE is not nil, display it in ISO 8601 format instead of now.

Interactively (if COPY? is non-nil), copy the result."
  (interactive
   (list nil t))
  (let ((ret (format-time-string "%FT%T%z" date)))
    (when copy?
      (kill-new ret))
    ret))

(defun k/today (&optional n)
  "Return today's date, taking `org-extend-today-until' into account.

Return values look like \"2020-01-23\".

If N is non-nil, return N days from today. For example, N = 1
means tomorrow, and N = -1 means yesterday."
  (interactive (list (and current-prefix-arg
                          (prefix-numeric-value current-prefix-arg))))
  (unless n
    (setq n 0))
  (format-time-string
   "%Y-%m-%d"
   (time-add
    (* n 86400)
    (time-since
     ;; if it's bound and it's a number, do the same thing `org-today' does
     (or (and (boundp 'org-extend-today-until)
              (numberp org-extend-today-until)
              (* 3600 org-extend-today-until))
         ;; otherwise just return (now - 0) = now.
         0)))))

(defun k/all-months-last-year-to-now ()
  "Return a list of months from January last year to this month.

Months are returned as strings of yyyy-mm.

For example, when it is 2021-03-01, this returns the list
  (\"2020-01\" \"2020-02\" ... \"2021-02\" \"2021-03\")."
  (require 'calendar)
  (let* ((now (calendar-current-date))
         (current-month (car now))
         (current-year (elt now 2)))
    (append (cl-loop for month in (-iota 12 1)
                     collect (format "%s-%02d" (1- current-year) month))
            (cl-loop for month in (-iota current-month 1)
                     collect (format "%s-%02d" current-year month)))))

;;;; Benchmark

(defmacro k/time (&rest body)
  "Run BODY, then return amount of time it ran for."
  `(let ((start (current-time)))
     ,@body
     (float-time (time-subtract (current-time) start))))

(defmacro k/benchmark (n &rest forms)
  "Run each of FORMS N times, then return average amount of time each ran for.

Extra: When AVERAGE is nil (it defaults to t), return the total
elapsed time, not averaged.
\n(fn N &key (AVERAGE t) &rest FORMS)"
  (declare (indent 1))
  (let* ((unique-nil (make-symbol "-nil-"))
         ;; This dance with an uninterned symbol allows us to
         ;; distinguish when it's not supplied and when a nil is
         ;; given.
         (average-arg (-some--> forms
                        (seq-take it 2)
                        (-replace nil unique-nil it)
                        (plist-get it :average)))
         (average? (not (eq average-arg unique-nil)))
         (forms
          (if average-arg (cddr forms) forms))
         (elem-vars (mapcar (lambda (_elem) (gensym)) forms))
         (dotimes (cl-find-if #'fboundp
                              '(dotimes-with-progress-reporter dotimes))))
    `(let (,@elem-vars)
       (,dotimes (_ ,n) "Benchmarking"
                 ,@(cl-mapcar
                    (lambda (elem elem-var) `(push (k/time ,elem) ,elem-var))
                    forms
                    elem-vars))
       (list ,@(mapcar
                (lambda (results-var)
                  (if average?
                      `(k/average ,results-var)
                    `(apply #'+ ,results-var)))
                elem-vars)))))

(defmacro k/benchmark-compiled (n &rest forms)
  "Run each of FORMS N times, then return average amount of time each ran for."
  (declare (indent 1))
  ;; uninterned symbol here to hide it from FORMS
  (let ((prep (make-symbol "--prep--"))
        (count (make-symbol "--count--")))
    `(let ((,prep (make-progress-reporter
                   ;; I guess if the string ends with a paren progress
                   ;; reporter doesn't add the ellipsis?
                   "Benchmarking (compiled)..."
                   ;; 1-, because length is 1-indexed but prep is 0-indexed
                   0 (1- ,(* n (length forms)))))
           (,count 0))
       (prog1 (mapcar
               #'k/average
               (list
                ,@(--map
                   ;; doing this because we cannot access our own
                   ;; progress reporter from within `benchmark-run-compiled'
                   `(let (results)
                      (dotimes (i ,n results)
                        (push
                         (car
                          (benchmark-run-compiled 1 ,it))
                         results)
                        (progress-reporter-update
                         ,prep (setq ,count (1+ ,count)))))
                   forms)))
         (progress-reporter-done ,prep)))))

;;;; Alternative: based on benchmark-run

(defmacro k/benchmark-run (n &rest forms)
  "Benchmark each of FORMS with `benchmark-run' with N repetitions."
  (declare (indent 1))
  (cl-with-gensyms (first-total)
    `(let (,first-total)
       (list
        '(form\# faster\ by total gc-count gc-time)
        'hline
        ,@(cl-loop
           with index = 0
           for form in forms
           collect
           (cl-with-gensyms (result)
             (cl-incf index)
             `(progn
                (garbage-collect)
                (let ((,result (benchmark-run ,n
                                 ,form)))
                  ,@(when (= index 1)
                      `((setq ,first-total (car ,result))))
                  (append
                   (list
                    ,index
                    (format "%.3fx"
                            (/ ,first-total
                               (car ,result)
                               1.0)))
                   ,result)))))))))

(defmacro k/benchmark-run-compiled (n &rest forms)
  "Benchmark each of FORMS, byte-compiled, with N repetitions."
  (declare (indent 1))
  `(list
    '(form\# total gc-count gc-time)
    'hline
    ,@(cl-loop with index = 1
               for form in forms
               collect
               (prog2
                   (garbage-collect)
                   `(cons ,index
                          ;; Because `benchmark-run-compiled'
                          ;; quotes the lambda, it is not able to
                          ;; see any let form around it.
                          (benchmark-call (byte-compile (lambda () ,form))
                                          ,n))
                 (cl-incf index)))))

;;;; Macros

(defmacro k/defun (name arglist &rest body)
  "Like `defun' but with BODY wrapped in a nil `cl-block'.
This means `cl-return' can be used alone like `return' in other
languages.

NAME, ARGLIST, DOCSTRING, DECL, INTERACTIVE and BODY are all
passed straight to `defun'.

\(fn NAME ARGLIST [DOCSTRING] [DECL] [INTERACTIVE] BODY...)"
  (declare (doc-string 3) (indent 2))
  (cl-destructuring-bind
      (decl . body)
      (macroexp-parse-body body)
    `(defun ,name ,arglist
       ,@decl
       (cl-block nil
         ,@body))))

(defmacro k-update (sym func)
  "Update SYM with FUNC.

The value of SYM is passed to FUNC, then its return value is used
as the new value of SYM."
  `(setq ,sym (funcall ,func ,sym)))

(defmacro k--update (sym &rest body)
  "Update SYM with the value of evaluating BODY.
Within BODY, `it' is bound to the value of SYM.
The anaphoric counterpart to `k-update'."
  (declare (indent 1))
  `(let ((it ,sym))
     (setq ,sym (progn ,@body))))

(defmacro k/once-after-make-frame (&rest body)
  "Eval BODY on `server-after-make-frame-hook' just once."
  (declare (indent 0))
  `(let (func)
     ;; Using `setq' allows the function itself to see the variable.
     ;; I learned this trick from deferred.el.
     (setq func (lambda ()
                  ,@body
                  (remove-hook 'server-after-make-frame-hook func)))
     (add-hook 'server-after-make-frame-hook func)))

;; TODO: somehow integrate this with evil
(cl-defmacro k/defun-string-or-region
    (name &optional no-replace docstring &rest body)
  "Define NAME as a function with BODY that works on both strings and regions.

By default, the resulting function replaces the active region with the
return value. If NO-REPLACE is non-nil, don't do that.

DOCSTRING is passed to `defun'.
`string' is bound in BODY representing the string input or region content.

Based on http://ergoemacs.org/emacs/elisp_command_working_on_string_or_region.html."
  (declare (indent 2)
           (doc-string 3))
  `(defun ,name (string &optional start end)
     ,(concat docstring "

If STRING is nil, the region between START and END is used as STRING."
              (if no-replace
                  ""
                "\nInteractively, replace the current paragraph or active region."))
     (interactive
      ;; when run interactively, STRING is always nil
      (if (use-region-p)
          (list nil (region-beginning) (region-end))
        (let ((bounds (bounds-of-thing-at-point 'paragraph)))
          (list nil (car bounds) (cdr bounds)))))
     (if string
         ,@body
       (save-excursion
         (let ((result
                (let ((string
                       (buffer-substring-no-properties start end)))
                  ,@body)))
           ,(if no-replace
                '(progn result)
              '(progn
                 (delete-region start end)
                 (goto-char start)
                 (insert result))))))))

(defmacro k/with-file (file save &rest body)
  "Run BODY with FILE opened as the current buffer.

When SAVE is non-nil, write the buffer to FILE afterwards.

This reuses the buffer that visits FILE, including all potential
modifications in it. Cursor position, mark, and narrowing are
reset, however."
  (declare (indent 2))
  (if save
      `(with-current-buffer (find-file-noselect ,file)
         (prog1 (save-mark-and-excursion
                  (save-restriction
                    (goto-char (point-min))
                    ,@body))
           (basic-save-buffer)))
    `(with-temp-buffer
       (insert-file-contents-literally ,file)
       (decode-coding-region (point-min) (point-max) 'utf-8)
       (goto-char (point-min))
       ,@body)))

(cl-defmacro k/define-command-palette (name docstring &key prompt commands)
  "Define NAME as a command palette.

A command palette is an interactive function that prompts with a
list of commands to select. DOCSTRING will be its documentation.
PROMPT is the prompt that will show up when selecting commands.

COMMANDS is a list of commands. It can also be a keymap. This is
evaluated at runtime, when the command palette itself is called.

If Marginalia is enabled, each command's docstring
will be shown in the prompt.

Example:

  (k/define-command-palette k/devdocs
    \"A command palette for devdocs commands.\"
    :prompt \"Devdocs command: \"
    :commands \\='(devdocs-lookup
                devdocs-delete))"
  (declare (indent 1) (doc-string 2))
  `(defun ,name ()
     ,docstring
     (interactive)
     (let* ((commands ,commands)
            (commands (if (keymapp commands)
                          (->>
                           ;; `-flatten' errors out on keymaps. For some reason.
                           (flatten-tree commands)
                           (--filter (and (functionp it) (commandp it)))
                           (-remove-item ',name)
                           -uniq)
                        commands))
            (candidates (k/mark-category commands 'command))
            (selection (completing-read ,prompt candidates nil t))
            (func (intern selection))
            ;; pass prefix arg to the command we're about to run
            (prefix-arg current-prefix-arg))
       (message "%s" selection)
       (command-execute func))))

(defmacro k/set-mode-local (mode &rest args)
  "Like `setq-mode-local', except:

- MODE is evaluated. This allows using variables.
- If MODE is a list, set local values for every MODE.

ARGS look like (VAR VALUE VAR2 VALUE2 ... ...)."
  (declare (indent 1))
  (when args
    (let (i ll bl sl tmp tmp2 mode-list-sym sym val)
      (setq i 0
            mode-list-sym (make-symbol "mode-list-sym")
            tmp2 (make-symbol "tmp2"))
      (while args
        (setq tmp  (make-symbol (format "tmp%d" i))
              i    (1+ i)
              sym  (car args)
              val  (cadr args)
              ll   (cons (list tmp val) ll)
              bl   (cons `(cons ',sym ,tmp) bl)
              sl   (cons `(set (make-local-variable ',sym) ,tmp) sl)
              args (cddr args)))
      `(let* ,(nreverse ll)
         ;; Use a uninterned symbol to avoid polluting the body
         (let ((,mode-list-sym (if (symbolp ,mode)
                                   (list ,mode)
                                 ,mode)))
           (dolist (,tmp2 ,mode-list-sym)
             ;; Save mode bindings
             (mode-local-bind (list ,@bl) '(mode-variable-flag t) ,tmp2)
             ;; Assign to local variables in all existing buffers in MODE
             (mode-local-map-mode-buffers #'(lambda () ,@sl) ,tmp2))
           ;; Return the last value
           ,tmp)))))

(defmacro k/for-in (var sequence &rest body)
  "Run BODY for each element in SEQUENCE, with VAR being bound to the element.

Basically `dolist', but:
- works on sequences, not just lists (kind of like `seq-doseq')
- has a less wordy syntax
- compiles to `cl-loop' and so supports `cl-loop'-style destructuring"
  (declare (indent 2))
  `(cl-loop for ,var being the elements of ,sequence
            do (progn ,@body)))

;;;; Helpers that return functions

;; From deferred.el (L94)
(defmacro lambda/self (args &rest body)
  "Like `lambda', except a symbol `self' is bound to the function itself.

ARGS and BODY are as in `lambda'.

`interactive' and `declare' do not work."
  (declare (indent defun)
           (doc-string 2))
  (let ((argsyms (mapcar (lambda (_) (cl-gensym)) args)))
    `(lambda ,argsyms
       (let (self)
         (setq self (lambda ,args ,@body))
         (funcall self ,@argsyms)))))

;; Although these are macros, I mainly use them for their return values.
(defmacro k/turn-off-minor-mode (minor-mode)
  "Define a function that will call MINOR-MODE with -1.

MINOR-MODE should NOT be a quoted."
  (unless (symbolp minor-mode)
    (error "The minor mode should be an unquoted symbol"))
  (let ((name (intern (format "k/turn-off-%s" minor-mode))))
    `(progn
       (defun ,name ()
         ,(format "Turn off `%s'.

Defined by `k/turn-off-minor-mode'."
                  minor-mode)
         (,minor-mode -1))
       ',name)))

(defmacro k/set-x-to-val (x val)
  "Define a function to set X to VAL.

Return the symbol to the function."
  (let ((name (intern (format "k/set-%s-to-%s" x val))))
    `(progn
       (defun ,name ()
         ,(format "Set `%s' to %s.\n\nDefined by `k/set-x-to-val'."
                  x val)
         (setq ,x ,val))
       ;; in case `defun' decides to no longer return the symbol
       ',name)))

;; Modified from https://nullprogram.com/blog/2010/09/29/
;; Now that we have lexical binding, I don't think we need `lexical-let'?
(defun k/expose (function)
  "Return an interactive version of FUNCTION."
  (lambda ()
    (interactive)
    (funcall function)))

(defun k/find-file-command (path)
  "Return an interactive command that visits PATH."
  (lambda ()
    (interactive)
    (find-file path)))

(defun k/listof (pred)
  "Is the argument a list of things satisfying PRED?

Return a function that asks the above question."
  (lambda (x)
    (and (listp x)
         (seq-every-p pred x))))

(defun k/consof (pred)
  "Return a function.

That function returns t when its argument is a cons and both
elements satisfy PRED."
  (lambda (x)
    (and (consp pred)
         (funcall pred (car x))
         (funcall pred (cdr x)))))

(defun k/call-process-to-string (command &rest args)
  "Call COMMAND with ARGS; return stdout.

If COMMAND returns with a non-zero exit code, signal an error."
  (declare (indent 0))
  (with-temp-buffer
    (let ((code (apply #'call-process command nil '(t nil) nil args)))
      (unless (= 0 code)
        (error "%s exited with exit code %s" command code)))
    (string-trim (buffer-string))))

(defun k/call-process (verbose command &rest args)
  "Run COMMAND with ARGS and put its output in current buffer.

If VERBOSE is non-nil, COMMAND and ARGS are also inserted to the
buffer."
  (declare (indent 1))
  (when verbose
    (insert (format "%s" (vconcat (list command) args)) "\n"))
  (apply #'call-process command nil t nil args))

(defmacro k/append-unique (&rest args)
  "Like `append' on ARGS but unique."
  `(-uniq
    (append ,@args)))

(defmacro k/append-unique! (place &rest sequences)
  "Set PLACE to the result of appending PLACE and SEQUENCES."
  (declare (indent 1))
  (gv-letplace (getter setter) place
    (funcall setter `(-uniq
                      (append ,getter ,@sequences)))))

(defmacro k/add! (place &rest elems)
  "Add ELEMS to PLACE, also ensuring PLACE has no duplicate items."
  (declare (indent 1))
  `(progn
     (dolist (elem (list ,@elems))
       (push elem ,place))
     (setf ,place (-uniq ,place))))

(defmacro k/remove! (place &rest elems)
  "Remove ELEMS from PLACE."
  (declare (indent 1))
  `(setf ,place
         ,(--reduce-from
           `(delete ,it ,acc)
           place
           elems)))

(cl-defmethod k/available? ((feature symbol))
  "Is FEATURE available?"
  (not (not (locate-library (symbol-name feature)))))

(defmacro k/find-modes (which &rest body)
  "Return modes that make BODY return non-nil.
The symbol of the major mode is bound to `it' in BODY.

WHICH can be `:major', to only return major modes, or `:both', to
return major modes and minor modes.

Minor modes are detected by name."
  (declare (indent 1))
  (let ((ret (make-symbol "ret"))
        (sym (make-symbol "sym")))
    `(let (,ret)
       (mapatoms
        (lambda (,sym)
          (when (and
                 (symbolp ,sym)
                 (or (and (not (eq ,which :major))
                          (s-suffix? "mode" (format "%s" ,sym)))
                     (map-contains-key
                      (symbol-plist ,sym)
                      'derived-mode-parent))
                 (let ((it ,sym))
                   ,@body))
            (push ,sym ,ret))))
       ,ret)))

(defmacro k/define-initial-template (mode str)
  "Define STR to be the initial template of MODE.
When a new file-visiting buffer in MODE is created (but the file
does not yet exist), STR will be inserted."
  (declare (indent 1))
  (let ((func (intern (format "k/%s-initialize" mode)))
        (hook (derived-mode-hook-name mode)))
    `(progn
       (defun ,func ()
         ,(format "Insert a default template for major mode `%s'." mode)
         (when (and buffer-file-name
                    (not (file-exists-p buffer-file-name))
                    (= (buffer-size) 0))
           (save-excursion
             (insert
              (string-trim ,str)))))
       (add-hook ',hook #',func))))

(defmacro k/face-set (faces &rest attr)
  "Set attributes in ATTR for FACES in all existing and future frames.

FACES can be a list of face symbols or a single face symbol. This
is evaluated at runtime, so variables will work as expected.

ATTR is like `set-face-attribute'\\='s arguments, except that
keyword values means to \"take the value of the corresponding key
of this face\". Or, in less jibberish terms:

  (k/face-set abc
    :foreground :org-quote)

is syntactic sugar for:

  (k/face-set abc
    :foreground (face-attribute \\='org-quote :foreground))

."
  (declare (indent 1))
  (let ((attr (--> (map-into attr 'alist)
                   (map-apply
                    (lambda (k v)
                      (cond
                       ((keywordp v)
                        (cons k `(face-attribute
                                  ',(-> v
                                        symbol-name
                                        (substring 1)
                                        intern)
                                  ,k)))
                       (t
                        (cons k v))))
                    it)
                   (map-into it 'plist))))
    `(dolist (face (ensure-list ,faces))
       (set-face-attribute face nil ,@attr))))

(defun k/set-buffer-substring (start end string)
  "Replace buffer content between START and END with STRING."
  (declare (indent 2))
  (save-excursion
    (delete-region start end)
    (goto-char start)
    (insert string)))

(provide 'kisaragi-helpers)
;;; kisaragi-helpers.el ends here

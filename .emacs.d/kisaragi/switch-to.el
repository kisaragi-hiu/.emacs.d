;;; switch-to.el --- Switch to Vterm, Eshell, etc. -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1
;; Package-Requires: ((emacs "26.1") (dash "2.19.1"))
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(require 'cl-lib)
(require 'dash)

(defgroup switch-to nil
  "Commands for switching to buffers of a mode."
  :group 'convenience
  :prefix "switch-to-")

(defcustom switch-to-toggle t
  "Enable toggling behavior."
  :group 'switch-to
  :type 'boolean)

(defun switch-to--mark-category (seq category)
  "Mark SEQ as being in CATEGORY.

Return a collection (as defined by `completing-read') that is the
same as passing SEQ to `completing-read' except with category
information attached.

See Info node `(elisp) Programmed Completion' for the official
documentation on this system."
  (lambda (str pred action)
    (pcase action
      ('metadata
       `(metadata (category . ,category)))
      (_
       (all-completions str seq pred)))))

(cl-defun switch-to--internal (new &key command mode prompt toggle)
  "Switch to a buffer of MODE or create a new one.

Applicable to Eshell, Vterm, EWW, etc.

- MODE is the major mode whose buffers we are considering.
- PROMPT is the prompt in the completion when there are more than
  one matching buffers.
- NEW is the prefix argument. When it is non-nil, we always
  create a new buffer of MODE with COMMAND. It is also passed
  onto COMMAND as the prefix argument.
- When TOGGLE is non-nil, if there is only one matching buffer
  and we're already in it, go to the previous buffer (by running
  `previous-buffer') instead."
  (declare (indent 1))
  (cl-block nil
    (let ((prefix-arg new))
      (when new
        (cl-return (call-interactively command)))
      (let* ((root (projectile-project-root))
             (buffers
              (->> (buffer-list)
                   ;; Only keep buffers that are derived from `mode'
                   (--filter (-> (buffer-local-value 'major-mode it)
                                 (provided-mode-derived-p mode)))
                   ;; Only consider buffers in current project
                   (--filter
                    (or (not root)
                        (projectile-project-buffer-p it root)))
                   (-map #'buffer-name))))
        (pcase (length buffers)
          (0 (call-interactively command))
          (1
           (let ((old-buffer (current-buffer)))
             (switch-to-buffer (car buffers))
             (when (and toggle (equal old-buffer (current-buffer)))
               ;; Ended up doing nothing. Go to the previous buffer
               ;; instead.
               (call-interactively #'previous-buffer))))
          (_ (switch-to-buffer
              (completing-read
               prompt
               (switch-to--mark-category buffers 'buffer)
               nil t nil 'buffer-name-history))))))))

(cl-defmacro switch-to--definer
    (name &key display-name command mode library)
  "Command definition helper.
LIBRARY: the command only loads after LIBRARY is loaded
NAME: the name of the defined command.
DISPLAY-NAME: the display name of the mode, like \"Vterm\" or \"EWW\"
MODE: the major mode
COMMAND: which command starts the major mode"
  (declare (indent 1))
  `(progn
     (autoload ',name (symbol-name ,library) nil t)
     (with-eval-after-load ,library
       (defun ,name (&optional arg)
         ,(format "Switch to an existing %s buffer or create a new one.

A new %s buffer is always created if none exist.

When ARG is non-nil, or with a \\[universal-argument], a new one
is also created. ARG will be passed onto `%s' in this case.

If `switch-to-toggle' is non-nil, running this command will
toggle between the current buffer and %s. This behavior is
disabled for predictability if there is more than one %s
buffer."
                  display-name
                  display-name
                  command
                  display-name
                  display-name)
         (interactive "P")
         (switch-to--internal arg
           :command ,command
           :mode ,mode
           :prompt ,(format "Which %s buffer: " display-name)
           :toggle switch-to-toggle)))))

(switch-to--definer switch-to-vterm
  :library 'vterm
  :display-name "Vterm"
  :command #'vterm
  :mode 'vterm-mode)

(switch-to--definer switch-to-eshell
  :library 'eshell
  :display-name "Eshell"
  :command #'eshell
  :mode 'eshell-mode)

(switch-to--definer switch-to-eww
  :library 'eww
  :display-name "EWW"
  :command #'eww
  :mode 'eww-mode)

(provide 'switch-to)

;;; switch-to.el ends here

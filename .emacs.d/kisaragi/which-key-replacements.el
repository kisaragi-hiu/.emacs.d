;;; which-key-replacements.el --- Annotations for which-key -*- lexical-binding: t -*-

;;; Commentary:

;; Add annotations.

;;; Code:

(require 'which-key)
(require 'ht)

(setq which-key-allow-multiple-replacements t)

(defun k/which-key-add-prefix (prefix &rest replacements)
  "Add PREFIX to REPLACEMENTS."
  (declare (indent 1))
  (->> (ht<-plist replacements)
       (ht-map (lambda (k v) `(,(format "%s %s" prefix k) . ,v)))
       ht<-alist
       ht->plist))

(defun k/which-key-add-key-based-replacements-in-prefix
    (prefix description &rest replacements)
  "Replace PREFIX's description with DESCRIPTION, then add REPLACEMENTS with PREFIX added."
  (declare (indent 2))
  (apply #'which-key-add-key-based-replacements
         prefix description
         (apply #'k/which-key-add-prefix prefix replacements)))

;; um.
(cl-defmacro k/which-key-add-major-mode-key-based-replacements-in-prefix
    (mode &rest lists)
  "Apply REPLACEMENTS in major MODE with PREFIX added.

LISTS is a list of (PREFIX KEY REPLACEMENT ...) items.

Example:

\(k/which-key-add-major-mode-key-based-replacements-in-prefix
 'org-mode
 (\"C-c\"
  \"~\" \"table.el style table\"))"
  (declare (indent 1))
  ;; Emacs lisp doesn't limit number of arguments, it seems
  ;; This runs just fine:
  ;; (apply (lambda (&rest args) args) (make-list 10000000 nil))
  ;; The Common Lisp equivalent overflows the control stack on SBCL
  ;; somewhere between 100000 and 1000000.
  `(apply
    #'which-key-add-major-mode-key-based-replacements ,mode
    (-flatten
     (mapcar
      (pcase-lambda (`(,prefix . ,replacements))
        (apply #'k/which-key-add-prefix
               prefix replacements))
      (list
       ;; This allows for using variables.
       ;; Arguments in the lists are still eval'ed, but we avoid
       ;; actually using `eval' directly.
       ,@(--map (cons 'list it) lists))))))

(k/which-key-add-key-based-replacements-in-prefix
    "C-x @" "Modifiers"
  "a" "Alt"
  "c" "Control"
  "h" "Hyper"
  "m" "Meta"
  "s" "Super"
  "S" "Shift")

(k/which-key-add-key-based-replacements-in-prefix
    "C-x n" "Narrowing"
  "d" "Narrow to defun"
  "n" "Narrow to region"
  "p" "Narrow to page"
  "w" "Widen")

(with-eval-after-load 'projectile
  (k/which-key-add-key-based-replacements-in-prefix
      "SPC p" "Projectile"
    "SPC" "Jump to buffer or file"
    "!" "Run command in root"
    "&" "Run command in root (async)"
    "4" "buffer"
    "5" "frame"
    "C" "Configure project"
    "D" "Open root in Dired"
    "E" "Edit .dir-locals"
    "I" "IBuffer"
    "O" "Project Org"
    "Oa" "Open project agenda"
    "Oc" "Capture into current project"
    "P" "Run project tests"
    "R" "Regenerate tags"
    "T" "Find test files"
    "V" "Browse dirty projects"
    "a" "Find file with same name"
    "c" "Compile project"
    "l" "Find file in directory"
    "o" "Search in project buffers"
    "p" "Switch project"
    "q" "Switch project (opened)"
    "r" "Replace string in project"
    "s" "Search"
    "t" "Toggle code and test"
    "u" "Run project"
    "v" "VC status"
    "x" "Run in project root"
    "xe" "eshell"
    "xi" "IELM (elisp REPL)"
    "xs" "shell"
    "xt" "term"))

(with-eval-after-load 'flycheck
  (k/which-key-add-key-based-replacements-in-prefix
      flycheck-keymap-prefix "Flycheck"
    "?" "Describe a checker"
    "s" "Select a checker"
    "x" "Disable a checker"
    "C" "Clear errors"
    "C-c" "Run `compile'"
    "V" "Version"
    "c" "Check this buffer"
    "e" "Explain error at point"
    "h" "Display error at point"
    "C-w" "Copy error at point"
    "i" "Open Flycheck manual"
    "l" "Error list"
    "n" "Next error"
    "p" "Previous error"
    "v" "List applicable checkers"))

(with-eval-after-load 'typo
  (which-key-add-key-based-replacements
    "C-c 8" "Typo symbols"))

(with-eval-after-load 'org
  (k/which-key-add-major-mode-key-based-replacements-in-prefix
      'org-mode
    ("C-c"
     "~" "table.el style table"
     "}" "Toggle table row numbers"
     "|" "Create table"
     "{" "Toggle formula debug"
     "`" "Edit field"
     "?" "Field info"
     "^" "Sort"
     "C-o" "Open thing"
     "@" "Select subtree"
     "=" "Table evaluate formula"
     "+" "Table sum"
     "C-x" "More..."
     "M-l" "Insert last stored"
     "C-q" "Set tags"
     "C-e" "Export..."
     "C-b" "Previous sibling"
     "C-f" "Next sibling"
     "C-p" "Previous heading"
     "C-n" "Next heading"
     "C-u" "Up a heading"
     "C-^" "Up an element"
     "C-_" "Down an element"
     "C-v" "Org-babel"
     "C-a" "Add attachment"
     "M-w" "Refile heading (copy)"
     "C-w" "Refile heading"
     "C-*" "リストをツリーに"
     "C-k" "Abort storing note"
     "C-d" "Deadline"
     "C-s" "Schedule"
     "C-r" "Reveal"
     "RET" "Insert heading"
     "C-," "Insert block template"
     "." "Insert timestamp"
     "," "Change priority"
     "\"" "Plot"
     "$" "Archive subtree"
     "#" "Update stat cookies"
     "<" "Insert date from calendar"
     ">" "Goto date"
     "C-<" "Promote heading"
     "C->" "Demote heading"
     "'" "Edit element"
     "[" "Add to agenda"
     "]" "Remove from agenda"
     "%" "Mark position"
     "SPC" "Blank table field"
     "C-y" "Evaluate time range"
     ":" "Toggle fixed-width"
     "/" "Highlight..." ; Sparse tree?
     "\\" "Match highlight..."
     ";" "Toggle comment")
    ("C-c C-x"
     "," "Pause timer"
     "-" "Insert timer item"
     "." "Insert and start timer"
     "0" "Start timer"
     ";" "Set timer"
     "_" "Stop timer"))
  (k/which-key-add-major-mode-key-based-replacements-in-prefix
      'org-agenda-mode
    ("C-x"
     "C-w" "Save agenda view"
     "C-s" "Save all Org buffers")
    ("C-c"
     "C-a" "Add attachment"
     "C-d" "Set deadline"
     "C-s" "Set schedule"
     "C-q" "Set tags"
     "," "Set priority"
     "C-t" "Todo"
     "C-w" "Refile"
     "C-o" "Open link"
     "C-n" "Next date line"
     "C-p" "Previous date line")
    ("s"
     "^" "Filter by top-headline"
     "c" "Filter by category"
     "e" "Filter by effort"
     "r" "Filter by regexp"
     "s" "Limit interactively"
     "t" "Filter by tag")
    ("c"
     "T" "Set timer"
     "c" "Cancel clock"
     "e" "Set effort"
     "g" "Go to clock"
     "r" "clocktable mode"
     "t" "Set tags")
    ("d"
     "A" "Archive entry (no confirm)"
     "a" "Archive entry"
     "d" "Kill (cut) entry")
    ("g"
     "C" "Convert date"
     "R" "Revert all agenda views"
     "r" "Revert this agenda"
     "c" "Go to date in calendar"
     "d" "Go to agenda of date"
     "h" "Display holidays"
     "m" "Display moon phase"
     "s" "Display sunrise & sunset"
     "j" "Next item"
     "k" "Previous item"
     "t" "Show tags")))

(with-eval-after-load 'dired-filter
  (k/which-key-add-major-mode-key-based-replacements-in-prefix
      'dired-mode
    (k/dired-filter-prefix
     "!" "Negate filters"
     "*" "Decompose OR or NOT"
     "." "Filter by extension"
     "/" "Pop all filters"
     "A" "Add saved filters"
     "D" "Delete saved filters"
     "L" "Load saved filters"
     "S" "Save filters"
     "TAB" "Transpose filters"
     "d" "Show only directories"
     "e" "Filter by predicate"
     "f" "Show only files"
     "g" "Hide garbage files"
     "h" "Hide dot files"
     "i g" "Hide git ignored"
     "m" "Filter by mode"
     "n" "Filter by name"
     "o" "Hide omitted files"
     "p" "Pop a filter"
     "r" "Filter by regexp"
     "s" "Show only symlinks"
     "x" "Show only executables"
     "|" "OR filters")
    (k/dired-filter-mark-prefix
     "."  "Mark by extension..."
     "L"  "Mark by saved filters..."
     "d"  "Mark directories"
     "e"  "Mark by predicate..."
     "f"  "Mark files"
     "g"  "Mark garbage files"
     "h"  "Mark dot files"
     "ig" "Mark gitignored files"
     "m"  "Mark by major-mode..."
     "n"  "Mark by name..."
     "o"  "Mark omitted files"
     "r"  "Mark by regexp..."
     "s"  "Mark symlinks"
     "x"  "Mark executable files")))

(provide 'which-key-replacements)

;;; which-key-replacements.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:

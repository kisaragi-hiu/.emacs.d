;;; project-local-yasnippets.el --- Project-local yasnippets -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (projectile "2.5.0") (yasnippet "0.14.0"))
;; Keywords: project, convenience

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; commentary

;;; Code:

(require 'yasnippet)
(require 'projectile)

(defvar project-local-yasnippets/dbg? t)

(defun project-local-yasnippets/dbg (format &rest args)
  "Pas FORMAT and ARGS to `message' if debug mode is on."
  (when project-local-yasnippets/dbg?
    (apply #'message format args)))

(defun project-local-yasnippets/local-snippets-dir ()
  "Return the project local snippets directory, if it exists."
  (when-let ((root (projectile-project-root))
             (snippets-dir (expand-file-name ".snippets" root)))
    (when (file-directory-p snippets-dir)
      snippets-dir)))

(defun project-local-yasnippets/maybe-register-local-snippets-dir ()
  "Register local snippets directory if it exists."
  (project-local-yasnippets/dbg "registering local snippets")
  (when-let ((local-snippets-dir (project-local-yasnippets/local-snippets-dir)))
    (make-local-variable 'yas-snippet-dirs)
    (push local-snippets-dir yas-snippet-dirs)
    (yas-load-directory local-snippets-dir)))

;; Perhaps we should use projectile-after-switch-project-hook and put
;; it in the global value. This way we don't have to load it every
;; time we open a new file.
(add-hook 'find-file-hook #'project-local-yasnippets/maybe-register-local-snippets-dir)

(provide 'project-local-yasnippets)

;;; project-local-yasnippets.el ends here

;; Local Variables:
;; nameless-separator: "/"
;; End:

;;; altcs.el --- Alternative character select -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1
;; Package-Requires: ((emacs "26.1"))
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; When using an input method such as Chewing or RIME, the user types
;; the pronunciation, and has the option to select which character to
;; use.

;; This option, however, is only available before the text is
;; committed out of the IME. Once the text has been input, the only
;; way to replace a character is to delete it and type its
;; pronunciation again.
;;
;; (Replace "pronunciation" with "code" for shape-based schemes like
;; Cangjie.)
;;
;; This package attempts to provide an option to select from homonyms
;; of the character at point.
;;
;; TODO: automatic RIME dict downloading like cangjie.el
;; TODO: choosing another RIME dict
;;   Right now "alternative" = "has the same Mandarin pronunciation".
;;   We should be able to change it to "has the same Taiwanese
;;     pronunciation" or "has the same Cangjie code".

;;; Code:

(require 'subr-x)

(defgroup altcs nil
  "Selecting alternative Han characters."
  :group 'convenience
  :prefix "altcs-")

(defcustom altcs-popup-method 'cr
  "How to show alternatives.

- `cr': `completing-read', for Ivy, Helm, etc.
- `ido': `ido-completing-read'
- `ivy-posframe': Ivy, but with `ivy-posframe' enabled. Falls
  back to `completing-read' when not in a graphical frame.
- A function: call that function with the candidates.
- Anything else: same as `cr'."
  :group 'altcs
  :type '(choice (const cr :tag "completing-read")
                 (const ido :tag "ido-completing-read")
                 (const ivy-posframe :tag "ivy-posframe")
                 (function :tag "A custom function")))

(defvar altcs-dict "/usr/share/rime-data/terra_pinyin.dict.yaml")

(defvar altcs--dict-buffer "*altcs: dict*"
  "Buffer holding the dictionary.")

(defmacro altcs--with-file (file &rest body)
  "Run BODY with FILE opened as the current buffer."
  (declare (indent 1))
  `(with-current-buffer (get-buffer-create altcs--dict-buffer)
     (when (= 0 (buffer-size))
       (insert-file-contents ,file))
     (goto-char (point-min))
     ,@body))

(defun altcs--han->pn (han)
  "Return pronunciations of HAN."
  (altcs--with-file altcs-dict
    (cl-loop while (re-search-forward (format "^%s\t" han) nil t)
             collect (thread-first
                       (buffer-substring-no-properties
                        (line-beginning-position)
                        (line-end-position))
                       (split-string "\t")
                       cadr))))

(defun altcs--pn->han (pn)
  "Return han characters matching PN."
  (altcs--with-file altcs-dict
    (cl-loop while (re-search-forward (format "\t%s\\(?:$\\|\t\\)" pn)
                                      nil t)
             collect (thread-first
                       (buffer-substring-no-properties
                        (line-beginning-position)
                        (line-end-position))
                       (split-string "\t")
                       car))))

(defun altcs--han-alternatives (han)
  "Return alternatives of HAN."
  (thread-last (altcs--han->pn han)
               (mapcar #'altcs--pn->han)
               (apply #'append)))

(defun altcs--menu (coll)
  "Show a menu to complete COLL."
  (pcase altcs-popup-method
    ('ido
     (ido-completing-read "Alternative: " coll nil t))
    ('ivy-posframe
     (cond ((display-graphic-p) (progn
                                  (require 'ivy)
                                  (require 'ivy-posframe)
                                  (defvar ivy-display-functions-alist)
                                  (defvar ivy-posframe-width)
                                  (declare-function ivy-read "ivy")
                                  (let ((ivy-display-functions-alist '((t . ivy-posframe-display-at-point)))
                                        (ivy-posframe-width 10))
                                    (ivy-read "" coll
                                              :require-match t))))
           (t (completing-read "Alternative: " coll nil t))))
    (_
     (completing-read "Alternative: " coll nil t))))

(defun altcs-choose (start end)
  "Choose an alternative for characters between START and END.
Interactively, operate on the character at point or on the active
region."
  (interactive
   (if (region-active-p)
       (list (region-beginning) (region-end))
     (list (point) (1+ (point)))))
  (let* ((str (buffer-substring-no-properties start end))
         (alternatives (altcs--han-alternatives str)))
    (unless (> (length alternatives) 0)
      (user-error "No alternatives for \"%s\"" str))
    (let ((choice (altcs--menu alternatives)))
      (save-excursion
        (delete-region start end)
        (insert choice)))))

(define-minor-mode altcs-mode
  "Minor mode providing binding for alternative Han character selection."
  :group 'altcs
  :global t
  :lighter "Altcs "
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-<down>") #'altcs-choose)
            map))

(provide 'altcs)

;;; altcs.el ends here

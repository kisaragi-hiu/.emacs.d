;;; midi-to-ust.el --- Write MIDI notes into a UST file -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1.0

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; UTAU's midi import doesn't work with midi files exported from
;; Reaper (SWS)'s CF_ExportMediaSource. So here's my attempt at
;; extracting notes and dumping them into a UST file.

;;; Code:

(require 'dash)
(require 's)
(require 'f)

;; For `k/with-file'
(require 'kisaragi-helpers)

(defun midi-to-ust--bytes-to-number (seq &optional byte-size)
  "Return the raw bytes of SEQ as a number (big endian).

For example, \"abc\" is (string #x61 #x62 #x63), and this
function will return #x616263.

Use BYTE-SIZE to concatenate bytes that are not 8-bit wide, for
example when working with 7-bit values."
  (let ((max (1- (length seq)))
        (byte-size (or byte-size 8)))
    ;; #xABCD = (+ (* (expt 2 8) #xAB)
    ;;             (* (expt 2 0) #xCD))
    (cl-loop
     for i from 0 to max
     collect (let ((char (elt seq (- max i))))
               (ash char (* byte-size i)))
     into val
     finally return (apply #'+ val))))

(defun midi-to-ust--read-bytes-and-advance (n)
  "Read the next N bytes and move point after them.

Return the bytes as a number."
  (prog1 (midi-to-ust--bytes-to-number
          (buffer-substring
           (point) (+ n (point))))
    (forward-char n)))

(defun midi-to-ust--read-vlv-and-advance ()
  "Read the next \"variable length value\" and advance point.

The first bit indicates whether more bytes will follow, the rest
encodes the value. For example, the bytes

  1 0000001  0 0000010 (#x81 #x02)

encodes the value #b00000010000010."
  (cl-flet ((continues? (value) (>= value 128)))
    (let (values)
      (while
          (let ((char (char-after)))
            (push char values)
            (forward-char)
            (continues? char)))
      (midi-to-ust--bytes-to-number
       (cl-loop for byte in (nreverse values)
                collect (if (>= byte 128)
                            (- byte 128)
                          byte))
       ;; Taking away the first bit leaves us with 7-bit values.
       7))))

(defvar midi-to-ust-timebase 480)

(defun midi-to-ust--read-note ()
  "Read the next note.

A data chunk is <time><event>."
  (let ((length (--> (midi-to-ust--read-vlv-and-advance)
                     ;; normalize to timebase = 480
                     (floor (* 480 (/ it midi-to-ust-timebase 1.0)))))
        (type (let ((val (midi-to-ust--read-bytes-and-advance 1)))
                ;; I'm ignoring the channel here.
                (cond
                 ((<= #x80 val (1- #x90))
                  'stop)
                 ((<= #x90 val (1- #xA0))
                  'play)
                 ((<= #xB0 val (1- #xC0))
                  'control-change))))
        (a (midi-to-ust--read-bytes-and-advance 1))
        (b (midi-to-ust--read-bytes-and-advance 1)))
    (if (eq type 'control-change)
        (list :length length
              :type type
              :control a
              :data b)
      (list :length length
            :type type
            :key a
            :vel b))))

;;;###autoload
(defun midi-to-ust (mid out)
  "Convert midi data in file MID to UST and write it to OUT."
  (interactive "fWhich MIDI file to convert to UST: \nFWhich UST to write to: ")
  (let (_track-count _format _data-length)
    (with-temp-buffer
      (erase-buffer)
      (set-buffer-multibyte nil)
      (save-excursion
        (insert-file-contents-literally mid))
      (goto-char 9) ; skip the first 8 bytes
      ;; format
      (midi-to-ust--read-bytes-and-advance 2)
      ;; track count
      (midi-to-ust--read-bytes-and-advance 2)
      (setq midi-to-ust-timebase (midi-to-ust--read-bytes-and-advance 2))
      ;; Skip to just after "MTrk", the track section marker
      (while (not (equal (buffer-substring (- (point) 4)
                                           (point))
                         "MTrk"))
        (forward-char))
      ;; track data length
      (midi-to-ust--read-bytes-and-advance 4)
      (let (notes)
        (while (ignore-errors (push (midi-to-ust--read-note) notes)))
        (setq notes (nreverse notes))
        (k/with-file out t
          (unless (eq buffer-file-coding-system 'japanese-shift-jis-dos)
            (set-buffer-file-coding-system 'japanese-shift-jis-dos))
          (goto-char (point-max))
          ;; Append to file if the file is already a real UST
          (if (s-contains? "\n[#TRACKEND]" (buffer-string))
              (progn
                (delete-region (- (point) 12)
                               (point))
                (when (= ?\C-j (char-before))
                  (delete-char -1)))
            ;; Or create the file from scratch if it doesn't
            (erase-buffer)
            (insert (format "[#VERSION]
UST Version1.2
[#SETTING]
Tempo=120.00
Tracks=1
ProjectName=%s
VoiceDir=placeholder
OutFile=
CacheDir=
Tool1=wavtool.exe
Tool2=resampler.exe
Mode2=True"
                            (f-base mid))))
          (cl-loop for note in notes
                   do
                   (let ((key (plist-get note :key))
                         (length (plist-get note :length)))
                     (unless (ignore-errors (= 0 length))
                       (insert
                        (if key
                            (format "\n[#0000]
Length=%s
Lyric=ら
Intensity=100
Modulation=0
NoteNum=%s
PreUtterance="
                                    length
                                    key)
                          (format "\n[#0000]
Length=%s
Lyric=R
Intensity=100
Modulation=0
NoteNum=60
PreUtterance="
                                  length))))))
          (insert "\n[#TRACKEND]"))))))

;;;###autoload
(defun midi-to-ust-batch (mdir udir)
  "Convert MIDI files in MDIR to UST files in UDIR."
  (interactive "DDirectory for MIDI files: \nDDirectory for UST output: ")
  (cl-loop for midi in (directory-files mdir t "\\.midi?\\'")
           do (midi-to-ust midi
                           (f-join udir
                                   (concat (f-base midi) ".ust")))))

(provide 'midi-to-ust)

;;; midi-to-ust.el ends here

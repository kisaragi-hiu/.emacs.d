;;; packages.el --- Where packages are declared -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(require 'elpaca)
(require 'kisaragi-constants)

(elpaca elpaca-use-package
  (elpaca-use-package-mode))

;; This... works, actually.
;; This allows avoiding having to download large repositories like for
;; protobuf-mode (which lives in protobuf's repository).
(require 'package)
(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa" . "https://melpa.org/packages/")))
(package-read-all-archive-contents)
(unless package-archive-contents
  (package-refresh-contents))

;;;; Ensure newer packages

(eval-and-compile
  (defvar k/pkgs/count 0))
(defvar k/pkgs/package.el nil)

;; Reset registered content on reload of this file
(setq k/pkgs/count 0)
(setq k/pkgs/package.el nil)

(defmacro k/pkg-register (&rest pkgs)
  "Pass each of PKGS to `elpaca'."
  ;; Incredibly clumsy way to limit the number of concurrent builds
  ;; and clones. Without this Emacs gets OOM'd on my phone when
  ;; cloning for the first time.
  (let ((forms '()))
    (dolist (x pkgs)
      (push `(elpaca ,x) forms)
      ; (when (>= k/pkgs/count 4)
      ;   (push `(elpaca-wait) forms)
      ;   (setq k/pkgs/count 0))
      (cl-incf k/pkgs/count))
    `(progn
       ,@(nreverse forms)
       (elpaca-wait))))

(defun k/pkg-register/package.el (&rest pkgs)
  "Register PKGS as packages to be installed with package.el."
  (setq k/pkgs/package.el
        (cl-delete-duplicates
         (nconc k/pkgs/package.el pkgs)
         :test #'equal)))

(defun k/pkg-activate-all ()
  "Make all packages in `k/pkgs' available for `require'."
  (interactive)
  (let (package.el-errors)
    (dolist (pkg k/pkgs/package.el)
      (condition-case e
          (progn
            (package-install pkg)
            (package-activate pkg))
        (error
         (push (cadr e) package.el-errors))))
    (dolist (e (nreverse package.el-errors))
      (warn "package.el: %s" e))))

;; Use GitHub mirror which is faster than Savannah
(elpaca '(org :host github
              :repo "emacs-straight/org-mode"))

(elpaca emacsql-sqlite-module)
;; (elpaca
;;   '(emacsql-sqlite-module :type git
;;                           :flavor melpa
;;                           :files ("emacsql-sqlite.el"
;;                                   "sqlite"
;;                                   "emacsql-sqlite-common.el"
;;                                   "emacsql-sqlite-module.el")
;;                           :host github
;;                           :repo "magit/emacsql"))

;;;; Base packages

(elpaca-queue
 (k/pkg-register
  'leaf
  'leaf-keywords
  '(tst :repo "kisaragi-hiu/tst.el" :host gitlab)
  'a
  'aio
  'async-await
  'all-the-icons
  'async
  'dash
  'f
  'ht
  'parseedn
  'request
  's
  'ts
  'uuidgen
  'xr))

;;;; In Development

(k/pkg-register
 '(set :host github :repo "kisaragi-hiu/set.el")
 '(awk-ward :host gitlab :repo "kisaragi-hiu/awk-ward.el")
 '(ntoml :repo "git@github.com:kisaragi-hiu/ntoml.el")
 '(send-notification
      :repo "git@github.com:kisaragi-hiu/send-notification")
 '(interris
   :repo "git@github.com:kisaragi-hiu/interris")
 '(kisaragi-translate
   :repo "git@github.com:kisaragi-hiu/kisaragi-translate.el")
 '(info-variable-pitch :repo "git@github.com:kisaragi-hiu/info-variable-pitch")
 '(synchronize-git :type git
                   :host github
                   :repo "kisaragi-hiu/synchronize-git.el"
                   :branch "main"))

;;;; Appearance

(k/pkg-register
 'monokai-theme
 'doom-themes
 'color-theme-sanityinc-tomorrow
 'solaire-mode ; dim special buffers
 'doom-modeline
 'nyan-mode
 'anzu
 'evil-anzu
 'hl-todo)

;;;; Org and Markdown

(k/pkg-register/package.el
 'adoc-mode)

(unless k/android?
  (k/pkg-register
    'valign))

(k/pkg-register
 'ob-async
 'org-modern
 ;; Org itself is registered
 '(minaduki :host github
            :repo "kisaragi-hiu/minaduki"
            :branch "main")
 'org-edit-indirect
 '(org-import-simplenote
   :repo "git@github.com:kisaragi-hiu/org-import-simplenote")
 '(org-inline-video-thumbnails
   :host github
   :repo "kisaragi-hiu/org-inline-video-thumbnails")
 'edit-indirect
 'evil-org
 'markdown-mode
 'org-variable-pitch)

;;;; Keys

(k/pkg-register
 'transient
 'general)

;;;; Autocompletion

(k/pkg-register
 'company
 'company-prescient
 'company-quickhelp
 'company-posframe
 'company-org-block)

;;;; Completion

(k/pkg-register
 'consult
 'consult-flycheck ; jump to flycheck error
 'embark
 'embark-consult
 'ivy
 'ivy-prescient
 'counsel
 'marginalia
 'orderless
 'prescient)

(k/pkg-register
 'yasnippet
 'yasnippet-snippets
 ;; like consult-yasnippet but entering an unmatched input will create
 ;; a new snippet instead of being rejected
 'ivy-yasnippet)

;;;; Projectile

(k/pkg-register
 'projectile
 'counsel-projectile)

(when (executable-find "ag")
  (k/pkg-register
   'ag
   'wgrep-ag))

(when (executable-find "rg")
  (k/pkg-register
   'rg))

;;;; Syntax checking
(k/pkg-register
 'flycheck
 ;; 'flycheck-inline
 'sideline
 'sideline-flycheck)

;;;; Evil
(k/pkg-register
 'undo-tree
 'evil
 'evil-collection
 '(evil-cutlass :repo "git@github.com:kisaragi-hiu/evil-cutlass")
 'evil-goggles
 'evil-commentary
 'evil-surround
 'evil-easymotion
 'evil-numbers)

;;;; Flyspell

(when (and (executable-find "hunspell")
           (not k/android?))
  (k/pkg-register
   'flyspell-correct
   'flyspell-correct-helm))

;;;; Apps

(defvar rime-user-data-dir "~/.local/share/fcitx5/rime/")
(when (file-directory-p rime-user-data-dir)
  (k/pkg-register 'rime))

;; NOTE: migrated
(k/pkg-register
 'lorem-ipsum
 '(pangu-spacing
   :host nil
   :repo "git@github.com:kisaragi-hiu/pangu-spacing")
 '(yabridgectl
   :host nil
   :repo "git@github.com:kisaragi-hiu/yabridgectl-emacs")
 'dashboard
 '(kisaragi-log :repo "kisaragi-hiu/kisaragi-log"
                :host gitlab)
 '(taskrunner
   :host github
   :repo "emacs-taskrunner/emacs-taskrunner")
 '(ivy-taskrunner
   :host github
   :repo "emacs-taskrunner/ivy-taskrunner"))

;; NOTE: migrated
(when (executable-find "pacman")
  (k/pkg-register 'pacfiles-mode))

(unless k/android?
  (k/pkg-register
   'speed-type
   'devdocs
   'emacs-everywhere))

(when (executable-find "docker")
  (k/pkg-register 'docker))

(when (and (executable-find "cmake")
           (executable-find "libtool")
           module-file-suffix)
  (k/pkg-register 'vterm))

(k/pkg-register
 '(helpful :type git :flavor melpa :host github :repo "Wilfred/helpful"
           :fork (:repo "git@github.com:kisaragi-hiu/helpful.git"
                        :branch "kisaragi")))

;;;; Git

(k/pkg-register
 'magit
 'magit-todos
 'magit-lfs
 'magit-svn
 'git-gutter
 'git
 'git-link
 ;; creating releases
 'sisyphus)

(k/pkg-register
 '(git-zip-commit :type git :host github :repo "kisaragi-hiu/git-zip-commit"))

(when (executable-find "delta")
  (k/pkg-register 'magit-delta))

;;;; Dired

(k/pkg-register
 'dired-git-info
 'dired-open
 'diredfl
 'dired-narrow
 ;; '(dired-show-readme :host gitlab
 ;;                     :repo "kisaragi-hiu/dired-show-readme")
 '(dired-auto-readme :host github
                     :repo "amno1/dired-auto-readme"))

;;;; LSP

(unless k/android?
  (k/pkg-register
   'lsp-mode
   'lsp-pyright
   'lsp-ui
   'lsp-tailwindcss
   'lsp-dart
   ;; 'eglot
   'eldoc-box))

;;;; Folding
(k/pkg-register
 'outshine
 'origami)

;;;; Miscellaneous

(k/pkg-register 'which-key)

(when (or (executable-find "fcitx5")
          (executable-find "fcitx"))
  (k/pkg-register 'fcitx))

(when (and (not k/android?)
           (or (executable-find "google-chrome")
               (executable-find "google-chrome-unstable")
               (executable-find "google-chrome-stable")
               (executable-find "chromium")
               (executable-find "firefox")))
  (k/pkg-register 'atomic-chrome))

(k/pkg-register/package.el
 'rainbow-mode
 'hl-indent-scope)

(k/pkg-register
 'aggressive-indent
 'buttercup
 ;; 'smartparens
 'puni
 'page-break-lines
 'editorconfig
 'rainbow-delimiters
 'highlight-numbers
 'multiple-cursors
 'apheleia)

(k/pkg-register
 'olivetti)

(k/pkg-register '(didyoumean :host gitlab
                             :repo "kisaragi-hiu/didyoumean.el"))

;;;; Commands
(k/pkg-register/package.el
 ;; zero dependencies
 'fold-this)

(k/pkg-register
 'dumb-jump
 'link-hint
 'unfill
 '(yasearch :type git :repo "git@git.sr.ht:~kisaragi_hiu/yasearch"))

;;;; Languages / Major modes

(k/pkg-register/package.el
 'protobuf-mode
 'ninja-mode
 'groovy-mode
 'just-mode)

(unless k/android?
  (k/pkg-register 'ein))

(when (executable-find "go")
  (k/pkg-register
   'go-mode))

(when (executable-find "dart")
  (k/pkg-register
   'dart-mode))

(k/pkg-register
 'bnf-mode
 'csv-mode
 'ini-mode
 'toml-mode
 'lua-mode
 '(ust-mode :host github :repo "kisaragi-hiu/ust-mode")
 '(git-modes :type git :host github :repo "magit/git-modes"))

(unless k/android?
  (k/pkg-register/package.el
   'clojure-mode)) ; also used for editing .edn

(when (executable-find "kotlin")
  (k/pkg-register
   'kotlin-mode
   'flycheck-kotlin))

(when (executable-find "ledger")
  (k/pkg-register
   'ledger-mode
   'company-ledger
   'flycheck-ledger
   'evil-ledger))

(when (executable-find "picolisp")
  (k/pkg-register 'plisp-mode))

(when (executable-find "ghc")
  (k/pkg-register 'haskell-mode))

(when (executable-find "racket")
  (k/pkg-register
   'racket-mode
   'scribble-mode
   '(ob-racket :host github :repo "hasu/emacs-ob-racket")))

(when (executable-find "rustc")
  (k/pkg-register
   'rustic))

(when (executable-find "janet")
  (k/pkg-register 'janet-mode))

(when (executable-find "R")
  (k/pkg-register 'ess))

(when (executable-find "clojure")
  (k/pkg-register 'cider))

(when (executable-find "pipenv")
  (k/pkg-register 'pipenv))

(k/pkg-register
 'cmake-mode
 'company-glsl
 'cuda-mode
 'disaster
 'glsl-mode
 'opencl-mode)

(k/pkg-register
 'company-shell)

;;;;; Emacs Lisp

(k/pkg-register
 '(parinfer :host github :repo "kisaragi-hiu/parinfer-mode")
 '(elisp-unused :host github :repo "kisaragi-hiu/elisp-unused")
 'elisp-autofmt
 'eros
 'flycheck-relint
 'flycheck-package
 'compat
 'macrostep
 'lisp-extra-font-lock
 'highlight-defined
 'nameless
 'auto-compile)

;; (when (executable-find "cask")
;;   (k/pkg-register
;;    'flycheck-elsa))

;;;;; Common Lisp

(k/pkg-register
 'common-lisp-snippets)

(when (executable-find "sbcl")
  (k/pkg-register 'sly))

;;;;; Web

(k/pkg-register
 'cakecrumbs
 'emmet-mode
 'ssass-mode
 'web-mode)

(k/pkg-register
 'js-comint
 'js-doc
 'js2-mode
 'js2-mode
 'js2-refactor
 'nodejs-repl
 'npm-mode
 'rjsx-mode
 'skewer-mode
 'typescript-mode
 'xref-js2)

(when (executable-find "tsc")
  (k/pkg-register
   'tide))

;;;; Footer

(k/pkg-activate-all)

(provide 'packages)

;;; packages.el ends here

;; Local Variables:
;; eval: (outshine-mode)
;; eval: (hs-minor-mode -1)
;; End:

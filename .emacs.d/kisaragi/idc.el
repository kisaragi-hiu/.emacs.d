;;; idc.el --- Tools for Ideographic Description Characters -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Kisaragi Hiu
;;
;; Author: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Maintainer: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Version: 0.0.1
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(defconst idc-characters
  "⿰⿱⿲⿳⿴⿵⿶⿷⿸⿹⿺⿻⿼⿽⿾⿿〾㇯"
  "A string containing all Ideographic Description Characters.
This includes the new ones added in Unicode 15.1: U+2FFC~U+2FFF
and the two other characters that don't fit in the block (U+303E,
U+31EF).")

(defun idc-copy-anychar-regexp ()
  "Make a regexp that matches any IDC and copy it.
This can then be used to search a buffer."
  (interactive)
  (kill-new (format "[%s]" idc-characters)))

(defun idc-copy ()
  "Copy every IDC into the clipboard."
  (interactive)
  (kill-new idc-characters))

(provide 'idc)
;;; idc.el ends here

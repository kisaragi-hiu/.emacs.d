;;; kisaragi-desktop.el --- desktop related functions -*- lexical-binding: t; -*-
;;; Commentary:
;;; Calling shell scripts is sometimes quite ugly. Since I have Emacs daemon running, automating my desktop with Emacs seems like a better option.
;;; Code:

(require 'subr-x)
(require 's)
(require 'dash)

;; I realize that `notifications' exists, but it doesn't support Termux.
(cl-defun k/send-notification
    (summary &key (body "") (app-name "Emacs") (icon "emacs"))
  "Send a system notification.

The notification ideally looks something like:

  <ICON> APP-NAME
  SUMMARY
  BODY

Supports `notify-send' and `termux-notification'."
  (declare (indent 1))
  (cond ((executable-find "notify-send")
         (start-process "notify-send" nil
                        "notify-send"
                        "--icon" icon
                        "--app-name" app-name
                        summary
                        body))
        ((executable-find "termux-notification")
         (start-process "notify" nil
                        "termux-notification"
                        "--title" app-name
                        "--content"
                        (if (equal "" body)
                            summary
                          (format "%s\n\n%s"
                                  summary
                                  body))))
        ((executable-find "osascript")
         (start-process
          "notify" nil
          "osascript" "-e"
          (format "display notification \"%s\" with title \"%s\" subtitle \"%s\""
                  body
                  app-name
                  summary)))
        (t nil)))

(defun k/discard-notification ()
  "Discard a notification by clicking on it.

Actually just clicks at 1862,77 of the screen."
  (interactive)
  (let* ((mouse-location
          (->> (shell-command-to-string
                "xdotool getmouselocation") ; "x:0 y:0 screen:0 window:0"
               s-trim
               (s-split " ")
               (--map (s-split ":" it)))) ; (("x" "0") ...)
         ;; need to extract value from ("0")
         (orig-x (cadr (assoc "x" mouse-location)))
         (orig-y (cadr (assoc "y" mouse-location)))
         (noti-x "1862")
         (noti-y "77"))
    ;; click and moving back to original position need to happen in order
    ;; so only the last one can avoid waiting
    (call-process "xdotool" nil nil nil "mousemove" noti-x noti-y)
    (call-process "xdotool" nil nil nil "click" "1")
    (call-process "xdotool" nil 0 nil "mousemove" orig-x orig-y)))

(defun k/gsettings-toggle (schema key)
  "Toggle a gsettings boolean KEY under SCHEMA."
  (cl-case (intern-soft
            (string-trim-right
             (shell-command-to-string
              (concat "gsettings get " schema " " key))))
    ((true)
     (start-process "gsettings" nil
                    "gsettings" "set" schema key "false"))
    ((false)
     (start-process "gsettings" nil
                    "gsettings" "set" schema key "true"))
    (t
     (user-error "%s %s is not a boolean value" schema key))))

(defun k/kill-process (match)
  "Kill system process matching MATCH.

Currently implemented as just a pkill wrapper."
  (shell-command-to-string (concat "pkill " match)))

(defun get-pulse-sink-input-ids ()
  "Get pulse sink inputs, and format them into a format suitable for outputting to rofi."
  (--> (shell-command-to-string
        "pactl list sink-inputs \\
         | grep -e 'Sink Input #' -e 'binary =' -e 'application.name =' \\
         | sed 's/\t*//g'")
       (s-split "\n" it t)
       (-partition 3 it)
       (-map (lambda (l) (-sort #'string-greaterp l)) it)
       ;; at this point:
       ;; (("application.process.binary = \"wine-preloader\""
       ;;   "application.name = \"osu!\""
       ;;   "Sink Input #17"))
       (-map (lambda (l) (concat (s-replace "Sink Input #" "" (caddr l))
                                 ": "
                                 (--> (car l)
                                      (s-replace "application.process.binary = " "" it)
                                      (string-trim it "\"" "\""))
                                 " ("
                                 (--> (cadr l)
                                      (s-replace "application.name = " "" it)
                                      (string-trim it "\"" "\""))
                                 ")"))
             it)
       ;; now: ("17: wine-preloader (osu!)")
       (s-join "\n" it)))

(provide 'kisaragi-desktop)
;;; kisaragi-desktop.el ends here

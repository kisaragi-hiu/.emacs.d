;;; layouts.el --- Yet another layouts management package -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Maintainer: 如月飛羽
;; Version: 0.0.1
;; Package-Requires: ((emacs "28.1"))
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Implementation reference: https://www.fileside.app/

;;; Code:
(require 's)
(defvar helpful-switch-buffer-function)
(declare-function helpful-callable "helpful" (symbol))
(declare-function helpful-variable "helpful" (symbol))

(defgroup layouts nil
  "Create custom layouts."
  :group 'applications
  :prefix "layouts-")
(defgroup layouts-ui-faces nil
  "Faces for use in `layouts-ui'."
  :group 'layouts)
(defface layouts-sans
  `((t (:font "Sans" :height 1.0)))
  "Sans serif."
  :group 'layouts-ui-faces)
(defface layouts-title
  '((t (:inherit layouts-sans :height 1.4 :weight bold)))
  "Title face."
  :group 'layouts-ui-faces)

(defun layouts-ui--insert (face &rest strings)
  "Insert STRINGS fontified with FACE."
  (declare (indent 1))
  (insert (propertize (string-join strings) 'face face)))

(define-derived-mode layouts-mode special-mode "Layouts"
  "Major mode for `layouts-ui'."
  (setq-local revert-buffer-function #'layouts-ui--update)
  (buffer-disable-undo))

(defun layouts-ui--update (&rest _)
  "Update the `layouts-ui' buffer."
  (let ((inhibit-read-only t))
    (erase-buffer)
    (layouts-ui--insert 'layouts-title
      "Layouts")
    (insert "\n\n")
    (layouts-ui--insert 'layouts-sans
      "No layouts saved.")))

(defun layouts--capture-window--dired (buf)
  "Capture BUF if it is a Dired buffer."
  (and (provided-mode-derived-p
        (buffer-local-value 'major-mode buf)
        'dired-mode)
       (buffer-local-value 'default-directory buf)))
(defun layouts--capture-window--helpful (buf)
  "Capture BUF if it is a helpful buffer."
  (and (eq (buffer-local-value 'major-mode buf)
           'helpful-mode)
       (let ((match (s-match
                     "^\\*helpful \\([^:]*\\): \\(.*\\)\\*"
                     (buffer-name buf))))
         (list :type 'helpful
               :callable-p (not
                            (equal (elt match 1) "variable"))
               :symbol (intern (elt match 2))))))
(defun layouts--capture-window (&optional window)
  "Capture the root WINDOW in a serializable object.
WINDOW should be nil when not called by this function itself."
  (pcase window
    ('nil (layouts--capture-window (car (window-tree))))
    ((pred windowp)
     (list :point (window-point window)
           :recovery
           (let ((buf (window-buffer window)))
             (or
              ;; String = file visiting buffer
              (buffer-file-name buf)
              (layouts--capture-window--dired buf)
              ;; Special case
              (layouts--capture-window--helpful buf)
              ;; Use a vector to mark other non-file-visiting
              ;; buffers
              (vector (buffer-name buf))))))
    (`(,vertical ,edges . ,windows)
     (list :vertical vertical
           :edges edges
           :windows (mapcar #'layouts--capture-window windows)))))

(defun layouts--restore-window-object (wobj &optional nested)
  "Restore the window configuration described in WOBJ.
If NESTED is nil, initialize the process by deleting all other
windows."
  (unless nested
    (delete-other-windows))
  (cond
   ((map-elt wobj :recovery)
    (map-let (:recovery :point) wobj
      (cond
       ((stringp recovery)
        (progn
          (find-file recovery)
          (goto-char point)))
       ((vectorp recovery)
        (pop-to-buffer-same-window
         (get-buffer-create (elt recovery 0))))
       (t (pcase (map-elt recovery :type)
            ('helpful
             (map-let (:symbol :callable-p) recovery
               (let ((helpful-switch-buffer-function #'pop-to-buffer-same-window))
                 (if callable-p
                     (helpful-callable symbol)
                   (helpful-variable symbol))))))))))
   ((map-elt wobj :windows)
    (map-let (:windows :vertical) wobj
      (let ((first t)
            (new-real-windows (list (selected-window))))
        (dolist (_window windows)
          (if first
              (setq first nil)
            (push (select-window
                   (if vertical
                       (split-window-below)
                     (split-window-right)))
                  new-real-windows)))
        (setq new-real-windows (nreverse new-real-windows))
        (cl-mapcar
         (lambda (real-window window)
           (with-selected-window real-window
             (save-selected-window
               (layouts--restore-window-object window t))))
         new-real-windows
         windows))))))

(defun layouts-ui ()
  "Display a list of layouts in a window."
  (interactive)
  (let ((buf (get-buffer-create "*Layouts*")))
    (pop-to-buffer buf)
    (with-current-buffer buf
      (unless (derived-mode-p 'layouts-mode)
        (layouts-mode))
      (layouts-ui--update))))

(layouts-ui)

(provide 'layouts)

;;; layouts.el ends here

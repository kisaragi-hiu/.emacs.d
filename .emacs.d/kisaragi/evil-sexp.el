;;; evil-sexp.el --- Sexp text objects and motions -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Maintainer: Kisaragi Hiu
;; Version: 0.1
;; Package-Requires: ((emacs "25.3"))
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Provides text objects and motions for s-expressions.
;;
;; A port of vim-sexp.

;;; Code:

(require 'evil)

(defgroup evil-sexp nil
  "Sexp text objects and motions."
  :prefix "evil-sexp-"
  :group 'evil)

(defcustom evil-sexp-want-meta-bindings t
  "Whether bindings starting with meta should be applied."
  :type 'boolean
  :group 'evil-sexp)

(defun evil-sexp--thing-range (thing expand count type &optional func)
  "Return an `evil-range' for THING, COUNT, and TYPE.

EXPAND: expand range by this amount.

If FUNC is non-nil, call FUNC instead of `evil-range' to create
the range object."
  (unless count
    (setq count 1))
  (let (from to)
    (cond ((> count 0)
           (beginning-of-thing thing)
           (setq from (point))
           (forward-thing thing count)
           (setq to (point)))
          ((< count 0)
           (end-of-thing thing)
           (setq from (point))
           (forward-thing thing count)
           (setq to (point))))
    (funcall (or func #'evil-range)
             (- from expand)
             (+ to expand)
             type
             :expanded t)))

;; DONE: Element = sexp

(evil-define-text-object evil-sexp-a-sexp (count &optional beg end type)
  "Select a sexp."
  (evil-sexp--thing-range
   'sexp
   0
   count
   type
   (lambda (beg end &optional type &rest properties)
     (if (save-excursion
           (beginning-of-thing 'sexp)
           (memql (char-before) (string-to-list " \t\r\n")))
         (apply #'evil-range (1- beg) end type properties)
       (apply #'evil-range beg end type properties)))))
(evil-define-text-object evil-sexp-inner-sexp (count &optional beg end type)
  "Select inner sexp."
  (evil-sexp--thing-range 'sexp 0 count type))

;; DONE: Compound Form
(defun evil-sexp--beginning-of-form ()
  "Go to the beginning of the current form.

Return number of characters moved."
  (save-match-data
    (let ((orig (point))
          (start-level (car (syntax-ppss))))
      (if (or (eql 0 start-level)
              (looking-at (rx (syntax open-parenthesis))))
          0
        (catch 'ret
          (while t
            (backward-char)
            (let ((this-level (car (syntax-ppss (point)))))
              (when (< this-level start-level)
                (throw 'ret nil)))))
        (- (point) orig)))))
(defun evil-sexp--end-of-form ()
  "Go to the end of the current form.

Return number of characters moved."
  (save-match-data
    (let ((orig (point))
          (start-level (car (syntax-ppss))))
      (if (looking-at (rx (syntax close-parenthesis)))
          0
        (catch 'ret
          (while t
            (forward-char)
            (let ((this-level (car (syntax-ppss (point)))))
              (when (or (= this-level 0)
                        (< this-level start-level))
                (throw 'ret nil)))))
        (- (point) orig)))))
(defun evil-sexp--bounds-of-form-at-point ()
  "Return bounds of form at point."
  (let (start end)
    (save-excursion
      (evil-sexp--beginning-of-form)
      (setq start (point)))
    (save-excursion
      (evil-sexp--end-of-form)
      (setq end (point)))
    (cons (1+ start) (1- end))))
(put 'evil-sexp-form 'beginning-op #'evil-sexp--beginning-of-form)
(put 'evil-sexp-form 'end-op #'evil-sexp--end-of-form)
(put 'evil-sexp-form 'bounds-of-thing-at-point #'evil-sexp--bounds-of-form-at-point)
(evil-define-text-object evil-sexp-a-form (count &optional beg end type)
  "Select a form."
  (let* ((bounds (bounds-of-thing-at-point 'evil-sexp-form))
         (start (1- (car bounds)))
         (end (1+ (cdr bounds))))
    (evil-range start end type :expanded t)))
(evil-define-text-object evil-sexp-inner-form (count &optional beg end type)
  "Select inner form."
  (let* ((bounds (bounds-of-thing-at-point 'evil-sexp-form))
         (start (car bounds))
         (end (cdr bounds)))
    (evil-range start end type :expanded t)))

;; DONE: Top level compound form = defun
(evil-define-text-object evil-sexp-a-defun (count &optional beg end type)
  "Select a top-level form."
  (evil-sexp--thing-range 'defun 0 count type))
(evil-define-text-object evil-sexp-inner-defun (count &optional beg end type)
  "Select inner top-level form."
  (evil-sexp--thing-range 'defun -1 count type))

;; DONE: Strings

(defun evil-sexp--bounds-of-string-at-point ()
  "Return bounds of string at point."
  (save-excursion
    (let ((ppss (syntax-ppss))
          start end)
      (when (elt ppss 3)
        (setq start (elt ppss 8))
        (while (elt (syntax-ppss) 3)
          (forward-char))
        (setq end (point))
        (cons start end)))))
(put 'evil-sexp-string 'bounds-of-thing-at-point #'evil-sexp--bounds-of-string-at-point)

(evil-define-text-object evil-sexp-a-string (count &optional beg end type)
  "Select a string."
  (let* ((bounds (bounds-of-thing-at-point 'evil-sexp-string))
         (start (car bounds))
         (end (cdr bounds)))
    (evil-range start end type :expanded t)))
(evil-define-text-object evil-sexp-inner-string (count &optional beg end type)
  "Select inner string."
  (let* ((bounds (bounds-of-thing-at-point 'evil-sexp-string))
         (start (1+ (car bounds)))
         (end (1- (cdr bounds))))
    (evil-range start end type :expanded t)))

;; Motions
(evil-define-motion evil-sexp-open-paren (count)
  "Move the cursor back to the open paren of this level."
  :type exclusive
  (let ((count (or count 1)))
    (evil-signal-at-bob-or-eob count)
    ;; Special case: consider position after last close paren to be
    ;; level 1
    (when (and (eql 0 (syntax-after (point)))
               (eql (car (syntax-after (1- (point))))
                    (car (string-to-syntax ")"))))
      (backward-char))
    (unless (= 0 (evil-sexp--beginning-of-form))
      (backward-char))))
(evil-define-motion evil-sexp-close-paren (count)
  "Move the cursor forward to the close paren at this level."
  :type exclusive
  (let ((count (or count 1)))
    (evil-signal-at-bob-or-eob count)
    ;; Special case: consider open paren to be inside the level it
    ;; opens
    (when (eql (car (syntax-after (point)))
               (car (string-to-syntax "(")))
      (forward-char))
    (unless (= 0 (evil-sexp--end-of-form))
      (forward-char))))

;; TODO
(evil-define-motion evil-sexp-select-next-sexp (count))
(evil-define-motion evil-sexp-select-previous-sexp (count))
(evil-define-motion evil-sexp-forward-sexp-begin (count))
(evil-define-motion evil-sexp-backward-sexp-begin (count))
(evil-define-motion evil-sexp-forward-sexp-end (count))
(evil-define-motion evil-sexp-backward-sexp-end (count))
(evil-define-motion evil-sexp-swap-form-with-previous (count))
(evil-define-motion evil-sexp-swap-form-with-next (count))
(evil-define-motion evil-sexp-swap-sexp-with-previous (count))
(evil-define-motion evil-sexp-swap-sexp-with-next (count))

;; Extra: Operator
(evil-define-operator evil-sexp-eval (beg end type register)
  "Evaluate characters in motion as Emacs Lisp.


For example, use \\<evil-sexp-mode-map>\\[evil-sexp-eval] \\<evil-visual-state-map>\\[evil-a-paren] to evaluate the immediately
surrounding Lisp code."
  :move-point nil
  :repeat nil
  (interactive "<R><x>")
  (unless (eq type 'block)
    (eval-expression (read (filter-buffer-substring beg end)))))

;;;###autoload
(define-minor-mode evil-sexp-mode
  "Text objects and motions for s-expressions."
  :keymap (make-sparse-keymap)
  (evil-normalize-keymaps))

(evil-define-key '(normal visual) evil-sexp-mode-map
  "-" #'evil-sexp-eval)

(evil-define-key '(visual operator) evil-sexp-mode-map
  "af" #'evil-sexp-a-form
  "if" #'evil-sexp-inner-form
  "aF" #'evil-sexp-a-defun
  "iF" #'evil-sexp-inner-defun
  "ae" #'evil-sexp-a-sexp
  "ie" #'evil-sexp-inner-sexp
  "as" #'evil-sexp-a-string
  "is" #'evil-sexp-inner-string)

(evil-define-key '(normal visual operator) evil-sexp-mode-map
  "(" #'evil-sexp-open-paren
  ")" #'evil-sexp-close-paren
  "[e" #'evil-sexp-select-next-sexp
  "]e" #'evil-sexp-select-previous-sexp)

(when evil-sexp-want-meta-bindings
  (evil-define-key '(normal visual operator) evil-sexp-mode-map
    (kbd "M-w") #'evil-sexp-forward-sexp-begin
    (kbd "M-b") #'evil-sexp-backward-sexp-begin
    (kbd "M-e") #'evil-sexp-forward-sexp-end
    (kbd "g M-e") #'evil-sexp-backward-sexp-end)
  (evil-define-key 'normal evil-sexp-mode-map
    (kbd "M-j") #'evil-sexp-swap-form-with-previous
    (kbd "M-k") #'evil-sexp-swap-form-with-next
    (kbd "M-h") #'evil-sexp-swap-sexp-with-previous
    (kbd "M-l") #'evil-sexp-swap-sexp-with-next))
;; There are two more commands that are just Barf and Slur.

;; No plan to implement:
;; == (indent form)
;; =- (indent defun)
;; Insert state mappings (use parinfer or smartparens)

(provide 'evil-sexp)

;;; evil-sexp.el ends here

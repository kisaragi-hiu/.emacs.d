;;; init-cc.el --- CC-mode config -*- lexical-binding: t -*-

;;; Commentary:

;; CC-mode config, using Doom Emacs as a reference.

;;; Code:

(require 'kisaragi-helpers)

(declare-function lsp-deferred "lsp-mode")

;; Make them easier to search in Ivy etc.
(defalias 'cpp-mode 'c++-mode)
(defvaralias 'cpp-mode-map 'c++-mode-map)

(defvar lsp-clients-clangd-args)
(with-eval-after-load 'lsp-clangd
  (setq lsp-clients-clangd-args
        '("-j=3"
          "--background-index"
          "--clang-tidy"
          "--completion-style=detailed"
          "--header-insertion=never"
          "--header-insertion-decorators=0")))

(leaf cc-mode
  :mode (("\\.mm\\'" . objc-mode)
         ("\\.cl\\'" . opencl-mode)
         ("\\.h\\'" . c-or-c++-mode))
  :config
  (setq-default c-basic-offset 4)
  (define-advice c-indent-line-or-region (:around (func &rest args)
                                          complete)
    "If indentation did nothing, perform completion."
    (let ((start (point)))
      (apply func args)
      (when (equal (point) start)
        (completion-at-point)))))

(leaf cmake-mode
  :defer-config
  (k:set-popup-rule "^\\*CMake Help\\*"
                    '(nil
                      (size 0.4)
                      (window-parameters . ((ttl . t)))))
  (leaf company-cmake
    :config
    (k:set-company-backend cmake-mode 'company-cmake)))

(leaf glsl-mode
  :defer-config)

(leaf company-glsl
  :after company glsl-mode
  :defer-config (k:set-company-backend 'glsl-mode #'company-glsl))

(leaf lsp
  :after lsp-mode
  :config
  (when (executable-find "clangd")
    (k/add-hook '(c-mode-hook
                  c++-mode-hook
                  cmake-mode-hook
                  objc-mode-hook)
      #'lsp-deferred)))

(provide 'init-cc)

;;; init-cc.el ends here

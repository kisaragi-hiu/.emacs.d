;;; org-and-markdown.el --- Settings for Org and Markdown -*- lexical-binding: t -*-

;;; Commentary:

;; These settings are getting pretty long, so here's their own config
;; file...

;;; Code:

(require 'leaf)
(require 'general)
(require 'mode-local)

(require 'thingatpt)

(require 'dash)
(require 's)

(require 'kisaragi-helpers)
(require 'kisaragi-paths)
(require 'kisaragi-constants)

;;;; Org

;;;;; Export

(leaf org-msr
  :commands (org-msr-mode)
  :disabled t
  :config
  (setq org-msr-keyword-frequency-alist
        '(("DAILY" . "1d")
          ("HARD" . "2d")
          ("SLIGHTLY" . "4d")
          ("UNFAMILIAR" . "8d")
          ("SOMEWHAT" . "2w")
          ("FAMILIAR" . "1m")
          ("BETTER" . "2m")
          ("GREAT" . "4m")
          ("CONFIDENT" . "8m")
          ("JUSTINCASE" . "16m")
          ("YEARS" . "2y")
          ("MEMORIZED" . "never")))
  (k/general-mode-leader
   :keymaps 'org-msr-mode-map
   "j" '(org-msr-insert-current-heading-definition
         :wk "Insert definition for entry")))

;;;;; Others

(leaf kisaragi-org-highlight-links-in-title
  :after org
  :require t
  :init
  (add-hook 'org-mode-hook #'kisaragi-org-highlight-links-in-title-mode))

;; NOTE: migrated
(leaf org-inline-video-thumbnails
  :after org
  :require t
  :unless k/android?)

;;;; Shared

(general-def
  :keymaps '(text-mode-map markdown-mode-map pollen-mode-map evil-org-mode-map)
  :states 'insert
  "<tab>" 'complete-symbol)

;; NOTE: migrated
(leaf minaduki
  :require t
  :after org
  :init
  (setq minaduki-mode:command-prefix "C-c"
        minaduki:index-file "readme.md")
  (k/general-leader/file
    "ed" '(minaduki/open-diary-entry :wk "diary: today's entry")
    "eD" '(minaduki/open-diary-entry-yesterday :wk "diary: yesterday's entry"))
  (general-def
    "C-x f" #'minaduki/open)
  (general-def
    :states 'visual
    :keymaps '(org-mode-map markdown-mode-map)
    "RET" #'minaduki:insert)
  (k/general-mode-leader
   :keymaps '(org-mode-map markdown-mode-map)
   "f" #'minaduki/open
   "if" #'minaduki:insert)
  (k/general-mode-leader
   :keymaps 'org-mode-map
   "r" '(:wk "more minaduki commands")
   "rt" '(:ignore t :wk "Tags")
   "rta" '(minaduki-add-tag :wk "Add tag...")
   "rtd" '(minaduki-delete-tag :wk "Delete tag...")
   "ra" '(:ignore t :wk "Aliases")
   "raa" '(minaduki-add-alias :wk "Add alias...")
   "rad" '(minaduki-delete-alias :wk "Delete alias..."))
  :config
  (setq minaduki-mode-hook nil)
  (minaduki-mode)
  (run-with-idle-timer 5 nil #'minaduki-db:build-cache)
  (add-hook 'minaduki-buffer/after-insert-hook (lambda () (org-content 2)))
  (general-def
    "C-x b" #'minaduki/literature-entries)
  (setq minaduki/literature-notes-directory k/literature-notes-directory
        minaduki:link-insertion-format 'absolute-in-vault
        orb-templates
        '(("r" "ref" plain
           #'minaduki-capture//get-point ""
           :file-name "reflection/${slug}"
           :head "#+TITLE: ${author} - ${title}\n#+key: ${ref}\n#+key: ${url}"
           :unnarrowed t))
        minaduki-buffer/hidden-tags '("reflection" "diary")
        minaduki-db/gc-threshold (* 100 1000 1000)
        minaduki/diary-directory (f-join k/notes/ "diary")
        minaduki/templates-directory (f-join k/notes/ "templates")
        minaduki/vaults `((:name "cloud" :path ,k/cloud/ :skipped t)
                          (:name "notes"
                           :path ,k/notes/
                           :skipped nil
                           :main t)
                          (:name "Info"
                           :path "/usr/share/info/"
                           :skipped nil)
                          (:name "public"
                           :path ,(-some-> k/notes/
                                    (f-join "public" "content"))
                           :skipped t))
        minaduki-lit/key-prop "bibtex_id"
        minaduki-lit/bibliography
        (--map
         (f-join k/notes/ it)
         '("anime.org"
           "apps.org"
           "articles.org"
           "books.org"
           "bookmarks.json"
           "contacts.org"
           "games.org"
           "music.org"
           "nkust.org"
           "people.org"
           "tweets.org"
           "videos.org"))
        minaduki-buffer/position 'right
        minaduki:completion-everywhere t
        minaduki-tag-sources '(org-prop first-directory nested-vault)
        minaduki/slug-replacements '(("[^[:alnum:][:digit:]]" . "-")
                                     ("--*" . "-")
                                     ("^-" . "")
                                     ("-$" . ""))
        minaduki-db/update-method 'immediate
        ;; I don't want a date in most notes; I'll use
        ;; `k/diary-new-entry' if I want it.
        minaduki-capture/templates
        '(("d" "default" plain #'minaduki-capture//get-point "%?"
           :file-name "${slug}"
           :head "#+title: ${title}\n"
           :unnarrowed t))
        minaduki-file-exclude-regexp
        (rx (or "task-archive.org"
                "node_modules"
                (regexp
                 (f-join "obsidian-docs"
                         (rx (or "da"
                                 "es"
                                 "fr"
                                 "it"
                                 "ru"
                                 "vi"
                                 "zh"))))
                (regexp
                 (f-join "templates"
                         ".*\\.org"))))))

;; NOTE: migrated
(leaf kisaragi-timestamp-highlight
  :commands kisaragi-timestamp-highlight-mode
  :init
  (add-hook 'org-mode-hook #'kisaragi-timestamp-highlight-mode))

;; NOTE: migrated
(leaf typo
  :init
  (k/add-hook '(markdown-mode-hook
                org-mode-hook)
    #'typo-mode))
(require 'typo)
(general-def
  :keymaps 'typo-mode-map
  "'" #'k/typo-cycle-right-single
  "`" #'k/typo-cycle-left-single
  ">" #'k/typo-cycle->
  "<" #'k/typo-cycle-<
  "$" #'k/typo-cycle-$)
(define-typo-cycle k/typo-cycle-$
  "My cycle for \"$\"."
  ("$" "§"))
(define-typo-cycle k/typo-cycle->
  "My cycle for \">\"."
  (">" "→"))
(define-typo-cycle k/typo-cycle-<
  "My cycle for \"<\"."
  ("<" "←"))
(define-typo-cycle k/typo-cycle-right-single
  "My cycle for \"'\"."
  ("'" "’"
   ;; prime and double prime, used for foot and inch marks
   ;; you can also emulate them with italicized straight quotes
   "′" "″"))
(define-typo-cycle k/typo-cycle-left-single
  "My cycle for \"`\"."
  ("`" "‘"))

;; NOTE: migrated
(leaf valign
  :when-available t
  :hook (org-mode-hook markdown-mode-hook))

;;;; Bibliography

(with-eval-after-load 'bibtex
  (dolist (entry-type '(("Game" "Interactive media"
                         (("title" "Title of the game")
                          ("author"))
                         (("publisher") ("year"))
                         (("series")
                          ("month")))
                        ("Software" "Software"
                         (("title" "Title of the piece of software")
                          ("author"))
                         nil
                         (("url")))
                        ("Video")
                        ("Course")
                        ("Music")
                        ("Audio")
                        ("Tweet")
                        ("Thing")))
    (add-to-list 'bibtex-biblatex-entry-alist entry-type))
  ;; for mvbook, etc.
  (bibtex-set-dialect 'biblatex))

(provide 'org-and-markdown)

;;; org-and-markdown.el ends here

;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:

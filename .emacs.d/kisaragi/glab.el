;;; glab.el --- UI for GitLab CLI -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2024 Kisaragi Hiu
;;
;; Author: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Maintainer: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; Version: 0.0.1
;; Keywords: tools vc gitlab
;; Package-Requires: ((emacs "25.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(require 'ansi-color)
(require 'pplist)

(declare-function evil-define-key* "evil")

(defun glab--execute-command-and-display (&rest command)
  "Execute COMMAND and display the output in BUFFER.

Before the output comes, display the string LOADING.

After loading finishes and output has been inserted into the
buffer, THEN will be called. This can be used for a major mode.

Example: (glab--execute-command-and-display
           :buffer \"*buf*\"
           :loading \"Loading issues...\"
           :command \"glab\" \"issue\" \"list\")

\(fn &key BUFFER THEN LOADING COMMAND)"
  (let ((buffer (car (pplist-elt command :buffer)))
        (loading (car (pplist-elt command :loading)))
        (then (car (pplist-elt command :then)))
        (command (pplist-elt command :command)))
    (unless (and buffer loading)
      (error "BUFFER and LOADING must be specified and non-nil"))
    (with-current-buffer (get-buffer-create buffer)
      (setq buffer-read-only nil)
      (fundamental-mode)
      (let ((this-buffer buffer))
        (erase-buffer)
        (insert (or loading "Loading..."))
        (let ((all ""))
          (make-process
           :name (string-join command " ")
           :filter (lambda (_process output)
                     (setq all
                           (concat all output)))
           :sentinel (lambda (&rest _)
                       (with-current-buffer this-buffer
                         (erase-buffer)
                         (insert
                          (string-trim all))
                         (ansi-color-apply-on-region
                          (point-min)
                          (point-max))
                         (when then
                           (funcall then))))
           :command command))))
    (if (derived-mode-p 'glab--special-mode)
        (pop-to-buffer-same-window buffer)
      (display-buffer buffer))))

(define-derived-mode glab--special-mode special-mode "Glab"
  "Root major mode of glab.el."
  :interactive nil)
(define-derived-mode glab--issue-list-mode glab--special-mode "Glab issue-list"
  "Major mode for the issue list.

\<glab--issue-list-mode-map>"
  :interactive nil)
(defvar glab--issue-list-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "RET") #'glab--issue-list-mode--RET)
    (when (featurep 'evil)
      (evil-define-key* 'normal map (kbd "RET") #'glab--issue-list-mode--RET))
    map))

(defun glab--issue-list-mode--RET ()
  "Visit thing at point in `glab--issue-list-mode'."
  (interactive nil glab--issue-list-mode)
  (save-excursion
    (beginning-of-line)
    (when (looking-at (rx "#" (group (+ digit))))
      (let ((issue (match-string 1)))
        (glab--execute-command-and-display
          :buffer (format "*glab issue #%s*" issue)
          :loading (format "Loading issue #%s..." issue)
          :command "glab" "issue" "view" issue
          :after #'goto-address-mode)))))

(defun glab-issue-list ()
  "List issues."
  (interactive)
  (glab--execute-command-and-display
    :buffer "*glab issues*"
    :loading "Loading issues..."
    :command "glab" "issue" "list"
    :then #'glab--issue-list-mode))

(provide 'glab)
;;; glab.el ends here

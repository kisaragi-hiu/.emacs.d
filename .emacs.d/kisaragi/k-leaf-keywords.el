;;; k-leaf-keywords.el --- My extra leaf keywords -*- lexical-binding: t -*-

;;; Commentary:

;; commentary

;;; Code:

(require 'leaf)
(require 'leaf-keywords)

(require 'dash)

(cl-defmacro k-leaf-keywords--define! (keyword
                                       (before-or-after what)
                                       def)
  "Define KEYWORD to stand for DEF in `leaf-keywords' BEFORE-OR-AFTER WHAT."
  (declare (indent 1))
  (let (k-offset d-offset)
    (pcase before-or-after
      (`before (setq k-offset 0 d-offset 1))
      (`after (setq k-offset 2 d-offset 3)))
    `(progn
       (when-let (pos (cl-position ,keyword leaf-keywords))
         (setq leaf-keywords
               (-remove-at-indices (list pos (1+ pos)) leaf-keywords)))
       (let ((idx (cl-position ,what leaf-keywords)))
         (setq leaf-keywords
               (->> leaf-keywords
                    (-insert-at (+ idx ,k-offset) ,keyword)
                    (-insert-at (+ idx ,d-offset) ',def)))))))

(k-leaf-keywords--define! :defer
  (after :config)
  `((run-with-idle-timer ,(car leaf--value) nil
                         (lambda () (require ',leaf--name)))
    ,@leaf--body))

(k-leaf-keywords--define! :when-available
  (before :when)
  (when leaf--body
    `((when (and
             ,@(mapcar
                (lambda (v)
                  `(locate-library (format "%s" ',v)))
                leaf--value))
        ,@leaf--body))))

;; Give `:when-available' the same normalization rules as `:require'
(cl-loop for (cond . _) in leaf-normalize
         when (and (listp cond)
                   (eq 'memq (car cond))
                   (let ((val (cadr (elt cond 2))))
                     (and (listp val)
                          (memq :require val)
                          (not (memq :when-available val)))))
         do (push :when-available (cadr (elt cond 2))))

(provide 'k-leaf-keywords)

;;; k-leaf-keywords.el ends here

;;; kisaragi-paths.el --- Path constants -*- lexical-binding: t -*-

;;; Commentary:

;; I'd like to use f.el and dash.el for these, but kisaragi-constants
;; is needed during package registration, so splitting these into
;; their own module is the next best thing.

;;; Code:

(require 'f)
(require 'dash)
(require 'xdg)
(require 'kisaragi-helpers)
(require 'kisaragi-constants)

;;;; Paths

(defconst k/cloud/
  (cond ((k/system-is-p "MF-PC")
         "/run/media/kisaragi-hiu/Data/cloud/")
        ((k/system-is-p "MF-manjaro")
         (f-join "~/cloud/"))
        ((eq system-type 'windows-nt)
         "d:/cloud/")
        (t nil))
  "Path to my primary cloud storage.")

(defconst k/cloud/scratchpad/ (-some-> k/cloud/
                                (f-join "scratchpad")
                                (concat "/"))
  "Scratchpad directory, storing non-text items.")

(defconst k/assets/
  (-some-> k/cloud/
    (f-join "assets")
    (concat "/"))
  "My assets directory.")

(defconst k/notes/
  (cond ((f-exists? "~/git/notes")
         (f-join "~/git/notes/"))
        (k/android?
         (f-expand "~/notes/"))
        ((eq system-type 'windows-nt)
         "d:/notes/")
        (t nil))
  "My directory for notes.")

(cl-assert k/notes/ nil "init.el assumes that k/notes/ exists")

(defconst k/ledger/
  (cond
   (k/notes/
    (f-join k/notes/ "ledger/"))
   (t
    ;; more likely to work when path separator is not /
    (f-join "~" "ledger/")))
  "My ledger directory.")

(defconst k/ledger-file
  (f-join k/ledger/ "journal.ledger")
  "My main ledger file.")

(defconst k/literature-notes-directory
  (when k/notes/
    (f-join k/notes/ "reflection"))
  "My directory for literature notes.")

(defconst k/school/
  (pcase (k/system-name)
    ((or "MF-PC" "MF-manjaro")
     (f-join k/cloud/ "school-works/"))
    (_ nil))
  "Path to my documents related to school / university.")

(defconst k/books/
  (cond (k/cloud/
         (f-join k/cloud/ "books/"))
        ((f-dir? "/storage/7D36-175C")
         (f-join "/storage/7D36-175C/Books/"))
        (t nil))
  "Directory for books.")

(defconst k/references/
  (cond (k/cloud/
         (f-join k/cloud/ "references/"))
        (t nil)))

(defconst k/recordings/
  (cond (k/cloud/
         (f-join k/cloud/ "recordings/"))
        ((file-exists-p "/sdcard/EasyVoiceRecorder")
         (file-truename "/sdcard/EasyVoiceRecorder/"))
        (t nil))
  "My directory for audio recordings.")

(defconst k/photos/
  (cond (k/cloud/
         (f-join k/cloud/ "photos/"))
        ((file-exists-p "/storage/7D36-175C/DCIM/Camera/")
         (f-join "/storage/7D36-175C/DCIM/Camera/"))
        (t nil))
  "My directory for photos.")

(defconst k/timetracking/
  (cond ((member (k/system-name)
                 '("MF-PC" "Nexus 7"))
         (f-join "~/git/" "timetracking"))
        (t
         (f-expand "~/timetracking/")))
  "Path to where I keep my Canrylog file.")

(defconst k/core-repositories
  (-filter
   #'cdr
   `(("assets" . ,k/assets/)
     ("books" . ,k/books/)
     ("cloud" . ,k/cloud/)
     ("emacs.d" . ,(expand-file-name user-emacs-directory))
     ("ledger" . ,k/ledger/)
     ("notes" . ,k/notes/)
     ("photos" . ,k/photos/)
     ("references" . ,k/references/)
     ("recordings" . ,k/recordings/)
     ("school" . ,k/school/)
     ("scratchpad" . ,k/cloud/scratchpad/)
     ("timetracking" . ,k/timetracking/))))

(defun k/visit-core-repo ()
  "Select a core repository and visit it."
  (interactive)
  (-some--> (mapcar #'car k/core-repositories)
    (completing-read "Repository: " it nil t)
    (assoc it k/core-repositories)
    cdr
    find-file))

(provide 'kisaragi-paths)

;;; kisaragi-paths.el ends here

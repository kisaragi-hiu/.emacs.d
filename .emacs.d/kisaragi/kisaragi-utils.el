;;; kisaragi-utils.el --- Zero-dependency util functions -*- lexical-binding: t -*-

;;; Commentary:

;; Util functions that can be loaded before packages are installed.

;;; Code:

;; afaik this is loaded early in doom
(require 'dash)

(defun k/add-hook (hooks &rest functions)
  "A replacement for `add-hook', adding FUNCTIONS to HOOKS.

HOOKS can be a single hook or a list of hooks.

The first argument can also be a vector [HOOKS DEPTH], which
allows specifying a depth for the functions. See `add-hook' for
the definition of DEPTH."
  (declare (indent 1))
  (let (depth)
    (when (vectorp hooks)
      (setq depth (elt hooks 1)
            hooks (elt hooks 0)))
    ;; this is Emacs 28+
    (let ((hooks (ensure-list hooks)))
      (dolist (hook hooks)
        (dolist (func functions)
          (add-hook hook func depth))))))

(defun k/add-hook-not-derived (modes &rest funcs)
  "Add FUNCS to MODES's hooks but not MODES's derived modes.
MODES can be a single mode symbol or a list of mode symbols."
  (declare (indent 1))
  (setq modes (ensure-list modes))
  (unless (-all? #'symbolp modes)
    (error "MODES must be made of symbols"))
  (dolist (mode modes)
    (let ((mode-hook (intern (format "%s-hook" mode))))
      (dolist (func funcs)
        (add-hook mode-hook
                  (lambda ()
                    (when (eq major-mode mode)
                      (funcall func))))))))

(defun k/add-hook-once (hook func &optional depth local)
  "Add FUNC to HOOK; after its first run, remove it.
DEPTH and LOCAL are passed to `add-hook'.
This is useful for enabling a global mode lazily."
  (add-hook
   hook
   (lambda ()
     (funcall func)
     (remove-hook hook func local))
   depth local))

(provide 'kisaragi-utils)

;;; kisaragi-utils.el ends here

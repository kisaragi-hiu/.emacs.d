;;; kisaragi-constants.el --- Constants to be require'd in other files -*- lexical-binding: t; -*-

;;; Commentary:

;; This is used before packages are registered.

;;; Code:

;; These have to be refreshed on frame initialization, so they are set
;; in `k/setup-fonts'.
(defvar k/font-mono nil
  "My preferred monospace font.")
(defvar k/font-serif nil
  "My preferred serif font.")
(defvar k/font-sans nil
  "My preferred sans serif font.")

(defconst k/android? (not (not (getenv "ANDROID_DATA")))
  "Are we running on Android?

Use this to skip having to use (getenv \"ANDROID_DATA\")
everywhere. Presumably variable access is faster than `getenv'.")

(defconst k/windows? (memq system-type '(cygwin windows-nt ms-dos)))

(defconst k/lisp-modes
  '(lisp-data-mode
    lisp-mode
    emacs-lisp-mode
    scheme-mode racket-mode fennel-mode
    clojure-mode hy-mode janet-mode)
  "Major modes that benefit from the same Lisp editing features.

Please make sure to put ancestors first for `k/add-mode-hook' to
function properly.")

;;;; Leader keys

(defconst k/primary-leader "SPC" "My primary leader key.")

(defconst k/mode-leader ","
  "My leader key for mode specific bindings.")

(defconst k/global-mode-leader "C-c"
  "My leader key for mode specific bindings in non-command states.")

(defconst k/global-leader "C-x x" "Leader key for use everywhere.")

(provide 'kisaragi-constants)
;;; kisaragi-constants.el ends here

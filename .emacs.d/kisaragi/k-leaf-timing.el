;;; k-leaf-timing.el --- summary -*- lexical-binding: t -*-

;;; Commentary:

;; Advise `leaf' to report how much time each block took.

;;; Code:

(defvar k-leaf-timing--data (make-hash-table :test #'eq))
(defconst k-leaf-timing--buffer "*k/leaf timing*")

(defun k-leaf-timing--advice (old pkg &rest body)
  "To be used as an advice on OLD (which should be the `leaf' macro).

PKG and BODY are passed to `leaf'."
  `(progn
     (puthash ',pkg (current-time) k-leaf-timing--data)
     (message "leaf block: %s" ',pkg)
     ,(apply old pkg body)
     (with-current-buffer (get-buffer-create ,k-leaf-timing--buffer)
       (when (= 0 (buffer-size))
         (tabulated-list-mode)
         (setq tabulated-list-format
               (vector
                '("leaf block" 30 t)
                (list "seconds" 20
                      (lambda (a b)
                        ;; An entry is (ID ["<name>" "<seconds>"]).
                        (setq a (elt (cadr a) 1))
                        (setq b (elt (cadr b) 1))
                        (< (string-to-number a) (string-to-number b))))))
         (tabulated-list-init-header))
       (push (list
              nil
              (vector
               (format "%s" ',pkg)
               (when-let* ((it k-leaf-timing--data)
                           (it (gethash ',pkg it))
                           (it (time-subtract (current-time) it))
                           (it (float-time it)))
                 (prin1-to-string it))))
             tabulated-list-entries))
     ',pkg))

(defun k-leaf-timing--revert-buffer ()
  "Revert `k-leaf-timing--buffer'."
  (when-let ((buf (get-buffer k-leaf-timing--buffer)))
    (with-current-buffer buf
      (revert-buffer))))

(define-minor-mode k-leaf-timing-mode
  "Minor mode to advice `leaf' and provide timing data."
  :global t :lighter ""
  (if k-leaf-timing-mode
      (progn
        (advice-add 'leaf :around #'k-leaf-timing--advice)
        (add-hook 'after-init-hook #'k-leaf-timing--revert-buffer))
    (advice-remove 'leaf #'k-leaf-timing--advice)
    (remove-hook 'after-init-hook #'k-leaf-timing--revert-buffer)))

(provide 'k-leaf-timing)

;;; k-leaf-timing.el ends here

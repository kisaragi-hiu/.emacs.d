;;; ghui.el --- An interface for GitHub CLI -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1
;; Package-Requires: ((emacs "25.1"))
;; Homepage: https://github.com/kisaragi-hiu/github-ui.el
;; Keywords: github tools vc


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; Skipped: gh alias, gh codespace, gh completion, gh extension

;;; Code:

(require 'transient)
(require 'dash)
(require 's)

;; (defmacro ghui--buffer (buf &rest body)
;;   "Create or switch to BUF, eval BODY there, then display it.

;; `this-buffer' is available in BODY as a lexical variable.

;; BUF can only be a string."
;;   (declare (indent 1))
;;   `(progn
;;      (with-current-buffer (get-buffer-create ,buf)
;;        (let ((this-buffer ,buf))
;;          ,@body))
;;      (display-buffer ,buf)))

;; This facilitates creating transient prefixes that are only used to
;; set some arguments. This way we don't have to create a billion
;; commands corresponding to every single supported call to `gh' and
;; spam the user's M-x.
(defun ghui--called-from-transient? ()
  "Figure out if this transient prefix is called from within itself."
  ;; Calling ourselves in the transient
  (eq transient-current-command this-command))

(defun ghui--execute-command-and-display (buffer loading &rest command)
  "Execute COMMAND and display the output in BUFFER.

Before the output comes, display the string LOADING.

Example: (ghui--execute-command-and-display \"*buf*\"
           \"Loading auth status...\"
           \"gh\" \"auth\" \"status\")"
  (declare (indent 1))
  (with-current-buffer (get-buffer-create buffer)
    (let ((this-buffer buffer))
      (erase-buffer)
      (insert (or loading "Loading..."))
      (let ((all ""))
        (make-process
         :name (s-join " " command)
         :filter (lambda (_process output)
                   (setq all
                         (concat all output)))
         :sentinel (lambda (&rest _)
                     (with-current-buffer this-buffer
                       (erase-buffer)
                       (insert
                        (s-trim all))
                       (ansi-color-apply-on-region
                        (point-min)
                        (point-max))))
         :command command))))
  (display-buffer buffer))

(defun ghui--execute-command-in-term (buf cmd-string)
  "Execute CMD-STRING in a new term buffer.

The new term buffer will be called *BUF*; the earmuffs are added,
so they should not be included in BUF.

CMD-STRING is the command to send to the shell in the terminal.

We use a timer to count the number of prompts in the buffer; if
there are two or more, that means the command has finished, and
the buffer is killed.

This is used for interfacing with `gh''s interactive commands."
  (with-current-buffer
      ;; make-term adds the earmuffs itself
      (make-term buf shell-file-name)
    (term-mode)
    (term-char-mode)
    (switch-to-buffer (current-buffer))
    (term-send-string (get-buffer-process (current-buffer))
                      (concat cmd-string "\n"))
    ;; This allows the timer to reference itself.
    ;; Learned from deferred.el
    (let (timer)
      (setq timer
            (run-at-time
             0 1
             (lambda ()
               (save-excursion
                 (goto-char (point-min))
                 ;; Count the number of prompts. If we have two
                 ;; prompts, that means the command is done, so we
                 ;; kill the process and the buffer.
                 (cl-loop
                  with x = 0
                  while (re-search-forward "^[^ ].*[$>#%] *" nil t)
                  do (cl-incf x)
                  finally do
                  (progn
                    ;; (message "%s prompts" x)
                    (when (>= x 2)
                      (cancel-timer timer)
                      (kill-process
                       (get-buffer-process (current-buffer)))
                      (let ((kill-buffer-query-functions))
                        (kill-buffer (current-buffer)))))))))))))

(defun ghui--call (&rest args)
  "Call the `gh' cli with ARGS."
  (apply #'call-process "gh" nil nil nil args))

(transient-define-prefix ghui ()
  "A UI to `gh', GitHub's CLI."
  ["Subcommands"
   ("a" "Auth"  ghui-auth)
   ("b" "Browse" ghui-browse)
   ("w" "Workflow"  ghui-workflow)])

(transient-define-prefix ghui-auth ()
  "Auth commands."
  ["Subcommands"
   ("i" "Log in"  ghui-auth-login)
   ("o" "Log out" ghui-auth-logout)
   ("r" "Refresh token" ghui-auth-refresh)
   ("g" "Make Git use GitHub CLI to access applicable HTTPS links"
    ghui-auth-setup-git)
   ("s" "Show authentication status"
    ghui-auth-status)])

;; TODO: test on Windows
(defun ghui-auth-login ()
  "Log into GitHub."
  (interactive)
  (ghui--execute-command-in-term
   "ghui Login"
   "gh auth login"))

(defun ghui-auth-logout ()
  "Log out of GitHub."
  (interactive)
  (ghui--execute-command-in-term
   "ghui Logout"
   "gh auth logout"))

(defun ghui-auth-refresh ()
  "Refresh the authentication token."
  (interactive)
  (ghui--execute-command-in-term
   "ghui Refresh"
   "gh auth refresh"))

(defun ghui-auth-setup-git ()
  "Make Git use GitHub CLI to access applicable HTTPS links."
  (interactive)
  (ghui--call "auth" "setup-git"))

(defun ghui-auth-status ()
  "Show auth status."
  (interactive)
  (ghui--execute-command-and-display "*ghui Auth Status*"
    "Loading auth status..."
    "gh" "auth" "status"))

(defclass ghui-issue (transient-variable)
  ((id :initform nil)))

(cl-defmethod transient-infix-read ((obj ghui-issue))
  (oref obj id))

(cl-defmethod transient-infix-set ((obj ghui-issue) _)
  (if (oref obj id)
      (oset obj id nil)
    (let* ((input (read-string "Issue / PR number: "))
           (n (string-to-number input)))
      (if (> n 0)
          (oset obj id input)
        (oset obj id nil)))))

(cl-defmethod transient-infix-value ((obj ghui-issue))
  (oref obj id))

(cl-defmethod transient-format ((obj ghui-issue))
  "Formatted version of OBJ."
  (format " %s Which Issue / PR to visit %s"
          (transient-format-key obj)
          (transient-format-value obj)))

(cl-defmethod transient-format-value ((obj ghui-issue))
  (if-let ((value (oref obj id)))
      (propertize (format "#%s" value) 'face 'transient-argument)
    (propertize "unset" 'face 'transient-inactive-argument)))

(transient-define-infix ghui--issue ()
  :class 'ghui-issue)

(transient-define-infix ghui--browse-branch ()
  :class 'transient-option
  :description "Branch"
  :key "-b"
  :argument "--branch="
  :choices (lambda (&rest _)
             (magit-list-branch-names)))

(transient-define-prefix ghui-browse (&optional recursive-call)
  "Visit the repository in the browser."
  ["Options"
   ("-i" ghui--issue)
   (ghui--browse-branch)
   ("-c" "Open the last commit" "--commit")
   ("-p" "Open projects page" "--projects")
   ("-s" "Open repo settings" "--settings")
   ("-w" "Open repo wiki" "--wiki")]
  ["Command"
   ("b" "Visit the repository" ghui-browse)]
  (interactive (list (ghui--called-from-transient?)))
  (if (not recursive-call)
      (transient-setup this-command)
    (apply #'ghui--call "browse"
           (transient-args this-command))))

(transient-define-prefix ghui-repo ()
  "Repo commands."
  ["Subcommands"
   ("i" "Log in"  ghui-auth-login)
   ("o" "Log out" ghui-auth-logout)
   ("r" "Refresh token" ghui-auth-refresh)
   ("g" "Make Git use GitHub CLI to access applicable HTTPS links"
    ghui-auth-setup-git)
   ("s" "Show authentication status"
    ghui-auth-status)])

(defun ghui--current-repo ()
  "Return the current repository as \"owner/repo\"."
  (with-temp-buffer
    (call-process "gh" nil '(t nil) nil
                  "repo" "view"
                  "--json" "nameWithOwner"
                  "--jq" ".nameWithOwner")
    (--> (buffer-string)
      s-trim
      (unless (equal "" it)
        it))))

(defun ghui-repo-archive ()
  "Archive a repository."
  (interactive)
  (-when-let (repo (ghui--current-repo))
    (unless (y-or-n-p (format "Archived %s. Undo? " repo))
      (ghui--call "repo" "archive" "-y"))))

;; (defun ghui-repo-clone ()
;;   "Clone a repository locally."
;;   (interactive))
;; (defun ghui-repo-create ()
;;   "Create a new repository."
;;   (interactive))
;; (defun ghui-repo-delete ()
;;   "Delete a repository."
;;   (interactive))
;; (defun ghui-repo-deploy-key ()
;;   "Manage deploy keys in a repository."
;;   (interactive))
;; (defun ghui-repo-edit ()
;;   "Edit repository settings."
;;   (interactive))
(defun ghui-repo-fork ()
  "Create a fork of a repository."
  (interactive)
  (ghui--execute-command-in-term
   "ghui repo Fork"
   "gh repo fork"))
;; (defun ghui-repo-list ()
;;   "List repositories owned by user or organization."
;;   (interactive))
;; (defun ghui-repo-rename ()
;;   "Rename a repository."
;;   (interactive))
;; (defun ghui-repo-sync ()
;;   "Sync a repository."
;;   (interactive))
;; (defun ghui-repo-view ()
;;   "View a repository."
;;   (interactive))

;; (transient-define-prefix ghui-config)

;; gh gist <command>
;; gh gist clone <gist> [<directory>] [-- <gitflags>...]
;; gh gist create [<filename>... | -] [flags]
;; gh gist delete {<id> | <url>}
;; gh gist edit {<id> | <url>} [<filename>] [flags]
;; gh gist list [flags]
;; gh gist view [<id> | <url>] [flags]

(transient-define-prefix ghui-workflow ()
  "Workflow commands."
  ["Subcommands"
   ("d" "Disable a workflow" ghui-workflow-disable)
   ("e" "Enable a workflow" ghui-workflow-enable)
   ("l" "List workflows" ghui-workflow-list)
   ("r" "Run a workflow by creating a workflow_dispatch event"
    ghui-workflow-run)
   ("v" "View the summary of a workflow" ghui-workflow-view)])

(defun ghui-workflow-run ()
  "Log into GitHub."
  (interactive)
  (ghui--execute-command-in-term
   "ghui workflow run"
   "gh workflow run"))

(defun ghui-workflow-list ()
  "List workflows."
  (interactive)
  (ghui--execute-command-and-display "*ghui Workflow List*"
    "Listing workflows..."
    "gh" "workflow" "list"))


(provide 'ghui)

;;; ghui.el ends here

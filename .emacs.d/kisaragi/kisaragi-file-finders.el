;;; kisaragi-file-finders.el --- File finders suitable for org-link-abbrev-alist -*- lexical-binding: t; -*-
;;; Commentary:
;;
;; Various functions to locate some files based on a tag, suitable for
;; use in `org-link-abbrev-alist'.
;;
;; Example setup:
;;
;;   (dolist (abbrev '((photo . k/org-link-abbrev-photo)
;;                     (recording . k/org-link-abbrev-recording)
;;                     (pixiv . k/org-link-abbrev-pixiv)))
;;     (add-to-list 'org-link-abbrev-alist abbrev))
;;
;; Use in Org mode after setting them up:
;;
;; - `photo': [[photo:20140206_150333]]
;; - `recording': [[recording:20200901T000101+0900]]
;; - `pixiv': [[pixiv:68561404]]
;;
;;; Code:

(require 'kisaragi-paths)
(require 'f)
(require 'cl-lib)
(require 'dash)

(defvar k/file-finders-cache-vars nil
  "List of symbols to caches of file finders that have been defined.")

(defun k/file-finders-reset-cache (&rest caches)
  "Reset CACHES of file finders.

Interactively, prompt for a cache variable from
`k/file-finders-cache-vars' to reset; with a
\\[universal-argument], reset all caches in that list."
  (interactive
   (if current-prefix-arg
       k/file-finders-cache-vars
     (list
      (intern
       (completing-read
        "File finder caches to reset: "
        k/file-finders-cache-vars)))))
  (dolist (cache caches)
    (set cache nil)))

(cl-defmacro k/define-file-finder
    (name docstring directory template
          &key (match-type 'regex) sort-pred split-path extra-args)
  "Define NAME as a file finder, suitable for use in `org-link-abbrev-alist'.

DOCSTRING is used as the new function's docstring.

The file finder receives a TAG argument, and searches DIRECTORY
for the first file whose name matches it.

TEMPLATE is expanded with \"%s\" replaced with TAG (using
`format'); this allows for better control over what \"matches
it\" means.

MATCH-TYPE determines how the result of TEMPLATE filled in with
TAG is passed to `find'. It should not be quoted. When it's
`regex' (default), TAG will be quoted with `regexp-quote';
otherwise this should be a symbol specifying a pattern matching
argument, for example `name' for \"-name\", `iname' for
\"-iname\", and so on.

SORT-PRED, if non-nil, is the predicate used to sort matching
results. For example, use `string<' to select favor the
\"smaller\" string when there are multiple results.

SPLIT-PATH, if non-nil, means to only try to find TAG up to the
first slash character. If it matches, the rest of TAG will be
appended back and returned.

EXTRA-ARGS, if non-nil, is a list of arguments to pass to `find'
before the matching options. Put `-maxdepth' here, for example."
  (declare (indent 1) (doc-string 2))
  (let ((match-args
         (pcase match-type
           ('regex '("-regextype" "emacs" "-regex"))
           (_ (list (concat "-" (symbol-name match-type))))))
        (cache-sym (intern (format "%s--cache" (symbol-name name)))))
    `(progn
       (defvar ,cache-sym nil)
       (cl-pushnew ',cache-sym k/file-finders-cache-vars)
       (defun ,name (tag)
         ,docstring
         (let ((rest ""))
           ,@(when split-path
               `((when-let ((first-slash-index (cl-position ?/ tag)))
                   (setq rest (substring tag (1+ first-slash-index))
                         tag (substring tag 0 first-slash-index)))))
           ,@(when (eq match-type 'regex)
               `((setq tag (regexp-quote tag))))
           ;; (message "rest: %s, tag: %s" rest tag)
           (or (cdr (assoc tag ,cache-sym))
               (-some--> ,directory
                 (with-temp-buffer
                   (call-process
                    find-program nil '(t nil) nil
                    it
                    ;; "The regular expressions understood by find
                    ;; are by default Emacs Regular Expressions
                    ;; (except that `.' matches newline)"
                    ;; -- man page for find
                    ,@extra-args
                    ;; -regextype emacs -regex if MATCH-TYPE is
                    ;; `regex', or:
                    ;; -name if it's `name', -iname if it's
                    ;; `iname', etc.
                    ,@match-args
                    (format
                     ,template
                     ;; quoted above
                     tag))
                   (s-split "\n" (buffer-string) :omit))
                 (cond ((= (length it) 1) it)
                       (,sort-pred (sort it ,sort-pred))
                       (t it))
                 car
                 (f-join it rest)
                 (setf (alist-get tag ,cache-sym nil nil #'equal) it))
               ;; I don't want this to error out or return a nil
               "/tmp/filenotfound")))
       (put ',name 'k/file-finders-abbrev-path ,directory))))

(cl-defun k/make-file-finder
    (directory template &key (match-type 'regex) sort-pred split-path extra-args)
  "Create a file finder function, suitable for use in `org-link-abbrev-alist'.

The file finder receives a TAG argument, and searches DIRECTORY
for the first file whose name matches it.

TEMPLATE is expanded with \"%s\" replaced with TAG (using
`format'); this allows for better control over what \"matches
it\" means.

MATCH-TYPE determines how the result of TEMPLATE filled in with
TAG is passed to `find'. When it's `regex' (default), TAG will be
quoted with `regexp-quote'; otherwise this should be a symbol
specifying a pattern matching argument, for example `name' for
\"-name\", `iname' for \"-iname\", and so on.

SORT-PRED, if non-nil, is the predicate used to sort matching
results. For example, use `string<' to select favor the
\"smaller\" string when there are multiple results.

SPLIT-PATH, if non-nil, means to only try to find TAG up to the
first slash character. If it matches, the rest of TAG will be
appended back and returned.

EXTRA-ARGS, if non-nil, is a list of arguments to pass to `find'
before the matching options. Put `-maxdepth' here, for example."
  (lambda (tag)
    (let ((rest "")
          (match-args
           (pcase match-type
             ('regex '("-regextype" "emacs" "-regex"))
             (_ (list (concat "-" (symbol-name match-type))))))
          (tag
           (pcase match-type
             ('regex (regexp-quote tag))
             (_ tag))))
      (when split-path
        (when-let ((first-slash-index (cl-position ?/ tag)))
          (setq rest (substring tag (1+ first-slash-index))
                tag (substring tag 0 first-slash-index))))
      (or (-some--> directory
            (with-temp-buffer
              (apply #'call-process find-program nil '(t nil) nil
                     it
                     ;; "The regular expressions understood by find
                     ;; are by default Emacs Regular Expressions
                     ;; (except that `.' matches newline)"
                     ;; -- man page for find
                     `(,@extra-args
                       ;; -regextype emacs -regex if MATCH-TYPE is
                       ;; `regex', or:
                       ;; -name if it's `name', -iname if it's
                       ;; `iname', etc.
                       ,@match-args
                       ,(format
                         template
                         ;; quoted above
                         tag)))
              (s-split "\n" (buffer-string) :omit))
            (cond ((= (length it) 1) it)
                  (sort-pred (sort it sort-pred))
                  (t it))
            car
            (f-join it rest))
          ;; I don't want this to error out or return a nil
          "/tmp/filenotfound"))))

(k/define-file-finder k/org-link-abbrev-photo
  "Locate my photo as specified by TAG.

IMG_20140206_150333.jpg should be located as \"20140206_150333\".

This makes it possible to write

    [[photo:20140206_150333]]

in Org, after setting up `org-link-abbrev-alist'."
  k/photos/
  (rx "/" (*? any) "_" "%s" (* any)))

(k/define-file-finder k/org-link-abbrev-recording
  "Locate my recording as specified by TAG."
  k/recordings/
  "*%s*"
  :match-type name)

(k/define-file-finder k/org-link-abbrev-reference
  "Locate a file in my references directory.

Search `k/references/' for a file with a matching
name, preferring files with shorter paths over longer ones."
  k/references/
  "*%s*"
  :match-type path
  :sort-pred (lambda (a b) (< (length a) (length b))))

(k/define-file-finder k/org-link-abbrev-project
  "Locate a project as specified by TAG."
  (when k/cloud/
    (f-join k/cloud/ "Projects"))
  ".*/%s.*"
  :split-path t
  :extra-args ("-maxdepth" "3" "-type" "d"))

(provide 'kisaragi-file-finders)
;;; kisaragi-file-finders.el ends here

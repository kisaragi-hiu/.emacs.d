;;; eel2.el --- A major mode for REAPER's EEL2 language -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Version: 0.1.0
;; Package-Requires: ((emacs "25.1"))
;; Homepage: homepage
;; Keywords: keywords


;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; A half-baked major mode for REAPER's EEL2.

;;; Code:

(require 'cc-mode)

(defgroup eel2 nil
  "EEL2 language support."
  :group 'languages
  :prefix "eel2-")

(defvar eel2--builtin-variables
  `("beat_position" "ext_midi_bus" "ext_nodenorm" "ext_noinit" "gfx_a" "gfx_a2"
    "gfx_b" "gfx_clear" "gfx_dest" "gfx_ext_flags" "gfx_ext_retina" "gfx_g"
    "gfx_h" "gfx_mode" "gfx_r" "gfx_texth" "gfx_w" "gfx_x" "gfx_y" "midi_bus"
    "mouse_cap" "mouse_hwheel" "mouse_wheel" "mouse_x" "mouse_y" "num_ch"
    "pdc_bot_ch" "pdc_delay" "pdc_midi" "pdc_top_ch" "play_position"
    "play_state" "samplesblock" "srate" "tempo" "trigger" "ts_denom" "ts_num"
    "#dbg_desc"
    ,@(cl-loop for i from 1 to 64
               collect (format "slider%s" i))
    ,@(cl-loop for i from 0 to 63
               collect (format "spl%s" i))))

(defvar eel2--builtin-functions
  (list "__memtop" "time" "time_precise" "abs" "acos" "asin" "atan" "atan2"
        "ceil" "convolve_c" "cos" "exp" "fft" "fft_ipermute" "fft_permute"
        "fft_real" "floor" "freembuf" "gfx_drawchar" "gfx_drawnumber"
        "gfx_lineto" "gfx_rectto" "ifft" "ifft_real" "imdct" "invsqrt" "log"
        "log10" "match" "matchi" "max" "mdct" "memcpy" "memset" "memset" "min"
        "pow" "rand" "sign" "sin" "sliderchange" "sprintf" "sqr" "sqrt"
        "stack_exch" "stack_peek" "stack_pop" "stack_push" "str_getchar"
        "str_setchar" "strcat" "strcmp" "strcpy" "strcpy_from" "strcpy_substr"
        "stricmp" "strlen" "strncat" "strncmp" "strncpy" "strnicmp" "tan"))

(defvar eel2-mode-syntax-table
  (with-syntax-table (copy-syntax-table)
    ;; Comment: from // to end of line
    (c-populate-syntax-table (syntax-table))
    (modify-syntax-entry ?\@ "w")
    (syntax-table))
  "`eel2-mode' syntax table.")

(defvar eel2-font-lock-keywords
  `((,(rx-to-string
       `(seq
         bol
         (group
          (or "desc" "tags" "author"
              ,@(cl-loop for i from 1 to 64
                         collect (format "slider%s" i))
              "in_pin" "out_pin" "filename"
              "options")
          ":")
         (* nonl)))
     . (1 font-lock-keyword-face))
    (,(rx-to-string
       `(seq
         symbol-start
         (or ,@eel2--builtin-variables)
         symbol-end))
     . font-lock-builtin-face)
    (,(rx-to-string
       `(seq
         symbol-start
         (group (or ,@eel2--builtin-functions))
         symbol-end))
     . (1 font-lock-function-name-face))
    (,(rx (any "|=;+/*><?:-"))
     . font-lock-keyword-face)
    (,(rx "!")
     . font-lock-negation-char-face)
    (,(rx symbol-start
          (or "@init" "@slider" "@block" "@sample" "@serialize" "@gfx"
              "while" "loop" "import")
          symbol-end)
     . font-lock-keyword-face)))

;;;###autoload
(define-derived-mode eel2-mode prog-mode "EEL2"
  "Major mode for editing EEL2 code."
  :group 'eel2
  :syntax-table eel2-mode-syntax-table
  (setq-local font-lock-defaults '(eel2-font-lock-keywords)))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\bREAPER\\b.*Effects" . eel2-mode))

(provide 'eel2)

;;; eel2.el ends here
